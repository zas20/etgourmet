-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 13 Avril 2015 à 22:14
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `halalexoticmeats`
--

-- --------------------------------------------------------

--
-- Structure de la table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(32) NOT NULL,
  `company_id` varchar(32) NOT NULL,
  `tax_id` varchar(32) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `address`
--

INSERT INTO `address` (`address_id`, `customer_id`, `firstname`, `lastname`, `company`, `company_id`, `tax_id`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`) VALUES
(1, 1, 'customer1', 'customer1', '', '', '', 'Tunis', 'Tunis', 'Tunis', '1002', 214, 3305),
(2, 2, 'customer2', 'customer2', '232323', '2323', '', 'Tunis', 'Tunis', 'Tunis', '2323', 214, 3300);

-- --------------------------------------------------------

--
-- Structure de la table `affiliate`
--

CREATE TABLE IF NOT EXISTS `affiliate` (
  `affiliate_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `company` varchar(32) NOT NULL,
  `website` varchar(255) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`affiliate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `affiliate_transaction`
--

CREATE TABLE IF NOT EXISTS `affiliate_transaction` (
  `affiliate_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`affiliate_transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `attribute`
--

CREATE TABLE IF NOT EXISTS `attribute` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `attribute`
--

INSERT INTO `attribute` (`attribute_id`, `attribute_group_id`, `sort_order`) VALUES
(1, 6, 1),
(2, 6, 5),
(3, 6, 3),
(4, 3, 1),
(5, 3, 2),
(6, 3, 3),
(7, 3, 4),
(8, 3, 5),
(9, 3, 6),
(10, 3, 7),
(11, 3, 8);

-- --------------------------------------------------------

--
-- Structure de la table `attribute_description`
--

CREATE TABLE IF NOT EXISTS `attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `attribute_description`
--

INSERT INTO `attribute_description` (`attribute_id`, `language_id`, `name`) VALUES
(1, 1, 'Description'),
(2, 1, 'No. of Cores'),
(4, 1, 'test 1'),
(5, 1, 'test 2'),
(6, 1, 'test 3'),
(7, 1, 'test 4'),
(8, 1, 'test 5'),
(9, 1, 'test 6'),
(10, 1, 'test 7'),
(11, 1, 'test 8'),
(3, 1, 'Clockspeed');

-- --------------------------------------------------------

--
-- Structure de la table `attribute_group`
--

CREATE TABLE IF NOT EXISTS `attribute_group` (
  `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `attribute_group`
--

INSERT INTO `attribute_group` (`attribute_group_id`, `sort_order`) VALUES
(3, 2),
(4, 1),
(5, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Structure de la table `attribute_group_description`
--

CREATE TABLE IF NOT EXISTS `attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`attribute_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `attribute_group_description`
--

INSERT INTO `attribute_group_description` (`attribute_group_id`, `language_id`, `name`) VALUES
(3, 1, 'Memory'),
(4, 1, 'Technical'),
(5, 1, 'Motherboard'),
(6, 1, 'Processor');

-- --------------------------------------------------------

--
-- Structure de la table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `banner`
--

INSERT INTO `banner` (`banner_id`, `name`, `status`) VALUES
(6, 'HP Products', 1),
(7, 'Samsung Tab', 1),
(8, 'Manufacturers', 1);

-- --------------------------------------------------------

--
-- Structure de la table `banner_image`
--

CREATE TABLE IF NOT EXISTS `banner_image` (
  `banner_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`banner_image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=78 ;

--
-- Contenu de la table `banner_image`
--

INSERT INTO `banner_image` (`banner_image_id`, `banner_id`, `link`, `image`) VALUES
(54, 7, 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'data/demo/samsung_banner.jpg'),
(77, 6, 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'data/demo/hp_banner.jpg'),
(75, 8, 'index.php?route=product/manufacturer/info&amp;manufacturer_id=5', 'data/demo/htc_logo.jpg'),
(73, 8, 'index.php?route=product/manufacturer/info&amp;manufacturer_id=8', 'data/demo/apple_logo.jpg'),
(74, 8, 'index.php?route=product/manufacturer/info&amp;manufacturer_id=9', 'data/demo/canon_logo.jpg'),
(71, 8, 'index.php?route=product/manufacturer/info&amp;manufacturer_id=10', 'data/demo/sony_logo.jpg'),
(72, 8, 'index.php?route=product/manufacturer/info&amp;manufacturer_id=6', 'data/demo/palm_logo.jpg'),
(76, 8, 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'data/demo/hp_logo.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `banner_image_description`
--

CREATE TABLE IF NOT EXISTS `banner_image_description` (
  `banner_image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  PRIMARY KEY (`banner_image_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `banner_image_description`
--

INSERT INTO `banner_image_description` (`banner_image_id`, `language_id`, `banner_id`, `title`) VALUES
(54, 1, 7, 'Samsung Tab 10.1'),
(77, 1, 6, 'HP Banner'),
(75, 1, 8, 'HTC'),
(74, 1, 8, 'Canon'),
(73, 1, 8, 'Apple'),
(72, 1, 8, 'Palm'),
(71, 1, 8, 'Sony'),
(76, 1, 8, 'Hewlett-Packard');

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(25, '', 0, 1, 1, 3, 1, '2009-01-31 01:04:25', '2011-05-30 12:14:55'),
(27, '', 20, 0, 0, 2, 1, '2009-01-31 01:55:34', '2010-08-22 06:32:15'),
(20, 'data/demo/compaq_presario.jpg', 0, 1, 1, 1, 1, '2009-01-05 21:49:43', '2011-07-16 02:14:42'),
(24, '', 0, 1, 1, 5, 1, '2009-01-20 02:36:26', '2011-05-30 12:15:18'),
(18, 'data/demo/hp_2.jpg', 0, 1, 0, 2, 1, '2009-01-05 21:49:15', '2011-05-30 12:13:55'),
(17, '', 0, 1, 1, 4, 1, '2009-01-03 21:08:57', '2011-05-30 12:15:11'),
(28, '', 25, 0, 0, 1, 1, '2009-02-02 13:11:12', '2010-08-22 06:32:46'),
(26, '', 20, 0, 0, 1, 1, '2009-01-31 01:55:14', '2010-08-22 06:31:45'),
(29, '', 25, 0, 0, 1, 1, '2009-02-02 13:11:37', '2010-08-22 06:32:39'),
(30, '', 25, 0, 0, 1, 1, '2009-02-02 13:11:59', '2010-08-22 06:33:00'),
(31, '', 25, 0, 0, 1, 1, '2009-02-03 14:17:24', '2010-08-22 06:33:06'),
(32, '', 25, 0, 0, 1, 1, '2009-02-03 14:17:34', '2010-08-22 06:33:12'),
(33, '', 0, 1, 1, 6, 1, '2009-02-03 14:17:55', '2011-05-30 12:15:25'),
(34, 'data/demo/ipod_touch_4.jpg', 0, 1, 4, 7, 1, '2009-02-03 14:18:11', '2011-05-30 12:15:31'),
(35, '', 28, 0, 0, 0, 1, '2010-09-17 10:06:48', '2010-09-18 14:02:42'),
(36, '', 28, 0, 0, 0, 1, '2010-09-17 10:07:13', '2010-09-18 14:02:55'),
(37, '', 34, 0, 0, 0, 1, '2010-09-18 14:03:39', '2011-04-22 01:55:08'),
(38, '', 34, 0, 0, 0, 1, '2010-09-18 14:03:51', '2010-09-18 14:03:51'),
(39, '', 34, 0, 0, 0, 1, '2010-09-18 14:04:17', '2011-04-22 01:55:20'),
(40, '', 34, 0, 0, 0, 1, '2010-09-18 14:05:36', '2010-09-18 14:05:36'),
(41, '', 34, 0, 0, 0, 1, '2010-09-18 14:05:49', '2011-04-22 01:55:30'),
(42, '', 34, 0, 0, 0, 1, '2010-09-18 14:06:34', '2010-11-07 20:31:04'),
(43, '', 34, 0, 0, 0, 1, '2010-09-18 14:06:49', '2011-04-22 01:55:40'),
(44, '', 34, 0, 0, 0, 1, '2010-09-21 15:39:21', '2010-11-07 20:30:55'),
(45, '', 18, 0, 0, 0, 1, '2010-09-24 18:29:16', '2011-04-26 08:52:11'),
(46, '', 18, 0, 0, 0, 1, '2010-09-24 18:29:31', '2011-04-26 08:52:23'),
(47, '', 34, 0, 0, 0, 1, '2010-11-07 11:13:16', '2010-11-07 11:13:16'),
(48, '', 34, 0, 0, 0, 1, '2010-11-07 11:13:33', '2010-11-07 11:13:33'),
(49, '', 34, 0, 0, 0, 1, '2010-11-07 11:14:04', '2010-11-07 11:14:04'),
(50, '', 34, 0, 0, 0, 1, '2010-11-07 11:14:23', '2011-04-22 01:16:01'),
(51, '', 34, 0, 0, 0, 1, '2010-11-07 11:14:38', '2011-04-22 01:16:13'),
(52, '', 34, 0, 0, 0, 1, '2010-11-07 11:16:09', '2011-04-22 01:54:57'),
(53, '', 34, 0, 0, 0, 1, '2010-11-07 11:28:53', '2011-04-22 01:14:36'),
(54, '', 34, 0, 0, 0, 1, '2010-11-07 11:29:16', '2011-04-22 01:16:50'),
(55, '', 34, 0, 0, 0, 1, '2010-11-08 10:31:32', '2010-11-08 10:31:32'),
(56, '', 34, 0, 0, 0, 1, '2010-11-08 10:31:50', '2011-04-22 01:16:37'),
(57, '', 0, 1, 1, 3, 1, '2011-04-26 08:53:16', '2011-05-30 12:15:05'),
(58, '', 52, 0, 0, 0, 1, '2011-05-08 13:44:16', '2011-05-08 13:44:16');

-- --------------------------------------------------------

--
-- Structure de la table `category_description`
--

CREATE TABLE IF NOT EXISTS `category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `category_description`
--

INSERT INTO `category_description` (`category_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`) VALUES
(28, 1, 'Monitors', '', '', ''),
(33, 1, 'Cameras', '', '', ''),
(32, 1, 'Web Cameras', '', '', ''),
(31, 1, 'Scanners', '', '', ''),
(30, 1, 'Printers', '', '', ''),
(29, 1, 'Mice and Trackballs', '', '', ''),
(27, 1, 'Mac', '', '', ''),
(26, 1, 'PC', '', '', ''),
(17, 1, 'Software', '', '', ''),
(25, 1, 'Components', '', '', ''),
(24, 1, 'Phones &amp; PDAs', '', '', ''),
(20, 1, 'Desktops', '&lt;p&gt;\r\n	Example of category description text&lt;/p&gt;\r\n', 'Example of category description', ''),
(35, 1, 'test 1', '', '', ''),
(36, 1, 'test 2', '', '', ''),
(37, 1, 'test 5', '', '', ''),
(38, 1, 'test 4', '', '', ''),
(39, 1, 'test 6', '', '', ''),
(40, 1, 'test 7', '', '', ''),
(41, 1, 'test 8', '', '', ''),
(42, 1, 'test 9', '', '', ''),
(43, 1, 'test 11', '', '', ''),
(34, 1, 'MP3 Players', '&lt;p&gt;\r\n	Shop Laptop feature only the best laptop deals on the market. By comparing laptop deals from the likes of PC World, Comet, Dixons, The Link and Carphone Warehouse, Shop Laptop has the most comprehensive selection of laptops on the internet. At Shop Laptop, we pride ourselves on offering customers the very best laptop deals. From refurbished laptops to netbooks, Shop Laptop ensures that every laptop - in every colour, style, size and technical spec - is featured on the site at the lowest possible price.&lt;/p&gt;\r\n', '', ''),
(18, 1, 'Laptops &amp; Notebooks', '&lt;p&gt;\r\n	Shop Laptop feature only the best laptop deals on the market. By comparing laptop deals from the likes of PC World, Comet, Dixons, The Link and Carphone Warehouse, Shop Laptop has the most comprehensive selection of laptops on the internet. At Shop Laptop, we pride ourselves on offering customers the very best laptop deals. From refurbished laptops to netbooks, Shop Laptop ensures that every laptop - in every colour, style, size and technical spec - is featured on the site at the lowest possible price.&lt;/p&gt;\r\n', '', ''),
(44, 1, 'test 12', '', '', ''),
(45, 1, 'Windows', '', '', ''),
(46, 1, 'Macs', '', '', ''),
(47, 1, 'test 15', '', '', ''),
(48, 1, 'test 16', '', '', ''),
(49, 1, 'test 17', '', '', ''),
(50, 1, 'test 18', '', '', ''),
(51, 1, 'test 19', '', '', ''),
(52, 1, 'test 20', '', '', ''),
(53, 1, 'test 21', '', '', ''),
(54, 1, 'test 22', '', '', ''),
(55, 1, 'test 23', '', '', ''),
(56, 1, 'test 24', '', '', ''),
(57, 1, 'Tablets', '', '', ''),
(58, 1, 'test 25', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `category_filter`
--

CREATE TABLE IF NOT EXISTS `category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `category_path`
--

CREATE TABLE IF NOT EXISTS `category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`path_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `category_path`
--

INSERT INTO `category_path` (`category_id`, `path_id`, `level`) VALUES
(25, 25, 0),
(28, 25, 0),
(28, 28, 1),
(35, 25, 0),
(35, 28, 1),
(35, 35, 2),
(36, 25, 0),
(36, 28, 1),
(36, 36, 2),
(29, 25, 0),
(29, 29, 1),
(30, 25, 0),
(30, 30, 1),
(31, 25, 0),
(31, 31, 1),
(32, 25, 0),
(32, 32, 1),
(20, 20, 0),
(27, 20, 0),
(27, 27, 1),
(26, 20, 0),
(26, 26, 1),
(24, 24, 0),
(18, 18, 0),
(45, 18, 0),
(45, 45, 1),
(46, 18, 0),
(46, 46, 1),
(17, 17, 0),
(33, 33, 0),
(34, 34, 0),
(37, 34, 0),
(37, 37, 1),
(38, 34, 0),
(38, 38, 1),
(39, 34, 0),
(39, 39, 1),
(40, 34, 0),
(40, 40, 1),
(41, 34, 0),
(41, 41, 1),
(42, 34, 0),
(42, 42, 1),
(43, 34, 0),
(43, 43, 1),
(44, 34, 0),
(44, 44, 1),
(47, 34, 0),
(47, 47, 1),
(48, 34, 0),
(48, 48, 1),
(49, 34, 0),
(49, 49, 1),
(50, 34, 0),
(50, 50, 1),
(51, 34, 0),
(51, 51, 1),
(52, 34, 0),
(52, 52, 1),
(58, 34, 0),
(58, 52, 1),
(58, 58, 2),
(53, 34, 0),
(53, 53, 1),
(54, 34, 0),
(54, 54, 1),
(55, 34, 0),
(55, 55, 1),
(56, 34, 0),
(56, 56, 1),
(57, 57, 0);

-- --------------------------------------------------------

--
-- Structure de la table `category_to_layout`
--

CREATE TABLE IF NOT EXISTS `category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `category_to_store`
--

CREATE TABLE IF NOT EXISTS `category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `category_to_store`
--

INSERT INTO `category_to_store` (`category_id`, `store_id`) VALUES
(17, 0),
(18, 0),
(20, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0);

-- --------------------------------------------------------

--
-- Structure de la table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=252 ;

--
-- Contenu de la table `country`
--

INSERT INTO `country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D''Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'Korea, Republic of', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People''s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(240, 'Jersey', 'JE', 'JEY', '', 1, 1),
(241, 'Guernsey', 'GG', 'GGY', '', 1, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `coupon`
--

CREATE TABLE IF NOT EXISTS `coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `code` varchar(10) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`coupon_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `coupon`
--

INSERT INTO `coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, '-10% Discount', '2222', 'P', '10.0000', 0, 0, '0.0000', '2011-01-01', '2012-01-01', 10, '10', 1, '2009-01-27 13:55:03'),
(5, 'Free Shipping', '3333', 'P', '0.0000', 0, 1, '100.0000', '2009-03-01', '2009-08-31', 10, '10', 1, '2009-03-14 21:13:53'),
(6, '-10.00 Discount', '1111', 'F', '10.0000', 0, 0, '10.0000', '1970-11-01', '2020-11-01', 100000, '10000', 1, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Structure de la table `coupon_category`
--

CREATE TABLE IF NOT EXISTS `coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`coupon_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `coupon_history`
--

CREATE TABLE IF NOT EXISTS `coupon_history` (
  `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`coupon_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `coupon_product`
--

CREATE TABLE IF NOT EXISTS `coupon_product` (
  `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`coupon_product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `currency`
--

INSERT INTO `currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 0.68159997, 1, '2015-04-13 20:34:36'),
(2, 'US Dollar', 'USD', '$', '', '2', 1.00000000, 1, '2015-04-13 21:20:11'),
(3, 'Euro', 'EUR', '', '€', '2', 0.94639999, 1, '2015-04-13 20:34:36');

-- --------------------------------------------------------

--
-- Structure de la table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `token` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `customer`
--

INSERT INTO `customer` (`customer_id`, `store_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `customer_group_id`, `ip`, `status`, `approved`, `token`, `date_added`, `code`) VALUES
(1, 0, 'customer1', 'customer1', 'customer1@gmail.com', '1123333', '12345678', 'a53a42ed891f6b50cc441fd8e8866255b33959d7', '3028093ff', 'a:1:{s:32:"30:YToxOntpOjIyNjtzOjI6IjE1Ijt9:";i:1;}', '', 1, 1, 1, '127.0.0.xxx', 1, 1, '', '2015-04-13 21:05:02', 'k1tAza231xqwy*'),
(2, 0, 'customer2', 'customer2', 'customer2@gmail.com', '1232323', '232323', 'bfcdaa2f6d811a0f177f4004c8dfd1c60f6e7a85', '2ded76752', 'a:0:{}', '', 1, 2, 1, '127.0.0.xxx', 1, 1, '', '2015-04-13 21:19:20', 'k1tAza231xqwy*');

-- --------------------------------------------------------

--
-- Structure de la table `customer_ban_ip`
--

CREATE TABLE IF NOT EXISTS `customer_ban_ip` (
  `customer_ban_ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(40) NOT NULL,
  PRIMARY KEY (`customer_ban_ip_id`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `customer_field`
--

CREATE TABLE IF NOT EXISTS `customer_field` (
  `customer_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `custom_field_value_id` int(11) NOT NULL,
  `name` int(128) NOT NULL,
  `value` text NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`customer_id`,`custom_field_id`,`custom_field_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `customer_group`
--

CREATE TABLE IF NOT EXISTS `customer_group` (
  `customer_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `approval` int(1) NOT NULL,
  `company_id_display` int(1) NOT NULL,
  `company_id_required` int(1) NOT NULL,
  `tax_id_display` int(1) NOT NULL,
  `tax_id_required` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`customer_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `customer_group`
--

INSERT INTO `customer_group` (`customer_group_id`, `approval`, `company_id_display`, `company_id_required`, `tax_id_display`, `tax_id_required`, `sort_order`) VALUES
(1, 0, 1, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `customer_group_description`
--

CREATE TABLE IF NOT EXISTS `customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`customer_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `customer_group_description`
--

INSERT INTO `customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Default', 'test');

-- --------------------------------------------------------

--
-- Structure de la table `customer_history`
--

CREATE TABLE IF NOT EXISTS `customer_history` (
  `customer_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `customer_ip`
--

CREATE TABLE IF NOT EXISTS `customer_ip` (
  `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_ip_id`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `customer_ip`
--

INSERT INTO `customer_ip` (`customer_ip_id`, `customer_id`, `ip`, `date_added`) VALUES
(1, 1, '127.0.0.xxx', '2015-04-13 21:05:05'),
(2, 2, '127.0.0.xxx', '2015-04-13 21:19:22');

-- --------------------------------------------------------

--
-- Structure de la table `customer_online`
--

CREATE TABLE IF NOT EXISTS `customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `customer_reward`
--

CREATE TABLE IF NOT EXISTS `customer_reward` (
  `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`customer_reward_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `customer_transaction`
--

CREATE TABLE IF NOT EXISTS `customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `custom_field`
--

CREATE TABLE IF NOT EXISTS `custom_field` (
  `custom_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  `location` varchar(32) NOT NULL,
  `position` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`custom_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `custom_field_description`
--

CREATE TABLE IF NOT EXISTS `custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`custom_field_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `custom_field_to_customer_group`
--

CREATE TABLE IF NOT EXISTS `custom_field_to_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  PRIMARY KEY (`custom_field_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `custom_field_value`
--

CREATE TABLE IF NOT EXISTS `custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`custom_field_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `custom_field_value_description`
--

CREATE TABLE IF NOT EXISTS `custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`custom_field_value_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `download`
--

CREATE TABLE IF NOT EXISTS `download` (
  `download_id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(128) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `remaining` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `download_description`
--

CREATE TABLE IF NOT EXISTS `download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`download_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `extension`
--

CREATE TABLE IF NOT EXISTS `extension` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL,
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=435 ;

--
-- Contenu de la table `extension`
--

INSERT INTO `extension` (`extension_id`, `type`, `code`) VALUES
(23, 'payment', 'cod'),
(22, 'total', 'shipping'),
(57, 'total', 'sub_total'),
(58, 'total', 'tax'),
(59, 'total', 'total'),
(410, 'module', 'banner'),
(426, 'module', 'carousel'),
(390, 'total', 'credit'),
(387, 'shipping', 'flat'),
(349, 'total', 'handling'),
(350, 'total', 'low_order_fee'),
(389, 'total', 'coupon'),
(413, 'module', 'category'),
(411, 'module', 'affiliate'),
(408, 'module', 'account'),
(393, 'total', 'reward'),
(398, 'total', 'voucher'),
(407, 'payment', 'free_checkout'),
(427, 'module', 'featured'),
(419, 'module', 'slideshow'),
(428, 'module', 'legal'),
(429, 'module', 'oxy'),
(430, 'module', 'oxy_banner'),
(431, 'module', 'oxy_custom_content_manager'),
(432, 'module', 'oxy_product_slider'),
(433, 'module', 'special'),
(434, 'module', 'special');

-- --------------------------------------------------------

--
-- Structure de la table `filter`
--

CREATE TABLE IF NOT EXISTS `filter` (
  `filter_id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `filter_description`
--

CREATE TABLE IF NOT EXISTS `filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`filter_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `filter_group`
--

CREATE TABLE IF NOT EXISTS `filter_group` (
  `filter_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`filter_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `filter_group_description`
--

CREATE TABLE IF NOT EXISTS `filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`filter_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `geo_zone`
--

CREATE TABLE IF NOT EXISTS `geo_zone` (
  `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`geo_zone_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `geo_zone`
--

INSERT INTO `geo_zone` (`geo_zone_id`, `name`, `description`, `date_modified`, `date_added`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2010-02-26 22:33:24', '2009-01-06 23:26:25'),
(4, 'UK Shipping', 'UK Shipping Zones', '2010-12-15 15:18:13', '2009-06-23 01:14:53'),
(5, 'Deutschland', 'Deutschland', '0000-00-00 00:00:00', '2015-04-13 20:40:43'),
(6, 'Europa', 'Europa', '0000-00-00 00:00:00', '2015-04-13 20:40:44'),
(7, 'Welt', 'Welt', '0000-00-00 00:00:00', '2015-04-13 20:40:45');

-- --------------------------------------------------------

--
-- Structure de la table `information`
--

CREATE TABLE IF NOT EXISTS `information` (
  `information_id` int(11) NOT NULL AUTO_INCREMENT,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`information_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `information`
--

INSERT INTO `information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(3, 1, 3, 1),
(4, 1, 1, 1),
(5, 1, 4, 1),
(6, 1, 2, 1),
(7, 1, 5, 1),
(8, 0, 6, 1),
(9, 0, 7, 1),
(10, 1, 6, 1),
(11, 0, 8, 1),
(12, 0, 8, 1);

-- --------------------------------------------------------

--
-- Structure de la table `information_description`
--

CREATE TABLE IF NOT EXISTS `information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`information_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `information_description`
--

INSERT INTO `information_description` (`information_id`, `language_id`, `title`, `description`) VALUES
(4, 1, 'About Us', '&lt;p&gt;\r\n	About Us&lt;/p&gt;\r\n'),
(5, 1, 'Terms &amp; Conditions', '&lt;p&gt;\r\n	Terms &amp;amp; Conditions&lt;/p&gt;\r\n'),
(3, 1, 'Privacy Policy', '&lt;p&gt;\r\n	Privacy Policy&lt;/p&gt;\r\n'),
(6, 1, 'Delivery Information', '&lt;p&gt;\r\n	Delivery Information&lt;/p&gt;\r\n'),
(7, 1, 'Withdrawal', 'Annex 1 to Article 246 &sect; 2 Section 3 Clause 1 of the Introductory Act to the German Civil Code (EGBGB)<br/><br/><H1 class="revoc_title">Notice of revocation rights</H1><br/><br/><H2 class="revoc_title">Revocation rights</H2><br/>You can revoke your contractual statement within [14 days] (1) without indicating reasons in text form (e.g. letter, fax, e-mail) [or - if the item is delivered to you before expiry of the time limit - also by returning the item ] (2). The deadline takes effect on receipt of this notice in text form (3). Timely dispatch of revocation [or the item] is sufficient to observe the revocation deadline. Revocations are to be submitted to: (4)<br/><br/><H2 class="revoc_title">Revocation consequences (5)</H2><br/>In the event of a valid revocation, the performance delivered by either party is to be returned and the proceeds of any utilization (e.g. interest) submitted.  (6) If you cannot submit / return the received performance and benefits (e.g. utilization benefits) to us, or submit / return them only partially or in a deteriorated condition, you need to compensate us to the required extent.  (7) [You must pay compensation for degraded items only insofar as the deterioration is due to handling of the items beyond testing of properties and functionality.  (8) &quot;Testing of properties and functionality&quot; is to be understood as testing and evaluation of the respective goods as is possible and common in retail shops, for instance. (9) Items which can be dispatched in packages are to be returned at our [expense and]  (10) risk. Items which cannot be dispatched in packages will be fetched from your premises.] (2) Payment obligations must be fulfilled within 30 days. The time limit becomes effective on dispatch of your revocation notice [or the item] (2) for you, and on its receipt for us.<br/><br/><H2 class="revoc_title">Special notes</H2><br/> (11)<br/>(12) <br/>(13) <br/><br/>(Location), (date) (consumer''s signature) (14)<br/><br/><H2 class="revoc_title">Notes on composition</H2><br/> (1) If the revocation notice is provided not by contract conclusion at the latest, but only afterward, the supplement in brackets must read &quot;one month&quot;.  In this case, composition note 9 also applies if the reference provided there is not issued in text form by contract conclusion at the latest. In the case of contracts on distance sales, a revocation notice issued in text form immediately after contract conclusion is equivalent to one issued on contract conclusion if the entrepreneur has informed the consumer as per Article 246 &sect; 1 Section 1 Number 10 EGBGB.<br/><br/> (2) The supplement in brackets does not apply to performances not involving provision of items.<br/><br/> (3) In any of the special cases mentioned next, the following must be inserted:<br/><br/> a) In the case of contracts to be concluded in writing: &quot;, but not before a contractual document, or your written request, or a duplicate of either of these has also been made available to you&quot;;<br/><br/> (b) In the case of distance sales contracts (&sect; 312b Section 1 Clause 1 of the German Civil Code (BGB)) concerning:<br/><br/> aa) Delivery of goods: &quot;, but not before receipt of the goods by the recipient (not before receipt of the first partial delivery in the case of recurrent supply of similar goods), nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with &sect; 1 Sections 1 and 2 EGBGB&quot;;<br/><br/>bb) Provision of services other than payment services: &quot;, but not before contract conclusion, nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with &sect; 1 Sections 1 and 2 EGBGB&quot;;<br/><br/>cc) Provision of payment services:<br/><br/>aaa) In the case of framework agreements on payment services: &quot;, but not before contract conclusion, nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with &sect; 1 Section 1 Numbers 8 to 12, Section 2 Numbers 2, 4 and 8, as well as Article 248 &sect; 4 Section 1 EGBGB&quot;;<br/><br/>bbb) In the case of instruments for small amounts in the sense of &sect; 675i Section 1 BGB: &quot;, but not before contract conclusion, nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with Section 1 Numbers 8 to 12, Section 2 Numbers 2, 4 and 8, as well as Article 248 &sect; 11 Section 1 EGBGB&quot;;<br/><br/>ccc) In the case of single payment contracts: &quot;, but not before contract conclusion, nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with &sect; 1 Section 1 Numbers 8 to 12, Section 2 Numbers 2, 4 and 8 as well as Article 248 &sect; 13, Sec. 1 EGBGB&quot;;<br/><br/> (c) In the case of contracts forming part of e-commerce (&sect; 312g Section 1 Clause 1 BGB): &quot;, but not before fulfilment of our obligations as per &sect; 312g Section 1 Clause 1 BGB in conjunction with Article 246 &sect; 3 EGBGB&quot;;<br/><br/> (d) In the case of trial purchases (&sect; 454 BGB): &quot;; but not before the sales contract has become binding through your endorsement of the purchased item&quot;;<br/><br/>If notice is served for a contract falling under several of the special cases mentioned above (for example, a distance sales contract for the supply of goods in e-commerce), the respective supplements must be combined (in this example, as follows: &quot;; but not before receipt of the goods by the recipient  [not before receipt of the first partial delivery in the case of recurrent supply of similar goods], nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with &sect; 1 Sections 1 and 2 EGBGB, as well as our obligations as per &sect; 312g Section 1 Clause 1 BGB in conjunction with Article 246 &sect; 3 EGBGB&quot;). If supplements to be combined are linguistically identical, their wording need not be repeated.<br/><br/> (4) Insert: Name/company and summonable address of the revocation''s intended recipient.<br/><br/>Also specifiable: Fax number, e-mail address and/or Internet address if the consumer receives confirmation of their revocation statement to the entrepreneur.<br/><br/> (5) This section may be omitted if the mutual performances are rendered only after expiry of the revocation deadline. The same applies if a reversal does not come into consideration (e.g. collection of a guarantee).<br/><br/> (6) If a fee for toleration of overdraft within the meaning of section &sect; 505 BGB has been agreed, the following must be inserted here:<br/><br/>&quot;If you overdraw your account without having been granted related approval, or exceed the overdraft scope granted to you, we may not demand any compensation of costs or interest from you beyond repayment of the overdraft or excess amount, if we have not informed you duly about the conditions and consequences of the overdraft or transgression (e.g. applicable interest on debt, costs).&quot;<br/><br/> (7) In the case of distance sales contracts for services, the following sentence must be added:<br/><br/>&quot;This may oblige you to nonetheless fulfil the contractual payment obligations for the period up to revocation.&quot;<br/><br/> (8) In the case of distance sales contracts for the supply of goods, the previously added sentence must be replaced by the following sentence: &quot;You must pay compensation for  degraded items and derived benefits only insofar as the benefits or deteriorations are due to handling of the items beyond testing of properties and functionality.&quot;<br/><br/> (9) If a reference concerning the obligation to compensate lost value as per &sect; 357 Section 3 Clause 1 BGB is not issued in text form by contract conclusion at the latest, the two preceding sentences must be replaced by the following supplement: &quot;You need not pay any compensation for deterioration caused by usage of the item for the intended purposes.&quot; In the case of distance sales contracts, a note provided in text form immediately after contract conclusion is equivalent to a note provided on contract conclusion, if the entrepreneur has informed the consumer in time, prior to issue of their contractual declaration, about the obligation for compensation of lost value in a manner appropriate for the employed means of telecommunication .<br/><br/>In the case of distance sales contracts for the supply of goods, the following must be added:<br/><br/>&quot;You must pay compensation for derived benefits only insofar as you have used the goods in a manner that goes beyond testing of properties and functionality. &quot;Testing of properties and functionality&quot; is to be understood as testing and evaluation of the respective goods as is possible and common in retail shops, for instance.<br/><br/>(10) If the consumer has agreed to bear shipping costs according to &sect; 357 Section 2 Clause 3 BGB, the addition in brackets can be omitted. Instead, the following must be inserted after &quot;to be returned at our [expense and] (10) risk.&quot;<br/><br/>&quot;You have to bear the regular costs of return if the delivered goods correspond to the order, and if the price of the goods to be returned does not exceed 40 EUR or, in case of a higher item price, if you had provided no consideration or contractually agreed partial payment at the time of revocation. Otherwise the return is free of charge for you.&quot;<br/><br/> (11) In the case of revocation rights as per &sect; 312d, Section 1 BGB, applicable to distance sales contracts for provision of services, the following reference must be included:<br/><br/>&quot;Your revocation right expires prematurely if the contract has been completely fulfilled by both parties at your explicit request before you exercise your revocation right.&quot;<br/><br/> (12) The following note for financed transactions can be omitted if there is no associated deal:<br/><br/>&quot;If you finance this contract through a loan and revoke it later, you are no longer bound to the loan contract either, if both contracts form a single economic unit. This can be assumed, in particular, if we are your lender or if your lender is enabling the financing with the help of our cooperation. If the loan has already been received by us once the revocation takes effect or the goods are returned, your lender, as concerns the legal consequences of the revocation or return in relation to you, assumes our rights and obligations arising from the financed contract. The latter does not apply if the present contract''s object is the purchase of financial instruments (e.g. securities, foreign exchange or derivatives).<br/><br/>If you want to avoid contractual binding as far as possible, make use of your right of revocation, and also revoke the loan contract if you have this additional right to withdrawal.&quot;<br/><br/>In the case of financed acquisition of a plot of land or an equivalent right, Sentence 2 of the afore-mentioned note must be changed as follows:<br/><br/>&quot;This is to be assumed only if the parties to both contracts are identical or, if in addition to providing loans, the lender promotes your real-estate transaction through interaction with the seller by wholly or partly taking over their sales interests, or their functions in planning, advertising or project implementation, or by unilaterally favouring the seller.&quot;<br/><br/> (13) The following note for distance sales contracts for financial services can be omitted in an absence of any additional distance sales contract for services:<br/><br/>&quot;On revocation of this distance sales contract for financial services, you are also no longer bound to any additional distance sales contract, if this contract''s object is a further service provided by us or a third party on the basis of an agreement between us and the third party.&quot;<br/><br/> (14) The location, date and signature strip can be omitted. In this case, these details must be replaced either by the words &quot;End of revocation notice&quot; or by the words &quot;Your (insert: entrepreneur''s company)&quot;.<br/><br/><br/><i>Source: </i><a href="http://www.twigg.de/widerrusbelehrungs-muster.htm" target="_blank">Withdrawal/Revocation Sample in English</a><br/><br/>'),
(7, 0, 'Widerruf', '<p><b style="color: red;">Nachstehende Punkte <b>[x]</b> sind mit dem jeweiligen Text zu ergänzen!</b></p>\r\n	<p><b>Widerrufsrecht</b></p>\r\n	<p>Sie haben das Recht diesen Vertrag binnen vierzehn Tagen ohne Angabe von Gründen zu widerrufen.</p>\r\n	<p>Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag <b>[1]</b>.</p>\r\n	<p>Um Ihr Widerrufsrecht auszuüben, müssen Sie uns (<b>[2]</b>) mittels einer eindeutigen Erklärung (z.B. ein mit der Post versandter Brief, Telefax oder Email) über Ihren Entschluss, diesen Vertrag zu widerrufen, informieren. Sie können dafür das beigefügte Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist. <b>[3]</b></p>\r\n	<p>Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.</p>\r\n	<p><b>Folgen des Widerrufs</b></p>\r\n	<p>Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, günstigste Standardlieferung gewählt haben), unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist. Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. <b>[4]</b> <b>[5]</b> <b>[6]</b></p>\r\n	<p><b>[1]</b><br />\r\n	a) im Falle eines Dienstleistungsvertrags oder eines Vertrags über die Lieferung von Wasser, Gas oder Strom, wenn sie nicht in einem begrenzten Volumen oder in einer bestimmten Menge zum Verkauf angeboten werden, von Fernwärme oder von digitalen Inhalten, die nicht auf einem körperlichen Datenträger geliefert werden:<br />\r\n	<b>des Vertragsabschlusses.</b>;\r\n	<br />\r\n	b) im Falle eines Kaufvertrags:<br />\r\n	<b>, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die Waren in Besitz genommen haben bzw. hat.</b>;\r\n	<br />\r\n	c) im Falle eines Vertrags über mehrere Waren, die der Verbraucher im Rahmen einer einheitlichen Bestellung bestellt hat und die getrennt geliefert werden:<br />\r\n	<b>, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die letzte Ware in Besitz genommen haben bzw. hat.</b>;\r\n	<br />\r\n	d) im Falle eines Vertrags über die Lieferung einer Ware in mehreren Teilsendungen oder Stücken:<br />\r\n	<b>, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die letzte Teilsendung oder das letzte Stück in Besitz genommen haben bzw. hat.</b>;\r\n	<br />\r\n	e) im Falle eines Vertrags zur regelmäßigen Lieferung von Waren über einen festgelegten Zeitraum hinweg:<br />\r\n	<b>, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die erste Ware in Besitz genommen haben bzw. hat.</b></p>\r\n	<p><b>[2]</b><br />\r\n	Hier eigenen Namen, Anschrift, Telefonnummer, Emailadresse und optional Faxnummer einfügen</p>\r\n	<p><b>[3]</b><br />\r\n	Wird dem Verbraucher die Wahl eingeräumt die Information über seinen Widerruf des Vertrags auf der eigenen Webseite elektronisch auszufüllen und zu übermitteln, dann Folgendes einfügen:<br />\r\n	Sie können das Muster-Widerrufsformular oder eine andere eindeutige Erklärung auch auf unserer Webseite [<i>Hier WebaAdresse einfügen</i>] elektronisch ausfüllen und übermitteln.<br />\r\n	Machen Sie von dieser Möglichkeit Gebrauch, so werden wir Ihnen unverzüglich (z. B. per E-Mail) eine Bestätigung über den Eingang eines solchen Widerrufs übermitteln.</p>\r\n	<p><b>[4]</b><br />\r\n	Im Falle von Kaufverträgen, in denen Sie nicht angeboten haben, im Fall des Widerrufs die Waren selbst abzuholen, folgendes einfügen:<br />\r\n	<b>Wir können die Rückzahlung verweigern, bis wir die Waren wieder zurückerhalten haben oder bis Sie den Nachweis erbracht haben, dass Sie die Waren zurückgesandt haben, je nachdem, welches der frühere Zeitpunkt ist.</b>\r\n	</p>\r\n	<p><b>[5]</b><br />\r\n	Wenn der Verbraucher Waren im Zusammenhang mit dem Vertrag erhalten hat:<br />\r\n	a) entweder:\r\n	<b>Wir holen die Waren ab.</b> oder<br />\r\n	<b>Sie haben die Waren unverzüglich und in jedem Fall spätestens binnen vierzehn Tagen ab dem Tag, an dem Sie uns über den Widerruf dieses Vertrags unterrichten, an ... uns oder an [<i>hier sind gegebenenfalls der Name und die Anschrift der von\r\n	Ihnen zur Entgegennahme der Waren ermächtigten Person einzufügen</i>] zurückzusenden oder zu übergeben. Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen absenden.</b><br />\r\n	b) oder:<br />\r\n	<b>Wir tragen die Kosten der Rücksendung der Waren.</b><br />\r\n	<b>Sie tragen die unmittelbaren Kosten der Rücksendung der Waren.</b><br /></p>\r\n	<p>Wenn Sie bei einem Fernabsatzvertrag nicht anbieten, die Kosten der Rücksendung der Waren zu tragen und die Waren aufgrund ihrer Beschaffenheit nicht normal mit der Post zurückgesandt werden können:<br />\r\n	<b>Sie tragen die unmittelbaren Kosten der Rücksendung der Waren in Höhe von ... EUR [<i>Betrag einfügen</i>].</b><br />,\r\n	oder wenn die Kosten vernünftigerweise nicht im Voraus berechnet werden können:<br />\r\n	<b>Sie tragen die unmittelbaren Kosten der Rücksendung der Waren. Die Kosten werden auf höchstens etwa ... EUR [<i>Betrag einfügen</i>] geschätzt.</b><br />\r\n	oder<br />\r\n	— wenn die Waren bei einem außerhalb von Geschäftsräumen geschlossenen Vertrag aufgrund ihrer Beschaffenheit nicht normal mit der Post zurückgesandt werden können und zum Zeitpunkt des Vertragsschlusses zur Wohnung des Verbrauchers geliefert worden sind:<br />\r\n	<b>Wir holen die Waren auf unsere Kosten ab.</b><br />und<br />\r\n	<b>Sie müssen für einen etwaigen Wertverlust der Waren nur aufkommen, wenn dieser Wertverlust auf einen zur Prüfung der Beschaffenheit, Eigenschaften und Funktionsweise der Waren nicht notwendigen Umgang mit ihnen zurückzuführen ist.</b></p>\r\n	<p><b>[6]</b><br />\r\n	Im Falle eines Vertrags zur Erbringung von Dienstleistungen oder der Lieferung von Wasser, Gas oder Strom, wenn sie nicht in einem begrenzten Volumen oder in einer bestimmten Menge zum Verkauf angeboten werden, oder von Fernwärme fügen Sie Folgendes ein:<br />\r\n	<b>Haben Sie verlangt, dass die Dienstleistungen oder Lieferung von Wasser/Gas/Strom/Fernwärme [<b>Unzutreffendes streichen</b>] während der Widerrufsfrist beginnen soll, so haben Sie uns einen angemessenen Betrag zu zahlen, der dem Anteil der bis zu dem\r\n	Zeitpunkt, zu dem Sie uns von der Ausübung des Widerrufsrechts hinsichtlich dieses Vertrags unterrichten, bereits erbrachten Dienstleistungen im Vergleich zum Gesamtumfang der im Vertrag vorgesehenen Dienstleistungen entspricht.</b></p>'),
(8, 1, 'Product Summary', '<p>\r\n	Below the summary of your order.<br />\r\n	To change an item click the link "Change".</p>\r\n	<p>\r\n	I have read the <a class="popTerms" href="index.php?route=information/information/info&amp;information_id=13" title="Terms">Terms</a> and <a class="popTerms" href="index.php?route=information/information/info&amp;information_id=17" title="Withdrawal">Widthdrawal</a> and agree.</p>'),
(8, 0, 'Zusammenfassung (Buttonlösung)', '<p>\r\n	Nachstehend eine Zusammenfassung der Bestellung.<br />\r\n	Für allfällige Änderungen bitte den Link "Ändern" oder das jeweilige Produktbild anklicken.</p>\r\n	<p>\r\n	<a class="popTerms" href="index.php?route=information/information/info&amp;information_id=13" title="AGB">AGB</a> und <a class="popTerms" href="index.php?route=information/information/info&amp;information_id=17" title="Widerruf">Widerruf</a> habe ich gelesen und bin damit einverstanden.</p>'),
(9, 1, 'Email Addon', '<p>Firma NAME HERE / CEO: NAME<br />ADDRESS<br />Tel: +41 123 45678 / Fax: +41 123 45678 / email: EMAIL HERE<br />USt-IdNr. AT 1234567 / Steuer-Nr.: 123/4567/8901 / Registered at XXXXX<br />Either you display the links to the texts or paste here the texts itself:<br /><a href="http://SHOPURL/agb">TOS</a> - <a href="http://SHOPURL/datenschutz">Privacy</a> - <a href="http://SHOPURL/versand">Delivery</a> - <a href="http://SHOPURL/withdrawal">Withdrawal</a> - <a href="http://SHOPURL/withdrawalform">Withdrawal PDF</a>\r\n                </p>'),
(9, 0, 'Emailanhang', '<p>Firma NAME HIER / Geschäftsführer: NAME ADRESSE<br />Tel: +41 123 45678 / Fax: +41 123 45678 / email: EMAIL HIER<br />USt-IdNr. XX1234567 / Steuer-Nr.: 123/4567/8901 / Registriert bei XXXXX<br />Hier wahlweise die Links zu den einzelnen Texten anführen oder die Text selber einfügen:<br /><a href="http://SHOPURL/agb">AGB</a> - <a href="http://SHOPURL/datenschutz">Privatsphäre und Datenschutz</a> - <a href="http://SHOPURL/versand">Versand und Lieferung</a> - <a href="http://SHOPURL/widerruf">Widerruf</a> - <a href="http://SHOPURL/widerrufsformular">Widerrufsformular PDF</a></p>'),
(10, 1, 'Privacy', '<p><h1>Privacy Statement</h1><h2>General</h2>Your personal data (e.g. title, name, house address, e-mail address, phone number, bank details, credit card number) are processed by us only in accordance with the provisions of German data privacy laws. The following provisions describe the type, scope and purpose of collecting, processing and utilizing personal data. This data privacy policy applies only to our web pages. If links on our pages route you to other pages, please inquire there about how your data are handled in such cases.<br/><h2>Inventory data</h2>(1) Your personal data, insofar as these are necessary for this contractual relationship (inventory data) in terms of its establishment, organization of content and modifications, are used exclusively for fulfilling the contract. For goods to be delivered, for instance, your name and address must be relayed to the supplier of the goods. <br/>(2) Without your explicit consent or a legal basis, your personal data are not passed on to third parties outside the scope of fulfilling this contract. After completion of the contract, your data are blocked against further use. After expiry of deadlines as per tax-related and commercial regulations, these data are deleted unless you have expressly consented to their further use.<br/><h2>Web analysis with Google Analytics</h2>This website uses Google Analytics, a web analysis service of Google Inc. (Google). Google Analytics uses cookies, i.e. text files stored on your computer to enable analysis of website usage by you. Information generated by the cookie about your use of this website is usually transmitted to a Google server in the United States and stored there. In case of activated IP anonymization on this website, however, your IP address is previously truncated by Google within member states of the European Union or in other states which are party to the agreement on the European Economic Area. Only in exceptional cases is a full IP address transmitted to a Google server in the United States and truncated there. On behalf this website''s owner, Google will use this information to evaluate your use of the website, compile reports about website activities, and provide the website''s operator with further services related to website and Internet usage. The IP address sent from your browser as part of Google Analytics is not merged with other data by Google. You can prevent storage of cookies by appropriately setting your browser software; in this case, however, please note that you might not be able to fully use all functions offered by this website. In addition, you can prevent data generated by the cookie and relating to your use of the website (including your IP address) from being collected and processed by Google, by downloading and installing a browser plug-in from the following link: <a href="http://tools.google.com/dlpage/gaoptout?hl=en" target="_blank">http://tools.google.com/dlpage/gaoptout?hl=en</a><br/><br/>This website uses Google Analytics with the extension &quot;anonymizeIP()&quot;, IP addresses being truncated before further processing in order to rule out direct associations to persons.<br/><h2>Information about cookies</h2>(1) To optimize our web presence, we use cookies. These are small text files stored in your computer''s main memory. These cookies are deleted after you close the browser. Other cookies remain on your computer (long-term cookies) and permit its recognition on your next visit. This allows us to improve your access to our site.<br/>(2) You can prevent storage of cookies by choosing a &quot;disable cookies&quot; option in your browser settings. But this can limit the functionality of our Internet offers as a result.<br/><h2>Social plug-ins from Facebook</h2>We use social plug-ins from facebook.com, operated by Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA. The plug-ins can be recognized by way of the Facebook logo or the supplement &quot;Facebook Social Plug-in&quot;. For example, if you click on the &quot;Like&quot; button or leave a comment, the relevant information is transmitted directly from your browser to Facebook and stored there. Furthermore, Facebook makes your likes public for your Facebook friends. If you are logged into Facebook, it can assign the invocation of our page directly to your Facebook account. Even if you are not logged in or don''t have a Facebook account, your browser sends information (e.g. which web pages you have called up, your IP address) which is then stored by Facebook. For details about handling of your personal data by Facebook and your related rights, please refer to the data privacy policy of Facebook: <a href="http://www.facebook.com/policy.php" target="_blank">http://www.facebook.com/policy.php</a>. If you do not want Facebook to map data collected about you via our Web sites to your Facebook account, you must log out of Facebook before you visit our web pages.<br/><h2>Social plug-ins from Twitter</h2>With Twitter and its Retweet functions, we use social plug-ins from Twitter.com, operated by Twitter Inc. 795 Folsom St., Suite 600, San Francisco, CA 94107. If you use Retweet, the websites visited by you are announced to third parties and associated with your Twitter account. Details about handling of your data by Twitter as well as your rights and setting options for protecting your personal information can be found in Twitter''s data privacy policy: <a href="http://twitter.com/privacy" target="_blank">http://twitter.com/privacy </a><br/><h2>Newsletter</h2>Following subscription to the newsletter, your e-mail address is used for our own advertising purposes until you cancel the newsletter again. Cancellation is possible at any time. The following consent has been expressly granted by you separately, or possibly in the course of an ordering process: <br/>( Ich möchte den Newsletter abonnieren und willige ein meine Adresse dafür zu verwenden. )<br/>You may revoke your consent at any time with future effect. If you no longer want to receive the newsletter, then unsubscribe as follows: <br/>( Email, Link auf Webseite )<br/><h2>Disclosure</h2>According to the Federal Data Protection Act, you have a right to free-of-charge information about your stored data, and possibly entitlement to correction, blocking or deletion of such data. Inquiries can be directed to the following e-mail addresses: ( <a href="mailto:MY.EMAIL@MYSHOP.com">MY.EMAIL@MYSHOP.com</a> )<br/><br/><i>Quelle: </i><a href="http://www.twigg.de/" target="_blank">twigg.de</a><br/><br/>'),
(10, 0, 'Datenschutz', '<p><strong>Datenschutz</strong></p>\r\n	<p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder Emailadressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.\r\n	</p>\r\n	<p>Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per Email) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich. </p>\r\n	<p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.</p><br />\r\n	<p><strong>Datenschutzerklärung für die Nutzung von Facebook-Plugins (Like-Button)</strong></p>\r\n	 <p>Auf unseren Seiten sind Plugins des sozialen Netzwerks Facebook (Facebook Inc., 1601 Willow Road, Menlo Park, California, 94025, USA) integriert. Die Facebook-Plugins erkennen Sie an dem Facebook-Logo oder dem "Like-Button" ("Gefällt mir") auf unserer Seite. Eine Übersicht über die Facebook-Plugins finden Sie hier: <a href="http://developers.facebook.com/docs/plugins/" target="_blank">http://developers.facebook.com/docs/plugins/</a>.<br />Wenn Sie unsere Seiten besuchen, wird über das Plugin eine direkte Verbindung zwischen Ihrem Browser und dem Facebook-Server hergestellt. Facebook erhält dadurch die Information, dass Sie mit Ihrer IP-Adresse unsere Seite besucht haben. Wenn Sie den Facebook "Like-Button" anklicken während Sie in Ihrem Facebook-Account eingeloggt sind, können Sie die Inhalte unserer Seiten auf Ihrem Facebook-Profil verlinken. Dadurch kann Facebook den Besuch unserer Seiten Ihrem Benutzerkonto zuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Facebook erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von facebook unter <a href="http://de-de.facebook.com/policy.php" target="_blank">http://de-de.facebook.com/policy.php</a></p>\r\n	<p>Wenn Sie nicht wünschen, dass Facebook den Besuch unserer Seiten Ihrem Facebook-Nutzerkonto zuordnen kann, loggen Sie sich bitte aus Ihrem Facebook-Benutzerkonto aus.</p><br />\r\n	<p><strong>Datenschutzerklärung für die Nutzung von Google Analytics</strong></p>\r\n	 <p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt.</p>\r\n	<p>Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.</p>\r\n	<p>Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: <a href="http://tools.google.com/dlpage/gaoptout?hl=de">http://tools.google.com/dlpage/gaoptout?hl=de</a>.<br />\r\n	<p><strong>Datenschutzerklärung für die Nutzung von Google Adsense</strong></p>\r\n	 <p>Diese Website benutzt Google AdSense, einen Dienst zum Einbinden von Werbeanzeigen der Google Inc. ("Google"). Google AdSense verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website ermöglicht. Google AdSense verwendet auch so genannte Web Beacons (unsichtbare Grafiken). Durch diese Web Beacons können Informationen wie der Besucherverkehr auf diesen Seiten ausgewertet werden.</p>\r\n	<p>Die durch Cookies und Web Beacons erzeugten Informationen über die Benutzung dieser Website (einschließlich Ihrer IP-Adresse) und Auslieferung von Werbeformaten werden an einen Server von Google in den USA übertragen und dort gespeichert. Diese Informationen können von Google an Vertragspartner von Google weiter gegeben werden. Google wird Ihre IP-Adresse jedoch nicht mit anderen von Ihnen gespeicherten Daten zusammenführen.</p>\r\n	<p>Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.</p><br />\r\n	<p><strong>Datenschutzerklärung für die Nutzung von Google +1</strong></p>\r\n	 <p><i>Erfassung und Weitergabe von Informationen:</i><br />Mithilfe der Google +1-Schaltfläche können Sie Informationen weltweit veröffentlichen. Über die Google +1-Schaltfläche erhalten Sie und andere Nutzer personalisierte Inhalte von Google und unseren Partnern. Google speichert sowohl die Information, dass Sie für einen Inhalt +1 gegeben haben, als auch Informationen über die Seite, die Sie beim Klicken auf +1 angesehen haben. Ihre +1 können als Hinweise zusammen mit Ihrem Profilnamen und Ihrem Foto in Google-Diensten, wie etwa in Suchergebnissen oder in Ihrem Google-Profil, oder an anderen Stellen auf Websites und Anzeigen im Internet eingeblendet werden.<br />Google zeichnet Informationen über Ihre +1-Aktivitäten auf, um die Google-Dienste für Sie und andere zu verbessern. Um die Google +1-Schaltfläche verwenden zu können, benötigen Sie ein weltweit sichtbares, öffentliches Google-Profil, das zumindest den für das Profil gewählten Namen enthalten muss. Dieser Name wird in allen Google-Diensten verwendet. In manchen Fällen kann dieser Name auch einen anderen Namen ersetzen, den Sie beim Teilen von Inhalten über Ihr Google-Konto verwendet haben. Die Identität Ihres Google-Profils kann Nutzern angezeigt werden, die Ihre Emailadresse kennen oder über andere identifizierende Informationen von Ihnen verfügen.<br /><br />\r\n	<i>Verwendung der erfassten Informationen:</i><br />\r\n	Neben den oben erläuterten Verwendungszwecken werden die von Ihnen bereitgestellten Informationen gemäß den geltenden Google-Datenschutzbestimmungen genutzt. Google veröffentlicht möglicherweise zusammengefasste Statistiken über die +1-Aktivitäten der Nutzer bzw. gibt diese an Nutzer und Partner weiter, wie etwa Publisher, Inserenten oder verbundene Websites. </p><br />\r\n	<p><strong>Datenschutzerklärung für die Nutzung von Twitter</strong></p>\r\n	 <p>Auf unseren Seiten sind Funktionen des Dienstes Twitter eingebunden. Diese Funktionen werden angeboten durch die Twitter Inc., Twitter, Inc. 1355 Market St, Suite 900, San Francisco, CA 94103, USA. Durch das Benutzen von Twitter und der Funktion "Re-Tweet" werden die von Ihnen besuchten Webseiten mit Ihrem Twitter-Account verknüpft und anderen Nutzern bekannt gegeben. Dabei werden auch Daten an Twitter übertragen.</p>\r\n	<p>Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Twitter erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Twitter unter <a href="http://twitter.com/privacy" target="_blank">http://twitter.com/privacy</a>.</p>\r\n	<p>Ihre Datenschutzeinstellungen bei Twitter können Sie in den Konto-Einstellungen unter <a href="http://twitter.com/account/settings" target="_blank">http://twitter.com/account/settings</a> ändern.</p>\r\n	<br />\r\n	<p><i>Quellen: <a rel="nofollow" href="http://www.e-recht24.de/muster-datenschutzerklaerung.html" target="_blank">eRecht24</a>, <a rel="nofollow" href="http://www.e-recht24.de/artikel/datenschutz/6590-facebook-like-button-datenschutz-disclaimer.html" target="_blank">eRecht24 Datenschutzerklärung für Facebook</a>, <a rel="nofollow" href="http://www.google.com/intl/de/analytics/learn/privacy.html" target="_blank">Google Analytics Bedingungen</a>, <a rel="nofollow" href="http://www.e-recht24.de/artikel/datenschutz/6635-datenschutz-rechtliche-risiken-bei-der-nutzung-von-google-analytics-und-googleadsense.html" target="_blank">Google Adsense Haftungsausschluss</a>, <a rel="nofollow" href="http://www.google.com/intl/de/+/policy/+1button.html" target="_blank">Datenschutzerklärung für Google +1</a>, <a rel="nofollow" href="http://twitter.com/privacy" target="_blank">Datenschutzerklärung für Twitter</a></i></p>'),
(11, 1, 'Sample Withdrawal Formular', '<p>If you want to cancel the contract, please fill in your details and send this formular back (email or yellow post).</p>\r\n	<p>\r\n	To<br /><br />\r\n	Name of the shop<br />\r\n	Adress of Shop<br />\r\n	Emailadress or faxnumber of shop</p>\r\n	<p>I the undersigned cancel the contract of purchasing following goods:<br /><br />\r\n	..............................................................................<br />\r\n	..............................................................................<br />\r\n	(articles, model and price)</p>\r\n	<p>Products ordered on:<br /><br />\r\n	.............................<br />\r\n	Date</p>\r\n	<p>Products obtained:<br /><br />\r\n	.............................<br />\r\n	Date/p>\r\n	<p>Name and address of buyer<br /><br />\r\n	.............................<br />\r\n	.............................<br />\r\n	.............................<br />\r\n	.............................<br />\r\n	Date</p>\r\n	<p>....................................................<br />\r\n	Signature customer<br />\r\n	(only at written withdrawal)</p>'),
(11, 0, 'Muster Widerrufsformular', '<p>Wenn Sie den Vertrag widerrufen wollen, dann füllen Sie bitte dieses Formular aus und senden Sie es zurück (Email oder Post).</p>\r\n	<p>\r\n	An<br /><br />\r\n	Name des Shops / des Unternehmers<br />\r\n	Anschrift Shopbetreiber<br />\r\n	Emailadress oder ggf. Faxnummer Shopbetreiber</p>\r\n	<p>Hiermit widerrufe(n) ich/wir den von mir/uns abgeschlossenen Vertrag über den Kauf der folgenden Waren / die Erbringung der folgenden Dienstleistung:<br /><br />\r\n	..............................................................................<br />\r\n	..............................................................................<br />\r\n	(Name der Ware, ggf. Bestellnummer und Preis)</p>\r\n	<p>Ware bestellt am:<br /><br />\r\n	.............................<br />\r\n	Datum</p>\r\n	<p>Ware erhalten am:<br /><br />\r\n	.............................<br />\r\n	Datum</p>\r\n	<p>Name und Anschrift des Verbrauchers<br /><br />\r\n	.............................<br />\r\n	.............................<br />\r\n	.............................<br />\r\n	.............................<br />\r\n	Datum</p>\r\n	<p>....................................................<br />\r\n	Unterschrift Kunde<br />\r\n	(nur bei schriftlichem Widerruf)</p>'),
(12, 1, 'Imprint', '<h1>Legal Disclosure</h1>Information in accordance with section 5 TMG<br/><br/>NAME SHOPOWNER<br/>NAME COMPANY<br/>STREET<br/>ADD. ADRESS<br/>ZIP CITY<br/><h2>Contact</h2>Telephone: 12345678<br/>E-Mail: <a href="mailto:MY.EMAIL@SHOP.COM">MY.EMAIL@SHOP.COM</a><br/>Webshop: <a href="http://MYSHOP.com" target="_blank">http://MYSHOP.com</a><br/><h2>VAT number</h2>VAT indentification number in accorance with section 27 a of the German VAT act<br/>UID_HIER_ANGEBEN<br/><h2>Regulating authority</h2>AUFSICHTSBEHOERDE (optional)<br/><h2>Regulated professions / Independent Professions</h2>Job title (Independent professions): BERUFSBEZEICHNUNG (Freier Beruf)<br/>Professional chamber: ZUST._KAMMER<br/>Professional regulations and how to access: WEBSITE TO REGULATION<br/><h2>Person responsible for content in accordance with 55 Abs. 2 RStV</h2>RESPONSIBLE FOR TEXT CONTENT<br/><h2>Indication of source for images and graphics</h2>IMAGE SOURCES IF EXTERNAL ARE USED<br/><h2>Disclaimer</h2>Accountability for content<br/>The contents of our pages have been created with the utmost care. However, we cannot guarantee the contents'' accuracy, completeness or topicality. According to statutory provisions, we are furthermore responsible for our own content on these web pages. In this context, please note that we are accordingly not obliged to monitor merely the transmitted or saved information of third parties, or investigate circumstances pointing to illegal activity. Our obligations to remove or block the use of information under generally applicable laws remain unaffected by this as per &sect;&sect; 8 to 10 of the Telemedia Act (TMG).<br/><br/>Accountability for links<br/>Responsibility for the content of external links (to web pages of third parties) lies solely with the operators of the linked pages. No violations were evident to us at the time of linking. Should any legal infringement become known to us, we will remove the respective link immediately.<br/><br/>Copyright<br/> Our web pages and their contents are subject to German copyright law. Unless expressly permitted by law (&sect; 44a et seq. of the copyright law), every form of utilizing, reproducing or processing works subject to copyright protection on our web pages requires the prior consent of the respective owner of the rights. Individual reproductions of a work are allowed only for private use, so must not serve either directly or indirectly for earnings. Unauthorized utilization of copyrighted works is punishable (&sect; 106 of the copyright law).<br/><br/><i>Supported through:</i><a href="http://www.berlin-klinik.de/" target="_blank">berlin-klinik.de</a><br/><br/>'),
(12, 0, 'Impressum', '<h1>Impressum</h1>Angaben gem&auml;&szlig; &sect; 5 TMG<br/><br/>NAME SHOPBETREIBER<br/>NAME FIRMA<br/>STRASSE<br/>ADRESSZUSATZ<br/>PLZ ORT<br/><h2>Kontakt</h2>Telefon: 12345678<br/>E-Mail: <a href="mailto:MEIN.EMAIL@SHOP.COM">MEIN.EMAIL@SHOP.COM</a><br/>Internetadresse: <a href="http://MEINSHOP.com" target="_blank">http://MEINSHOP.com</a><br/><h2>Umsatzsteuer-ID</h2>Umsatzsteuer-Identifikationsnummer gem&auml;&szlig; &sect;27 a Umsatzsteuergesetz<br/>UID_HIER_ANGEBEN<br/><h2>Aufsichtsbeh&ouml;rde</h2>AUFSICHTSBEHOERDE (optional)<br/><h2>Reglementierte Berufe / Freie Berufe</h2>Berufsbezeichnung<br/>(freie Berufe): BERUFSBEZEICHNUNG (Freier Beruf)<br/>Berufsst&auml;ndische Kammer: ZUST._KAMMER<br/>Bezeichnung Berufsregeln<br/>und deren Zugang: BERUFSREGELN_UND_WEBSEITE<br/><h2>Verantwortlich f&uuml;r den Inhalt nach &sect; 55 Abs. 2 RStV</h2>VERANTWORTLICH_FUER_REDAKTIONELLE_TEXTE<br/><h2>Quellenangaben f&uuml;r die verwendeten Bilder und Grafiken</h2>BILDQUELLEN_DRITTER_HIER_NENNEN<br/><h2>Haftungsausschluss</h2>Haftung f&uuml;r Inhalte<br/>Die Inhalte unserer Seiten wurden mit gr&ouml;&szlig;ter Sorgfalt erstellt. F&uuml;r die Richtigkeit, Vollst&auml;ndigkeit und Aktualit&auml;t der Inhalte k&ouml;nnen wir jedoch keine Gew&auml;hr &uuml;bernehmen. Als Diensteanbieter sind wir gem&auml;&szlig; &sect; 7 Abs.1 TMG f&uuml;r eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach &sect;&sect; 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, &uuml;bermittelte oder gespeicherte fremde Informationen zu &uuml;berwachen oder nach Umst&auml;nden zu forschen, die auf eine rechtswidrige T&auml;tigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unber&uuml;hrt. Eine diesbez&uuml;gliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung m&ouml;glich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.<br/><br/>Haftung f&uuml;r Links<br/>Unser Angebot enth&auml;lt Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb k&ouml;nnen wir f&uuml;r diese fremden Inhalte auch keine Gew&auml;hr &uuml;bernehmen. F&uuml;r die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf m&ouml;gliche Rechtsverst&ouml;&szlig;e &uuml;berpr&uuml;ft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.<br/><br/>Urheberrecht<br/>Die durch die Seitenbetreiber erstellten bzw. verwendeten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielf&auml;ltigung, Bearbeitung, Verbreitung und jede Art der Verwertung au&szlig;erhalb der Grenzen des Urheberrechtes bed&uuml;rfen der Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur f&uuml;r den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.<br/><br/><i>Unterstützt durch: </i><a href="http://www.berlin-klinik.de/" target="_blank">berlin-klinik.de</a><br/><br/>');

-- --------------------------------------------------------

--
-- Structure de la table `information_to_layout`
--

CREATE TABLE IF NOT EXISTS `information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`information_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `information_to_layout`
--

INSERT INTO `information_to_layout` (`information_id`, `store_id`, `layout_id`) VALUES
(7, 0, 0),
(8, 0, 0),
(9, 0, 0),
(10, 0, 0),
(11, 0, 0),
(12, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `information_to_store`
--

CREATE TABLE IF NOT EXISTS `information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`information_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `information_to_store`
--

INSERT INTO `information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(11, 0),
(12, 0);

-- --------------------------------------------------------

--
-- Structure de la table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `filename` varchar(64) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `language`
--

INSERT INTO `language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `filename`, `sort_order`, `status`) VALUES
(1, 'English', 'en', 'en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 'english', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `layout`
--

CREATE TABLE IF NOT EXISTS `layout` (
  `layout_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`layout_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `layout`
--

INSERT INTO `layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information');

-- --------------------------------------------------------

--
-- Structure de la table `layout_route`
--

CREATE TABLE IF NOT EXISTS `layout_route` (
  `layout_route_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(255) NOT NULL,
  PRIMARY KEY (`layout_route_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Contenu de la table `layout_route`
--

INSERT INTO `layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(30, 6, 0, 'account'),
(17, 10, 0, 'affiliate/'),
(29, 3, 0, 'product/category'),
(26, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(22, 5, 0, 'product/manufacturer'),
(23, 7, 0, 'checkout/'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap');

-- --------------------------------------------------------

--
-- Structure de la table `length_class`
--

CREATE TABLE IF NOT EXISTS `length_class` (
  `length_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) NOT NULL,
  PRIMARY KEY (`length_class_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `length_class`
--

INSERT INTO `length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Structure de la table `length_class_description`
--

CREATE TABLE IF NOT EXISTS `length_class_description` (
  `length_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  PRIMARY KEY (`length_class_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `length_class_description`
--

INSERT INTO `length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Structure de la table `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `manufacturer`
--

INSERT INTO `manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`) VALUES
(5, 'HTC', 'data/demo/htc_logo.jpg', 0),
(6, 'Palm', 'data/demo/palm_logo.jpg', 0),
(7, 'Hewlett-Packard', 'data/demo/hp_logo.jpg', 0),
(8, 'Apple', 'data/demo/apple_logo.jpg', 0),
(9, 'Canon', 'data/demo/canon_logo.jpg', 0),
(10, 'Sony', 'data/demo/sony_logo.jpg', 0);

-- --------------------------------------------------------

--
-- Structure de la table `manufacturer_to_store`
--

CREATE TABLE IF NOT EXISTS `manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`manufacturer_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `manufacturer_to_store`
--

INSERT INTO `manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0);

-- --------------------------------------------------------

--
-- Structure de la table `option`
--

CREATE TABLE IF NOT EXISTS `option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `option`
--

INSERT INTO `option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 2),
(2, 'checkbox', 3),
(4, 'text', 4),
(5, 'select', 1),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 1),
(12, 'date', 1);

-- --------------------------------------------------------

--
-- Structure de la table `option_description`
--

CREATE TABLE IF NOT EXISTS `option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`option_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `option_description`
--

INSERT INTO `option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 1, 'Checkbox'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(5, 1, 'Select'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(11, 1, 'Size');

-- --------------------------------------------------------

--
-- Structure de la table `option_value`
--

CREATE TABLE IF NOT EXISTS `option_value` (
  `option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_value_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Contenu de la table `option_value`
--

INSERT INTO `option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(46, 11, '', 1),
(47, 11, '', 2),
(48, 11, '', 3);

-- --------------------------------------------------------

--
-- Structure de la table `option_value_description`
--

CREATE TABLE IF NOT EXISTS `option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`option_value_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `option_value_description`
--

INSERT INTO `option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(48, 1, 11, 'Large'),
(47, 1, 11, 'Medium'),
(46, 1, 11, 'Small');

-- --------------------------------------------------------

--
-- Structure de la table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(32) NOT NULL,
  `payment_company_id` varchar(32) NOT NULL,
  `payment_tax_id` varchar(32) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(32) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `order`
--

INSERT INTO `order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_company_id`, `payment_tax_id`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(1, 0, 'INV-2013-00', 0, 'Your Store', 'http://localhost/halalexoticmeats/', 1, 1, 'customer1', 'customer1', 'customer1@gmail.com', '1123333', '12345678', 'customer1', 'customer1', '', '', '', 'Tunis', 'Tunis', 'Tunis', '1002', 'Tunisia', 214, 'Monastir', 3305, '', 'Cash On Delivery', 'cod', 'customer1', 'customer1', '', 'Tunis', 'Tunis', 'Tunis', '1002', 'Tunisia', 214, 'Monastir', 3305, '', 'Flat Shipping Rate', 'flat.flat', '', '206.0000', 1, 0, '0.0000', 1, 2, 'USD', '1.00000000', '127.0.0.xxx', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36', 'fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4', '2015-04-13 21:06:48', '2015-04-13 21:08:31');

-- --------------------------------------------------------

--
-- Structure de la table `order_download`
--

CREATE TABLE IF NOT EXISTS `order_download` (
  `order_download_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `remaining` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `order_field`
--

CREATE TABLE IF NOT EXISTS `order_field` (
  `order_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `custom_field_value_id` int(11) NOT NULL,
  `name` int(128) NOT NULL,
  `value` text NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`order_id`,`custom_field_id`,`custom_field_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `order_fraud`
--

CREATE TABLE IF NOT EXISTS `order_fraud` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `country_match` varchar(3) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `high_risk_country` varchar(3) NOT NULL,
  `distance` int(11) NOT NULL,
  `ip_region` varchar(255) NOT NULL,
  `ip_city` varchar(255) NOT NULL,
  `ip_latitude` decimal(10,6) NOT NULL,
  `ip_longitude` decimal(10,6) NOT NULL,
  `ip_isp` varchar(255) NOT NULL,
  `ip_org` varchar(255) NOT NULL,
  `ip_asnum` int(11) NOT NULL,
  `ip_user_type` varchar(255) NOT NULL,
  `ip_country_confidence` varchar(3) NOT NULL,
  `ip_region_confidence` varchar(3) NOT NULL,
  `ip_city_confidence` varchar(3) NOT NULL,
  `ip_postal_confidence` varchar(3) NOT NULL,
  `ip_postal_code` varchar(10) NOT NULL,
  `ip_accuracy_radius` int(11) NOT NULL,
  `ip_net_speed_cell` varchar(255) NOT NULL,
  `ip_metro_code` int(3) NOT NULL,
  `ip_area_code` int(3) NOT NULL,
  `ip_time_zone` varchar(255) NOT NULL,
  `ip_region_name` varchar(255) NOT NULL,
  `ip_domain` varchar(255) NOT NULL,
  `ip_country_name` varchar(255) NOT NULL,
  `ip_continent_code` varchar(2) NOT NULL,
  `ip_corporate_proxy` varchar(3) NOT NULL,
  `anonymous_proxy` varchar(3) NOT NULL,
  `proxy_score` int(3) NOT NULL,
  `is_trans_proxy` varchar(3) NOT NULL,
  `free_mail` varchar(3) NOT NULL,
  `carder_email` varchar(3) NOT NULL,
  `high_risk_username` varchar(3) NOT NULL,
  `high_risk_password` varchar(3) NOT NULL,
  `bin_match` varchar(10) NOT NULL,
  `bin_country` varchar(2) NOT NULL,
  `bin_name_match` varchar(3) NOT NULL,
  `bin_name` varchar(255) NOT NULL,
  `bin_phone_match` varchar(3) NOT NULL,
  `bin_phone` varchar(32) NOT NULL,
  `customer_phone_in_billing_location` varchar(8) NOT NULL,
  `ship_forward` varchar(3) NOT NULL,
  `city_postal_match` varchar(3) NOT NULL,
  `ship_city_postal_match` varchar(3) NOT NULL,
  `score` decimal(10,5) NOT NULL,
  `explanation` text NOT NULL,
  `risk_score` decimal(10,5) NOT NULL,
  `queries_remaining` int(11) NOT NULL,
  `maxmind_id` varchar(8) NOT NULL,
  `error` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `order_history`
--

CREATE TABLE IF NOT EXISTS `order_history` (
  `order_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(5) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`order_history_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `order_history`
--

INSERT INTO `order_history` (`order_history_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_added`) VALUES
(1, 1, 1, 1, '', '2015-04-13 21:08:31');

-- --------------------------------------------------------

--
-- Structure de la table `order_option`
--

CREATE TABLE IF NOT EXISTS `order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`order_option_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `order_option`
--

INSERT INTO `order_option` (`order_option_id`, `order_id`, `order_product_id`, `product_option_id`, `product_option_value_id`, `name`, `value`, `type`) VALUES
(1, 1, 1, 225, 0, 'Delivery Date', '2011-04-22', 'date');

-- --------------------------------------------------------

--
-- Structure de la table `order_product`
--

CREATE TABLE IF NOT EXISTS `order_product` (
  `order_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL,
  PRIMARY KEY (`order_product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `order_product`
--

INSERT INTO `order_product` (`order_product_id`, `order_id`, `product_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`) VALUES
(1, 1, 47, 'HP LP3065', 'Product 21', 1, '100.0000', '100.0000', '0.0000', 300),
(2, 1, 40, 'iPhone', 'product 11', 1, '101.0000', '101.0000', '0.0000', 0);

-- --------------------------------------------------------

--
-- Structure de la table `order_recurring`
--

CREATE TABLE IF NOT EXISTS `order_recurring` (
  `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `profile_name` varchar(255) NOT NULL,
  `profile_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `profile_reference` varchar(255) NOT NULL,
  PRIMARY KEY (`order_recurring_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `order_recurring_transaction`
--

CREATE TABLE IF NOT EXISTS `order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_recurring_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`order_recurring_transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `order_status`
--

CREATE TABLE IF NOT EXISTS `order_status` (
  `order_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`order_status_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `order_status`
--

INSERT INTO `order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 1, 'Processing'),
(3, 1, 'Shipped'),
(7, 1, 'Canceled'),
(5, 1, 'Complete'),
(8, 1, 'Denied'),
(9, 1, 'Canceled Reversal'),
(10, 1, 'Failed'),
(11, 1, 'Refunded'),
(12, 1, 'Reversed'),
(13, 1, 'Chargeback'),
(1, 1, 'Pending'),
(16, 1, 'Voided'),
(15, 1, 'Processed'),
(14, 1, 'Expired');

-- --------------------------------------------------------

--
-- Structure de la table `order_total`
--

CREATE TABLE IF NOT EXISTS `order_total` (
  `order_total_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`order_total_id`),
  KEY `idx_orders_total_orders_id` (`order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `order_total`
--

INSERT INTO `order_total` (`order_total_id`, `order_id`, `code`, `title`, `text`, `value`, `sort_order`) VALUES
(1, 1, 'sub_total', 'Sub-Total', '$201.00', '201.0000', 1),
(2, 1, 'shipping', 'Flat Shipping Rate', '$5.00', '5.0000', 3),
(3, 1, 'total', 'Total', '$206.00', '206.0000', 9);

-- --------------------------------------------------------

--
-- Structure de la table `order_voucher`
--

CREATE TABLE IF NOT EXISTS `order_voucher` (
  `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  PRIMARY KEY (`order_voucher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(13) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL,
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Contenu de la table `product`
--

INSERT INTO `product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `date_added`, `date_modified`, `viewed`) VALUES
(28, 'Product 1', '', '', '', '', '', '', '', 939, 7, 'data/demo/htc_touch_hd_1.jpg', 5, 1, '100.0000', 200, 9, '2009-02-03', '146.40000000', 2, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, '2009-02-03 16:06:50', '2011-09-30 01:05:39', 0),
(29, 'Product 2', '', '', '', '', '', '', '', 999, 6, 'data/demo/palm_treo_pro_1.jpg', 6, 1, '279.9900', 0, 9, '2009-02-03', '133.00000000', 2, '0.00000000', '0.00000000', '0.00000000', 3, 1, 1, 0, 1, '2009-02-03 16:42:17', '2011-09-30 01:06:08', 0),
(30, 'Product 3', '', '', '', '', '', '', '', 7, 6, 'data/demo/canon_eos_5d_1.jpg', 9, 1, '100.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, '2009-02-03 16:59:00', '2011-09-30 01:05:23', 1),
(31, 'Product 4', '', '', '', '', '', '', '', 1000, 6, 'data/demo/nikon_d300_1.jpg', 0, 1, '80.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 3, 1, 1, 0, 1, '2009-02-03 17:00:10', '2011-09-30 01:06:00', 0),
(32, 'Product 5', '', '', '', '', '', '', '', 999, 6, 'data/demo/ipod_touch_1.jpg', 8, 1, '100.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, '2009-02-03 17:07:26', '2011-09-30 01:07:22', 0),
(33, 'Product 6', '', '', '', '', '', '', '', 1000, 6, 'data/demo/samsung_syncmaster_941bw.jpg', 0, 1, '200.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, '2009-02-03 17:08:31', '2011-09-30 01:06:29', 0),
(34, 'Product 7', '', '', '', '', '', '', '', 1000, 6, 'data/demo/ipod_shuffle_1.jpg', 8, 1, '100.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, '2009-02-03 18:07:54', '2011-09-30 01:07:17', 0),
(35, 'Product 8', '', '', '', '', '', '', '', 1000, 5, '', 0, 0, '100.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, '2009-02-03 18:08:31', '2011-09-30 01:06:17', 0),
(36, 'Product 9', '', '', '', '', '', '', '', 994, 6, 'data/demo/ipod_nano_1.jpg', 8, 0, '100.0000', 100, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, '2009-02-03 18:09:19', '2011-09-30 01:07:12', 0),
(40, 'product 11', '', '', '', '', '', '', '', 969, 5, 'data/demo/iphone_1.jpg', 8, 1, '101.0000', 0, 9, '2009-02-03', '10.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, '2009-02-03 21:07:12', '2011-09-30 01:06:53', 0),
(41, 'Product 14', '', '', '', '', '', '', '', 977, 5, 'data/demo/imac_1.jpg', 8, 1, '100.0000', 0, 9, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, '2009-02-03 21:07:26', '2011-09-30 01:06:44', 0),
(43, 'Product 16', '', '', '', '', '', '', '', 929, 5, 'data/demo/macbook_1.jpg', 8, 0, '500.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, '2009-02-03 21:07:49', '2011-09-30 01:05:46', 2),
(44, 'Product 17', '', '', '', '', '', '', '', 1000, 5, 'data/demo/macbook_air_1.jpg', 8, 1, '1000.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, '2009-02-03 21:08:00', '2011-09-30 01:05:53', 0),
(45, 'Product 18', '', '', '', '', '', '', '', 998, 5, 'data/demo/macbook_pro_1.jpg', 8, 1, '2000.0000', 0, 100, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, '2009-02-03 21:08:17', '2011-09-15 22:22:01', 0),
(46, 'Product 19', '', '', '', '', '', '', '', 1000, 5, 'data/demo/sony_vaio_1.jpg', 10, 1, '1000.0000', 0, 9, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, '2009-02-03 21:08:29', '2011-09-30 01:06:39', 0),
(47, 'Product 21', '', '', '', '', '', '', '', 1000, 5, 'data/demo/hp_1.jpg', 7, 1, '100.0000', 400, 9, '2009-02-03', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 0, 1, 0, 1, '2009-02-03 21:08:40', '2011-09-30 01:05:28', 1),
(48, 'product 20', 'test 1', '', '', '', '', '', 'test 2', 995, 5, 'data/demo/ipod_classic_1.jpg', 8, 1, '100.0000', 0, 9, '2009-02-08', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, '2009-02-08 17:21:51', '2011-09-30 01:07:06', 0),
(49, 'SAM1', '', '', '', '', '', '', '', 0, 8, 'data/demo/samsung_tab_1.jpg', 0, 1, '199.9900', 0, 9, '2011-04-25', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, '2011-04-26 08:57:34', '2011-09-30 01:06:23', 1);

-- --------------------------------------------------------

--
-- Structure de la table `product_additional`
--

CREATE TABLE IF NOT EXISTS `product_additional` (
  `product_id` int(11) NOT NULL,
  `end_of_life` tinyint(1) NOT NULL DEFAULT '0',
  `condition` varchar(1) NOT NULL,
  `guarantee` varchar(2) NOT NULL,
  `package_content` varchar(5) NOT NULL,
  `package_content_unit` varchar(4) NOT NULL,
  `base_price_quantity` varchar(4) NOT NULL,
  `base_price_unit` varchar(4) NOT NULL,
  `base_price_factor` varchar(4) NOT NULL,
  `group_access` int(11) DEFAULT NULL,
  `publish_down` datetime NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='OSWorX Legal Mod Additional Product Attributes';

-- --------------------------------------------------------

--
-- Structure de la table `product_attribute`
--

CREATE TABLE IF NOT EXISTS `product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`product_id`,`attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `product_attribute`
--

INSERT INTO `product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES
(43, 2, 1, '1'),
(47, 4, 1, '16GB'),
(43, 4, 1, '8gb'),
(47, 2, 1, '4');

-- --------------------------------------------------------

--
-- Structure de la table `product_description`
--

CREATE TABLE IF NOT EXISTS `product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `tag` text NOT NULL,
  PRIMARY KEY (`product_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `product_description`
--

INSERT INTO `product_description` (`product_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`, `tag`) VALUES
(35, 1, 'Product 8', '&lt;p&gt;\r\n	Product 8&lt;/p&gt;\r\n', '', '', ''),
(48, 1, 'iPod Classic', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;More room to move.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Cover Flow.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Browse through your music collection by flipping through album art. Select an album to turn it over and see the track list.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Enhanced interface.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Experience a whole new way to browse and view your music and video.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Sleeker design.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Beautiful, durable, and sleeker than ever, iPod classic now features an anodized aluminum and polished stainless steel enclosure with rounded edges.&lt;/p&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', '', ''),
(40, 1, 'iPhone', '&lt;p class=&quot;intro&quot;&gt;\r\n	iPhone is a revolutionary new mobile phone that allows you to make a call by simply tapping a name or number in your address book, a favorites list, or a call log. It also automatically syncs all your contacts from a PC, Mac, or Internet service. And it lets you select and listen to voicemail messages in whatever order you want just like email.&lt;/p&gt;\r\n', '', '', ''),
(28, 1, 'HTC Touch HD', '&lt;p&gt;\r\n	HTC Touch - in High Definition. Watch music videos and streaming content in awe-inspiring high definition clarity for a mobile experience you never thought possible. Seductively sleek, the HTC Touch HD provides the next generation of mobile functionality, all at a simple touch. Fully integrated with Windows Mobile Professional 6.1, ultrafast 3.5G, GPS, 5MP camera, plus lots more - all delivered on a breathtakingly crisp 3.8&amp;quot; WVGA touchscreen - you can take control of your mobile world with the HTC Touch HD.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Features&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Processor Qualcomm&amp;reg; MSM 7201A&amp;trade; 528 MHz&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Windows Mobile&amp;reg; 6.1 Professional Operating System&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Memory: 512 MB ROM, 288 MB RAM&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Dimensions: 115 mm x 62.8 mm x 12 mm / 146.4 grams&lt;/li&gt;\r\n	&lt;li&gt;\r\n		3.8-inch TFT-LCD flat touch-sensitive screen with 480 x 800 WVGA resolution&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HSDPA/WCDMA: Europe/Asia: 900/2100 MHz; Up to 2 Mbps up-link and 7.2 Mbps down-link speeds&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Quad-band GSM/GPRS/EDGE: Europe/Asia: 850/900/1800/1900 MHz (Band frequency, HSUPA availability, and data speed are operator dependent.)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Device Control via HTC TouchFLO&amp;trade; 3D &amp;amp; Touch-sensitive front panel buttons&lt;/li&gt;\r\n	&lt;li&gt;\r\n		GPS and A-GPS ready&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Bluetooth&amp;reg; 2.0 with Enhanced Data Rate and A2DP for wireless stereo headsets&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Wi-Fi&amp;reg;: IEEE 802.11 b/g&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HTC ExtUSB&amp;trade; (11-pin mini-USB 2.0)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		5 megapixel color camera with auto focus&lt;/li&gt;\r\n	&lt;li&gt;\r\n		VGA CMOS color camera&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Built-in 3.5 mm audio jack, microphone, speaker, and FM radio&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Ring tone formats: AAC, AAC+, eAAC+, AMR-NB, AMR-WB, QCP, MP3, WMA, WAV&lt;/li&gt;\r\n	&lt;li&gt;\r\n		40 polyphonic and standard MIDI format 0 and 1 (SMF)/SP MIDI&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Rechargeable Lithium-ion or Lithium-ion polymer 1350 mAh battery&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Expansion Slot: microSD&amp;trade; memory card (SD 2.0 compatible)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		AC Adapter Voltage range/frequency: 100 ~ 240V AC, 50/60 Hz DC output: 5V and 1A&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Special Features: FM Radio, G-Sensor&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', '', ''),
(44, 1, 'MacBook Air', '&lt;div&gt;\r\n	MacBook Air is ultrathin, ultraportable, and ultra unlike anything else. But you don&amp;rsquo;t lose inches and pounds overnight. It&amp;rsquo;s the result of rethinking conventions. Of multiple wireless innovations. And of breakthrough design. With MacBook Air, mobile computing suddenly has a new standard.&lt;/div&gt;\r\n', '', '', ''),
(45, 1, 'MacBook Pro', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Latest Intel mobile architecture&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Powered by the most advanced mobile processors from Intel, the new Core 2 Duo MacBook Pro is over 50% faster than the original Core Duo MacBook Pro and now supports up to 4GB of RAM.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Leading-edge graphics&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			The NVIDIA GeForce 8600M GT delivers exceptional graphics processing power. For the ultimate creative canvas, you can even configure the 17-inch model with a 1920-by-1200 resolution display.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Designed for life on the road&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Innovations such as a magnetic power connection and an illuminated keyboard with ambient light sensor put the MacBook Pro in a class by itself.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Connect. Create. Communicate.&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Quickly set up a video conference with the built-in iSight camera. Control presentations and media from up to 30 feet away with the included Apple Remote. Connect to high-bandwidth peripherals with FireWire 800 and DVI.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Next-generation wireless&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Featuring 802.11n wireless technology, the MacBook Pro delivers up to five times the performance and up to twice the range of previous-generation technologies.&lt;/p&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', '', ''),
(29, 1, 'Palm Treo Pro', '&lt;p&gt;\r\n	Redefine your workday with the Palm Treo Pro smartphone. Perfectly balanced, you can respond to business and personal email, stay on top of appointments and contacts, and use Wi-Fi or GPS when you&amp;rsquo;re out and about. Then watch a video on YouTube, catch up with news and sports on the web, or listen to a few songs. Balance your work and play the way you like it, with the Palm Treo Pro.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Features&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Windows Mobile&amp;reg; 6.1 Professional Edition&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Qualcomm&amp;reg; MSM7201 400MHz Processor&lt;/li&gt;\r\n	&lt;li&gt;\r\n		320x320 transflective colour TFT touchscreen&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HSDPA/UMTS/EDGE/GPRS/GSM radio&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Tri-band UMTS &amp;mdash; 850MHz, 1900MHz, 2100MHz&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Quad-band GSM &amp;mdash; 850/900/1800/1900&lt;/li&gt;\r\n	&lt;li&gt;\r\n		802.11b/g with WPA, WPA2, and 801.1x authentication&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Built-in GPS&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Bluetooth Version: 2.0 + Enhanced Data Rate&lt;/li&gt;\r\n	&lt;li&gt;\r\n		256MB storage (100MB user available), 128MB RAM&lt;/li&gt;\r\n	&lt;li&gt;\r\n		2.0 megapixel camera, up to 8x digital zoom and video capture&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Removable, rechargeable 1500mAh lithium-ion battery&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Up to 5.0 hours talk time and up to 250 hours standby&lt;/li&gt;\r\n	&lt;li&gt;\r\n		MicroSDHC card expansion (up to 32GB supported)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		MicroUSB 2.0 for synchronization and charging&lt;/li&gt;\r\n	&lt;li&gt;\r\n		3.5mm stereo headset jack&lt;/li&gt;\r\n	&lt;li&gt;\r\n		60mm (W) x 114mm (L) x 13.5mm (D) / 133g&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', '', ''),
(36, 1, 'iPod Nano', '&lt;div&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Video in your pocket.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Its the small iPod with one very big idea: video. The worlds most popular music player now lets you enjoy movies, TV shows, and more on a two-inch display thats 65% brighter than before.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Cover Flow.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Browse through your music collection by flipping through album art. Select an album to turn it over and see the track list.&lt;strong&gt;&amp;nbsp;&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Enhanced interface.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Experience a whole new way to browse and view your music and video.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Sleek and colorful.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		With an anodized aluminum and polished stainless steel enclosure and a choice of five colors, iPod nano is dressed to impress.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;iTunes.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Available as a free download, iTunes makes it easy to browse and buy millions of songs, movies, TV shows, audiobooks, and games and download free podcasts all at the iTunes Store. And you can import your own music, manage your whole media library, and sync your iPod or iPhone with ease.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', '', ''),
(46, 1, 'Sony VAIO', '&lt;div&gt;\r\n	Unprecedented power. The next generation of processing technology has arrived. Built into the newest VAIO notebooks lies Intel&amp;#39;s latest, most powerful innovation yet: Intel&amp;reg; Centrino&amp;reg; 2 processor technology. Boasting incredible speed, expanded wireless connectivity, enhanced multimedia support and greater energy efficiency, all the high-performance essentials are seamlessly combined into a single chip.&lt;/div&gt;\r\n', '', '', ''),
(47, 1, 'HP LP3065', '&lt;p&gt;\r\n	Stop your co-workers in their tracks with the stunning new 30-inch diagonal HP LP3065 Flat Panel Monitor. This flagship monitor features best-in-class performance and presentation features on a huge wide-aspect screen while letting you work as comfortably as possible - you might even forget you&amp;#39;re at the office&lt;/p&gt;\r\n', '', '', ''),
(32, 1, 'iPod Touch', '&lt;p&gt;\r\n	&lt;strong&gt;Revolutionary multi-touch interface.&lt;/strong&gt;&lt;br /&gt;\r\n	iPod touch features the same multi-touch screen technology as iPhone. Pinch to zoom in on a photo. Scroll through your songs and videos with a flick. Flip through your library by album artwork with Cover Flow.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Gorgeous 3.5-inch widescreen display.&lt;/strong&gt;&lt;br /&gt;\r\n	Watch your movies, TV shows, and photos come alive with bright, vivid color on the 320-by-480-pixel display.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Music downloads straight from iTunes.&lt;/strong&gt;&lt;br /&gt;\r\n	Shop the iTunes Wi-Fi Music Store from anywhere with Wi-Fi.1 Browse or search to find the music youre looking for, preview it, and buy it with just a tap.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Surf the web with Wi-Fi.&lt;/strong&gt;&lt;br /&gt;\r\n	Browse the web using Safari and watch YouTube videos on the first iPod with Wi-Fi built in&lt;br /&gt;\r\n	&amp;nbsp;&lt;/p&gt;\r\n', '', '', ''),
(41, 1, 'iMac', '&lt;div&gt;\r\n	Just when you thought iMac had everything, now there&acute;s even more. More powerful Intel Core 2 Duo processors. And more memory standard. Combine this with Mac OS X Leopard and iLife &acute;08, and it&acute;s more all-in-one than ever. iMac packs amazing performance into a stunningly slim space.&lt;/div&gt;\r\n', '', '', ''),
(33, 1, 'Samsung SyncMaster 941BW', '&lt;div&gt;\r\n	Imagine the advantages of going big without slowing down. The big 19&amp;quot; 941BW monitor combines wide aspect ratio with fast pixel response time, for bigger images, more room to work and crisp motion. In addition, the exclusive MagicBright 2, MagicColor and MagicTune technologies help deliver the ideal image in every situation, while sleek, narrow bezels and adjustable stands deliver style just the way you want it. With the Samsung 941BW widescreen analog/digital LCD monitor, it&amp;#39;s not hard to imagine.&lt;/div&gt;\r\n', '', '', ''),
(34, 1, 'iPod Shuffle', '&lt;div&gt;\r\n	&lt;strong&gt;Born to be worn.&lt;/strong&gt;\r\n	&lt;p&gt;\r\n		Clip on the worlds most wearable music player and take up to 240 songs with you anywhere. Choose from five colors including four new hues to make your musical fashion statement.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Random meets rhythm.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		With iTunes autofill, iPod shuffle can deliver a new musical experience every time you sync. For more randomness, you can shuffle songs during playback with the slide of a switch.&lt;/p&gt;\r\n	&lt;strong&gt;Everything is easy.&lt;/strong&gt;\r\n	&lt;p&gt;\r\n		Charge and sync with the included USB dock. Operate the iPod shuffle controls with one hand. Enjoy up to 12 hours straight of skip-free music playback.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', '', ''),
(43, 1, 'MacBook', '&lt;div&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Intel Core 2 Duo processor&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Powered by an Intel Core 2 Duo processor at speeds up to 2.16GHz, the new MacBook is the fastest ever.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;1GB memory, larger hard drives&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		The new MacBook now comes with 1GB of memory standard and larger hard drives for the entire line perfect for running more of your favorite applications and storing growing media collections.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Sleek, 1.08-inch-thin design&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		MacBook makes it easy to hit the road thanks to its tough polycarbonate case, built-in wireless technologies, and innovative MagSafe Power Adapter that releases automatically if someone accidentally trips on the cord.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Built-in iSight camera&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Right out of the box, you can have a video chat with friends or family,2 record a video at your desk, or take fun pictures with Photo Booth&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', '', ''),
(31, 1, 'Nikon D300', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		Engineered with pro-level features and performance, the 12.3-effective-megapixel D300 combines brand new technologies with advanced features inherited from Nikon&amp;#39;s newly announced D3 professional digital SLR camera to offer serious photographers remarkable performance combined with agility.&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		Similar to the D3, the D300 features Nikon&amp;#39;s exclusive EXPEED Image Processing System that is central to driving the speed and processing power needed for many of the camera&amp;#39;s new features. The D300 features a new 51-point autofocus system with Nikon&amp;#39;s 3D Focus Tracking feature and two new LiveView shooting modes that allow users to frame a photograph using the camera&amp;#39;s high-resolution LCD monitor. The D300 shares a similar Scene Recognition System as is found in the D3; it promises to greatly enhance the accuracy of autofocus, autoexposure, and auto white balance by recognizing the subject or scene being photographed and applying this information to the calculations for the three functions.&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		The D300 reacts with lightning speed, powering up in a mere 0.13 seconds and shooting with an imperceptible 45-millisecond shutter release lag time. The D300 is capable of shooting at a rapid six frames per second and can go as fast as eight frames per second when using the optional MB-D10 multi-power battery pack. In continuous bursts, the D300 can shoot up to 100 shots at full 12.3-megapixel resolution. (NORMAL-LARGE image setting, using a SanDisk Extreme IV 1GB CompactFlash card.)&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		The D300 incorporates a range of innovative technologies and features that will significantly improve the accuracy, control, and performance photographers can get from their equipment. Its new Scene Recognition System advances the use of Nikon&amp;#39;s acclaimed 1,005-segment sensor to recognize colors and light patterns that help the camera determine the subject and the type of scene being photographed before a picture is taken. This information is used to improve the accuracy of autofocus, autoexposure, and auto white balance functions in the D300. For example, the camera can track moving subjects better and by identifying them, it can also automatically select focus points faster and with greater accuracy. It can also analyze highlights and more accurately determine exposure, as well as infer light sources to deliver more accurate white balance detection.&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', '', ''),
(49, 1, 'Samsung Galaxy Tab 10.1', '&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1, is the world&amp;rsquo;s thinnest tablet, measuring 8.6 mm thickness, running with Android 3.0 Honeycomb OS on a 1GHz dual-core Tegra 2 processor, similar to its younger brother Samsung Galaxy Tab 8.9.&lt;/p&gt;\r\n&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1 gives pure Android 3.0 experience, adding its new TouchWiz UX or TouchWiz 4.0 &amp;ndash; includes a live panel, which lets you to customize with different content, such as your pictures, bookmarks, and social feeds, sporting a 10.1 inches WXGA capacitive touch screen with 1280 x 800 pixels of resolution, equipped with 3 megapixel rear camera with LED flash and a 2 megapixel front camera, HSPA+ connectivity up to 21Mbps, 720p HD video recording capability, 1080p HD playback, DLNA support, Bluetooth 2.1, USB 2.0, gyroscope, Wi-Fi 802.11 a/b/g/n, micro-SD slot, 3.5mm headphone jack, and SIM slot, including the Samsung Stick &amp;ndash; a Bluetooth microphone that can be carried in a pocket like a pen and sound dock with powered subwoofer.&lt;/p&gt;\r\n&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1 will come in 16GB / 32GB / 64GB verities and pre-loaded with Social Hub, Reader&amp;rsquo;s Hub, Music Hub and Samsung Mini Apps Tray &amp;ndash; which gives you access to more commonly used apps to help ease multitasking and it is capable of Adobe Flash Player 10.2, powered by 6860mAh battery that gives you 10hours of video-playback time.&amp;nbsp;&amp;auml;&amp;ouml;&lt;/p&gt;\r\n', '', '', ''),
(30, 1, 'Canon EOS 5D', '&lt;p&gt;\r\n	Canon''s press material for the EOS 5D states that it ''defines (a) new D-SLR category'', while we''re not typically too concerned with marketing talk this particular statement is clearly pretty accurate. The EOS 5D is unlike any previous digital SLR in that it combines a full-frame (35 mm sized) high resolution sensor (12.8 megapixels) with a relatively compact body (slightly larger than the EOS 20D, although in your hand it feels noticeably ''chunkier''). The EOS 5D is aimed to slot in between the EOS 20D and the EOS-1D professional digital SLR''s, an important difference when compared to the latter is that the EOS 5D doesn''t have any environmental seals. While Canon don''t specifically refer to the EOS 5D as a ''professional'' digital SLR it will have obvious appeal to professionals who want a high quality digital SLR in a body lighter than the EOS-1D. It will also no doubt appeal to current EOS 20D owners (although lets hope they''ve not bought too many EF-S lenses...) äë&lt;/p&gt;\r\n', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `product_discount`
--

CREATE TABLE IF NOT EXISTS `product_discount` (
  `product_discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`product_discount_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=441 ;

-- --------------------------------------------------------

--
-- Structure de la table `product_filter`
--

CREATE TABLE IF NOT EXISTS `product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `product_image`
--

CREATE TABLE IF NOT EXISTS `product_image` (
  `product_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2352 ;

--
-- Contenu de la table `product_image`
--

INSERT INTO `product_image` (`product_image_id`, `product_id`, `image`, `sort_order`) VALUES
(2345, 30, 'data/demo/canon_eos_5d_2.jpg', 0),
(2321, 47, 'data/demo/hp_3.jpg', 0),
(2035, 28, 'data/demo/htc_touch_hd_2.jpg', 0),
(2351, 41, 'data/demo/imac_3.jpg', 0),
(1982, 40, 'data/demo/iphone_6.jpg', 0),
(2001, 36, 'data/demo/ipod_nano_5.jpg', 0),
(2000, 36, 'data/demo/ipod_nano_4.jpg', 0),
(2005, 34, 'data/demo/ipod_shuffle_5.jpg', 0),
(2004, 34, 'data/demo/ipod_shuffle_4.jpg', 0),
(2011, 32, 'data/demo/ipod_touch_7.jpg', 0),
(2010, 32, 'data/demo/ipod_touch_6.jpg', 0),
(2009, 32, 'data/demo/ipod_touch_5.jpg', 0),
(1971, 43, 'data/demo/macbook_5.jpg', 0),
(1970, 43, 'data/demo/macbook_4.jpg', 0),
(1974, 44, 'data/demo/macbook_air_4.jpg', 0),
(1973, 44, 'data/demo/macbook_air_2.jpg', 0),
(1977, 45, 'data/demo/macbook_pro_2.jpg', 0),
(1976, 45, 'data/demo/macbook_pro_3.jpg', 0),
(1986, 31, 'data/demo/nikon_d300_3.jpg', 0),
(1985, 31, 'data/demo/nikon_d300_2.jpg', 0),
(1988, 29, 'data/demo/palm_treo_pro_3.jpg', 0),
(1995, 46, 'data/demo/sony_vaio_5.jpg', 0),
(1994, 46, 'data/demo/sony_vaio_4.jpg', 0),
(1991, 48, 'data/demo/ipod_classic_4.jpg', 0),
(1990, 48, 'data/demo/ipod_classic_3.jpg', 0),
(1981, 40, 'data/demo/iphone_2.jpg', 0),
(1980, 40, 'data/demo/iphone_5.jpg', 0),
(2344, 30, 'data/demo/canon_eos_5d_3.jpg', 0),
(2320, 47, 'data/demo/hp_2.jpg', 0),
(2034, 28, 'data/demo/htc_touch_hd_3.jpg', 0),
(2350, 41, 'data/demo/imac_2.jpg', 0),
(1979, 40, 'data/demo/iphone_3.jpg', 0),
(1978, 40, 'data/demo/iphone_4.jpg', 0),
(1989, 48, 'data/demo/ipod_classic_2.jpg', 0),
(1999, 36, 'data/demo/ipod_nano_2.jpg', 0),
(1998, 36, 'data/demo/ipod_nano_3.jpg', 0),
(2003, 34, 'data/demo/ipod_shuffle_2.jpg', 0),
(2002, 34, 'data/demo/ipod_shuffle_3.jpg', 0),
(2008, 32, 'data/demo/ipod_touch_2.jpg', 0),
(2007, 32, 'data/demo/ipod_touch_3.jpg', 0),
(2006, 32, 'data/demo/ipod_touch_4.jpg', 0),
(1969, 43, 'data/demo/macbook_2.jpg', 0),
(1968, 43, 'data/demo/macbook_3.jpg', 0),
(1972, 44, 'data/demo/macbook_air_3.jpg', 0),
(1975, 45, 'data/demo/macbook_pro_4.jpg', 0),
(1984, 31, 'data/demo/nikon_d300_4.jpg', 0),
(1983, 31, 'data/demo/nikon_d300_5.jpg', 0),
(1987, 29, 'data/demo/palm_treo_pro_2.jpg', 0),
(1993, 46, 'data/demo/sony_vaio_2.jpg', 0),
(1992, 46, 'data/demo/sony_vaio_3.jpg', 0),
(2327, 49, 'data/demo/samsung_tab_7.jpg', 0),
(2326, 49, 'data/demo/samsung_tab_6.jpg', 0),
(2325, 49, 'data/demo/samsung_tab_5.jpg', 0),
(2324, 49, 'data/demo/samsung_tab_4.jpg', 0),
(2323, 49, 'data/demo/samsung_tab_3.jpg', 0),
(2322, 49, 'data/demo/samsung_tab_2.jpg', 0);

-- --------------------------------------------------------

--
-- Structure de la table `product_option`
--

CREATE TABLE IF NOT EXISTS `product_option` (
  `product_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  PRIMARY KEY (`product_option_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=227 ;

--
-- Contenu de la table `product_option`
--

INSERT INTO `product_option` (`product_option_id`, `product_id`, `option_id`, `option_value`, `required`) VALUES
(224, 35, 11, '', 1),
(225, 47, 12, '2011-04-22', 1),
(226, 30, 5, '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `product_option_value`
--

CREATE TABLE IF NOT EXISTS `product_option_value` (
  `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL,
  PRIMARY KEY (`product_option_value_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `product_option_value`
--

INSERT INTO `product_option_value` (`product_option_value_id`, `product_option_id`, `product_id`, `option_id`, `option_value_id`, `quantity`, `subtract`, `price`, `price_prefix`, `points`, `points_prefix`, `weight`, `weight_prefix`) VALUES
(12, 224, 35, 11, 46, 0, 1, '5.0000', '+', 0, '+', '0.00000000', '+'),
(13, 224, 35, 11, 47, 10, 1, '10.0000', '+', 0, '+', '0.00000000', '+'),
(14, 224, 35, 11, 48, 15, 1, '15.0000', '+', 0, '+', '0.00000000', '+'),
(16, 226, 30, 5, 40, 5, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(15, 226, 30, 5, 39, 2, 1, '0.0000', '+', 0, '+', '0.00000000', '+');

-- --------------------------------------------------------

--
-- Structure de la table `product_profile`
--

CREATE TABLE IF NOT EXISTS `product_profile` (
  `product_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`profile_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `product_recurring`
--

CREATE TABLE IF NOT EXISTS `product_recurring` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `product_related`
--

CREATE TABLE IF NOT EXISTS `product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `product_reward`
--

CREATE TABLE IF NOT EXISTS `product_reward` (
  `product_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_reward_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=546 ;

--
-- Contenu de la table `product_reward`
--

INSERT INTO `product_reward` (`product_reward_id`, `product_id`, `customer_group_id`, `points`) VALUES
(519, 47, 1, 300),
(379, 28, 1, 400),
(329, 43, 1, 600),
(339, 29, 1, 0),
(343, 48, 1, 0),
(335, 40, 1, 0),
(539, 30, 1, 200),
(331, 44, 1, 700),
(333, 45, 1, 800),
(337, 31, 1, 0),
(425, 35, 1, 0),
(345, 33, 1, 0),
(347, 46, 1, 0),
(545, 41, 1, 0),
(351, 36, 1, 0),
(353, 34, 1, 0),
(355, 32, 1, 0),
(521, 49, 1, 1000);

-- --------------------------------------------------------

--
-- Structure de la table `product_special`
--

CREATE TABLE IF NOT EXISTS `product_special` (
  `product_special_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`product_special_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=440 ;

--
-- Contenu de la table `product_special`
--

INSERT INTO `product_special` (`product_special_id`, `product_id`, `customer_group_id`, `priority`, `price`, `date_start`, `date_end`) VALUES
(439, 30, 1, 2, '90.0000', '0000-00-00', '0000-00-00'),
(438, 30, 1, 1, '80.0000', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `product_to_category`
--

CREATE TABLE IF NOT EXISTS `product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `product_to_category`
--

INSERT INTO `product_to_category` (`product_id`, `category_id`) VALUES
(28, 20),
(28, 24),
(29, 20),
(29, 24),
(30, 20),
(30, 33),
(31, 33),
(32, 34),
(33, 20),
(33, 28),
(34, 34),
(35, 20),
(36, 34),
(40, 20),
(40, 24),
(41, 27),
(43, 18),
(43, 20),
(44, 18),
(44, 20),
(45, 18),
(46, 18),
(46, 20),
(47, 18),
(47, 20),
(48, 20),
(48, 34),
(49, 57);

-- --------------------------------------------------------

--
-- Structure de la table `product_to_download`
--

CREATE TABLE IF NOT EXISTS `product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `product_to_layout`
--

CREATE TABLE IF NOT EXISTS `product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `product_to_store`
--

CREATE TABLE IF NOT EXISTS `product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `product_to_store`
--

INSERT INTO `product_to_store` (`product_id`, `store_id`) VALUES
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(40, 0),
(41, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0);

-- --------------------------------------------------------

--
-- Structure de la table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) unsigned NOT NULL,
  `cycle` int(10) unsigned NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) unsigned NOT NULL,
  `trial_cycle` int(10) unsigned NOT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `profile_description`
--

CREATE TABLE IF NOT EXISTS `profile_description` (
  `profile_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`profile_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `return`
--

CREATE TABLE IF NOT EXISTS `return` (
  `return_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`return_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `return_action`
--

CREATE TABLE IF NOT EXISTS `return_action` (
  `return_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`return_action_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `return_action`
--

INSERT INTO `return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Structure de la table `return_history`
--

CREATE TABLE IF NOT EXISTS `return_history` (
  `return_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`return_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `return_reason`
--

CREATE TABLE IF NOT EXISTS `return_reason` (
  `return_reason_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`return_reason_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `return_reason`
--

INSERT INTO `return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Structure de la table `return_status`
--

CREATE TABLE IF NOT EXISTS `return_status` (
  `return_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`return_status_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `return_status`
--

INSERT INTO `return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Structure de la table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`review_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `group` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2602 ;

--
-- Contenu de la table `setting`
--

INSERT INTO `setting` (`setting_id`, `store_id`, `group`, `key`, `value`, `serialized`) VALUES
(1, 0, 'shipping', 'shipping_sort_order', '3', 0),
(2, 0, 'sub_total', 'sub_total_sort_order', '1', 0),
(3, 0, 'sub_total', 'sub_total_status', '1', 0),
(4, 0, 'tax', 'tax_status', '1', 0),
(5, 0, 'total', 'total_sort_order', '9', 0),
(6, 0, 'total', 'total_status', '1', 0),
(7, 0, 'tax', 'tax_sort_order', '5', 0),
(8, 0, 'free_checkout', 'free_checkout_sort_order', '1', 0),
(9, 0, 'cod', 'cod_sort_order', '5', 0),
(10, 0, 'cod', 'cod_total', '0.01', 0),
(11, 0, 'cod', 'cod_order_status_id', '1', 0),
(12, 0, 'cod', 'cod_geo_zone_id', '0', 0),
(13, 0, 'cod', 'cod_status', '1', 0),
(14, 0, 'shipping', 'shipping_status', '1', 0),
(15, 0, 'shipping', 'shipping_estimator', '1', 0),
(27, 0, 'coupon', 'coupon_sort_order', '4', 0),
(28, 0, 'coupon', 'coupon_status', '1', 0),
(34, 0, 'flat', 'flat_sort_order', '1', 0),
(35, 0, 'flat', 'flat_status', '1', 0),
(36, 0, 'flat', 'flat_geo_zone_id', '0', 0),
(37, 0, 'flat', 'flat_tax_class_id', '9', 0),
(38, 0, 'carousel', 'carousel_module', 'a:1:{i:0;a:10:{s:9:"banner_id";s:1:"8";s:5:"limit";s:1:"5";s:6:"scroll";s:1:"3";s:5:"width";s:2:"80";s:6:"height";s:2:"80";s:11:"resize_type";s:7:"default";s:9:"layout_id";s:1:"1";s:8:"position";s:14:"content_bottom";s:6:"status";s:1:"1";s:10:"sort_order";s:2:"-1";}}', 1),
(39, 0, 'featured', 'featured_product', '43,40,42,49,46,47,28', 0),
(40, 0, 'featured', 'featured_module', 'a:1:{i:0;a:8:{s:5:"limit";s:1:"6";s:11:"image_width";s:2:"80";s:12:"image_height";s:2:"80";s:11:"resize_type";s:7:"default";s:9:"layout_id";s:1:"1";s:8:"position";s:11:"content_top";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}}', 1),
(41, 0, 'flat', 'flat_cost', '5.00', 0),
(42, 0, 'credit', 'credit_sort_order', '7', 0),
(43, 0, 'credit', 'credit_status', '1', 0),
(53, 0, 'reward', 'reward_sort_order', '2', 0),
(54, 0, 'reward', 'reward_status', '1', 0),
(56, 0, 'affiliate', 'affiliate_module', 'a:1:{i:0;a:4:{s:9:"layout_id";s:2:"10";s:8:"position";s:12:"column_right";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"1";}}', 1),
(57, 0, 'category', 'category_module', 'a:2:{i:0;a:5:{s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:5:"count";s:1:"0";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"1";}i:1;a:5:{s:9:"layout_id";s:1:"2";s:8:"position";s:11:"column_left";s:5:"count";s:1:"0";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"1";}}', 1),
(60, 0, 'account', 'account_module', 'a:1:{i:0;a:4:{s:9:"layout_id";s:1:"6";s:8:"position";s:12:"column_right";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"1";}}', 1),
(94, 0, 'voucher', 'voucher_sort_order', '8', 0),
(95, 0, 'voucher', 'voucher_status', '1', 0),
(225, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai''hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(224, 0, 'config', 'config_shared', '0', 0),
(223, 0, 'config', 'config_secure', '0', 0),
(103, 0, 'free_checkout', 'free_checkout_status', '1', 0),
(104, 0, 'free_checkout', 'free_checkout_order_status_id', '1', 0),
(222, 0, 'config', 'config_fraud_status_id', '7', 0),
(221, 0, 'config', 'config_fraud_score', '', 0),
(220, 0, 'config', 'config_fraud_key', '', 0),
(2600, 0, 'banner', 'banner_module', 'a:1:{i:0;a:7:{s:9:"banner_id";s:1:"6";s:5:"width";s:3:"182";s:6:"height";s:3:"182";s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:6:"status";s:1:"0";s:10:"sort_order";s:1:"3";}}', 1),
(219, 0, 'config', 'config_fraud_detection', '0', 0),
(218, 0, 'config', 'config_alert_emails', '', 0),
(217, 0, 'config', 'config_account_mail', '0', 0),
(216, 0, 'config', 'config_alert_mail', '0', 0),
(215, 0, 'config', 'config_smtp_timeout', '5', 0),
(214, 0, 'config', 'config_smtp_port', '25', 0),
(213, 0, 'config', 'config_smtp_password', '', 0),
(212, 0, 'config', 'config_smtp_username', '', 0),
(211, 0, 'config', 'config_smtp_host', '', 0),
(210, 0, 'config', 'config_mail_parameter', '', 0),
(209, 0, 'config', 'config_mail_protocol', 'mail', 0),
(208, 0, 'config', 'config_ftp_status', '0', 0),
(207, 0, 'config', 'config_ftp_root', '', 0),
(206, 0, 'config', 'config_ftp_password', '', 0),
(205, 0, 'config', 'config_ftp_username', '', 0),
(137, 0, 'config', 'config_email', 'admin@demo.fr', 0),
(138, 0, 'config', 'config_telephone', '123456789', 0),
(139, 0, 'config', 'config_fax', '', 0),
(140, 0, 'config', 'config_title', 'Your Store', 0),
(141, 0, 'config', 'config_meta_description', 'My Store', 0),
(142, 0, 'config', 'config_template', 'oxy', 0),
(143, 0, 'config', 'config_layout_id', '4', 0),
(144, 0, 'config', 'config_country_id', '222', 0),
(145, 0, 'config', 'config_zone_id', '3563', 0),
(146, 0, 'config', 'config_language', 'en', 0),
(147, 0, 'config', 'config_admin_language', 'en', 0),
(148, 0, 'config', 'config_currency', 'USD', 0),
(149, 0, 'config', 'config_currency_auto', '1', 0),
(150, 0, 'config', 'config_length_class_id', '1', 0),
(151, 0, 'config', 'config_weight_class_id', '1', 0),
(152, 0, 'config', 'config_catalog_limit', '15', 0),
(153, 0, 'config', 'config_admin_limit', '20', 0),
(154, 0, 'config', 'config_product_count', '1', 0),
(155, 0, 'config', 'config_review_status', '1', 0),
(156, 0, 'config', 'config_download', '1', 0),
(157, 0, 'config', 'config_voucher_min', '1', 0),
(158, 0, 'config', 'config_voucher_max', '1000', 0),
(159, 0, 'config', 'config_tax', '1', 0),
(160, 0, 'config', 'config_vat', '0', 0),
(161, 0, 'config', 'config_tax_default', 'shipping', 0),
(162, 0, 'config', 'config_tax_customer', 'shipping', 0),
(163, 0, 'config', 'config_customer_online', '0', 0),
(164, 0, 'config', 'config_customer_group_id', '1', 0),
(165, 0, 'config', 'config_customer_group_display', 'a:1:{i:0;s:1:"1";}', 1),
(166, 0, 'config', 'config_customer_price', '0', 0),
(167, 0, 'config', 'config_account_id', '3', 0),
(168, 0, 'config', 'config_cart_weight', '1', 0),
(169, 0, 'config', 'config_guest_checkout', '1', 0),
(170, 0, 'config', 'config_checkout_id', '5', 0),
(171, 0, 'config', 'config_order_edit', '100', 0),
(172, 0, 'config', 'config_invoice_prefix', 'INV-2013-00', 0),
(173, 0, 'config', 'config_order_status_id', '1', 0),
(174, 0, 'config', 'config_complete_status_id', '5', 0),
(175, 0, 'config', 'config_stock_display', '0', 0),
(176, 0, 'config', 'config_stock_warning', '0', 0),
(177, 0, 'config', 'config_stock_checkout', '0', 0),
(178, 0, 'config', 'config_stock_status_id', '5', 0),
(179, 0, 'config', 'config_affiliate_id', '4', 0),
(180, 0, 'config', 'config_commission', '5', 0),
(181, 0, 'config', 'config_return_id', '0', 0),
(182, 0, 'config', 'config_return_status_id', '2', 0),
(183, 0, 'config', 'config_logo', 'data/logo.png', 0),
(184, 0, 'config', 'config_icon', 'data/cart.png', 0),
(185, 0, 'config', 'config_image_category_width', '80', 0),
(186, 0, 'config', 'config_image_category_height', '80', 0),
(187, 0, 'config', 'config_image_thumb_width', '228', 0),
(188, 0, 'config', 'config_image_thumb_height', '228', 0),
(189, 0, 'config', 'config_image_popup_width', '500', 0),
(190, 0, 'config', 'config_image_popup_height', '500', 0),
(191, 0, 'config', 'config_image_product_width', '80', 0),
(192, 0, 'config', 'config_image_product_height', '80', 0),
(193, 0, 'config', 'config_image_additional_width', '74', 0),
(194, 0, 'config', 'config_image_additional_height', '74', 0),
(195, 0, 'config', 'config_image_related_width', '80', 0),
(196, 0, 'config', 'config_image_related_height', '80', 0),
(197, 0, 'config', 'config_image_compare_width', '90', 0),
(198, 0, 'config', 'config_image_compare_height', '90', 0),
(199, 0, 'config', 'config_image_wishlist_width', '47', 0),
(200, 0, 'config', 'config_image_wishlist_height', '47', 0),
(201, 0, 'config', 'config_image_cart_width', '47', 0),
(202, 0, 'config', 'config_image_cart_height', '47', 0),
(203, 0, 'config', 'config_ftp_host', 'localhost', 0),
(204, 0, 'config', 'config_ftp_port', '21', 0),
(136, 0, 'config', 'config_address', 'Address 1', 0),
(134, 0, 'config', 'config_name', 'Your Store', 0),
(135, 0, 'config', 'config_owner', 'Your Name', 0),
(226, 0, 'config', 'config_seo_url', '0', 0),
(227, 0, 'config', 'config_file_extension_allowed', 'txt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc\r\nrtf\r\nxls\r\nppt\r\nodt\r\nods', 0),
(228, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/jpeg\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/vnd.microsoft.icon\r\nimage/tiff\r\nimage/tiff\r\nimage/svg+xml\r\nimage/svg+xml\r\napplication/zip\r\napplication/x-rar-compressed\r\napplication/x-msdownload\r\napplication/vnd.ms-cab-compressed\r\naudio/mpeg\r\nvideo/quicktime\r\nvideo/quicktime\r\napplication/pdf\r\nimage/vnd.adobe.photoshop\r\napplication/postscript\r\napplication/postscript\r\napplication/postscript\r\napplication/msword\r\napplication/rtf\r\napplication/vnd.ms-excel\r\napplication/vnd.ms-powerpoint\r\napplication/vnd.oasis.opendocument.text\r\napplication/vnd.oasis.opendocument.spreadsheet', 0),
(229, 0, 'config', 'config_maintenance', '0', 0),
(230, 0, 'config', 'config_password', '1', 0),
(231, 0, 'config', 'config_encryption', '580c787a55bf0b49867d68c23d2d8c5e', 0),
(232, 0, 'config', 'config_compression', '0', 0),
(233, 0, 'config', 'config_error_display', '1', 0),
(234, 0, 'config', 'config_error_log', '1', 0),
(235, 0, 'config', 'config_error_filename', 'error.txt', 0),
(236, 0, 'config', 'config_google_analytics', '', 0),
(237, 0, 'legal', 'hint', ' *', 0),
(238, 0, 'legal', 'text_summary', '4', 0),
(239, 0, 'legal', 'popup_width', '750', 0),
(240, 0, 'legal', 'popup_height', '650', 0),
(241, 0, 'legal', 'image_width', '80', 0),
(242, 0, 'legal', 'image_height', '80', 0),
(243, 0, 'legal', 'position_hint', 'rightBefore', 0),
(244, 0, 'legal', 'description_len', '', 0),
(245, 0, 'legal', 'button_text', 'a:1:{i:1;s:0:"";}', 1),
(246, 0, 'legal', 'price_text', 'a:1:{i:1;a:2:{s:4:"wtax";s:0:"";s:5:"wotax";s:0:"";}}', 1),
(247, 0, 'legal', 'slider_text', 'a:1:{i:1;s:0:"";}', 1),
(248, 0, 'legal', 'text_to_email', '4', 0),
(249, 0, 'legal', 'textToEmails', 'a:14:{i:7;a:1:{s:7:"text_id";s:0:"";}i:9;a:1:{s:7:"text_id";s:0:"";}i:13;a:1:{s:7:"text_id";s:0:"";}i:5;a:1:{s:7:"text_id";s:0:"";}i:8;a:1:{s:7:"text_id";s:0:"";}i:14;a:1:{s:7:"text_id";s:0:"";}i:10;a:1:{s:7:"text_id";s:0:"";}i:1;a:1:{s:7:"text_id";s:0:"";}i:15;a:1:{s:7:"text_id";s:0:"";}i:2;a:1:{s:7:"text_id";s:0:"";}i:11;a:1:{s:7:"text_id";s:0:"";}i:12;a:1:{s:7:"text_id";s:0:"";}i:3;a:1:{s:7:"text_id";s:0:"";}i:16;a:1:{s:7:"text_id";s:0:"";}}', 1),
(250, 0, 'legal', 'specials_text', 'a:1:{i:1;s:0:"";}', 1),
(251, 0, 'legal', 'xmlLine', '1234', 0),
(252, 0, 'legal', 'base_price_content_text', 'a:1:{i:1;s:0:"";}', 1),
(253, 0, 'legal', 'base_price_price_text', 'a:1:{i:1;s:0:"";}', 1),
(254, 0, 'legal', 'shopCountry', '81', 0),
(255, 0, 'legal', 'legal_supportKey', '', 0);
INSERT INTO `setting` (`setting_id`, `store_id`, `group`, `key`, `value`, `serialized`) VALUES
(256, 0, 'color_schema', 'color_schema', '[{"skin_name":"Default","oxy_layout_style":"1","oxy_search_bar_position":"center","oxy_body_bg_color":"#F6F6F6","oxy_headings_color":"#464646","oxy_body_text_color":"#464646","oxy_light_text_color":"#B6B6B6","oxy_other_links_color":"#4BB8E2","oxy_links_hover_color":"#ED5053","oxy_general_icons_style":"1","oxy_main_column_status":"1","oxy_main_column_bg_color":"#FFFFFF","oxy_main_column_border_status":"0","oxy_main_column_border_size":"1","oxy_main_column_border_style":"solid","oxy_main_column_border_color":"#CCCCCC","oxy_main_column_shadow":"1","oxy_content_column_hli_bg_color":"#F6F6F6","oxy_content_column_head_border_status":"1","oxy_content_column_head_border_size":"1","oxy_content_column_head_border_style":"solid","oxy_content_column_head_border_color":"#EAEAEA","oxy_content_column_separator_size":"1","oxy_content_column_separator_style":"solid","oxy_content_column_separator_color":"#EAEAEA","oxy_left_column_head_status":"1","oxy_left_column_head_bg_color":"#4BB8E2","oxy_left_column_head_custom":"","oxy_left_column_head_title_color":"#FFFFFF","oxy_left_column_head_border_status":"0","oxy_left_column_head_border_size":"1","oxy_left_column_head_border_style":"solid","oxy_left_column_head_border_color":"#EAEAEA","oxy_left_column_box_status":"1","oxy_left_column_box_bg_color":"#F6F6F6","oxy_left_column_box_links_color":"#464646","oxy_left_column_box_links_color_hover":"#ED5053","oxy_right_column_head_status":"1","oxy_right_column_head_bg_color":"#4BB8E2","oxy_right_column_head_custom":"","oxy_right_column_head_title_color":"#FFFFFF","oxy_right_column_head_border_status":"0","oxy_right_column_head_border_size":"1","oxy_right_column_head_border_style":"solid","oxy_right_column_head_border_color":"#EAEAEA","oxy_right_column_box_status":"1","oxy_right_column_box_bg_color":"#F6F6F6","oxy_right_column_box_links_color":"#464646","oxy_right_column_box_links_color_hover":"#ED5053","oxy_category_box_head_status":"1","oxy_category_box_head_bg_color":"#ED5053","oxy_category_box_head_custom":"","oxy_category_box_head_title_color":"#FFFFFF","oxy_category_box_head_border_status":"0","oxy_category_box_head_border_size":"1","oxy_category_box_head_border_style":"solid","oxy_category_box_head_border_color":"#EAEAEA","oxy_category_box_box_status":"1","oxy_category_box_box_bg_color":"#F6F6F6","oxy_category_box_box_bg_color_hover":"#F0F0F0","oxy_category_box_box_links_color":"#464646","oxy_category_box_box_links_color_hover":"#4BB8E2","oxy_category_box_box_separator_size":"1","oxy_category_box_box_separator_style":"solid","oxy_category_box_box_separator_color":"#FFFFFF","oxy_filter_box_head_status":"1","oxy_filter_box_head_bg_color":"#424242","oxy_filter_box_head_custom":"","oxy_filter_box_head_title_color":"#FFFFFF","oxy_filter_box_head_border_status":"0","oxy_filter_box_head_border_size":"1","oxy_filter_box_head_border_style":"solid","oxy_filter_box_head_border_color":"#EAEAEA","oxy_filter_box_box_status":"1","oxy_filter_box_box_bg_color":"#F6F6F6","oxy_filter_box_box_links_color":"#464646","oxy_filter_box_box_links_color_hover":"#ED5053","oxy_price_color":"#4BB8E2","oxy_price_old_color":"#B6B6B6","oxy_price_new_color":"#ED5053","oxy_price_tax_color":"#B6B6B6","oxy_button_border_radius":"0","oxy_button_bg_color":"#4BB8E2","oxy_button_bg_hover_color":"#ED5053","oxy_button_text_color":"#FFFFFF","oxy_button_text_hover_color":"#FFFFFF","oxy_button_exclusive_bg_color":"#ED5053","oxy_button_exclusive_bg_hover_color":"#4BB8E2","oxy_button_exclusive_text_color":"#FFFFFF","oxy_button_exclusive_text_hover_color":"#FFFFFF","oxy_button_light_bg_color":"#EFEFEF","oxy_button_light_bg_hover_color":"#4BB8E2","oxy_button_light_text_color":"#464646","oxy_button_light_text_hover_color":"#FFFFFF","oxy_button_slider_bg_color":"#EEEEEE","oxy_button_slider_bg_hover_color":"#4BB8E2","oxy_top_area_status":"0","oxy_top_area_bg_color":"#F8F8F8","oxy_top_area_mini_bg_color":"#FFFFFF","oxy_top_area_tb_bg_status":"1","oxy_top_area_tb_bg_color":"#424242","oxy_top_area_tb_top_border_status":"0","oxy_top_area_tb_top_border_color":"#4BB8E2","oxy_top_area_tb_bottom_border_status":"0","oxy_top_area_tb_bottom_border_color":"#525252","oxy_top_area_tb_text_color":"#CCCCCC","oxy_top_area_tb_link_color":"#4BB8E2","oxy_top_area_tb_link_color_hover":"#ED5053","oxy_top_area_tb_separator_color":"#525252","oxy_top_area_tb_dropdown_bg_color":"#FFFFFF","oxy_top_area_tb_dropdown_bg_color_hover":"#4BB8E2","oxy_top_area_tb_dropdown_link_color":"#464646","oxy_top_area_tb_dropdown_link_color_hover":"#FFFFFF","oxy_top_area_search_bg_color":"#F3F3F3","oxy_top_area_search_border_color":"#DFDFDF","oxy_top_area_search_border_color_hover":"#CCCCCC","oxy_top_area_search_text_color":"#464646","oxy_top_area_cart_text_color":"#B6B6B6","oxy_top_area_cart_link_color":"#4BB8E2","oxy_top_area_cart_link_color_hover":"#ED5053","oxy_top_area_cart_separator_color":"#EDEDED","oxy_top_area_cart_icon_style":"1","oxy_mm_bg_color_status":"1","oxy_mm_bg_color":"#424242","oxy_mm_separator_status":"0","oxy_mm_separator_size":"1","oxy_mm_separator_style":"solid","oxy_mm_separator_color":"#4F4F4F","oxy_mm_border_top_status":"0","oxy_mm_border_top_size":"5","oxy_mm_border_top_style":"solid","oxy_mm_border_top_color":"#EEEEEE","oxy_mm_border_bottom_status":"0","oxy_mm_border_bottom_size":"5","oxy_mm_border_bottom_style":"solid","oxy_mm_border_bottom_color":"#EEEEEE","oxy_mm1_bg_color_status":"1","oxy_mm1_bg_color":"#4BB8E2","oxy_mm1_bg_hover_color":"#ED5053","oxy_mm1_link_color":"#FFFFFF","oxy_mm1_link_hover_color":"#FFFFFF","oxy_mm2_bg_color_status":"1","oxy_mm2_bg_color":"#ED5053","oxy_mm2_bg_hover_color":"#4BB8E2","oxy_mm2_link_color":"#FFFFFF","oxy_mm2_link_hover_color":"#FFFFFF","oxy_mm3_bg_color_status":"0","oxy_mm3_bg_color":"#424242","oxy_mm3_bg_hover_color":"#4BB8E2","oxy_mm3_link_color":"#FFFFFF","oxy_mm3_link_hover_color":"#FFFFFF","oxy_mm4_bg_color_status":"0","oxy_mm4_bg_color":"#424242","oxy_mm4_bg_hover_color":"#4BB8E2","oxy_mm4_link_color":"#FFFFFF","oxy_mm4_link_hover_color":"#FFFFFF","oxy_mm6_bg_color_status":"0","oxy_mm6_bg_color":"#424242","oxy_mm6_bg_hover_color":"#4BB8E2","oxy_mm6_link_color":"#FFFFFF","oxy_mm6_link_hover_color":"#FFFFFF","oxy_mm8_bg_color_status":"0","oxy_mm8_bg_color":"#424242","oxy_mm8_bg_hover_color":"#4BB8E2","oxy_mm8_link_color":"#FFFFFF","oxy_mm8_link_hover_color":"#FFFFFF","oxy_mm5_bg_color_status":"0","oxy_mm5_bg_color":"#424242","oxy_mm5_bg_hover_color":"#4BB8E2","oxy_mm5_link_color":"#FFFFFF","oxy_mm5_link_hover_color":"#FFFFFF","oxy_mm7_bg_color_status":"0","oxy_mm7_bg_color":"#424242","oxy_mm7_bg_hover_color":"#4BB8E2","oxy_mm7_link_color":"#FFFFFF","oxy_mm7_link_hover_color":"#FFFFFF","oxy_mm_sub_bg_color":"#FFFFFF","oxy_mm_sub_bg_hover_color":"#4BB8E2","oxy_mm_sub_titles_bg_color":"#F5F5F5","oxy_mm_sub_text_color":"#464646","oxy_mm_sub_link_color":"#464646","oxy_mm_sub_link_hover_color":"#FFFFFF","oxy_mm_sub_separator_style":"solid","oxy_mm_sub_separator_color":"#F1F1F1","oxy_mm_sub_box_shadow":"1","oxy_mm_mobile_bg_color":"#424242","oxy_mm_mobile_bg_hover_color":"#ED5053","oxy_mm_mobile_text_color":"#FFFFFF","oxy_mid_prod_box_bg_hover_color":"#FFFFFF","oxy_mid_prod_box_shadow_hover":"1","oxy_mid_prod_box_sale_icon_color":"#ED5053","oxy_mid_prod_stars_color":"1","oxy_mid_prod_page_tabs_bg_color":"#424242","oxy_mid_prod_page_tabs_selected_bg_color":"#4BB8E2","oxy_mid_prod_page_tabs_text_color":"#FFFFFF","oxy_mid_prod_slider_bg_color":"#FFFFFF","oxy_mid_prod_slider_name_color":"#464646","oxy_mid_prod_slider_desc_color":"#A3A3A3","oxy_mid_prod_slider_price_color":"#ED5053","oxy_mid_prod_slider_links_color_hover":"#4BB8E2","oxy_mid_prod_slider_bottom_bar_bg_color":"#E8E8E8","oxy_mid_prod_slider_bottom_bar_bg_color_hover":"#ED5053","oxy_mid_prod_slider_bottom_bar_bg_color_active":"#4BB8E2","oxy_fp_bg_color":"#F6F6F6","oxy_fp_title_color":"#464646","oxy_fp_title_color_hover":"#ED5053","oxy_fp_subtitle_color":"#B6B6B6","oxy_f1_bg_color":"#373737","oxy_f1_titles_color":"#FFFFFF","oxy_f1_titles_border_bottom_size":"1","oxy_f1_titles_border_bottom_style":"solid","oxy_f1_titles_border_bottom_color":"#464646","oxy_f1_text_color":"#8C8C8C","oxy_f1_link_color":"#4BB8E2","oxy_f1_link_hover_color":"#FFFFFF","oxy_f1_icon_color":"#525252","oxy_f1_border_top_status":"0","oxy_f1_border_top_size":"3","oxy_f1_border_top_style":"solid","oxy_f1_border_top_color":"#000000","oxy_f2_bg_color":"#2F2F2F","oxy_f2_titles_color":"#FFFFFF","oxy_f2_titles_border_bottom_size":"1","oxy_f2_titles_border_bottom_style":"solid","oxy_f2_titles_border_bottom_color":"#464646","oxy_f2_link_color":"#8C8C8C","oxy_f2_link_hover_color":"#FFFFFF","oxy_f2_border_top_status":"0","oxy_f2_border_top_size":"1","oxy_f2_border_top_style":"solid","oxy_f2_border_top_color":"#464646","oxy_f4_bg_color":"#2F2F2F","oxy_f4_text_color":"#8C8C8C","oxy_f4_link_color":"#4BB8E2","oxy_f4_link_hover_color":"#FFFFFF","oxy_f4_border_top_status":"1","oxy_f4_border_top_size":"1","oxy_f4_border_top_style":"solid","oxy_f4_border_top_color":"#464646","oxy_f5_bg_color":"#2F2F2F","oxy_f5_text_color":"#8C8C8C","oxy_f5_link_color":"#4BB8E2","oxy_f5_link_hover_color":"#FFFFFF","oxy_f5_border_top_status":"1","oxy_f5_border_top_size":"1","oxy_f5_border_top_style":"solid","oxy_f5_border_top_color":"#464646","oxy_pattern_oxy":"","oxy_bg_image_custom":"","oxy_bg_image_position":"top center","oxy_bg_image_repeat":"no-repeat","oxy_bg_image_attachment":"scroll","oxy_pattern_oxy_mc":"","oxy_bg_image_mc_custom":"","oxy_bg_image_mc_position":"top center","oxy_bg_image_mc_repeat":"repeat","oxy_bg_image_mc_attachment":"scroll","oxy_pattern_oxy_ta":"","oxy_bg_image_ta_custom":"","oxy_bg_image_ta_position":"top center","oxy_bg_image_ta_repeat":"repeat","oxy_bg_image_ta_attachment":"scroll","oxy_pattern_oxy_mm":"","oxy_bg_image_mm_custom":"","oxy_bg_image_mm_repeat":"repeat","oxy_pattern_oxy_f1":"","oxy_bg_image_f1_custom":"","oxy_bg_image_f1_position":"top center","oxy_bg_image_f1_repeat":"repeat","oxy_pattern_oxy_f2":"","oxy_bg_image_f2_custom":"","oxy_bg_image_f2_position":"top center","oxy_bg_image_f2_repeat":"repeat","oxy_pattern_oxy_f4":"","oxy_bg_image_f4_custom":"","oxy_bg_image_f4_position":"top center","oxy_bg_image_f4_repeat":"repeat","oxy_pattern_oxy_f5":"","oxy_bg_image_f5_custom":"","oxy_bg_image_f5_position":"top center","oxy_bg_image_f5_repeat":"repeat","oxy_body_font":"","oxy_title_font":"","oxy_title_font_weight":"normal","oxy_title_font_uppercase":"1","oxy_price_font":"","oxy_price_font_weight":"normal","oxy_button_font":"","oxy_button_font_weight":"normal","oxy_button_font_uppercase":"1","oxy_search_font":"","oxy_search_font_weight":"normal","oxy_search_font_size":"13","oxy_search_font_uppercase":"1","oxy_cart_font":"","oxy_cart_font_weight":"normal","oxy_cart_font_size":"17","oxy_cart_font_uppercase":"0","oxy_main_menu_font":"","oxy_mm_font_weight":"normal","oxy_mm_font_size":"16","oxy_mm_font_uppercase":"1","oxy_fp_fb1_bg_color":"#4BB8E2","oxy_fp_fb1_bg_color_hover":"#4BB8E2","oxy_fp_fb2_bg_color":"#ED5053","oxy_fp_fb2_bg_color_hover":"#ED5053","oxy_fp_fb3_bg_color":"#FFCA00","oxy_fp_fb3_bg_color_hover":"#FFCA00","oxy_fp_fb4_bg_color":"#9AE24B","oxy_fp_fb4_bg_color_hover":"#9AE24B","oxy_video_box_bg":"#ED5053","oxy_custom_box_bg":"#FFCA00"},{"skin_name":"Fashion","oxy_layout_style":"2","oxy_search_bar_position":"center","oxy_body_bg_color":"#F6F6F6","oxy_headings_color":"#000000","oxy_body_text_color":"#000000","oxy_light_text_color":"#B6B6B6","oxy_other_links_color":"#A98D54","oxy_links_hover_color":"#A98D54","oxy_general_icons_style":"1","oxy_main_column_status":"1","oxy_main_column_bg_color":"#FFFFFF","oxy_main_column_border_status":"0","oxy_main_column_border_size":"1","oxy_main_column_border_style":"solid","oxy_main_column_border_color":"#CCCCCC","oxy_main_column_shadow":"1","oxy_content_column_hli_bg_color":"#F6F6F6","oxy_content_column_head_border_status":"1","oxy_content_column_head_border_size":"1","oxy_content_column_head_border_style":"solid","oxy_content_column_head_border_color":"#EAEAEA","oxy_content_column_separator_size":"1","oxy_content_column_separator_style":"solid","oxy_content_column_separator_color":"#EAEAEA","oxy_left_column_head_status":"1","oxy_left_column_head_bg_color":"#000000","oxy_left_column_head_custom":"","oxy_left_column_head_title_color":"#FFFFFF","oxy_left_column_head_border_status":"0","oxy_left_column_head_border_size":"1","oxy_left_column_head_border_style":"solid","oxy_left_column_head_border_color":"#EAEAEA","oxy_left_column_box_status":"1","oxy_left_column_box_bg_color":"#F6F6F6","oxy_left_column_box_links_color":"#000000","oxy_left_column_box_links_color_hover":"#A98D54","oxy_right_column_head_status":"1","oxy_right_column_head_bg_color":"#000000","oxy_right_column_head_custom":"","oxy_right_column_head_title_color":"#FFFFFF","oxy_right_column_head_border_status":"0","oxy_right_column_head_border_size":"1","oxy_right_column_head_border_style":"solid","oxy_right_column_head_border_color":"#EAEAEA","oxy_right_column_box_status":"1","oxy_right_column_box_bg_color":"#F6F6F6","oxy_right_column_box_links_color":"#000000","oxy_right_column_box_links_color_hover":"#A98D54","oxy_category_box_head_status":"1","oxy_category_box_head_bg_color":"#A98D54","oxy_category_box_head_custom":"","oxy_category_box_head_title_color":"#FFFFFF","oxy_category_box_head_border_status":"0","oxy_category_box_head_border_size":"1","oxy_category_box_head_border_style":"solid","oxy_category_box_head_border_color":"#EAEAEA","oxy_category_box_box_status":"1","oxy_category_box_box_bg_color":"#F6F6F6","oxy_category_box_box_bg_color_hover":"#F0F0F0","oxy_category_box_box_links_color":"#000000","oxy_category_box_box_links_color_hover":"#A98D54","oxy_category_box_box_separator_size":"1","oxy_category_box_box_separator_style":"solid","oxy_category_box_box_separator_color":"#FFFFFF","oxy_filter_box_head_status":"1","oxy_filter_box_head_bg_color":"#000000","oxy_filter_box_head_custom":"","oxy_filter_box_head_title_color":"#FFFFFF","oxy_filter_box_head_border_status":"1","oxy_filter_box_head_border_size":"1","oxy_filter_box_head_border_style":"solid","oxy_filter_box_head_border_color":"#191919","oxy_filter_box_box_status":"1","oxy_filter_box_box_bg_color":"#000000","oxy_filter_box_box_links_color":"#FFFFFF","oxy_filter_box_box_links_color_hover":"#BDA97F","oxy_price_color":"#5C5C5C","oxy_price_old_color":"#B6B6B6","oxy_price_new_color":"#A98D54","oxy_price_tax_color":"#B6B6B6","oxy_button_border_radius":"0","oxy_button_bg_color":"#000000","oxy_button_bg_hover_color":"#A98D54","oxy_button_text_color":"#FFFFFF","oxy_button_text_hover_color":"#FFFFFF","oxy_button_exclusive_bg_color":"#A98D54","oxy_button_exclusive_bg_hover_color":"#000000","oxy_button_exclusive_text_color":"#FFFFFF","oxy_button_exclusive_text_hover_color":"#FFFFFF","oxy_button_light_bg_color":"#EFEFEF","oxy_button_light_bg_hover_color":"#A98D54","oxy_button_light_text_color":"#000000","oxy_button_light_text_hover_color":"#FFFFFF","oxy_button_slider_bg_color":"#EEEEEE","oxy_button_slider_bg_hover_color":"#A98D54","oxy_top_area_status":"1","oxy_top_area_bg_color":"#000000","oxy_top_area_mini_bg_color":"#000000","oxy_top_area_tb_bg_status":"1","oxy_top_area_tb_bg_color":"#1F1F1F","oxy_top_area_tb_top_border_status":"0","oxy_top_area_tb_top_border_color":"#BDA97F","oxy_top_area_tb_bottom_border_status":"0","oxy_top_area_tb_bottom_border_color":"#525252","oxy_top_area_tb_text_color":"#FFFFFF","oxy_top_area_tb_link_color":"#BDA97F","oxy_top_area_tb_link_color_hover":"#FFFFFF","oxy_top_area_tb_separator_color":"#333333","oxy_top_area_tb_dropdown_bg_color":"#FFFFFF","oxy_top_area_tb_dropdown_bg_color_hover":"#BDA97F","oxy_top_area_tb_dropdown_link_color":"#464646","oxy_top_area_tb_dropdown_link_color_hover":"#FFFFFF","oxy_top_area_search_bg_color":"#000000","oxy_top_area_search_border_color":"#333333","oxy_top_area_search_border_color_hover":"#CCCCCC","oxy_top_area_search_text_color":"#FFFFFF","oxy_top_area_cart_text_color":"#FFFFFF","oxy_top_area_cart_link_color":"#BDA97F","oxy_top_area_cart_link_color_hover":"#FFFFFF","oxy_top_area_cart_separator_color":"#333333","oxy_top_area_cart_icon_style":"1","oxy_mm_bg_color_status":"1","oxy_mm_bg_color":"#1F1F1F","oxy_mm_separator_status":"0","oxy_mm_separator_size":"1","oxy_mm_separator_style":"solid","oxy_mm_separator_color":"#4F4F4F","oxy_mm_border_top_status":"0","oxy_mm_border_top_size":"1","oxy_mm_border_top_style":"solid","oxy_mm_border_top_color":"#EEEEEE","oxy_mm_border_bottom_status":"0","oxy_mm_border_bottom_size":"1","oxy_mm_border_bottom_style":"solid","oxy_mm_border_bottom_color":"#EEEEEE","oxy_mm1_bg_color_status":"0","oxy_mm1_bg_color":"#424242","oxy_mm1_bg_hover_color":"#A98D54","oxy_mm1_link_color":"#FFFFFF","oxy_mm1_link_hover_color":"#FFFFFF","oxy_mm2_bg_color_status":"0","oxy_mm2_bg_color":"#424242","oxy_mm2_bg_hover_color":"#A98D54","oxy_mm2_link_color":"#FFFFFF","oxy_mm2_link_hover_color":"#FFFFFF","oxy_mm3_bg_color_status":"0","oxy_mm3_bg_color":"#424242","oxy_mm3_bg_hover_color":"#A98D54","oxy_mm3_link_color":"#FFFFFF","oxy_mm3_link_hover_color":"#FFFFFF","oxy_mm4_bg_color_status":"0","oxy_mm4_bg_color":"#424242","oxy_mm4_bg_hover_color":"#A98D54","oxy_mm4_link_color":"#FFFFFF","oxy_mm4_link_hover_color":"#FFFFFF","oxy_mm6_bg_color_status":"0","oxy_mm6_bg_color":"#424242","oxy_mm6_bg_hover_color":"#A98D54","oxy_mm6_link_color":"#FFFFFF","oxy_mm6_link_hover_color":"#FFFFFF","oxy_mm8_bg_color_status":"0","oxy_mm8_bg_color":"#424242","oxy_mm8_bg_hover_color":"#A98D54","oxy_mm8_link_color":"#FFFFFF","oxy_mm8_link_hover_color":"#FFFFFF","oxy_mm5_bg_color_status":"0","oxy_mm5_bg_color":"#424242","oxy_mm5_bg_hover_color":"#A98D54","oxy_mm5_link_color":"#FFFFFF","oxy_mm5_link_hover_color":"#FFFFFF","oxy_mm7_bg_color_status":"0","oxy_mm7_bg_color":"#424242","oxy_mm7_bg_hover_color":"#A98D54","oxy_mm7_link_color":"#FFFFFF","oxy_mm7_link_hover_color":"#FFFFFF","oxy_mm_sub_bg_color":"#FFFFFF","oxy_mm_sub_bg_hover_color":"#A98D54","oxy_mm_sub_titles_bg_color":"#F5F5F5","oxy_mm_sub_text_color":"#000000","oxy_mm_sub_link_color":"#000000","oxy_mm_sub_link_hover_color":"#FFFFFF","oxy_mm_sub_separator_style":"solid","oxy_mm_sub_separator_color":"#F1F1F1","oxy_mm_sub_box_shadow":"1","oxy_mm_mobile_bg_color":"#424242","oxy_mm_mobile_bg_hover_color":"#A98D54","oxy_mm_mobile_text_color":"#FFFFFF","oxy_mid_prod_box_bg_hover_color":"#FFFFFF","oxy_mid_prod_box_shadow_hover":"1","oxy_mid_prod_box_sale_icon_color":"#A98D54","oxy_mid_prod_stars_color":"8","oxy_mid_prod_page_tabs_bg_color":"#000000","oxy_mid_prod_page_tabs_selected_bg_color":"#A98D54","oxy_mid_prod_page_tabs_text_color":"#FFFFFF","oxy_mid_prod_slider_bg_color":"#FFFFFF","oxy_mid_prod_slider_name_color":"#000000","oxy_mid_prod_slider_desc_color":"#A3A3A3","oxy_mid_prod_slider_price_color":"#A98D54","oxy_mid_prod_slider_links_color_hover":"#000000","oxy_mid_prod_slider_bottom_bar_bg_color":"#E8E8E8","oxy_mid_prod_slider_bottom_bar_bg_color_hover":"#A98D54","oxy_mid_prod_slider_bottom_bar_bg_color_active":"#000000","oxy_fp_bg_color":"#F6F6F6","oxy_fp_title_color":"#000000","oxy_fp_title_color_hover":"#A98D54","oxy_fp_subtitle_color":"#B6B6B6","oxy_f1_bg_color":"#000000","oxy_f1_titles_color":"#FFFFFF","oxy_f1_titles_border_bottom_size":"2","oxy_f1_titles_border_bottom_style":"solid","oxy_f1_titles_border_bottom_color":"#464646","oxy_f1_text_color":"#FFFFFF","oxy_f1_link_color":"#BDA97F","oxy_f1_link_hover_color":"#FFFFFF","oxy_f1_icon_color":"#464646","oxy_f1_border_top_status":"0","oxy_f1_border_top_size":"3","oxy_f1_border_top_style":"solid","oxy_f1_border_top_color":"#000000","oxy_f2_bg_color":"#000000","oxy_f2_titles_color":"#FFFFFF","oxy_f2_titles_border_bottom_size":"2","oxy_f2_titles_border_bottom_style":"solid","oxy_f2_titles_border_bottom_color":"#464646","oxy_f2_link_color":"#FFFFFF","oxy_f2_link_hover_color":"#BDA97F","oxy_f2_border_top_status":"1","oxy_f2_border_top_size":"1","oxy_f2_border_top_style":"solid","oxy_f2_border_top_color":"#292929","oxy_f4_bg_color":"#000000","oxy_f4_text_color":"#FFFFFF","oxy_f4_link_color":"#BDA97F","oxy_f4_link_hover_color":"#FFFFFF","oxy_f4_border_top_status":"1","oxy_f4_border_top_size":"1","oxy_f4_border_top_style":"solid","oxy_f4_border_top_color":"#292929","oxy_f5_bg_color":"#000000","oxy_f5_text_color":"#8C8C8C","oxy_f5_link_color":"#BDA97F","oxy_f5_link_hover_color":"#FFFFFF","oxy_f5_border_top_status":"1","oxy_f5_border_top_size":"1","oxy_f5_border_top_style":"solid","oxy_f5_border_top_color":"#292929","oxy_pattern_oxy":"","oxy_bg_image_custom":"","oxy_bg_image_position":"top center","oxy_bg_image_repeat":"no-repeat","oxy_bg_image_attachment":"scroll","oxy_pattern_oxy_mc":"","oxy_bg_image_mc_custom":"","oxy_bg_image_mc_position":"top center","oxy_bg_image_mc_repeat":"repeat","oxy_bg_image_mc_attachment":"scroll","oxy_pattern_oxy_ta":"","oxy_bg_image_ta_custom":"","oxy_bg_image_ta_position":"top center","oxy_bg_image_ta_repeat":"repeat","oxy_bg_image_ta_attachment":"scroll","oxy_pattern_oxy_mm":"","oxy_bg_image_mm_custom":"","oxy_bg_image_mm_repeat":"repeat","oxy_pattern_oxy_f1":"","oxy_bg_image_f1_custom":"","oxy_bg_image_f1_position":"top center","oxy_bg_image_f1_repeat":"repeat","oxy_pattern_oxy_f2":"","oxy_bg_image_f2_custom":"","oxy_bg_image_f2_position":"top center","oxy_bg_image_f2_repeat":"repeat","oxy_pattern_oxy_f4":"","oxy_bg_image_f4_custom":"","oxy_bg_image_f4_position":"top center","oxy_bg_image_f4_repeat":"repeat","oxy_pattern_oxy_f5":"","oxy_bg_image_f5_custom":"","oxy_bg_image_f5_position":"top center","oxy_bg_image_f5_repeat":"repeat","oxy_body_font":"","oxy_title_font":"","oxy_title_font_weight":"normal","oxy_title_font_uppercase":"1","oxy_price_font":"","oxy_price_font_weight":"normal","oxy_button_font":"","oxy_button_font_weight":"normal","oxy_button_font_uppercase":"1","oxy_search_font":"","oxy_search_font_weight":"normal","oxy_search_font_size":"13","oxy_search_font_uppercase":"1","oxy_cart_font":"","oxy_cart_font_weight":"normal","oxy_cart_font_size":"18","oxy_cart_font_uppercase":"0","oxy_main_menu_font":"","oxy_mm_font_weight":"normal","oxy_mm_font_size":"16","oxy_mm_font_uppercase":"1","oxy_fp_fb1_bg_color":"#D1D1D1","oxy_fp_fb1_bg_color_hover":"#A98D54","oxy_fp_fb2_bg_color":"#D1D1D1","oxy_fp_fb2_bg_color_hover":"#A98D54","oxy_fp_fb3_bg_color":"#D1D1D1","oxy_fp_fb3_bg_color_hover":"#A98D54","oxy_fp_fb4_bg_color":"#D1D1D1","oxy_fp_fb4_bg_color_hover":"#A98D54","oxy_video_box_bg":"#A98D54","oxy_custom_box_bg":"#464646"},{"skin_name":"Food","oxy_layout_style":"2","oxy_search_bar_position":"center","oxy_body_bg_color":"#52453C","oxy_headings_color":"#F3F1DC","oxy_body_text_color":"#F3F1DC","oxy_light_text_color":"#D4D0B0","oxy_other_links_color":"#FFD666","oxy_links_hover_color":"#FFFFFF","oxy_general_icons_style":"2","oxy_main_column_status":"0","oxy_main_column_bg_color":"#FFFFFF","oxy_main_column_border_status":"0","oxy_main_column_border_size":"1","oxy_main_column_border_style":"solid","oxy_main_column_border_color":"#CCCCCC","oxy_main_column_shadow":"0","oxy_content_column_hli_bg_color":"#3E342D","oxy_content_column_head_border_status":"1","oxy_content_column_head_border_size":"1","oxy_content_column_head_border_style":"dotted","oxy_content_column_head_border_color":"#A49572","oxy_content_column_separator_size":"1","oxy_content_column_separator_style":"dotted","oxy_content_column_separator_color":"#A49572","oxy_left_column_head_status":"1","oxy_left_column_head_bg_color":"#FFD666","oxy_left_column_head_custom":"","oxy_left_column_head_title_color":"#52453C","oxy_left_column_head_border_status":"0","oxy_left_column_head_border_size":"1","oxy_left_column_head_border_style":"solid","oxy_left_column_head_border_color":"#EAEAEA","oxy_left_column_box_status":"1","oxy_left_column_box_bg_color":"#3E342D","oxy_left_column_box_links_color":"#F3F1DC","oxy_left_column_box_links_color_hover":"#FFD666","oxy_right_column_head_status":"1","oxy_right_column_head_bg_color":"#FFD666","oxy_right_column_head_custom":"","oxy_right_column_head_title_color":"#52453C","oxy_right_column_head_border_status":"0","oxy_right_column_head_border_size":"1","oxy_right_column_head_border_style":"solid","oxy_right_column_head_border_color":"#EAEAEA","oxy_right_column_box_status":"1","oxy_right_column_box_bg_color":"#3E342D","oxy_right_column_box_links_color":"#F3F1DC","oxy_right_column_box_links_color_hover":"#FFD666","oxy_category_box_head_status":"1","oxy_category_box_head_bg_color":"#DB6440","oxy_category_box_head_custom":"","oxy_category_box_head_title_color":"#FFFFFF","oxy_category_box_head_border_status":"0","oxy_category_box_head_border_size":"1","oxy_category_box_head_border_style":"solid","oxy_category_box_head_border_color":"#EAEAEA","oxy_category_box_box_status":"1","oxy_category_box_box_bg_color":"#3E342D","oxy_category_box_box_bg_color_hover":"#5E4F44","oxy_category_box_box_links_color":"#F3F1DC","oxy_category_box_box_links_color_hover":"#FFD666","oxy_category_box_box_separator_size":"1","oxy_category_box_box_separator_style":"dotted","oxy_category_box_box_separator_color":"#5E4F44","oxy_filter_box_head_status":"1","oxy_filter_box_head_bg_color":"#3E342D","oxy_filter_box_head_custom":"","oxy_filter_box_head_title_color":"#FFD666","oxy_filter_box_head_border_status":"1","oxy_filter_box_head_border_size":"1","oxy_filter_box_head_border_style":"dotted","oxy_filter_box_head_border_color":"#5E4F44","oxy_filter_box_box_status":"1","oxy_filter_box_box_bg_color":"#3E342D","oxy_filter_box_box_links_color":"#F3F1DC","oxy_filter_box_box_links_color_hover":"#FFD666","oxy_price_color":"#FFFFFF","oxy_price_old_color":"#F3F1DC","oxy_price_new_color":"#FFD666","oxy_price_tax_color":"#F3F1DC","oxy_button_border_radius":"0","oxy_button_bg_color":"#FFD666","oxy_button_bg_hover_color":"#DB6440","oxy_button_text_color":"#3E342D","oxy_button_text_hover_color":"#FFFFFF","oxy_button_exclusive_bg_color":"#DB6440","oxy_button_exclusive_bg_hover_color":"#FFD666","oxy_button_exclusive_text_color":"#FFFFFF","oxy_button_exclusive_text_hover_color":"#3E342D","oxy_button_light_bg_color":"#DB6440","oxy_button_light_bg_hover_color":"#FFD666","oxy_button_light_text_color":"#FFFFFF","oxy_button_light_text_hover_color":"#3E342D","oxy_button_slider_bg_color":"#F3F1DC","oxy_button_slider_bg_hover_color":"#DB6440","oxy_top_area_status":"1","oxy_top_area_bg_color":"#3E342D","oxy_top_area_mini_bg_color":"#463D37","oxy_top_area_tb_bg_status":"1","oxy_top_area_tb_bg_color":"#3E342D","oxy_top_area_tb_top_border_status":"0","oxy_top_area_tb_top_border_color":"#FFD666","oxy_top_area_tb_bottom_border_status":"0","oxy_top_area_tb_bottom_border_color":"#525252","oxy_top_area_tb_text_color":"#F3F1DC","oxy_top_area_tb_link_color":"#FFD666","oxy_top_area_tb_link_color_hover":"#DB6440","oxy_top_area_tb_separator_color":"#594B42","oxy_top_area_tb_dropdown_bg_color":"#3E342D","oxy_top_area_tb_dropdown_bg_color_hover":"#DB6440","oxy_top_area_tb_dropdown_link_color":"#F3F1DC","oxy_top_area_tb_dropdown_link_color_hover":"#F3F1DC","oxy_top_area_search_bg_color":"#F3F1DC","oxy_top_area_search_border_color":"#3E342D","oxy_top_area_search_border_color_hover":"#3E342D","oxy_top_area_search_text_color":"#3E342D","oxy_top_area_cart_text_color":"#FFFFFF","oxy_top_area_cart_link_color":"#FFD666","oxy_top_area_cart_link_color_hover":"#FFD666","oxy_top_area_cart_separator_color":"#615248","oxy_top_area_cart_icon_style":"1","oxy_mm_bg_color_status":"1","oxy_mm_bg_color":"#3E342D","oxy_mm_separator_status":"0","oxy_mm_separator_size":"1","oxy_mm_separator_style":"dotted","oxy_mm_separator_color":"#594B42","oxy_mm_border_top_status":"0","oxy_mm_border_top_size":"1","oxy_mm_border_top_style":"dotted","oxy_mm_border_top_color":"#5E4F44","oxy_mm_border_bottom_status":"0","oxy_mm_border_bottom_size":"1","oxy_mm_border_bottom_style":"solid","oxy_mm_border_bottom_color":"#EEEEEE","oxy_mm1_bg_color_status":"1","oxy_mm1_bg_color":"#DB6440","oxy_mm1_bg_hover_color":"#3E342D","oxy_mm1_link_color":"#FFFFFF","oxy_mm1_link_hover_color":"#FFD666","oxy_mm2_bg_color_status":"0","oxy_mm2_bg_color":"#ED5053","oxy_mm2_bg_hover_color":"#3E342D","oxy_mm2_link_color":"#F3F1DC","oxy_mm2_link_hover_color":"#FFD666","oxy_mm3_bg_color_status":"0","oxy_mm3_bg_color":"#424242","oxy_mm3_bg_hover_color":"#3E342D","oxy_mm3_link_color":"#F3F1DC","oxy_mm3_link_hover_color":"#FFD666","oxy_mm4_bg_color_status":"0","oxy_mm4_bg_color":"#424242","oxy_mm4_bg_hover_color":"#3E342D","oxy_mm4_link_color":"#F3F1DC","oxy_mm4_link_hover_color":"#FFD666","oxy_mm6_bg_color_status":"0","oxy_mm6_bg_color":"#424242","oxy_mm6_bg_hover_color":"#3E342D","oxy_mm6_link_color":"#F3F1DC","oxy_mm6_link_hover_color":"#FFD666","oxy_mm8_bg_color_status":"0","oxy_mm8_bg_color":"#424242","oxy_mm8_bg_hover_color":"#3E342D","oxy_mm8_link_color":"#F3F1DC","oxy_mm8_link_hover_color":"#FFD666","oxy_mm5_bg_color_status":"0","oxy_mm5_bg_color":"#424242","oxy_mm5_bg_hover_color":"#3E342D","oxy_mm5_link_color":"#F3F1DC","oxy_mm5_link_hover_color":"#FFD666","oxy_mm7_bg_color_status":"0","oxy_mm7_bg_color":"#424242","oxy_mm7_bg_hover_color":"#3E342D","oxy_mm7_link_color":"#F3F1DC","oxy_mm7_link_hover_color":"#FFD666","oxy_mm_sub_bg_color":"#3E342D","oxy_mm_sub_bg_hover_color":"#DB6440","oxy_mm_sub_titles_bg_color":"#52453C","oxy_mm_sub_text_color":"#F3F1DC","oxy_mm_sub_link_color":"#F3F1DC","oxy_mm_sub_link_hover_color":"#FFFFFF","oxy_mm_sub_separator_style":"dotted","oxy_mm_sub_separator_color":"#6E5C51","oxy_mm_sub_box_shadow":"1","oxy_mm_mobile_bg_color":"#3E342D","oxy_mm_mobile_bg_hover_color":"#DB6440","oxy_mm_mobile_text_color":"#F3F1DC","oxy_mid_prod_box_bg_hover_color":"#978667","oxy_mid_prod_box_shadow_hover":"1","oxy_mid_prod_box_sale_icon_color":"#DB6440","oxy_mid_prod_stars_color":"3","oxy_mid_prod_page_tabs_bg_color":"#3E342D","oxy_mid_prod_page_tabs_selected_bg_color":"#DB6440","oxy_mid_prod_page_tabs_text_color":"#FFFFFF","oxy_mid_prod_slider_bg_color":"#FFFFFF","oxy_mid_prod_slider_name_color":"#52453C","oxy_mid_prod_slider_desc_color":"#786557","oxy_mid_prod_slider_price_color":"#DB6440","oxy_mid_prod_slider_links_color_hover":"#DB6440","oxy_mid_prod_slider_bottom_bar_bg_color":"#3E342D","oxy_mid_prod_slider_bottom_bar_bg_color_hover":"#FFD666","oxy_mid_prod_slider_bottom_bar_bg_color_active":"#DB6440","oxy_fp_bg_color":"#52453C","oxy_fp_title_color":"#FFFFFF","oxy_fp_title_color_hover":"#FFD666","oxy_fp_subtitle_color":"#F3F1DC","oxy_f1_bg_color":"#26201C","oxy_f1_titles_color":"#FFD666","oxy_f1_titles_border_bottom_size":"1","oxy_f1_titles_border_bottom_style":"dotted","oxy_f1_titles_border_bottom_color":"#A49572","oxy_f1_text_color":"#F3F1DC","oxy_f1_link_color":"#FFD666","oxy_f1_link_hover_color":"#FFFFFF","oxy_f1_icon_color":"#52453C","oxy_f1_border_top_status":"0","oxy_f1_border_top_size":"1","oxy_f1_border_top_style":"solid","oxy_f1_border_top_color":"#000000","oxy_f2_bg_color":"#26201C","oxy_f2_titles_color":"#FFD666","oxy_f2_titles_border_bottom_size":"1","oxy_f2_titles_border_bottom_style":"dotted","oxy_f2_titles_border_bottom_color":"#A49572","oxy_f2_link_color":"#FFFFFF","oxy_f2_link_hover_color":"#FFD666","oxy_f2_border_top_status":"1","oxy_f2_border_top_size":"1","oxy_f2_border_top_style":"dotted","oxy_f2_border_top_color":"#A49572","oxy_f4_bg_color":"#52453C","oxy_f4_text_color":"#F3F1DC","oxy_f4_link_color":"#FFD666","oxy_f4_link_hover_color":"#FFFFFF","oxy_f4_border_top_status":"1","oxy_f4_border_top_size":"1","oxy_f4_border_top_style":"dotted","oxy_f4_border_top_color":"#A49572","oxy_f5_bg_color":"#52453C","oxy_f5_text_color":"#F3F1DC","oxy_f5_link_color":"#FFD666","oxy_f5_link_hover_color":"#FFFFFF","oxy_f5_border_top_status":"1","oxy_f5_border_top_size":"1","oxy_f5_border_top_style":"dotted","oxy_f5_border_top_color":"#A49572","oxy_pattern_oxy":"69","oxy_bg_image_custom":"","oxy_bg_image_position":"top center","oxy_bg_image_repeat":"repeat","oxy_bg_image_attachment":"scroll","oxy_pattern_oxy_mc":"","oxy_bg_image_mc_custom":"","oxy_bg_image_mc_position":"top center","oxy_bg_image_mc_repeat":"repeat","oxy_bg_image_mc_attachment":"scroll","oxy_pattern_oxy_ta":"69","oxy_bg_image_ta_custom":"","oxy_bg_image_ta_position":"top center","oxy_bg_image_ta_repeat":"repeat","oxy_bg_image_ta_attachment":"scroll","oxy_pattern_oxy_mm":"","oxy_bg_image_mm_custom":"","oxy_bg_image_mm_repeat":"repeat","oxy_pattern_oxy_f1":"69","oxy_bg_image_f1_custom":"","oxy_bg_image_f1_position":"top center","oxy_bg_image_f1_repeat":"repeat","oxy_pattern_oxy_f2":"69","oxy_bg_image_f2_custom":"","oxy_bg_image_f2_position":"top center","oxy_bg_image_f2_repeat":"repeat","oxy_pattern_oxy_f4":"69","oxy_bg_image_f4_custom":"","oxy_bg_image_f4_position":"top center","oxy_bg_image_f4_repeat":"repeat","oxy_pattern_oxy_f5":"69","oxy_bg_image_f5_custom":"","oxy_bg_image_f5_position":"top center","oxy_bg_image_f5_repeat":"repeat","oxy_body_font":"Donegal+One","oxy_title_font":"Donegal+One","oxy_title_font_weight":"normal","oxy_title_font_uppercase":"1","oxy_price_font":"Donegal+One","oxy_price_font_weight":"normal","oxy_button_font":"Donegal+One","oxy_button_font_weight":"normal","oxy_button_font_uppercase":"1","oxy_search_font":"","oxy_search_font_weight":"normal","oxy_search_font_size":"13","oxy_search_font_uppercase":"1","oxy_cart_font":"","oxy_cart_font_weight":"normal","oxy_cart_font_size":"16","oxy_cart_font_uppercase":"0","oxy_main_menu_font":"Donegal+One","oxy_mm_font_weight":"normal","oxy_mm_font_size":"15","oxy_mm_font_uppercase":"1","oxy_fp_fb1_bg_color":"#A49572","oxy_fp_fb1_bg_color_hover":"#3E342D","oxy_fp_fb2_bg_color":"#A49572","oxy_fp_fb2_bg_color_hover":"#3E342D","oxy_fp_fb3_bg_color":"#A49572","oxy_fp_fb3_bg_color_hover":"#3E342D","oxy_fp_fb4_bg_color":"#A49572","oxy_fp_fb4_bg_color_hover":"#3E342D","oxy_video_box_bg":"#DB6440","oxy_custom_box_bg":"#FFD666"},{"skin_name":"Kids","oxy_layout_style":"2","oxy_search_bar_position":"right","oxy_body_bg_color":"#F8F2BE","oxy_headings_color":"#4C665A","oxy_body_text_color":"#4C665A","oxy_light_text_color":"#AFB494","oxy_other_links_color":"#8FAD50","oxy_links_hover_color":"#DE665E","oxy_general_icons_style":"2","oxy_main_column_status":"0","oxy_main_column_bg_color":"#FFFFFF","oxy_main_column_border_status":"0","oxy_main_column_border_size":"1","oxy_main_column_border_style":"solid","oxy_main_column_border_color":"#CCCCCC","oxy_main_column_shadow":"0","oxy_content_column_hli_bg_color":"#FFFFFF","oxy_content_column_head_border_status":"1","oxy_content_column_head_border_size":"1","oxy_content_column_head_border_style":"dotted","oxy_content_column_head_border_color":"#E1DBB2","oxy_content_column_separator_size":"1","oxy_content_column_separator_style":"dotted","oxy_content_column_separator_color":"#E1DBB2","oxy_left_column_head_status":"1","oxy_left_column_head_bg_color":"#9DBF58","oxy_left_column_head_custom":"","oxy_left_column_head_title_color":"#FFFFFF","oxy_left_column_head_border_status":"0","oxy_left_column_head_border_size":"1","oxy_left_column_head_border_style":"solid","oxy_left_column_head_border_color":"#EAEAEA","oxy_left_column_box_status":"1","oxy_left_column_box_bg_color":"#FFFFFF","oxy_left_column_box_links_color":"#4C665A","oxy_left_column_box_links_color_hover":"#DE665E","oxy_right_column_head_status":"1","oxy_right_column_head_bg_color":"#9DBF58","oxy_right_column_head_custom":"","oxy_right_column_head_title_color":"#FFFFFF","oxy_right_column_head_border_status":"0","oxy_right_column_head_border_size":"1","oxy_right_column_head_border_style":"solid","oxy_right_column_head_border_color":"#EAEAEA","oxy_right_column_box_status":"1","oxy_right_column_box_bg_color":"#FFFFFF","oxy_right_column_box_links_color":"#4C665A","oxy_right_column_box_links_color_hover":"#DE665E","oxy_category_box_head_status":"1","oxy_category_box_head_bg_color":"#86B9D4","oxy_category_box_head_custom":"","oxy_category_box_head_title_color":"#FFFFFF","oxy_category_box_head_border_status":"0","oxy_category_box_head_border_size":"1","oxy_category_box_head_border_style":"solid","oxy_category_box_head_border_color":"#EAEAEA","oxy_category_box_box_status":"1","oxy_category_box_box_bg_color":"#FFFFFF","oxy_category_box_box_bg_color_hover":"#FFFBD7","oxy_category_box_box_links_color":"#4C665A","oxy_category_box_box_links_color_hover":"#4C665A","oxy_category_box_box_separator_size":"1","oxy_category_box_box_separator_style":"solid","oxy_category_box_box_separator_color":"#F2F0E9","oxy_filter_box_head_status":"1","oxy_filter_box_head_bg_color":"#B97EC5","oxy_filter_box_head_custom":"","oxy_filter_box_head_title_color":"#FFFFFF","oxy_filter_box_head_border_status":"0","oxy_filter_box_head_border_size":"1","oxy_filter_box_head_border_style":"solid","oxy_filter_box_head_border_color":"#EAEAEA","oxy_filter_box_box_status":"1","oxy_filter_box_box_bg_color":"#FFFFFF","oxy_filter_box_box_links_color":"#4C665A","oxy_filter_box_box_links_color_hover":"#DE665E","oxy_price_color":"#8FAD50","oxy_price_old_color":"#AFB494","oxy_price_new_color":"#DE665E","oxy_price_tax_color":"#AFB494","oxy_button_border_radius":"0","oxy_button_bg_color":"#86B9D4","oxy_button_bg_hover_color":"#DE665E","oxy_button_text_color":"#FFFFFF","oxy_button_text_hover_color":"#FFFFFF","oxy_button_exclusive_bg_color":"#DE665E","oxy_button_exclusive_bg_hover_color":"#86B9D4","oxy_button_exclusive_text_color":"#FFFFFF","oxy_button_exclusive_text_hover_color":"#FFFFFF","oxy_button_light_bg_color":"#86B9D4","oxy_button_light_bg_hover_color":"#DE665E","oxy_button_light_text_color":"#FFFFFF","oxy_button_light_text_hover_color":"#FFFFFF","oxy_button_slider_bg_color":"#FFFFFF","oxy_button_slider_bg_hover_color":"#DE665E","oxy_top_area_status":"0","oxy_top_area_bg_color":"#F8F8F8","oxy_top_area_mini_bg_color":"#CFECDF","oxy_top_area_tb_bg_status":"1","oxy_top_area_tb_bg_color":"#86B9D4","oxy_top_area_tb_top_border_status":"0","oxy_top_area_tb_top_border_color":"#4BB8E2","oxy_top_area_tb_bottom_border_status":"0","oxy_top_area_tb_bottom_border_color":"#525252","oxy_top_area_tb_text_color":"#FFFFFF","oxy_top_area_tb_link_color":"#F8F2BE","oxy_top_area_tb_link_color_hover":"#DE665E","oxy_top_area_tb_separator_color":"#79AFCC","oxy_top_area_tb_dropdown_bg_color":"#86B9D4","oxy_top_area_tb_dropdown_bg_color_hover":"#9DBF58","oxy_top_area_tb_dropdown_link_color":"#FFFFFF","oxy_top_area_tb_dropdown_link_color_hover":"#FFFFFF","oxy_top_area_search_bg_color":"#FFFFFF","oxy_top_area_search_border_color":"#FFFFFF","oxy_top_area_search_border_color_hover":"#FFFFFF","oxy_top_area_search_text_color":"#4C665A","oxy_top_area_cart_text_color":"#FFFFFF","oxy_top_area_cart_link_color":"#5A7B82","oxy_top_area_cart_link_color_hover":"#6BA2BF","oxy_top_area_cart_separator_color":"#C4D7D4","oxy_top_area_cart_icon_style":"1","oxy_mm_bg_color_status":"1","oxy_mm_bg_color":"#E86B62","oxy_mm_separator_status":"0","oxy_mm_separator_size":"1","oxy_mm_separator_style":"solid","oxy_mm_separator_color":"#FC8E86","oxy_mm_border_top_status":"0","oxy_mm_border_top_size":"1","oxy_mm_border_top_style":"solid","oxy_mm_border_top_color":"#EEEEEE","oxy_mm_border_bottom_status":"0","oxy_mm_border_bottom_size":"1","oxy_mm_border_bottom_style":"solid","oxy_mm_border_bottom_color":"#EEEEEE","oxy_mm1_bg_color_status":"1","oxy_mm1_bg_color":"#9DBF58","oxy_mm1_bg_hover_color":"#86B9D4","oxy_mm1_link_color":"#FFFFFF","oxy_mm1_link_hover_color":"#FFFFFF","oxy_mm2_bg_color_status":"1","oxy_mm2_bg_color":"#EAD44F","oxy_mm2_bg_hover_color":"#86B9D4","oxy_mm2_link_color":"#FFFFFF","oxy_mm2_link_hover_color":"#FFFFFF","oxy_mm3_bg_color_status":"0","oxy_mm3_bg_color":"#424242","oxy_mm3_bg_hover_color":"#86B9D4","oxy_mm3_link_color":"#FFFFFF","oxy_mm3_link_hover_color":"#FFFFFF","oxy_mm4_bg_color_status":"0","oxy_mm4_bg_color":"#424242","oxy_mm4_bg_hover_color":"#86B9D4","oxy_mm4_link_color":"#FFFFFF","oxy_mm4_link_hover_color":"#FFFFFF","oxy_mm6_bg_color_status":"0","oxy_mm6_bg_color":"#424242","oxy_mm6_bg_hover_color":"#86B9D4","oxy_mm6_link_color":"#FFFFFF","oxy_mm6_link_hover_color":"#FFFFFF","oxy_mm8_bg_color_status":"0","oxy_mm8_bg_color":"#424242","oxy_mm8_bg_hover_color":"#86B9D4","oxy_mm8_link_color":"#FFFFFF","oxy_mm8_link_hover_color":"#FFFFFF","oxy_mm5_bg_color_status":"0","oxy_mm5_bg_color":"#424242","oxy_mm5_bg_hover_color":"#86B9D4","oxy_mm5_link_color":"#FFFFFF","oxy_mm5_link_hover_color":"#FFFFFF","oxy_mm7_bg_color_status":"0","oxy_mm7_bg_color":"#424242","oxy_mm7_bg_hover_color":"#86B9D4","oxy_mm7_link_color":"#FFFFFF","oxy_mm7_link_hover_color":"#FFFFFF","oxy_mm_sub_bg_color":"#FFFFFF","oxy_mm_sub_bg_hover_color":"#86B9D4","oxy_mm_sub_titles_bg_color":"#F5F5F5","oxy_mm_sub_text_color":"#71827A","oxy_mm_sub_link_color":"#71827A","oxy_mm_sub_link_hover_color":"#FFFFFF","oxy_mm_sub_separator_style":"solid","oxy_mm_sub_separator_color":"#F1F1F1","oxy_mm_sub_box_shadow":"1","oxy_mm_mobile_bg_color":"#E86B62","oxy_mm_mobile_bg_hover_color":"#9DBF58","oxy_mm_mobile_text_color":"#FFFFFF","oxy_mid_prod_box_bg_hover_color":"#FFFFFF","oxy_mid_prod_box_shadow_hover":"1","oxy_mid_prod_box_sale_icon_color":"#DE665E","oxy_mid_prod_stars_color":"3","oxy_mid_prod_page_tabs_bg_color":"#9DBF58","oxy_mid_prod_page_tabs_selected_bg_color":"#DE665E","oxy_mid_prod_page_tabs_text_color":"#FFFFFF","oxy_mid_prod_slider_bg_color":"#FFFFFF","oxy_mid_prod_slider_name_color":"#4C665A","oxy_mid_prod_slider_desc_color":"#AFB494","oxy_mid_prod_slider_price_color":"#DE665E","oxy_mid_prod_slider_links_color_hover":"#DE665E","oxy_mid_prod_slider_bottom_bar_bg_color":"#E8E8E8","oxy_mid_prod_slider_bottom_bar_bg_color_hover":"#DE665E","oxy_mid_prod_slider_bottom_bar_bg_color_active":"#86B9D4","oxy_fp_bg_color":"#5C4D43","oxy_fp_title_color":"#FFFFFF","oxy_fp_title_color_hover":"#FFFFFF","oxy_fp_subtitle_color":"#BAAFA8","oxy_f1_bg_color":"#453932","oxy_f1_titles_color":"#CE9B59","oxy_f1_titles_border_bottom_size":"1","oxy_f1_titles_border_bottom_style":"dotted","oxy_f1_titles_border_bottom_color":"#615147","oxy_f1_text_color":"#BAAFA8","oxy_f1_link_color":"#CE9B59","oxy_f1_link_hover_color":"#FFFFFF","oxy_f1_icon_color":"#5C4D43","oxy_f1_border_top_status":"0","oxy_f1_border_top_size":"1","oxy_f1_border_top_style":"solid","oxy_f1_border_top_color":"#000000","oxy_f2_bg_color":"#453932","oxy_f2_titles_color":"#CE9B59","oxy_f2_titles_border_bottom_size":"1","oxy_f2_titles_border_bottom_style":"dotted","oxy_f2_titles_border_bottom_color":"#615147","oxy_f2_link_color":"#BAAFA8","oxy_f2_link_hover_color":"#FFFFFF","oxy_f2_border_top_status":"1","oxy_f2_border_top_size":"1","oxy_f2_border_top_style":"dotted","oxy_f2_border_top_color":"#615147","oxy_f4_bg_color":"#453932","oxy_f4_text_color":"#BAAFA8","oxy_f4_link_color":"#CE9B59","oxy_f4_link_hover_color":"#FFFFFF","oxy_f4_border_top_status":"1","oxy_f4_border_top_size":"1","oxy_f4_border_top_style":"dotted","oxy_f4_border_top_color":"#615147","oxy_f5_bg_color":"#453932","oxy_f5_text_color":"#BAAFA8","oxy_f5_link_color":"#CE9B59","oxy_f5_link_hover_color":"#FFFFFF","oxy_f5_border_top_status":"1","oxy_f5_border_top_size":"1","oxy_f5_border_top_style":"dotted","oxy_f5_border_top_color":"#615147","oxy_pattern_oxy":"","oxy_bg_image_custom":"data\\/bg_kids.png","oxy_bg_image_position":"top center","oxy_bg_image_repeat":"repeat-x","oxy_bg_image_attachment":"scroll","oxy_pattern_oxy_mc":"","oxy_bg_image_mc_custom":"","oxy_bg_image_mc_position":"top center","oxy_bg_image_mc_repeat":"repeat","oxy_bg_image_mc_attachment":"scroll","oxy_pattern_oxy_ta":"","oxy_bg_image_ta_custom":"data\\/header_kids.png","oxy_bg_image_ta_position":"top center","oxy_bg_image_ta_repeat":"no-repeat","oxy_bg_image_ta_attachment":"scroll","oxy_pattern_oxy_mm":"","oxy_bg_image_mm_custom":"","oxy_bg_image_mm_repeat":"repeat","oxy_pattern_oxy_f1":"","oxy_bg_image_f1_custom":"","oxy_bg_image_f1_position":"top center","oxy_bg_image_f1_repeat":"repeat","oxy_pattern_oxy_f2":"","oxy_bg_image_f2_custom":"","oxy_bg_image_f2_position":"top center","oxy_bg_image_f2_repeat":"repeat","oxy_pattern_oxy_f4":"","oxy_bg_image_f4_custom":"","oxy_bg_image_f4_position":"top center","oxy_bg_image_f4_repeat":"repeat","oxy_pattern_oxy_f5":"","oxy_bg_image_f5_custom":"","oxy_bg_image_f5_position":"top center","oxy_bg_image_f5_repeat":"repeat","oxy_body_font":"","oxy_title_font":"","oxy_title_font_weight":"normal","oxy_title_font_uppercase":"1","oxy_price_font":"","oxy_price_font_weight":"normal","oxy_button_font":"","oxy_button_font_weight":"normal","oxy_button_font_uppercase":"1","oxy_search_font":"","oxy_search_font_weight":"normal","oxy_search_font_size":"13","oxy_search_font_uppercase":"1","oxy_cart_font":"","oxy_cart_font_weight":"normal","oxy_cart_font_size":"18","oxy_cart_font_uppercase":"0","oxy_main_menu_font":"","oxy_mm_font_weight":"normal","oxy_mm_font_size":"16","oxy_mm_font_uppercase":"1","oxy_fp_fb1_bg_color":"#9DBF58","oxy_fp_fb1_bg_color_hover":"#9DBF58","oxy_fp_fb2_bg_color":"#DE665E","oxy_fp_fb2_bg_color_hover":"#DE665E","oxy_fp_fb3_bg_color":"#86B9D4","oxy_fp_fb3_bg_color_hover":"#86B9D4","oxy_fp_fb4_bg_color":"#EAD44F","oxy_fp_fb4_bg_color_hover":"#EAD44F","oxy_video_box_bg":"#E86B62","oxy_custom_box_bg":"#EAD44F"},{"skin_name":"Sport","oxy_layout_style":"2","oxy_search_bar_position":"center","oxy_body_bg_color":"#0F0F0F","oxy_headings_color":"#FFFFFF","oxy_body_text_color":"#FFFFFF","oxy_light_text_color":"#9E9E9E","oxy_other_links_color":"#FEFB00","oxy_links_hover_color":"#53A3DF","oxy_general_icons_style":"2","oxy_main_column_status":"0","oxy_main_column_bg_color":"#FFFFFF","oxy_main_column_border_status":"0","oxy_main_column_border_size":"1","oxy_main_column_border_style":"solid","oxy_main_column_border_color":"#CCCCCC","oxy_main_column_shadow":"0","oxy_content_column_hli_bg_color":"#464646","oxy_content_column_head_border_status":"1","oxy_content_column_head_border_size":"1","oxy_content_column_head_border_style":"solid","oxy_content_column_head_border_color":"#464646","oxy_content_column_separator_size":"1","oxy_content_column_separator_style":"solid","oxy_content_column_separator_color":"#575757","oxy_left_column_head_status":"1","oxy_left_column_head_bg_color":"#383838","oxy_left_column_head_custom":"","oxy_left_column_head_title_color":"#FFFFFF","oxy_left_column_head_border_status":"0","oxy_left_column_head_border_size":"1","oxy_left_column_head_border_style":"solid","oxy_left_column_head_border_color":"#EAEAEA","oxy_left_column_box_status":"1","oxy_left_column_box_bg_color":"#424242","oxy_left_column_box_links_color":"#FFFFFF","oxy_left_column_box_links_color_hover":"#FEFB00","oxy_right_column_head_status":"1","oxy_right_column_head_bg_color":"#383838","oxy_right_column_head_custom":"","oxy_right_column_head_title_color":"#FFFFFF","oxy_right_column_head_border_status":"0","oxy_right_column_head_border_size":"1","oxy_right_column_head_border_style":"solid","oxy_right_column_head_border_color":"#EAEAEA","oxy_right_column_box_status":"1","oxy_right_column_box_bg_color":"#424242","oxy_right_column_box_links_color":"#FFFFFF","oxy_right_column_box_links_color_hover":"#FEFB00","oxy_category_box_head_status":"1","oxy_category_box_head_bg_color":"#FEFB00","oxy_category_box_head_custom":"","oxy_category_box_head_title_color":"#000000","oxy_category_box_head_border_status":"0","oxy_category_box_head_border_size":"1","oxy_category_box_head_border_style":"solid","oxy_category_box_head_border_color":"#EAEAEA","oxy_category_box_box_status":"1","oxy_category_box_box_bg_color":"#424242","oxy_category_box_box_bg_color_hover":"#525252","oxy_category_box_box_links_color":"#FFFFFF","oxy_category_box_box_links_color_hover":"#FEFB00","oxy_category_box_box_separator_size":"1","oxy_category_box_box_separator_style":"solid","oxy_category_box_box_separator_color":"#525252","oxy_filter_box_head_status":"1","oxy_filter_box_head_bg_color":"#53A3DF","oxy_filter_box_head_custom":"","oxy_filter_box_head_title_color":"#FFFFFF","oxy_filter_box_head_border_status":"0","oxy_filter_box_head_border_size":"1","oxy_filter_box_head_border_style":"solid","oxy_filter_box_head_border_color":"#EAEAEA","oxy_filter_box_box_status":"1","oxy_filter_box_box_bg_color":"#424242","oxy_filter_box_box_links_color":"#FFFFFF","oxy_filter_box_box_links_color_hover":"#FEFB00","oxy_price_color":"#FFFFFF","oxy_price_old_color":"#B6B6B6","oxy_price_new_color":"#FEFB00","oxy_price_tax_color":"#B6B6B6","oxy_button_border_radius":"0","oxy_button_bg_color":"#FEFB00","oxy_button_bg_hover_color":"#53A3DF","oxy_button_text_color":"#000000","oxy_button_text_hover_color":"#FFFFFF","oxy_button_exclusive_bg_color":"#FEFB00","oxy_button_exclusive_bg_hover_color":"#53A3DF","oxy_button_exclusive_text_color":"#000000","oxy_button_exclusive_text_hover_color":"#FFFFFF","oxy_button_light_bg_color":"#FEFB00","oxy_button_light_bg_hover_color":"#53A3DF","oxy_button_light_text_color":"#000000","oxy_button_light_text_hover_color":"#FFFFFF","oxy_button_slider_bg_color":"#FFFFFF","oxy_button_slider_bg_hover_color":"#53A3DF","oxy_top_area_status":"0","oxy_top_area_bg_color":"#F8F8F8","oxy_top_area_mini_bg_color":"#000000","oxy_top_area_tb_bg_status":"1","oxy_top_area_tb_bg_color":"#424242","oxy_top_area_tb_top_border_status":"0","oxy_top_area_tb_top_border_color":"#FEFB00","oxy_top_area_tb_bottom_border_status":"0","oxy_top_area_tb_bottom_border_color":"#525252","oxy_top_area_tb_text_color":"#CCCCCC","oxy_top_area_tb_link_color":"#FFFFFF","oxy_top_area_tb_link_color_hover":"#FEFB00","oxy_top_area_tb_separator_color":"#525252","oxy_top_area_tb_dropdown_bg_color":"#404040","oxy_top_area_tb_dropdown_bg_color_hover":"#525252","oxy_top_area_tb_dropdown_link_color":"#FFFFFF","oxy_top_area_tb_dropdown_link_color_hover":"#FEFB00","oxy_top_area_search_bg_color":"#F3F3F3","oxy_top_area_search_border_color":"#DFDFDF","oxy_top_area_search_border_color_hover":"#CCCCCC","oxy_top_area_search_text_color":"#464646","oxy_top_area_cart_text_color":"#FFFFFF","oxy_top_area_cart_link_color":"#FEFB00","oxy_top_area_cart_link_color_hover":"#FEFB00","oxy_top_area_cart_separator_color":"#464646","oxy_top_area_cart_icon_style":"5","oxy_mm_bg_color_status":"1","oxy_mm_bg_color":"#3D3D3D","oxy_mm_separator_status":"0","oxy_mm_separator_size":"1","oxy_mm_separator_style":"solid","oxy_mm_separator_color":"#575757","oxy_mm_border_top_status":"0","oxy_mm_border_top_size":"3","oxy_mm_border_top_style":"solid","oxy_mm_border_top_color":"#FEFB00","oxy_mm_border_bottom_status":"1","oxy_mm_border_bottom_size":"3","oxy_mm_border_bottom_style":"solid","oxy_mm_border_bottom_color":"#FEFB00","oxy_mm1_bg_color_status":"1","oxy_mm1_bg_color":"#FEFB00","oxy_mm1_bg_hover_color":"#FEFB00","oxy_mm1_link_color":"#000000","oxy_mm1_link_hover_color":"#000000","oxy_mm2_bg_color_status":"0","oxy_mm2_bg_color":"#1C1B1C","oxy_mm2_bg_hover_color":"#FEFB00","oxy_mm2_link_color":"#FFFFFF","oxy_mm2_link_hover_color":"#000000","oxy_mm3_bg_color_status":"0","oxy_mm3_bg_color":"#1C1B1C","oxy_mm3_bg_hover_color":"#FEFB00","oxy_mm3_link_color":"#FFFFFF","oxy_mm3_link_hover_color":"#000000","oxy_mm4_bg_color_status":"0","oxy_mm4_bg_color":"#1C1B1C","oxy_mm4_bg_hover_color":"#FEFB00","oxy_mm4_link_color":"#FFFFFF","oxy_mm4_link_hover_color":"#000000","oxy_mm6_bg_color_status":"0","oxy_mm6_bg_color":"#1C1B1C","oxy_mm6_bg_hover_color":"#FEFB00","oxy_mm6_link_color":"#FFFFFF","oxy_mm6_link_hover_color":"#000000","oxy_mm8_bg_color_status":"0","oxy_mm8_bg_color":"#1C1B1C","oxy_mm8_bg_hover_color":"#FEFB00","oxy_mm8_link_color":"#FFFFFF","oxy_mm8_link_hover_color":"#000000","oxy_mm5_bg_color_status":"0","oxy_mm5_bg_color":"#1C1B1C","oxy_mm5_bg_hover_color":"#FEFB00","oxy_mm5_link_color":"#FFFFFF","oxy_mm5_link_hover_color":"#000000","oxy_mm7_bg_color_status":"0","oxy_mm7_bg_color":"#1C1B1C","oxy_mm7_bg_hover_color":"#FEFB00","oxy_mm7_link_color":"#FFFFFF","oxy_mm7_link_hover_color":"#000000","oxy_mm_sub_bg_color":"#404040","oxy_mm_sub_bg_hover_color":"#525252","oxy_mm_sub_titles_bg_color":"#525252","oxy_mm_sub_text_color":"#FFFFFF","oxy_mm_sub_link_color":"#FFFFFF","oxy_mm_sub_link_hover_color":"#FEFB00","oxy_mm_sub_separator_style":"solid","oxy_mm_sub_separator_color":"#525252","oxy_mm_sub_box_shadow":"1","oxy_mm_mobile_bg_color":"#424242","oxy_mm_mobile_bg_hover_color":"#53A3DF","oxy_mm_mobile_text_color":"#FFFFFF","oxy_mid_prod_box_bg_hover_color":"#424242","oxy_mid_prod_box_shadow_hover":"0","oxy_mid_prod_box_sale_icon_color":"#53A3DF","oxy_mid_prod_stars_color":"1","oxy_mid_prod_page_tabs_bg_color":"#424242","oxy_mid_prod_page_tabs_selected_bg_color":"#53A3DF","oxy_mid_prod_page_tabs_text_color":"#FFFFFF","oxy_mid_prod_slider_bg_color":"#FFFFFF","oxy_mid_prod_slider_name_color":"#464646","oxy_mid_prod_slider_desc_color":"#A3A3A3","oxy_mid_prod_slider_price_color":"#A3A3A3","oxy_mid_prod_slider_links_color_hover":"#53A3DF","oxy_mid_prod_slider_bottom_bar_bg_color":"#464646","oxy_mid_prod_slider_bottom_bar_bg_color_hover":"#A3A3A3","oxy_mid_prod_slider_bottom_bar_bg_color_active":"#53A3DF","oxy_fp_bg_color":"#F6F6F6","oxy_fp_title_color":"#464646","oxy_fp_title_color_hover":"#53A3DF","oxy_fp_subtitle_color":"#B6B6B6","oxy_f1_bg_color":"#1C1B1C","oxy_f1_titles_color":"#FFFFFF","oxy_f1_titles_border_bottom_size":"1","oxy_f1_titles_border_bottom_style":"solid","oxy_f1_titles_border_bottom_color":"#464646","oxy_f1_text_color":"#8C8C8C","oxy_f1_link_color":"#FEFB00","oxy_f1_link_hover_color":"#FFFFFF","oxy_f1_icon_color":"#525252","oxy_f1_border_top_status":"0","oxy_f1_border_top_size":"3","oxy_f1_border_top_style":"solid","oxy_f1_border_top_color":"#000000","oxy_f2_bg_color":"#1C1B1C","oxy_f2_titles_color":"#FFFFFF","oxy_f2_titles_border_bottom_size":"1","oxy_f2_titles_border_bottom_style":"solid","oxy_f2_titles_border_bottom_color":"#464646","oxy_f2_link_color":"#8C8C8C","oxy_f2_link_hover_color":"#FFFFFF","oxy_f2_border_top_status":"1","oxy_f2_border_top_size":"1","oxy_f2_border_top_style":"solid","oxy_f2_border_top_color":"#464646","oxy_f4_bg_color":"#2F2F2F","oxy_f4_text_color":"#8C8C8C","oxy_f4_link_color":"#FFFFFF","oxy_f4_link_hover_color":"#FEFB00","oxy_f4_border_top_status":"1","oxy_f4_border_top_size":"1","oxy_f4_border_top_style":"solid","oxy_f4_border_top_color":"#464646","oxy_f5_bg_color":"#2F2F2F","oxy_f5_text_color":"#8C8C8C","oxy_f5_link_color":"#FEFB00","oxy_f5_link_hover_color":"#FFFFFF","oxy_f5_border_top_status":"1","oxy_f5_border_top_size":"1","oxy_f5_border_top_style":"solid","oxy_f5_border_top_color":"#464646","oxy_pattern_oxy":"","oxy_bg_image_custom":"","oxy_bg_image_position":"top center","oxy_bg_image_repeat":"no-repeat","oxy_bg_image_attachment":"scroll","oxy_pattern_oxy_mc":"153","oxy_bg_image_mc_custom":"","oxy_bg_image_mc_position":"top center","oxy_bg_image_mc_repeat":"repeat","oxy_bg_image_mc_attachment":"scroll","oxy_pattern_oxy_ta":"","oxy_bg_image_ta_custom":"","oxy_bg_image_ta_position":"top center","oxy_bg_image_ta_repeat":"repeat","oxy_bg_image_ta_attachment":"scroll","oxy_pattern_oxy_mm":"","oxy_bg_image_mm_custom":"","oxy_bg_image_mm_repeat":"repeat","oxy_pattern_oxy_f1":"153","oxy_bg_image_f1_custom":"","oxy_bg_image_f1_position":"top center","oxy_bg_image_f1_repeat":"repeat","oxy_pattern_oxy_f2":"153","oxy_bg_image_f2_custom":"","oxy_bg_image_f2_position":"top center","oxy_bg_image_f2_repeat":"repeat","oxy_pattern_oxy_f4":"","oxy_bg_image_f4_custom":"","oxy_bg_image_f4_position":"top center","oxy_bg_image_f4_repeat":"repeat","oxy_pattern_oxy_f5":"","oxy_bg_image_f5_custom":"","oxy_bg_image_f5_position":"top center","oxy_bg_image_f5_repeat":"repeat","oxy_body_font":"Armata","oxy_title_font":"Armata","oxy_title_font_weight":"normal","oxy_title_font_uppercase":"1","oxy_price_font":"Armata","oxy_price_font_weight":"normal","oxy_button_font":"Armata","oxy_button_font_weight":"normal","oxy_button_font_uppercase":"1","oxy_search_font":"Armata","oxy_search_font_weight":"normal","oxy_search_font_size":"13","oxy_search_font_uppercase":"1","oxy_cart_font":"Armata","oxy_cart_font_weight":"normal","oxy_cart_font_size":"15","oxy_cart_font_uppercase":"1","oxy_main_menu_font":"Armata","oxy_mm_font_weight":"normal","oxy_mm_font_size":"15","oxy_mm_font_uppercase":"1","oxy_fp_fb1_bg_color":"#53A3DF","oxy_fp_fb1_bg_color_hover":"#FEFB00","oxy_fp_fb2_bg_color":"#53A3DF","oxy_fp_fb2_bg_color_hover":"#FEFB00","oxy_fp_fb3_bg_color":"#53A3DF","oxy_fp_fb3_bg_color_hover":"#FEFB00","oxy_fp_fb4_bg_color":"#53A3DF","oxy_fp_fb4_bg_color_hover":"#FEFB00","oxy_video_box_bg":"#3D3D3D","oxy_custom_box_bg":"#3D3D3D"}]', 0);
INSERT INTO `setting` (`setting_id`, `store_id`, `group`, `key`, `value`, `serialized`) VALUES
(2558, 0, 'oxy', 'oxy_i_c_4_3_status', '1', 0),
(2557, 0, 'oxy', 'oxy_i_c_4_2_status', '1', 0),
(2556, 0, 'oxy', 'oxy_i_c_4_1_status', '1', 0),
(2555, 0, 'oxy', 'oxy_information_column_4_status', '1', 0),
(2554, 0, 'oxy', 'oxy_i_c_3_4_status', '1', 0),
(2553, 0, 'oxy', 'oxy_i_c_3_3_status', '1', 0),
(2552, 0, 'oxy', 'oxy_i_c_3_2_status', '1', 0),
(2551, 0, 'oxy', 'oxy_i_c_3_1_status', '1', 0),
(2549, 0, 'oxy', 'oxy_i_c_2_3_status', '1', 0),
(2550, 0, 'oxy', 'oxy_information_column_3_status', '1', 0),
(2548, 0, 'oxy', 'oxy_i_c_2_2_status', '1', 0),
(2547, 0, 'oxy', 'oxy_i_c_2_1_status', '1', 0),
(2546, 0, 'oxy', 'oxy_information_column_2_status', '1', 0),
(2545, 0, 'oxy', 'oxy_information_column_1_status', '1', 0),
(2542, 0, 'oxy', 'oxy_contact_location21', '', 0),
(2543, 0, 'oxy', 'oxy_contact_hours1', '', 0),
(2544, 0, 'oxy', 'oxy_information_block_status', '1', 0),
(2539, 0, 'oxy', 'oxy_contact_skype11', '', 0),
(2541, 0, 'oxy', 'oxy_contact_location11', '', 0),
(2540, 0, 'oxy', 'oxy_contact_skype21', '', 0),
(2538, 0, 'oxy', 'oxy_contact_email21', '', 0),
(2537, 0, 'oxy', 'oxy_contact_email11', '', 0),
(2536, 0, 'oxy', 'oxy_contact_fax21', '', 0),
(2535, 0, 'oxy', 'oxy_contact_fax11', '', 0),
(2534, 0, 'oxy', 'oxy_contact_sphone21', '', 0),
(2533, 0, 'oxy', 'oxy_contact_sphone11', '', 0),
(2532, 0, 'oxy', 'oxy_contact_mphone21', '', 0),
(2529, 0, 'oxy', 'oxy_contacts_status', '0', 0),
(2531, 0, 'oxy', 'oxy_contact_mphone11', '', 0),
(2530, 0, 'oxy', 'oxy_contacts_title1', '', 0),
(2528, 0, 'oxy', 'oxy_reddit', '', 0),
(2527, 0, 'oxy', 'oxy_tumblr', '', 0),
(2526, 0, 'oxy', 'oxy_myspace', '', 0),
(2524, 0, 'oxy', 'oxy_forrst', '', 0),
(2525, 0, 'oxy', 'oxy_bing', '', 0),
(2522, 0, 'oxy', 'oxy_behance', '', 0),
(2523, 0, 'oxy', 'oxy_skype', '', 0),
(2521, 0, 'oxy', 'oxy_instagram', '', 0),
(2519, 0, 'oxy', 'oxy_youtube', '', 0),
(2520, 0, 'oxy', 'oxy_dribbble', '', 0),
(2518, 0, 'oxy', 'oxy_linkedin', '', 0),
(2517, 0, 'oxy', 'oxy_flickr', '', 0),
(2516, 0, 'oxy', 'oxy_vimeo', '', 0),
(2515, 0, 'oxy', 'oxy_pinterest', '', 0),
(2513, 0, 'oxy', 'oxy_googleplus', '', 0),
(2514, 0, 'oxy', 'oxy_rss', '', 0),
(2512, 0, 'oxy', 'oxy_twitter', '', 0),
(2511, 0, 'oxy', 'oxy_facebook', '', 0),
(2510, 0, 'oxy', 'oxy_follow_us_title1', '', 0),
(2507, 0, 'oxy', 'oxy_custom_2_title1', '', 0),
(2508, 0, 'oxy', 'oxy_custom_2_content1', '', 0),
(2509, 0, 'oxy', 'oxy_follow_us_status', '0', 0),
(2506, 0, 'oxy', 'oxy_custom_2_status', '0', 0),
(2505, 0, 'oxy', 'oxy_custom_1_content1', '', 0),
(2504, 0, 'oxy', 'oxy_custom_1_title1', '', 0),
(2503, 0, 'oxy', 'oxy_custom_1_status', '0', 0),
(2502, 0, 'oxy', 'oxy_fp_fb4_content1', '', 0),
(2500, 0, 'oxy', 'oxy_fp_fb4_title1', '', 0),
(2501, 0, 'oxy', 'oxy_fp_fb4_subtitle1', '', 0),
(2499, 0, 'oxy', 'oxy_fp_fb4_bg_color_hover', '#3E342D', 0),
(2498, 0, 'oxy', 'oxy_fp_fb4_bg_color', '#A49572', 0),
(2497, 0, 'oxy', 'oxy_fp_fb4_icon', '', 0),
(2496, 0, 'oxy', 'oxy_fp_fb3_content1', '', 0),
(2494, 0, 'oxy', 'oxy_fp_fb3_title1', '', 0),
(2495, 0, 'oxy', 'oxy_fp_fb3_subtitle1', '', 0),
(2493, 0, 'oxy', 'oxy_fp_fb3_bg_color_hover', '#3E342D', 0),
(2492, 0, 'oxy', 'oxy_fp_fb3_bg_color', '#A49572', 0),
(2489, 0, 'oxy', 'oxy_fp_fb2_subtitle1', '', 0),
(2490, 0, 'oxy', 'oxy_fp_fb2_content1', '', 0),
(2491, 0, 'oxy', 'oxy_fp_fb3_icon', '', 0),
(2488, 0, 'oxy', 'oxy_fp_fb2_title1', '', 0),
(2487, 0, 'oxy', 'oxy_fp_fb2_bg_color_hover', '#3E342D', 0),
(2485, 0, 'oxy', 'oxy_fp_fb2_icon', '', 0),
(2486, 0, 'oxy', 'oxy_fp_fb2_bg_color', '#A49572', 0),
(2484, 0, 'oxy', 'oxy_fp_fb1_content1', '', 0),
(2483, 0, 'oxy', 'oxy_fp_fb1_subtitle1', '', 0),
(2482, 0, 'oxy', 'oxy_fp_fb1_title1', '', 0),
(2481, 0, 'oxy', 'oxy_fp_fb1_bg_color_hover', '#3E342D', 0),
(2480, 0, 'oxy', 'oxy_fp_fb1_bg_color', '#A49572', 0),
(2479, 0, 'oxy', 'oxy_fp_fb1_icon', '', 0),
(2478, 0, 'oxy', 'oxy_mm_font_uppercase', '1', 0),
(2477, 0, 'oxy', 'oxy_mm_font_size', '15', 0),
(2476, 0, 'oxy', 'oxy_mm_font_weight', 'normal', 0),
(2475, 0, 'oxy', 'oxy_main_menu_font', 'Donegal+One', 0),
(2473, 0, 'oxy', 'oxy_cart_font_size', '16', 0),
(2474, 0, 'oxy', 'oxy_cart_font_uppercase', '0', 0),
(2472, 0, 'oxy', 'oxy_cart_font_weight', 'normal', 0),
(2471, 0, 'oxy', 'oxy_cart_font', '', 0),
(2470, 0, 'oxy', 'oxy_search_font_uppercase', '1', 0),
(2469, 0, 'oxy', 'oxy_search_font_size', '13', 0),
(2467, 0, 'oxy', 'oxy_search_font', '', 0),
(2468, 0, 'oxy', 'oxy_search_font_weight', 'normal', 0),
(2465, 0, 'oxy', 'oxy_button_font_weight', 'normal', 0),
(2466, 0, 'oxy', 'oxy_button_font_uppercase', '1', 0),
(2463, 0, 'oxy', 'oxy_price_font_weight', 'normal', 0),
(2464, 0, 'oxy', 'oxy_button_font', 'Donegal+One', 0),
(2462, 0, 'oxy', 'oxy_price_font', 'Donegal+One', 0),
(2460, 0, 'oxy', 'oxy_title_font_weight', 'normal', 0),
(2461, 0, 'oxy', 'oxy_title_font_uppercase', '1', 0),
(2458, 0, 'oxy', 'oxy_body_font', 'Donegal+One', 0),
(2459, 0, 'oxy', 'oxy_title_font', 'Donegal+One', 0),
(2457, 0, 'oxy', 'oxy_bg_image_f5_repeat', 'repeat', 0),
(2456, 0, 'oxy', 'oxy_bg_image_f5_position', 'top center', 0),
(2455, 0, 'oxy', 'oxy_bg_image_f5_custom', '', 0),
(2454, 0, 'oxy', 'oxy_pattern_oxy_f5', '69', 0),
(2453, 0, 'oxy', 'oxy_bg_image_f4_repeat', 'repeat', 0),
(2451, 0, 'oxy', 'oxy_bg_image_f4_custom', '', 0),
(2452, 0, 'oxy', 'oxy_bg_image_f4_position', 'top center', 0),
(2449, 0, 'oxy', 'oxy_bg_image_f2_repeat', 'repeat', 0),
(2450, 0, 'oxy', 'oxy_pattern_oxy_f4', '69', 0),
(2448, 0, 'oxy', 'oxy_bg_image_f2_position', 'top center', 0),
(2447, 0, 'oxy', 'oxy_bg_image_f2_custom', '', 0),
(2445, 0, 'oxy', 'oxy_bg_image_f1_repeat', 'repeat', 0),
(2446, 0, 'oxy', 'oxy_pattern_oxy_f2', '69', 0),
(2443, 0, 'oxy', 'oxy_bg_image_f1_custom', '', 0),
(2444, 0, 'oxy', 'oxy_bg_image_f1_position', 'top center', 0),
(2442, 0, 'oxy', 'oxy_pattern_oxy_f1', '69', 0),
(2439, 0, 'oxy', 'oxy_pattern_oxy_mm', '', 0),
(2440, 0, 'oxy', 'oxy_bg_image_mm_custom', '', 0),
(2441, 0, 'oxy', 'oxy_bg_image_mm_repeat', 'repeat', 0),
(2438, 0, 'oxy', 'oxy_bg_image_ta_attachment', 'scroll', 0),
(2437, 0, 'oxy', 'oxy_bg_image_ta_repeat', 'repeat', 0),
(2436, 0, 'oxy', 'oxy_bg_image_ta_position', 'top center', 0),
(2435, 0, 'oxy', 'oxy_bg_image_ta_custom', '', 0),
(2434, 0, 'oxy', 'oxy_pattern_oxy_ta', '69', 0),
(2433, 0, 'oxy', 'oxy_bg_image_mc_attachment', 'scroll', 0),
(2432, 0, 'oxy', 'oxy_bg_image_mc_repeat', 'repeat', 0),
(2431, 0, 'oxy', 'oxy_bg_image_mc_position', 'top center', 0),
(2430, 0, 'oxy', 'oxy_bg_image_mc_custom', '', 0),
(2429, 0, 'oxy', 'oxy_pattern_oxy_mc', '', 0),
(2428, 0, 'oxy', 'oxy_bg_image_attachment', 'scroll', 0),
(2427, 0, 'oxy', 'oxy_bg_image_repeat', 'repeat', 0),
(2426, 0, 'oxy', 'oxy_bg_image_position', 'top center', 0),
(2425, 0, 'oxy', 'oxy_bg_image_custom', '', 0),
(2424, 0, 'oxy', 'oxy_pattern_oxy', '69', 0),
(2421, 0, 'oxy', 'oxy_f5_border_top_size', '1', 0),
(2422, 0, 'oxy', 'oxy_f5_border_top_style', 'dotted', 0),
(2423, 0, 'oxy', 'oxy_f5_border_top_color', '#A49572', 0),
(2420, 0, 'oxy', 'oxy_f5_border_top_status', '1', 0),
(2419, 0, 'oxy', 'oxy_f5_link_hover_color', '#FFFFFF', 0),
(2417, 0, 'oxy', 'oxy_f5_text_color', '#F3F1DC', 0),
(2418, 0, 'oxy', 'oxy_f5_link_color', '#FFD666', 0),
(2415, 0, 'oxy', 'oxy_f4_border_top_color', '#A49572', 0),
(2416, 0, 'oxy', 'oxy_f5_bg_color', '#52453C', 0),
(2412, 0, 'oxy', 'oxy_f4_border_top_status', '1', 0),
(2413, 0, 'oxy', 'oxy_f4_border_top_size', '1', 0),
(2414, 0, 'oxy', 'oxy_f4_border_top_style', 'dotted', 0),
(2410, 0, 'oxy', 'oxy_f4_link_color', '#FFD666', 0),
(2411, 0, 'oxy', 'oxy_f4_link_hover_color', '#FFFFFF', 0),
(2407, 0, 'oxy', 'oxy_f2_border_top_color', '#A49572', 0),
(2408, 0, 'oxy', 'oxy_f4_bg_color', '#52453C', 0),
(2409, 0, 'oxy', 'oxy_f4_text_color', '#F3F1DC', 0),
(2404, 0, 'oxy', 'oxy_f2_border_top_status', '1', 0),
(2405, 0, 'oxy', 'oxy_f2_border_top_size', '1', 0),
(2406, 0, 'oxy', 'oxy_f2_border_top_style', 'dotted', 0),
(2403, 0, 'oxy', 'oxy_f2_link_hover_color', '#FFD666', 0),
(2401, 0, 'oxy', 'oxy_f2_titles_border_bottom_color', '#A49572', 0),
(2402, 0, 'oxy', 'oxy_f2_link_color', '#FFFFFF', 0),
(2400, 0, 'oxy', 'oxy_f2_titles_border_bottom_style', 'dotted', 0),
(2399, 0, 'oxy', 'oxy_f2_titles_border_bottom_size', '1', 0),
(2397, 0, 'oxy', 'oxy_f2_bg_color', '#26201C', 0),
(2398, 0, 'oxy', 'oxy_f2_titles_color', '#FFD666', 0),
(2394, 0, 'oxy', 'oxy_f1_border_top_size', '1', 0),
(2395, 0, 'oxy', 'oxy_f1_border_top_style', 'solid', 0),
(2396, 0, 'oxy', 'oxy_f1_border_top_color', '#000000', 0),
(2393, 0, 'oxy', 'oxy_f1_border_top_status', '0', 0),
(2392, 0, 'oxy', 'oxy_f1_icon_color', '#52453C', 0),
(2391, 0, 'oxy', 'oxy_f1_link_hover_color', '#FFFFFF', 0),
(2390, 0, 'oxy', 'oxy_f1_link_color', '#FFD666', 0),
(2387, 0, 'oxy', 'oxy_f1_titles_border_bottom_style', 'dotted', 0),
(2388, 0, 'oxy', 'oxy_f1_titles_border_bottom_color', '#A49572', 0),
(2389, 0, 'oxy', 'oxy_f1_text_color', '#F3F1DC', 0),
(2386, 0, 'oxy', 'oxy_f1_titles_border_bottom_size', '1', 0),
(2385, 0, 'oxy', 'oxy_f1_titles_color', '#FFD666', 0),
(2384, 0, 'oxy', 'oxy_f1_bg_color', '#26201C', 0),
(2383, 0, 'oxy', 'oxy_fp_subtitle_color', '#F3F1DC', 0),
(2382, 0, 'oxy', 'oxy_fp_title_color_hover', '#FFD666', 0),
(2381, 0, 'oxy', 'oxy_fp_title_color', '#FFFFFF', 0),
(2380, 0, 'oxy', 'oxy_fp_bg_color', '#52453C', 0),
(2379, 0, 'oxy', 'oxy_mid_prod_slider_bottom_bar_bg_color_active', '#DB6440', 0),
(2377, 0, 'oxy', 'oxy_mid_prod_slider_bottom_bar_bg_color', '#3E342D', 0),
(2378, 0, 'oxy', 'oxy_mid_prod_slider_bottom_bar_bg_color_hover', '#FFD666', 0),
(2376, 0, 'oxy', 'oxy_mid_prod_slider_links_color_hover', '#DB6440', 0),
(2375, 0, 'oxy', 'oxy_mid_prod_slider_price_color', '#DB6440', 0),
(2374, 0, 'oxy', 'oxy_mid_prod_slider_desc_color', '#786557', 0),
(2373, 0, 'oxy', 'oxy_mid_prod_slider_name_color', '#52453C', 0),
(2372, 0, 'oxy', 'oxy_mid_prod_slider_custom_repeat', 'repeat', 0),
(2371, 0, 'oxy', 'oxy_mid_prod_slider_custom_position', 'top center', 0),
(2370, 0, 'oxy', 'oxy_mid_prod_slider_custom', '', 0),
(2368, 0, 'oxy', 'oxy_mid_prod_page_tabs_text_color', '#FFFFFF', 0),
(2369, 0, 'oxy', 'oxy_mid_prod_slider_bg_color', '#FFFFFF', 0),
(2365, 0, 'oxy', 'oxy_mid_prod_stars_color', '3', 0),
(2366, 0, 'oxy', 'oxy_mid_prod_page_tabs_bg_color', '#3E342D', 0),
(2367, 0, 'oxy', 'oxy_mid_prod_page_tabs_selected_bg_color', '#DB6440', 0),
(2364, 0, 'oxy', 'oxy_mid_prod_box_sale_icon_color', '#DB6440', 0),
(2363, 0, 'oxy', 'oxy_mid_prod_box_shadow_hover', '1', 0),
(2362, 0, 'oxy', 'oxy_mid_prod_box_bg_hover_color', '#978667', 0),
(2361, 0, 'oxy', 'oxy_mm_mobile_text_color', '#F3F1DC', 0),
(2360, 0, 'oxy', 'oxy_mm_mobile_bg_hover_color', '#DB6440', 0),
(2359, 0, 'oxy', 'oxy_mm_mobile_bg_color', '#3E342D', 0),
(2358, 0, 'oxy', 'oxy_mm_sub_box_shadow', '1', 0),
(2357, 0, 'oxy', 'oxy_mm_sub_separator_color', '#6E5C51', 0),
(2356, 0, 'oxy', 'oxy_mm_sub_separator_style', 'dotted', 0),
(2355, 0, 'oxy', 'oxy_mm_sub_link_hover_color', '#FFFFFF', 0),
(2354, 0, 'oxy', 'oxy_mm_sub_link_color', '#F3F1DC', 0),
(2353, 0, 'oxy', 'oxy_mm_sub_text_color', '#F3F1DC', 0),
(2352, 0, 'oxy', 'oxy_mm_sub_titles_bg_color', '#52453C', 0),
(2351, 0, 'oxy', 'oxy_mm_sub_bg_hover_color', '#DB6440', 0),
(2350, 0, 'oxy', 'oxy_mm_sub_bg_color', '#3E342D', 0),
(2349, 0, 'oxy', 'oxy_mm7_link_hover_color', '#FFD666', 0),
(2347, 0, 'oxy', 'oxy_mm7_bg_hover_color', '#3E342D', 0),
(2348, 0, 'oxy', 'oxy_mm7_link_color', '#F3F1DC', 0),
(2346, 0, 'oxy', 'oxy_mm7_bg_color', '#424242', 0),
(2344, 0, 'oxy', 'oxy_mm5_link_hover_color', '#FFD666', 0),
(2345, 0, 'oxy', 'oxy_mm7_bg_color_status', '0', 0),
(2343, 0, 'oxy', 'oxy_mm5_link_color', '#F3F1DC', 0),
(2340, 0, 'oxy', 'oxy_mm5_bg_color_status', '0', 0),
(2341, 0, 'oxy', 'oxy_mm5_bg_color', '#424242', 0),
(2342, 0, 'oxy', 'oxy_mm5_bg_hover_color', '#3E342D', 0),
(2338, 0, 'oxy', 'oxy_mm8_link_color', '#F3F1DC', 0),
(2339, 0, 'oxy', 'oxy_mm8_link_hover_color', '#FFD666', 0),
(2337, 0, 'oxy', 'oxy_mm8_bg_hover_color', '#3E342D', 0),
(2336, 0, 'oxy', 'oxy_mm8_bg_color', '#424242', 0),
(2334, 0, 'oxy', 'oxy_mm6_link_hover_color', '#FFD666', 0),
(2335, 0, 'oxy', 'oxy_mm8_bg_color_status', '0', 0),
(2333, 0, 'oxy', 'oxy_mm6_link_color', '#F3F1DC', 0),
(2331, 0, 'oxy', 'oxy_mm6_bg_color', '#424242', 0),
(2332, 0, 'oxy', 'oxy_mm6_bg_hover_color', '#3E342D', 0),
(2330, 0, 'oxy', 'oxy_mm6_bg_color_status', '0', 0),
(2329, 0, 'oxy', 'oxy_mm4_link_hover_color', '#FFD666', 0),
(2327, 0, 'oxy', 'oxy_mm4_bg_hover_color', '#3E342D', 0),
(2328, 0, 'oxy', 'oxy_mm4_link_color', '#F3F1DC', 0),
(2326, 0, 'oxy', 'oxy_mm4_bg_color', '#424242', 0),
(2325, 0, 'oxy', 'oxy_mm4_bg_color_status', '0', 0),
(2324, 0, 'oxy', 'oxy_mm3_link_hover_color', '#FFD666', 0),
(2323, 0, 'oxy', 'oxy_mm3_link_color', '#F3F1DC', 0),
(2322, 0, 'oxy', 'oxy_mm3_bg_hover_color', '#3E342D', 0),
(2321, 0, 'oxy', 'oxy_mm3_bg_color', '#424242', 0),
(2320, 0, 'oxy', 'oxy_mm3_bg_color_status', '0', 0),
(2319, 0, 'oxy', 'oxy_mm2_link_hover_color', '#FFD666', 0),
(2318, 0, 'oxy', 'oxy_mm2_link_color', '#F3F1DC', 0),
(2317, 0, 'oxy', 'oxy_mm2_bg_hover_color', '#3E342D', 0),
(2315, 0, 'oxy', 'oxy_mm2_bg_color_status', '0', 0),
(2316, 0, 'oxy', 'oxy_mm2_bg_color', '#ED5053', 0),
(2313, 0, 'oxy', 'oxy_mm1_link_color', '#FFFFFF', 0),
(2314, 0, 'oxy', 'oxy_mm1_link_hover_color', '#FFD666', 0),
(2312, 0, 'oxy', 'oxy_mm1_bg_hover_color', '#3E342D', 0),
(2310, 0, 'oxy', 'oxy_mm1_bg_color_status', '1', 0),
(2311, 0, 'oxy', 'oxy_mm1_bg_color', '#DB6440', 0),
(2309, 0, 'oxy', 'oxy_mm_border_bottom_color', '#EEEEEE', 0),
(2307, 0, 'oxy', 'oxy_mm_border_bottom_size', '1', 0),
(2308, 0, 'oxy', 'oxy_mm_border_bottom_style', 'solid', 0),
(2306, 0, 'oxy', 'oxy_mm_border_bottom_status', '0', 0),
(2304, 0, 'oxy', 'oxy_mm_border_top_style', 'dotted', 0),
(2305, 0, 'oxy', 'oxy_mm_border_top_color', '#5E4F44', 0),
(2303, 0, 'oxy', 'oxy_mm_border_top_size', '1', 0),
(2302, 0, 'oxy', 'oxy_mm_border_top_status', '0', 0),
(2300, 0, 'oxy', 'oxy_mm_separator_style', 'dotted', 0),
(2301, 0, 'oxy', 'oxy_mm_separator_color', '#594B42', 0),
(2299, 0, 'oxy', 'oxy_mm_separator_size', '1', 0),
(2298, 0, 'oxy', 'oxy_mm_separator_status', '0', 0),
(2297, 0, 'oxy', 'oxy_mm_bg_color', '#3E342D', 0),
(2296, 0, 'oxy', 'oxy_mm_bg_color_status', '1', 0),
(2295, 0, 'oxy', 'oxy_top_area_cart_icon_style', '1', 0),
(2294, 0, 'oxy', 'oxy_top_area_cart_separator_color', '#615248', 0),
(2293, 0, 'oxy', 'oxy_top_area_cart_link_color_hover', '#FFD666', 0),
(2292, 0, 'oxy', 'oxy_top_area_cart_link_color', '#FFD666', 0),
(2291, 0, 'oxy', 'oxy_top_area_cart_text_color', '#FFFFFF', 0),
(2290, 0, 'oxy', 'oxy_top_area_search_text_color', '#3E342D', 0),
(2289, 0, 'oxy', 'oxy_top_area_search_border_color_hover', '#3E342D', 0),
(2288, 0, 'oxy', 'oxy_top_area_search_border_color', '#3E342D', 0),
(2287, 0, 'oxy', 'oxy_top_area_search_bg_color', '#F3F1DC', 0),
(2286, 0, 'oxy', 'oxy_top_area_tb_dropdown_link_color_hover', '#F3F1DC', 0),
(2285, 0, 'oxy', 'oxy_top_area_tb_dropdown_link_color', '#F3F1DC', 0),
(2284, 0, 'oxy', 'oxy_top_area_tb_dropdown_bg_color_hover', '#DB6440', 0),
(2283, 0, 'oxy', 'oxy_top_area_tb_dropdown_bg_color', '#3E342D', 0),
(2282, 0, 'oxy', 'oxy_top_area_tb_separator_color', '#594B42', 0),
(2281, 0, 'oxy', 'oxy_top_area_tb_link_color_hover', '#DB6440', 0),
(2279, 0, 'oxy', 'oxy_top_area_tb_text_color', '#F3F1DC', 0),
(2280, 0, 'oxy', 'oxy_top_area_tb_link_color', '#FFD666', 0),
(2278, 0, 'oxy', 'oxy_top_area_tb_bottom_border_color', '#525252', 0),
(2277, 0, 'oxy', 'oxy_top_area_tb_bottom_border_status', '0', 0),
(2276, 0, 'oxy', 'oxy_top_area_tb_top_border_color', '#FFD666', 0),
(2275, 0, 'oxy', 'oxy_top_area_tb_top_border_status', '0', 0),
(2274, 0, 'oxy', 'oxy_top_area_tb_bg_color', '#3E342D', 0),
(2273, 0, 'oxy', 'oxy_top_area_tb_bg_status', '1', 0),
(2272, 0, 'oxy', 'oxy_top_area_mini_bg_color', '#463D37', 0),
(2271, 0, 'oxy', 'oxy_top_area_bg_color', '#3E342D', 0),
(2270, 0, 'oxy', 'oxy_top_area_status', '1', 0),
(2268, 0, 'oxy', 'oxy_button_slider_bg_color', '#F3F1DC', 0),
(2269, 0, 'oxy', 'oxy_button_slider_bg_hover_color', '#DB6440', 0),
(2266, 0, 'oxy', 'oxy_button_light_text_color', '#FFFFFF', 0),
(2267, 0, 'oxy', 'oxy_button_light_text_hover_color', '#3E342D', 0),
(2265, 0, 'oxy', 'oxy_button_light_bg_hover_color', '#FFD666', 0),
(2264, 0, 'oxy', 'oxy_button_light_bg_color', '#DB6440', 0),
(2263, 0, 'oxy', 'oxy_button_exclusive_text_hover_color', '#3E342D', 0),
(2262, 0, 'oxy', 'oxy_button_exclusive_text_color', '#FFFFFF', 0),
(2261, 0, 'oxy', 'oxy_button_exclusive_bg_hover_color', '#FFD666', 0),
(2260, 0, 'oxy', 'oxy_button_exclusive_bg_color', '#DB6440', 0),
(2259, 0, 'oxy', 'oxy_button_text_hover_color', '#FFFFFF', 0),
(2257, 0, 'oxy', 'oxy_button_bg_hover_color', '#DB6440', 0),
(2258, 0, 'oxy', 'oxy_button_text_color', '#3E342D', 0),
(2256, 0, 'oxy', 'oxy_button_bg_color', '#FFD666', 0),
(2254, 0, 'oxy', 'oxy_price_tax_color', '#F3F1DC', 0),
(2255, 0, 'oxy', 'oxy_button_border_radius', '0', 0),
(2251, 0, 'oxy', 'oxy_price_color', '#FFFFFF', 0),
(2252, 0, 'oxy', 'oxy_price_old_color', '#F3F1DC', 0),
(2253, 0, 'oxy', 'oxy_price_new_color', '#FFD666', 0),
(2250, 0, 'oxy', 'oxy_filter_box_box_links_color_hover', '#FFD666', 0),
(2249, 0, 'oxy', 'oxy_filter_box_box_links_color', '#F3F1DC', 0),
(2248, 0, 'oxy', 'oxy_filter_box_box_bg_color', '#3E342D', 0),
(2247, 0, 'oxy', 'oxy_filter_box_box_status', '1', 0),
(2246, 0, 'oxy', 'oxy_filter_box_head_border_color', '#5E4F44', 0),
(2245, 0, 'oxy', 'oxy_filter_box_head_border_style', 'dotted', 0),
(2243, 0, 'oxy', 'oxy_filter_box_head_border_status', '1', 0),
(2244, 0, 'oxy', 'oxy_filter_box_head_border_size', '1', 0),
(2242, 0, 'oxy', 'oxy_filter_box_head_title_color', '#FFD666', 0),
(2240, 0, 'oxy', 'oxy_filter_box_head_bg_color', '#3E342D', 0),
(2241, 0, 'oxy', 'oxy_filter_box_head_custom', '', 0),
(2239, 0, 'oxy', 'oxy_filter_box_head_status', '1', 0),
(2238, 0, 'oxy', 'oxy_category_box_box_separator_color', '#5E4F44', 0),
(2236, 0, 'oxy', 'oxy_category_box_box_separator_size', '1', 0),
(2237, 0, 'oxy', 'oxy_category_box_box_separator_style', 'dotted', 0),
(2235, 0, 'oxy', 'oxy_category_box_box_links_color_hover', '#FFD666', 0),
(2234, 0, 'oxy', 'oxy_category_box_box_links_color', '#F3F1DC', 0),
(2232, 0, 'oxy', 'oxy_category_box_box_bg_color', '#3E342D', 0),
(2233, 0, 'oxy', 'oxy_category_box_box_bg_color_hover', '#5E4F44', 0),
(2231, 0, 'oxy', 'oxy_category_box_box_status', '1', 0),
(2230, 0, 'oxy', 'oxy_category_box_head_border_color', '#EAEAEA', 0),
(2229, 0, 'oxy', 'oxy_category_box_head_border_style', 'solid', 0),
(2228, 0, 'oxy', 'oxy_category_box_head_border_size', '1', 0),
(2226, 0, 'oxy', 'oxy_category_box_head_title_color', '#FFFFFF', 0),
(2227, 0, 'oxy', 'oxy_category_box_head_border_status', '0', 0),
(2225, 0, 'oxy', 'oxy_category_box_head_custom', '', 0),
(2224, 0, 'oxy', 'oxy_category_box_head_bg_color', '#DB6440', 0),
(2223, 0, 'oxy', 'oxy_category_box_head_status', '1', 0),
(2222, 0, 'oxy', 'oxy_right_column_box_links_color_hover', '#FFD666', 0),
(2221, 0, 'oxy', 'oxy_right_column_box_links_color', '#F3F1DC', 0),
(2220, 0, 'oxy', 'oxy_right_column_box_bg_color', '#3E342D', 0),
(2219, 0, 'oxy', 'oxy_right_column_box_status', '1', 0),
(2218, 0, 'oxy', 'oxy_right_column_head_border_color', '#EAEAEA', 0),
(2217, 0, 'oxy', 'oxy_right_column_head_border_style', 'solid', 0),
(2216, 0, 'oxy', 'oxy_right_column_head_border_size', '1', 0),
(2215, 0, 'oxy', 'oxy_right_column_head_border_status', '0', 0),
(2214, 0, 'oxy', 'oxy_right_column_head_title_color', '#52453C', 0),
(2213, 0, 'oxy', 'oxy_right_column_head_custom', '', 0),
(2212, 0, 'oxy', 'oxy_right_column_head_bg_color', '#FFD666', 0),
(2211, 0, 'oxy', 'oxy_right_column_head_status', '1', 0),
(2210, 0, 'oxy', 'oxy_left_column_box_links_color_hover', '#FFD666', 0),
(2208, 0, 'oxy', 'oxy_left_column_box_bg_color', '#3E342D', 0),
(2209, 0, 'oxy', 'oxy_left_column_box_links_color', '#F3F1DC', 0),
(2207, 0, 'oxy', 'oxy_left_column_box_status', '1', 0),
(2206, 0, 'oxy', 'oxy_left_column_head_border_color', '#EAEAEA', 0),
(2205, 0, 'oxy', 'oxy_left_column_head_border_style', 'solid', 0),
(2204, 0, 'oxy', 'oxy_left_column_head_border_size', '1', 0),
(2203, 0, 'oxy', 'oxy_left_column_head_border_status', '0', 0),
(2202, 0, 'oxy', 'oxy_left_column_head_title_color', '#52453C', 0),
(2201, 0, 'oxy', 'oxy_left_column_head_custom', '', 0),
(2200, 0, 'oxy', 'oxy_left_column_head_bg_color', '#FFD666', 0),
(2199, 0, 'oxy', 'oxy_left_column_head_status', '1', 0),
(2198, 0, 'oxy', 'oxy_content_column_separator_color', '#A49572', 0),
(2197, 0, 'oxy', 'oxy_content_column_separator_style', 'dotted', 0),
(2195, 0, 'oxy', 'oxy_content_column_head_border_color', '#A49572', 0),
(2196, 0, 'oxy', 'oxy_content_column_separator_size', '1', 0),
(2194, 0, 'oxy', 'oxy_content_column_head_border_style', 'dotted', 0),
(2193, 0, 'oxy', 'oxy_content_column_head_border_size', '1', 0),
(2191, 0, 'oxy', 'oxy_content_column_hli_bg_color', '#3E342D', 0),
(2192, 0, 'oxy', 'oxy_content_column_head_border_status', '1', 0),
(2190, 0, 'oxy', 'oxy_main_column_shadow', '0', 0),
(2189, 0, 'oxy', 'oxy_main_column_border_color', '#CCCCCC', 0),
(2188, 0, 'oxy', 'oxy_main_column_border_style', 'solid', 0),
(2187, 0, 'oxy', 'oxy_main_column_border_size', '1', 0),
(2186, 0, 'oxy', 'oxy_main_column_border_status', '0', 0),
(2185, 0, 'oxy', 'oxy_main_column_bg_color', '#FFFFFF', 0),
(2183, 0, 'oxy', 'oxy_general_icons_style', '2', 0),
(2184, 0, 'oxy', 'oxy_main_column_status', '0', 0),
(2182, 0, 'oxy', 'oxy_links_hover_color', '#FFFFFF', 0),
(2181, 0, 'oxy', 'oxy_other_links_color', '#FFD666', 0),
(2180, 0, 'oxy', 'oxy_light_text_color', '#D4D0B0', 0),
(2179, 0, 'oxy', 'oxy_body_text_color', '#F3F1DC', 0),
(2177, 0, 'oxy', 'oxy_body_bg_color', '#52453C', 0),
(2178, 0, 'oxy', 'oxy_headings_color', '#F3F1DC', 0),
(2176, 0, 'oxy', 'oxy_others_totop', '1', 0),
(2175, 0, 'oxy', 'oxy_left_right_column_categories_type', '0', 0),
(2173, 0, 'oxy', 'oxy_contact_map_ll', '', 0),
(2174, 0, 'oxy', 'oxy_contact_map_type', 'ROADMAP', 0),
(2172, 0, 'oxy', 'oxy_contact_map_status', '0', 0),
(2171, 0, 'oxy', 'oxy_contact_custom_content1', '', 0),
(2170, 0, 'oxy', 'oxy_contact_custom_status', '0', 0),
(2169, 0, 'oxy', 'oxy_product_custom_tab_3_content1', '', 0),
(2167, 0, 'oxy', 'oxy_product_custom_tab_3_status', '0', 0),
(2168, 0, 'oxy', 'oxy_product_custom_tab_3_title1', '', 0),
(2166, 0, 'oxy', 'oxy_product_custom_tab_2_content1', '', 0),
(2164, 0, 'oxy', 'oxy_product_custom_tab_2_status', '0', 0),
(2165, 0, 'oxy', 'oxy_product_custom_tab_2_title1', '', 0),
(2163, 0, 'oxy', 'oxy_product_custom_tab_1_content1', '', 0),
(2162, 0, 'oxy', 'oxy_product_custom_tab_1_title1', '', 0),
(2161, 0, 'oxy', 'oxy_product_custom_tab_1_status', '0', 0),
(2159, 0, 'oxy', 'oxy_product_fb3_subtitle1', '', 0),
(2160, 0, 'oxy', 'oxy_product_fb3_content1', '', 0),
(2158, 0, 'oxy', 'oxy_product_fb3_title1', '', 0),
(2156, 0, 'oxy', 'oxy_product_fb2_content1', '', 0),
(2157, 0, 'oxy', 'oxy_product_fb3_icon', '', 0),
(2155, 0, 'oxy', 'oxy_product_fb2_subtitle1', '', 0),
(2153, 0, 'oxy', 'oxy_product_fb2_icon', '', 0),
(2154, 0, 'oxy', 'oxy_product_fb2_title1', '', 0),
(2152, 0, 'oxy', 'oxy_product_fb1_content1', '', 0),
(2151, 0, 'oxy', 'oxy_product_fb1_subtitle1', '', 0),
(2150, 0, 'oxy', 'oxy_product_fb1_title1', '', 0),
(2149, 0, 'oxy', 'oxy_product_fb1_icon', '', 0),
(2148, 0, 'oxy', 'oxy_product_custom_content1', '', 0),
(2147, 0, 'oxy', 'oxy_product_custom_status', '0', 0),
(2146, 0, 'oxy', 'oxy_product_related_style', '0', 0),
(2145, 0, 'oxy', 'oxy_product_related_position', '1', 0),
(2143, 0, 'oxy', 'oxy_product_zoom_type', '0', 0),
(2144, 0, 'oxy', 'oxy_product_related_status', '1', 0),
(2142, 0, 'oxy', 'oxy_product_i_c_status', '1', 0),
(2141, 0, 'oxy', 'oxy_product_viewed_status', '1', 0),
(2140, 0, 'oxy', 'oxy_product_tax_status', '1', 0),
(2139, 0, 'oxy', 'oxy_product_save_percent_status', '1', 0),
(2138, 0, 'oxy', 'oxy_product_manufacturer_logo_status', '1', 0),
(2137, 0, 'oxy', 'oxy_layout_product_page', '10', 0),
(2136, 0, 'oxy', 'oxy_category_prod_align', '1', 0),
(2135, 0, 'oxy', 'oxy_category_prod_swap_status', '1', 0),
(2134, 0, 'oxy', 'oxy_category_prod_zoom_status', '0', 0),
(2133, 0, 'oxy', 'oxy_category_prod_wis_com_status', '1', 0),
(2132, 0, 'oxy', 'oxy_category_prod_ratings_status', '1', 0),
(2131, 0, 'oxy', 'oxy_category_prod_cart_status', '1', 0),
(2130, 0, 'oxy', 'oxy_category_prod_price_status', '1', 0),
(2129, 0, 'oxy', 'oxy_category_prod_brand_status', '1', 0),
(2128, 0, 'oxy', 'oxy_category_prod_name_status', '1', 0),
(2127, 0, 'oxy', 'oxy_category_sale_badge_status', '1', 0),
(2126, 0, 'oxy', 'oxy_layout_pb_noc', 'three', 0),
(2125, 0, 'oxy', 'oxy_category_subcategories_style', '0', 0),
(2124, 0, 'oxy', 'oxy_category_subcategories_status', '0', 0),
(2123, 0, 'oxy', 'oxy_category_prod_display', 'grid', 0),
(2122, 0, 'oxy', 'oxy_homepage_brands_wall_per_row', 'three', 0),
(2121, 0, 'oxy', 'oxy_homepage_brands_wall_style', '1', 0),
(2120, 0, 'oxy', 'oxy_homepage_brands_wall_status', '0', 0),
(2119, 0, 'oxy', 'oxy_homepage_category_wall_sub_number', '5', 0),
(2118, 0, 'oxy', 'oxy_homepage_category_wall_per_row', 'three', 0),
(2117, 0, 'oxy', 'oxy_homepage_category_wall_icon_status', '1', 0),
(2116, 0, 'oxy', 'oxy_homepage_category_wall_status', '0', 0),
(2115, 0, 'oxy', 'oxy_menu_contacts_address_status', '0', 0),
(2114, 0, 'oxy', 'oxy_menu_contacts_status', '0', 0),
(2113, 0, 'oxy', 'oxy_menu_contacts_block_status', '1', 0),
(2112, 0, 'oxy', 'oxy_menu_information_pages_status', '1', 0),
(2111, 0, 'oxy', 'oxy_menu_custom_block_content_31', '', 0),
(2110, 0, 'oxy', 'oxy_menu_custom_block_title_31', '', 0),
(2109, 0, 'oxy', 'oxy_menu_custom_block_3_status', '0', 0),
(2108, 0, 'oxy', 'oxy_menu_custom_block_content_21', '', 0),
(2107, 0, 'oxy', 'oxy_menu_custom_block_title_21', '', 0),
(2106, 0, 'oxy', 'oxy_menu_custom_block_2_status', '0', 0),
(2105, 0, 'oxy', 'oxy_menu_custom_block_content_11', '', 0),
(2104, 0, 'oxy', 'oxy_menu_custom_block_title_11', '', 0),
(2103, 0, 'oxy', 'oxy_menu_custom_block_1_status', '0', 0),
(2102, 0, 'oxy', 'oxy_menu_cm_link_10_target', '_self', 0),
(2101, 0, 'oxy', 'oxy_menu_cm_link_10_url', '', 0),
(2100, 0, 'oxy', 'oxy_menu_cm_link_101', '', 0),
(2099, 0, 'oxy', 'oxy_menu_cm_link_9_target', '_self', 0),
(2098, 0, 'oxy', 'oxy_menu_cm_link_9_url', '', 0),
(2097, 0, 'oxy', 'oxy_menu_cm_link_91', '', 0),
(2096, 0, 'oxy', 'oxy_menu_cm_link_8_target', '_self', 0),
(2095, 0, 'oxy', 'oxy_menu_cm_link_8_url', '', 0),
(2094, 0, 'oxy', 'oxy_menu_cm_link_81', '', 0),
(2093, 0, 'oxy', 'oxy_menu_cm_link_7_target', '_self', 0),
(2091, 0, 'oxy', 'oxy_menu_cm_link_71', '', 0),
(2092, 0, 'oxy', 'oxy_menu_cm_link_7_url', '', 0),
(2090, 0, 'oxy', 'oxy_menu_cm_link_6_target', '_self', 0),
(2089, 0, 'oxy', 'oxy_menu_cm_link_6_url', '', 0),
(2088, 0, 'oxy', 'oxy_menu_cm_link_61', '', 0),
(2087, 0, 'oxy', 'oxy_menu_cm_link_5_target', '_self', 0),
(2086, 0, 'oxy', 'oxy_menu_cm_link_5_url', '', 0),
(2085, 0, 'oxy', 'oxy_menu_cm_link_51', '', 0),
(2084, 0, 'oxy', 'oxy_menu_cm_link_4_target', '_self', 0),
(2083, 0, 'oxy', 'oxy_menu_cm_link_4_url', '', 0),
(2082, 0, 'oxy', 'oxy_menu_cm_link_41', '', 0),
(2081, 0, 'oxy', 'oxy_menu_cm_link_3_target', '_self', 0),
(2079, 0, 'oxy', 'oxy_menu_cm_link_31', '', 0),
(2080, 0, 'oxy', 'oxy_menu_cm_link_3_url', '', 0),
(2078, 0, 'oxy', 'oxy_menu_cm_link_2_target', '_self', 0),
(2077, 0, 'oxy', 'oxy_menu_cm_link_2_url', '', 0),
(2076, 0, 'oxy', 'oxy_menu_cm_link_21', '', 0),
(2075, 0, 'oxy', 'oxy_menu_cm_link_1_target', '_self', 0),
(2074, 0, 'oxy', 'oxy_menu_cm_link_1_url', '', 0),
(2601, 0, 'oxy_banner', 'oxy_banner_module', 'a:1:{i:0;a:8:{s:9:"banner_id";s:1:"6";s:5:"width";s:3:"750";s:6:"height";s:3:"250";s:5:"pr_id";s:3:"pr1";s:9:"layout_id";s:1:"6";s:8:"position";s:11:"content_top";s:6:"status";s:1:"0";s:10:"sort_order";s:0:"";}}', 1),
(2563, 0, 'oxy', 'oxy_payment_block_custom', '', 0),
(2562, 0, 'oxy', 'oxy_payment_block_status', '0', 0),
(2015, 0, 'oxy_product_slider', 'product', '', 0),
(2016, 0, 'oxy_product_slider', 'oxy_product_slider_product', '47', 0),
(2559, 0, 'oxy', 'oxy_i_c_4_4_status', '1', 0),
(2560, 0, 'oxy', 'oxy_powered_status', '0', 0),
(2561, 0, 'oxy', 'oxy_powered_content1', '', 0),
(2073, 0, 'oxy', 'oxy_menu_cm_link_11', '', 0),
(2072, 0, 'oxy', 'oxy_menu_cm_title1', '', 0),
(2071, 0, 'oxy', 'oxy_menu_cm_status', '0', 0),
(2070, 0, 'oxy', 'oxy_menu_link_10_target', '_self', 0),
(2069, 0, 'oxy', 'oxy_menu_link_10_url', '', 0),
(2068, 0, 'oxy', 'oxy_menu_link_101', '', 0),
(2067, 0, 'oxy', 'oxy_menu_link_9_target', '_self', 0),
(2066, 0, 'oxy', 'oxy_menu_link_9_url', '', 0),
(2065, 0, 'oxy', 'oxy_menu_link_91', '', 0),
(2064, 0, 'oxy', 'oxy_menu_link_8_target', '_self', 0),
(2063, 0, 'oxy', 'oxy_menu_link_8_url', '', 0),
(2062, 0, 'oxy', 'oxy_menu_link_81', '', 0),
(2061, 0, 'oxy', 'oxy_menu_link_7_target', '_self', 0),
(2060, 0, 'oxy', 'oxy_menu_link_7_url', '', 0),
(2059, 0, 'oxy', 'oxy_menu_link_71', '', 0),
(2058, 0, 'oxy', 'oxy_menu_link_6_target', '_self', 0),
(2057, 0, 'oxy', 'oxy_menu_link_6_url', '', 0),
(2056, 0, 'oxy', 'oxy_menu_link_61', '', 0),
(2055, 0, 'oxy', 'oxy_menu_link_5_target', '_self', 0),
(2054, 0, 'oxy', 'oxy_menu_link_5_url', '', 0),
(2053, 0, 'oxy', 'oxy_menu_link_51', '', 0),
(2052, 0, 'oxy', 'oxy_menu_link_4_target', '_self', 0),
(2051, 0, 'oxy', 'oxy_menu_link_4_url', '', 0),
(2050, 0, 'oxy', 'oxy_menu_link_41', '', 0),
(2049, 0, 'oxy', 'oxy_menu_link_3_target', '_self', 0),
(2048, 0, 'oxy', 'oxy_menu_link_3_url', '', 0),
(2047, 0, 'oxy', 'oxy_menu_link_31', '', 0),
(2046, 0, 'oxy', 'oxy_menu_link_2_target', '_self', 0),
(2045, 0, 'oxy', 'oxy_menu_link_2_url', '', 0),
(2044, 0, 'oxy', 'oxy_menu_link_21', '', 0),
(2043, 0, 'oxy', 'oxy_menu_link_1_target', '_self', 0),
(2042, 0, 'oxy', 'oxy_menu_link_1_url', '', 0),
(2041, 0, 'oxy', 'oxy_menu_link_11', '', 0),
(2040, 0, 'oxy', 'oxy_menu_brands_per_row', '6', 0),
(2039, 0, 'oxy', 'oxy_menu_brands_style', '2', 0),
(2038, 0, 'oxy', 'oxy_menu_brands_status', '1', 0),
(2037, 0, 'oxy', 'oxy_menu_categories_3_level', '0', 0),
(2036, 0, 'oxy', 'oxy_menu_categories_per_row', '5', 0),
(2035, 0, 'oxy', 'oxy_mm2_main_category_icon_status', '0', 0),
(2034, 0, 'oxy', 'oxy_menu_categories_style', '2', 0),
(2033, 0, 'oxy', 'oxy_menu_categories_status', '1', 0),
(2032, 0, 'oxy', 'oxy_menu_homepage_style', '2', 0),
(2031, 0, 'oxy', 'oxy_menu_homepage_status', '1', 0),
(2030, 0, 'oxy', 'oxy_search_bar_position', 'center', 0),
(2029, 0, 'oxy', 'oxy_logo_position', 'left', 0),
(2028, 0, 'oxy', 'oxy_header_auto_suggest_status', '1', 0),
(2027, 0, 'oxy', 'oxy_header_fixed_header_status', '1', 0),
(2026, 0, 'oxy', 'oxy_homepage_specials_status', '0', 0),
(2025, 0, 'oxy', 'oxy_homepage_latest_status', '0', 0),
(2024, 0, 'oxy', 'oxy_homepage_featured_status', '0', 0),
(2023, 0, 'oxy', 'oxy_homepage_bestseller_status', '0', 0),
(2022, 0, 'oxy', 'oxy_homepage_banner_slider_type', '1', 0),
(2021, 0, 'oxy', 'oxy_layout_l', '2', 0),
(2020, 0, 'oxy', 'oxy_layout_s', '1', 0),
(2019, 0, 'oxy', 'oxy_layout_style', '2', 0),
(2018, 0, 'oxy', 'oxy_skin', '2', 0),
(2017, 0, 'oxy', 'oxy_status', '1', 0),
(2012, 0, 'slideshow', 'slideshow_module', 'a:1:{i:0;a:7:{s:9:"banner_id";s:1:"7";s:5:"width";s:3:"980";s:6:"height";s:3:"280";s:9:"layout_id";s:1:"1";s:8:"position";s:11:"content_top";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"1";}}', 1),
(2014, 0, 'oxy_custom_content_manager', 'oxy_custom_content_manager_module', 'a:1:{i:1;a:6:{s:10:"occm_title";a:1:{i:1;s:12:"Custom Block";}s:11:"description";a:1:{i:1;s:75:"&lt;p&gt;This is a CMS Block Edited from Admin Pannel&amp;nbsp;&lt;/p&gt;\r\n";}s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"3";}}', 1),
(2564, 0, 'oxy', 'oxy_payment_block_custom_url', '', 0),
(2565, 0, 'oxy', 'oxy_payment_paypal_url', '', 0),
(2566, 0, 'oxy', 'oxy_payment_visa_url', '', 0),
(2567, 0, 'oxy', 'oxy_payment_mastercard_url', '', 0),
(2568, 0, 'oxy', 'oxy_payment_maestro_url', '', 0),
(2569, 0, 'oxy', 'oxy_payment_discover_url', '', 0),
(2570, 0, 'oxy', 'oxy_payment_moneybookers_url', '', 0),
(2571, 0, 'oxy', 'oxy_payment_american_express_url', '', 0),
(2572, 0, 'oxy', 'oxy_payment_cirrus_url', '', 0),
(2573, 0, 'oxy', 'oxy_payment_delta_url', '', 0),
(2574, 0, 'oxy', 'oxy_payment_google_url', '', 0),
(2575, 0, 'oxy', 'oxy_payment_2co_url', '', 0),
(2576, 0, 'oxy', 'oxy_payment_sage_url', '', 0),
(2577, 0, 'oxy', 'oxy_payment_solo_url', '', 0),
(2578, 0, 'oxy', 'oxy_payment_switch_url', '', 0),
(2579, 0, 'oxy', 'oxy_payment_western_union_url', '', 0),
(2580, 0, 'oxy', 'oxy_custom_3_status', '0', 0),
(2581, 0, 'oxy', 'oxy_custom_3_content1', '', 0),
(2582, 0, 'oxy', 'oxy_facebook_likebox_status', '1', 0),
(2583, 0, 'oxy', 'oxy_facebook_likebox_id', '', 0),
(2584, 0, 'oxy', 'oxy_facebook_likebox_position', 'right', 0),
(2585, 0, 'oxy', 'oxy_twitter_block_status', '1', 0),
(2586, 0, 'oxy', 'oxy_twitter_block_user', '', 0),
(2587, 0, 'oxy', 'oxy_twitter_block_widget_id', '', 0),
(2588, 0, 'oxy', 'oxy_twitter_block_tweets', '3', 0),
(2589, 0, 'oxy', 'oxy_twitter_block_position', 'right', 0),
(2590, 0, 'oxy', 'oxy_video_box_status', '0', 0),
(2591, 0, 'oxy', 'oxy_video_box_content1', '', 0),
(2592, 0, 'oxy', 'oxy_video_box_position', 'right', 0),
(2593, 0, 'oxy', 'oxy_video_box_bg', '#DB6440', 0),
(2594, 0, 'oxy', 'oxy_custom_box_status', '0', 0),
(2595, 0, 'oxy', 'oxy_custom_box_content1', '', 0),
(2596, 0, 'oxy', 'oxy_custom_box_position', 'right', 0),
(2597, 0, 'oxy', 'oxy_custom_box_bg', '#FFD666', 0),
(2598, 0, 'oxy', 'oxy_custom_css', '', 0),
(2599, 0, 'oxy', 'oxy_custom_js', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `stock_status`
--

CREATE TABLE IF NOT EXISTS `stock_status` (
  `stock_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`stock_status_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `stock_status`
--

INSERT INTO `stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '2 - 3 Days');

-- --------------------------------------------------------

--
-- Structure de la table `store`
--

CREATE TABLE IF NOT EXISTS `store` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `tax_class`
--

CREATE TABLE IF NOT EXISTS `tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tax_class_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `tax_class`
--

INSERT INTO `tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed Stuff', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36'),
(11, 'DE19', 'Deutschland 19%', '2015-04-13 20:40:57', '0000-00-00 00:00:00'),
(12, 'DE7', 'Deutschland 7%', '2015-04-13 20:40:58', '0000-00-00 00:00:00'),
(13, 'DE0', 'Deutschland 0%', '2015-04-13 20:40:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `tax_rate`
--

CREATE TABLE IF NOT EXISTS `tax_rate` (
  `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tax_rate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=97 ;

--
-- Contenu de la table `tax_rate`
--

INSERT INTO `tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (17.5%)', '17.5000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19'),
(88, 5, 'USt. 19%', '19.0000', 'P', '2015-04-13 20:40:47', '2015-04-13 20:40:47'),
(89, 5, 'USt. 7%', '7.0000', 'P', '2015-04-13 20:40:48', '2015-04-13 20:40:48'),
(90, 5, 'USt. 0%', '0.0000', 'P', '2015-04-13 20:40:49', '2015-04-13 20:40:49'),
(91, 6, 'EX EU USt. 19%', '19.0000', 'P', '2015-04-13 20:40:49', '2015-04-13 20:40:49'),
(92, 6, 'EX EU USt. 7%', '7.0000', 'P', '2015-04-13 20:40:51', '2015-04-13 20:40:51'),
(93, 6, 'EX EU 0%', '0.0000', 'P', '2015-04-13 20:40:52', '2015-04-13 20:40:52'),
(94, 7, 'EX USt. 19%', '19.0000', 'P', '2015-04-13 20:40:52', '2015-04-13 20:40:52'),
(95, 7, 'EX USt. 7%', '7.0000', 'P', '2015-04-13 20:40:53', '2015-04-13 20:40:53'),
(96, 7, 'EX 0%', '0.0000', 'P', '2015-04-13 20:40:54', '2015-04-13 20:40:54');

-- --------------------------------------------------------

--
-- Structure de la table `tax_rate_to_customer_group`
--

CREATE TABLE IF NOT EXISTS `tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  PRIMARY KEY (`tax_rate_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tax_rate_to_customer_group`
--

INSERT INTO `tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tax_rule`
--

CREATE TABLE IF NOT EXISTS `tax_rule` (
  `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tax_rule_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=141 ;

--
-- Contenu de la table `tax_rule`
--

INSERT INTO `tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2),
(129, 11, 88, 'payment', 1),
(130, 11, 91, 'payment', 2),
(131, 11, 94, 'payment', 3),
(132, 11, 93, 'payment', 4),
(133, 11, 96, 'payment', 5),
(134, 12, 89, 'payment', 1),
(135, 12, 92, 'payment', 2),
(136, 12, 93, 'payment', 3),
(137, 12, 96, 'payment', 4),
(138, 13, 90, 'payment', 1),
(139, 13, 93, 'payment', 2),
(140, 13, 96, 'payment', 3);

-- --------------------------------------------------------

--
-- Structure de la table `url_alias`
--

CREATE TABLE IF NOT EXISTS `url_alias` (
  `url_alias_id` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`url_alias_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=780 ;

--
-- Contenu de la table `url_alias`
--

INSERT INTO `url_alias` (`url_alias_id`, `query`, `keyword`) VALUES
(704, 'product_id=48', 'ipod_classic'),
(773, 'category_id=20', 'desktops'),
(503, 'category_id=26', 'pc'),
(505, 'category_id=27', 'mac'),
(730, 'manufacturer_id=8', 'apple'),
(772, 'information_id=4', 'about_us'),
(767, 'category_id=34', 'mp3-players'),
(536, 'category_id=36', 'Normal'),
(774, 'information_id=7', 'widerruf'),
(775, 'information_id=8', 'zusammenfassung'),
(776, 'information_id=9', 'email'),
(777, 'information_id=10', 'datenschutz'),
(778, 'information_id=11', 'widerrufsformular'),
(779, 'information_id=12', 'impressum');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code_user` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `code`, `ip`, `status`, `date_added`, `code_user`) VALUES
(1, 1, 'admin', '751c07ae9e79f5d04ee8047959ca2639a3073941', '1f40bca73', '', '', 'admin@demo.fr', '', '127.0.0.1', 1, '2015-04-13 20:31:26', ''),
(2, 11, 'khaled', '5661d7534b7f187a3f21c2c59b8b6407278cee71', 'edc15b2d4', 'khaled', 'khaled', 'rahalkhaled85@gmail.com', '', '127.0.0.1', 1, '2015-04-13 21:02:31', 'k1tAza231xqwy*');

-- --------------------------------------------------------

--
-- Structure de la table `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `user_group`
--

INSERT INTO `user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Top Administrator', 'a:2:{s:6:"access";a:153:{i:0;s:17:"catalog/attribute";i:1;s:23:"catalog/attribute_group";i:2;s:16:"catalog/category";i:3;s:16:"catalog/download";i:4;s:14:"catalog/filter";i:5;s:19:"catalog/information";i:6;s:20:"catalog/manufacturer";i:7;s:14:"catalog/option";i:8;s:15:"catalog/product";i:9;s:15:"catalog/profile";i:10;s:14:"catalog/review";i:11;s:18:"common/filemanager";i:12;s:13:"design/banner";i:13;s:19:"design/custom_field";i:14;s:13:"design/layout";i:15;s:14:"extension/feed";i:16;s:17:"extension/manager";i:17;s:16:"extension/module";i:18;s:17:"extension/openbay";i:19;s:17:"extension/payment";i:20;s:18:"extension/shipping";i:21;s:15:"extension/total";i:22;s:16:"feed/google_base";i:23;s:19:"feed/google_sitemap";i:24;s:20:"localisation/country";i:25;s:21:"localisation/currency";i:26;s:21:"localisation/geo_zone";i:27;s:21:"localisation/language";i:28;s:25:"localisation/length_class";i:29;s:25:"localisation/order_status";i:30;s:26:"localisation/return_action";i:31;s:26:"localisation/return_reason";i:32;s:26:"localisation/return_status";i:33;s:25:"localisation/stock_status";i:34;s:22:"localisation/tax_class";i:35;s:21:"localisation/tax_rate";i:36;s:25:"localisation/weight_class";i:37;s:17:"localisation/zone";i:38;s:14:"module/account";i:39;s:16:"module/affiliate";i:40;s:29:"module/amazon_checkout_layout";i:41;s:13:"module/banner";i:42;s:17:"module/bestseller";i:43;s:15:"module/carousel";i:44;s:15:"module/category";i:45;s:18:"module/ebaydisplay";i:46;s:15:"module/featured";i:47;s:13:"module/filter";i:48;s:18:"module/google_talk";i:49;s:18:"module/information";i:50;s:13:"module/latest";i:51;s:17:"module/openbaypro";i:52;s:16:"module/pp_layout";i:53;s:16:"module/slideshow";i:54;s:14:"module/special";i:55;s:12:"module/store";i:56;s:14:"module/welcome";i:57;s:14:"openbay/amazon";i:58;s:22:"openbay/amazon_listing";i:59;s:22:"openbay/amazon_product";i:60;s:16:"openbay/amazonus";i:61;s:24:"openbay/amazonus_listing";i:62;s:24:"openbay/amazonus_product";i:63;s:20:"openbay/ebay_profile";i:64;s:21:"openbay/ebay_template";i:65;s:15:"openbay/openbay";i:66;s:23:"payment/amazon_checkout";i:67;s:24:"payment/authorizenet_aim";i:68;s:21:"payment/bank_transfer";i:69;s:14:"payment/cheque";i:70;s:11:"payment/cod";i:71;s:21:"payment/free_checkout";i:72;s:22:"payment/klarna_account";i:73;s:22:"payment/klarna_invoice";i:74;s:14:"payment/liqpay";i:75;s:20:"payment/moneybookers";i:76;s:14:"payment/nochex";i:77;s:15:"payment/paymate";i:78;s:16:"payment/paypoint";i:79;s:13:"payment/payza";i:80;s:26:"payment/perpetual_payments";i:81;s:18:"payment/pp_express";i:82;s:25:"payment/pp_payflow_iframe";i:83;s:14:"payment/pp_pro";i:84;s:21:"payment/pp_pro_iframe";i:85;s:17:"payment/pp_pro_pf";i:86;s:17:"payment/pp_pro_uk";i:87;s:19:"payment/pp_standard";i:88;s:15:"payment/sagepay";i:89;s:22:"payment/sagepay_direct";i:90;s:18:"payment/sagepay_us";i:91;s:19:"payment/twocheckout";i:92;s:28:"payment/web_payment_software";i:93;s:16:"payment/worldpay";i:94;s:27:"report/affiliate_commission";i:95;s:22:"report/customer_credit";i:96;s:22:"report/customer_online";i:97;s:21:"report/customer_order";i:98;s:22:"report/customer_reward";i:99;s:24:"report/product_purchased";i:100;s:21:"report/product_viewed";i:101;s:18:"report/sale_coupon";i:102;s:17:"report/sale_order";i:103;s:18:"report/sale_return";i:104;s:20:"report/sale_shipping";i:105;s:15:"report/sale_tax";i:106;s:14:"sale/affiliate";i:107;s:12:"sale/contact";i:108;s:11:"sale/coupon";i:109;s:13:"sale/customer";i:110;s:20:"sale/customer_ban_ip";i:111;s:19:"sale/customer_group";i:112;s:10:"sale/order";i:113;s:14:"sale/recurring";i:114;s:11:"sale/return";i:115;s:12:"sale/voucher";i:116;s:18:"sale/voucher_theme";i:117;s:15:"setting/setting";i:118;s:13:"setting/store";i:119;s:16:"shipping/auspost";i:120;s:17:"shipping/citylink";i:121;s:14:"shipping/fedex";i:122;s:13:"shipping/flat";i:123;s:13:"shipping/free";i:124;s:13:"shipping/item";i:125;s:23:"shipping/parcelforce_48";i:126;s:15:"shipping/pickup";i:127;s:19:"shipping/royal_mail";i:128;s:12:"shipping/ups";i:129;s:13:"shipping/usps";i:130;s:15:"shipping/weight";i:131;s:11:"tool/backup";i:132;s:14:"tool/error_log";i:133;s:12:"total/coupon";i:134;s:12:"total/credit";i:135;s:14:"total/handling";i:136;s:16:"total/klarna_fee";i:137;s:19:"total/low_order_fee";i:138;s:12:"total/reward";i:139;s:14:"total/shipping";i:140;s:15:"total/sub_total";i:141;s:9:"total/tax";i:142;s:11:"total/total";i:143;s:13:"total/voucher";i:144;s:9:"user/user";i:145;s:20:"user/user_permission";i:146;s:12:"module/legal";i:147;s:10:"module/oxy";i:148;s:17:"module/oxy_banner";i:149;s:33:"module/oxy_custom_content_manager";i:150;s:25:"module/oxy_product_slider";i:151;s:14:"module/special";i:152;s:14:"module/special";}s:6:"modify";a:153:{i:0;s:17:"catalog/attribute";i:1;s:23:"catalog/attribute_group";i:2;s:16:"catalog/category";i:3;s:16:"catalog/download";i:4;s:14:"catalog/filter";i:5;s:19:"catalog/information";i:6;s:20:"catalog/manufacturer";i:7;s:14:"catalog/option";i:8;s:15:"catalog/product";i:9;s:15:"catalog/profile";i:10;s:14:"catalog/review";i:11;s:18:"common/filemanager";i:12;s:13:"design/banner";i:13;s:19:"design/custom_field";i:14;s:13:"design/layout";i:15;s:14:"extension/feed";i:16;s:17:"extension/manager";i:17;s:16:"extension/module";i:18;s:17:"extension/openbay";i:19;s:17:"extension/payment";i:20;s:18:"extension/shipping";i:21;s:15:"extension/total";i:22;s:16:"feed/google_base";i:23;s:19:"feed/google_sitemap";i:24;s:20:"localisation/country";i:25;s:21:"localisation/currency";i:26;s:21:"localisation/geo_zone";i:27;s:21:"localisation/language";i:28;s:25:"localisation/length_class";i:29;s:25:"localisation/order_status";i:30;s:26:"localisation/return_action";i:31;s:26:"localisation/return_reason";i:32;s:26:"localisation/return_status";i:33;s:25:"localisation/stock_status";i:34;s:22:"localisation/tax_class";i:35;s:21:"localisation/tax_rate";i:36;s:25:"localisation/weight_class";i:37;s:17:"localisation/zone";i:38;s:14:"module/account";i:39;s:16:"module/affiliate";i:40;s:29:"module/amazon_checkout_layout";i:41;s:13:"module/banner";i:42;s:17:"module/bestseller";i:43;s:15:"module/carousel";i:44;s:15:"module/category";i:45;s:18:"module/ebaydisplay";i:46;s:15:"module/featured";i:47;s:13:"module/filter";i:48;s:18:"module/google_talk";i:49;s:18:"module/information";i:50;s:13:"module/latest";i:51;s:17:"module/openbaypro";i:52;s:16:"module/pp_layout";i:53;s:16:"module/slideshow";i:54;s:14:"module/special";i:55;s:12:"module/store";i:56;s:14:"module/welcome";i:57;s:14:"openbay/amazon";i:58;s:22:"openbay/amazon_listing";i:59;s:22:"openbay/amazon_product";i:60;s:16:"openbay/amazonus";i:61;s:24:"openbay/amazonus_listing";i:62;s:24:"openbay/amazonus_product";i:63;s:20:"openbay/ebay_profile";i:64;s:21:"openbay/ebay_template";i:65;s:15:"openbay/openbay";i:66;s:23:"payment/amazon_checkout";i:67;s:24:"payment/authorizenet_aim";i:68;s:21:"payment/bank_transfer";i:69;s:14:"payment/cheque";i:70;s:11:"payment/cod";i:71;s:21:"payment/free_checkout";i:72;s:22:"payment/klarna_account";i:73;s:22:"payment/klarna_invoice";i:74;s:14:"payment/liqpay";i:75;s:20:"payment/moneybookers";i:76;s:14:"payment/nochex";i:77;s:15:"payment/paymate";i:78;s:16:"payment/paypoint";i:79;s:13:"payment/payza";i:80;s:26:"payment/perpetual_payments";i:81;s:18:"payment/pp_express";i:82;s:25:"payment/pp_payflow_iframe";i:83;s:14:"payment/pp_pro";i:84;s:21:"payment/pp_pro_iframe";i:85;s:17:"payment/pp_pro_pf";i:86;s:17:"payment/pp_pro_uk";i:87;s:19:"payment/pp_standard";i:88;s:15:"payment/sagepay";i:89;s:22:"payment/sagepay_direct";i:90;s:18:"payment/sagepay_us";i:91;s:19:"payment/twocheckout";i:92;s:28:"payment/web_payment_software";i:93;s:16:"payment/worldpay";i:94;s:27:"report/affiliate_commission";i:95;s:22:"report/customer_credit";i:96;s:22:"report/customer_online";i:97;s:21:"report/customer_order";i:98;s:22:"report/customer_reward";i:99;s:24:"report/product_purchased";i:100;s:21:"report/product_viewed";i:101;s:18:"report/sale_coupon";i:102;s:17:"report/sale_order";i:103;s:18:"report/sale_return";i:104;s:20:"report/sale_shipping";i:105;s:15:"report/sale_tax";i:106;s:14:"sale/affiliate";i:107;s:12:"sale/contact";i:108;s:11:"sale/coupon";i:109;s:13:"sale/customer";i:110;s:20:"sale/customer_ban_ip";i:111;s:19:"sale/customer_group";i:112;s:10:"sale/order";i:113;s:14:"sale/recurring";i:114;s:11:"sale/return";i:115;s:12:"sale/voucher";i:116;s:18:"sale/voucher_theme";i:117;s:15:"setting/setting";i:118;s:13:"setting/store";i:119;s:16:"shipping/auspost";i:120;s:17:"shipping/citylink";i:121;s:14:"shipping/fedex";i:122;s:13:"shipping/flat";i:123;s:13:"shipping/free";i:124;s:13:"shipping/item";i:125;s:23:"shipping/parcelforce_48";i:126;s:15:"shipping/pickup";i:127;s:19:"shipping/royal_mail";i:128;s:12:"shipping/ups";i:129;s:13:"shipping/usps";i:130;s:15:"shipping/weight";i:131;s:11:"tool/backup";i:132;s:14:"tool/error_log";i:133;s:12:"total/coupon";i:134;s:12:"total/credit";i:135;s:14:"total/handling";i:136;s:16:"total/klarna_fee";i:137;s:19:"total/low_order_fee";i:138;s:12:"total/reward";i:139;s:14:"total/shipping";i:140;s:15:"total/sub_total";i:141;s:9:"total/tax";i:142;s:11:"total/total";i:143;s:13:"total/voucher";i:144;s:9:"user/user";i:145;s:20:"user/user_permission";i:146;s:12:"module/legal";i:147;s:10:"module/oxy";i:148;s:17:"module/oxy_banner";i:149;s:33:"module/oxy_custom_content_manager";i:150;s:25:"module/oxy_product_slider";i:151;s:14:"module/special";i:152;s:14:"module/special";}}'),
(10, 'Demonstration', ''),
(11, 'Seller', 'a:2:{s:6:"access";a:1:{i:0;s:13:"sale/customer";}s:6:"modify";a:1:{i:0;s:13:"sale/customer";}}');

-- --------------------------------------------------------

--
-- Structure de la table `voucher`
--

CREATE TABLE IF NOT EXISTS `voucher` (
  `voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`voucher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `voucher_history`
--

CREATE TABLE IF NOT EXISTS `voucher_history` (
  `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`voucher_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `voucher_theme`
--

CREATE TABLE IF NOT EXISTS `voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`voucher_theme_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `voucher_theme`
--

INSERT INTO `voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'data/demo/canon_eos_5d_2.jpg'),
(7, 'data/demo/gift-voucher-birthday.jpg'),
(6, 'data/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `voucher_theme_description`
--

CREATE TABLE IF NOT EXISTS `voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`voucher_theme_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `voucher_theme_description`
--

INSERT INTO `voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Structure de la table `weight_class`
--

CREATE TABLE IF NOT EXISTS `weight_class` (
  `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  PRIMARY KEY (`weight_class_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `weight_class`
--

INSERT INTO `weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Structure de la table `weight_class_description`
--

CREATE TABLE IF NOT EXISTS `weight_class_description` (
  `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  PRIMARY KEY (`weight_class_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `weight_class_description`
--

INSERT INTO `weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Structure de la table `zone`
--

CREATE TABLE IF NOT EXISTS `zone` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4033 ;

--
-- Contenu de la table `zone`
--

INSERT INTO `zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M''Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu''a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Aragatsotn', 'AGT', 1),
(181, 11, 'Ararat', 'ARR', 1),
(182, 11, 'Armavir', 'ARM', 1),
(183, 11, 'Geghark''unik''', 'GEG', 1),
(184, 11, 'Kotayk''', 'KOT', 1),
(185, 11, 'Lorri', 'LOR', 1),
(186, 11, 'Shirak', 'SHI', 1),
(187, 11, 'Syunik''', 'SYU', 1),
(188, 11, 'Tavush', 'TAV', 1),
(189, 11, 'Vayots'' Dzor', 'VAY', 1),
(190, 11, 'Yerevan', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', 1),
(202, 14, 'Ober&ouml;sterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore''s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Brestskaya (Brest)', 'BR', 1),
(338, 20, 'Homyel''skaya (Homyel'')', 'HO', 1),
(339, 20, 'Horad Minsk', 'HM', 1),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 1),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 1),
(342, 20, 'Minskaya', 'MI', 1),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George''s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith''s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M''Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O''Hi', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chi', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovar-Bilogora', 'BB', 1),
(848, 53, 'City of Zagreb', 'CZ', 1),
(849, 53, 'Dubrovnik-Neretva', 'DN', 1),
(850, 53, 'Istra', 'IS', 1),
(851, 53, 'Karlovac', 'KA', 1),
(852, 53, 'Koprivnica-Krizevci', 'KK', 1),
(853, 53, 'Krapina-Zagorje', 'KZ', 1),
(854, 53, 'Lika-Senj', 'LS', 1),
(855, 53, 'Medimurje', 'ME', 1),
(856, 53, 'Osijek-Baranja', 'OB', 1),
(857, 53, 'Pozega-Slavonia', 'PS', 1),
(858, 53, 'Primorje-Gorski Kotar', 'PG', 1),
(859, 53, 'Sibenik', 'SI', 1),
(860, 53, 'Sisak-Moslavina', 'SM', 1),
(861, 53, 'Slavonski Brod-Posavina', 'SB', 1),
(862, 53, 'Split-Dalmatia', 'SD', 1),
(863, 53, 'Varazdin', 'VA', 1),
(864, 53, 'Virovitica-Podravina', 'VP', 1),
(865, 53, 'Vukovar-Srijem', 'VS', 1),
(866, 53, 'Zadar-Knin', 'ZK', 1),
(867, 53, 'Zagreb', 'ZA', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '''Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma''iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa''id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina''', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina''', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan Laani', 'AL', 1),
(1086, 72, 'Etela-Suomen Laani', 'ES', 1),
(1087, 72, 'Ita-Suomen Laani', 'IS', 1),
(1088, 72, 'Lansi-Suomen Laani', 'LS', 1),
(1089, 72, 'Lapin Lanani', 'LA', 1),
(1090, 72, 'Oulun Laani', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d''Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Th&uuml;ringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand''Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1428, 97, 'Bacs-Kiskun', 'BK', 1),
(1429, 97, 'Baranya', 'BA', 1),
(1430, 97, 'Bekes', 'BE', 1),
(1431, 97, 'Bekescsaba', 'BS', 1),
(1432, 97, 'Borsod-Abauj-Zemplen', 'BZ', 1),
(1433, 97, 'Budapest', 'BU', 1),
(1434, 97, 'Csongrad', 'CS', 1),
(1435, 97, 'Debrecen', 'DE', 1),
(1436, 97, 'Dunaujvaros', 'DU', 1),
(1437, 97, 'Eger', 'EG', 1),
(1438, 97, 'Fejer', 'FE', 1),
(1439, 97, 'Gyor', 'GY', 1),
(1440, 97, 'Gyor-Moson-Sopron', 'GM', 1),
(1441, 97, 'Hajdu-Bihar', 'HB', 1),
(1442, 97, 'Heves', 'HE', 1),
(1443, 97, 'Hodmezovasarhely', 'HO', 1),
(1444, 97, 'Jasz-Nagykun-Szolnok', 'JN', 1),
(1445, 97, 'Kaposvar', 'KA', 1),
(1446, 97, 'Kecskemet', 'KE', 1),
(1447, 97, 'Komarom-Esztergom', 'KO', 1),
(1448, 97, 'Miskolc', 'MI', 1),
(1449, 97, 'Nagykanizsa', 'NA', 1),
(1450, 97, 'Nograd', 'NO', 1),
(1451, 97, 'Nyiregyhaza', 'NY', 1),
(1452, 97, 'Pecs', 'PE', 1),
(1453, 97, 'Pest', 'PS', 1),
(1454, 97, 'Somogy', 'SO', 1),
(1455, 97, 'Sopron', 'SP', 1),
(1456, 97, 'Szabolcs-Szatmar-Bereg', 'SS', 1),
(1457, 97, 'Szeged', 'SZ', 1),
(1458, 97, 'Szekesfehervar', 'SE', 1),
(1459, 97, 'Szolnok', 'SL', 1),
(1460, 97, 'Szombathely', 'SM', 1),
(1461, 97, 'Tatabanya', 'TA', 1),
(1462, 97, 'Tolna', 'TO', 1),
(1463, 97, 'Vas', 'VA', 1),
(1464, 97, 'Veszprem', 'VE', 1),
(1465, 97, 'Zala', 'ZA', 1),
(1466, 97, 'Zalaegerszeg', 'ZZ', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Pondicherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'BoDeTaBek', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta Raya', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1);
INSERT INTO `zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta''mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1),
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be''er Sheva', 'BS', 1),
(1613, 104, 'Bika''at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '''Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al ''Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa''', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa''', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma''an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Almaty', 'AL', 1),
(1717, 109, 'Almaty City', 'AC', 1),
(1718, 109, 'Aqmola', 'AM', 1),
(1719, 109, 'Aqtobe', 'AQ', 1),
(1720, 109, 'Astana City', 'AS', 1),
(1721, 109, 'Atyrau', 'AT', 1),
(1722, 109, 'Batys Qazaqstan', 'BA', 1),
(1723, 109, 'Bayqongyr City', 'BY', 1),
(1724, 109, 'Mangghystau', 'MA', 1),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 1),
(1726, 109, 'Pavlodar', 'PA', 1),
(1727, 109, 'Qaraghandy', 'QA', 1),
(1728, 109, 'Qostanay', 'QO', 1),
(1729, 109, 'Qyzylorda', 'QY', 1),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 1),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 1),
(1732, 109, 'Zhambyl', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P''yongan-bukto', 'PYB', 1),
(1769, 112, 'P''yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P''yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch''ungch''ong-bukto', 'CO', 1),
(1774, 113, 'Ch''ungch''ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch''on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t''ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al ''Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra''', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1819, 117, 'Aizkraukles Rajons', 'AIZ', 1),
(1820, 117, 'Aluksnes Rajons', 'ALU', 1),
(1821, 117, 'Balvu Rajons', 'BAL', 1),
(1822, 117, 'Bauskas Rajons', 'BAU', 1),
(1823, 117, 'Cesu Rajons', 'CES', 1),
(1824, 117, 'Daugavpils Rajons', 'DGR', 1),
(1825, 117, 'Dobeles Rajons', 'DOB', 1),
(1826, 117, 'Gulbenes Rajons', 'GUL', 1),
(1827, 117, 'Jekabpils Rajons', 'JEK', 1),
(1828, 117, 'Jelgavas Rajons', 'JGR', 1),
(1829, 117, 'Kraslavas Rajons', 'KRA', 1),
(1830, 117, 'Kuldigas Rajons', 'KUL', 1),
(1831, 117, 'Liepajas Rajons', 'LPR', 1),
(1832, 117, 'Limbazu Rajons', 'LIM', 1),
(1833, 117, 'Ludzas Rajons', 'LUD', 1),
(1834, 117, 'Madonas Rajons', 'MAD', 1),
(1835, 117, 'Ogres Rajons', 'OGR', 1),
(1836, 117, 'Preilu Rajons', 'PRE', 1),
(1837, 117, 'Rezeknes Rajons', 'RZR', 1),
(1838, 117, 'Rigas Rajons', 'RGR', 1),
(1839, 117, 'Saldus Rajons', 'SAL', 1),
(1840, 117, 'Talsu Rajons', 'TAL', 1),
(1841, 117, 'Tukuma Rajons', 'TUK', 1),
(1842, 117, 'Valkas Rajons', 'VLK', 1),
(1843, 117, 'Valmieras Rajons', 'VLM', 1),
(1844, 117, 'Ventspils Rajons', 'VSR', 1),
(1845, 117, 'Daugavpils', 'DGV', 1),
(1846, 117, 'Jelgava', 'JGV', 1),
(1847, 117, 'Jurmala', 'JUR', 1),
(1848, 117, 'Liepaja', 'LPK', 1),
(1849, 117, 'Rezekne', 'RZK', 1),
(1850, 117, 'Riga', 'RGA', 1),
(1851, 117, 'Ventspils', 'VSL', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale''s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha''s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al ''Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati''', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'JO', 1),
(1972, 129, 'Kedah', 'KE', 1),
(1973, 129, 'Kelantan', 'KL', 1),
(1974, 129, 'Labuan', 'LA', 1),
(1975, 129, 'Melaka', 'ME', 1),
(1976, 129, 'Negeri Sembilan', 'NS', 1),
(1977, 129, 'Pahang', 'PA', 1),
(1978, 129, 'Perak', 'PE', 1),
(1979, 129, 'Perlis', 'PR', 1),
(1980, 129, 'Pulau Pinang', 'PP', 1),
(1981, 129, 'Sabah', 'SA', 1),
(1982, 129, 'Sarawak', 'SR', 1),
(1983, 129, 'Selangor', 'SE', 1),
(1984, 129, 'Terengganu', 'TE', 1),
(1985, 129, 'Wilayah Persekutuan', 'WP', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord Brabant', 'NB', 1),
(2336, 150, 'Noord Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke''s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa''id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Abakan', 'AB', 1),
(2722, 176, 'Aginskoye', 'AG', 1),
(2723, 176, 'Anadyr', 'AN', 1),
(2724, 176, 'Arkahangelsk', 'AR', 1),
(2725, 176, 'Astrakhan', 'AS', 1),
(2726, 176, 'Barnaul', 'BA', 1),
(2727, 176, 'Belgorod', 'BE', 1),
(2728, 176, 'Birobidzhan', 'BI', 1),
(2729, 176, 'Blagoveshchensk', 'BL', 1),
(2730, 176, 'Bryansk', 'BR', 1),
(2731, 176, 'Cheboksary', 'CH', 1),
(2732, 176, 'Chelyabinsk', 'CL', 1),
(2733, 176, 'Cherkessk', 'CR', 1),
(2734, 176, 'Chita', 'CI', 1),
(2735, 176, 'Dudinka', 'DU', 1),
(2736, 176, 'Elista', 'EL', 1),
(2737, 176, 'Gomo-Altaysk', 'GO', 1),
(2738, 176, 'Gorno-Altaysk', 'GA', 1),
(2739, 176, 'Groznyy', 'GR', 1),
(2740, 176, 'Irkutsk', 'IR', 1),
(2741, 176, 'Ivanovo', 'IV', 1),
(2742, 176, 'Izhevsk', 'IZ', 1),
(2743, 176, 'Kalinigrad', 'KA', 1),
(2744, 176, 'Kaluga', 'KL', 1),
(2745, 176, 'Kasnodar', 'KS', 1),
(2746, 176, 'Kazan', 'KZ', 1),
(2747, 176, 'Kemerovo', 'KE', 1),
(2748, 176, 'Khabarovsk', 'KH', 1),
(2749, 176, 'Khanty-Mansiysk', 'KM', 1),
(2750, 176, 'Kostroma', 'KO', 1),
(2751, 176, 'Krasnodar', 'KR', 1),
(2752, 176, 'Krasnoyarsk', 'KN', 1),
(2753, 176, 'Kudymkar', 'KU', 1),
(2754, 176, 'Kurgan', 'KG', 1),
(2755, 176, 'Kursk', 'KK', 1),
(2756, 176, 'Kyzyl', 'KY', 1),
(2757, 176, 'Lipetsk', 'LI', 1),
(2758, 176, 'Magadan', 'MA', 1),
(2759, 176, 'Makhachkala', 'MK', 1),
(2760, 176, 'Maykop', 'MY', 1),
(2761, 176, 'Moscow', 'MO', 1),
(2762, 176, 'Murmansk', 'MU', 1),
(2763, 176, 'Nalchik', 'NA', 1),
(2764, 176, 'Naryan Mar', 'NR', 1),
(2765, 176, 'Nazran', 'NZ', 1),
(2766, 176, 'Nizhniy Novgorod', 'NI', 1),
(2767, 176, 'Novgorod', 'NO', 1),
(2768, 176, 'Novosibirsk', 'NV', 1),
(2769, 176, 'Omsk', 'OM', 1),
(2770, 176, 'Orel', 'OR', 1),
(2771, 176, 'Orenburg', 'OE', 1),
(2772, 176, 'Palana', 'PA', 1),
(2773, 176, 'Penza', 'PE', 1),
(2774, 176, 'Perm', 'PR', 1),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 1),
(2776, 176, 'Petrozavodsk', 'PT', 1),
(2777, 176, 'Pskov', 'PS', 1),
(2778, 176, 'Rostov-na-Donu', 'RO', 1),
(2779, 176, 'Ryazan', 'RY', 1),
(2780, 176, 'Salekhard', 'SL', 1),
(2781, 176, 'Samara', 'SA', 1),
(2782, 176, 'Saransk', 'SR', 1),
(2783, 176, 'Saratov', 'SV', 1),
(2784, 176, 'Smolensk', 'SM', 1),
(2785, 176, 'St. Petersburg', 'SP', 1),
(2786, 176, 'Stavropol', 'ST', 1),
(2787, 176, 'Syktyvkar', 'SY', 1),
(2788, 176, 'Tambov', 'TA', 1),
(2789, 176, 'Tomsk', 'TO', 1),
(2790, 176, 'Tula', 'TU', 1),
(2791, 176, 'Tura', 'TR', 1),
(2792, 176, 'Tver', 'TV', 1),
(2793, 176, 'Tyumen', 'TY', 1),
(2794, 176, 'Ufa', 'UF', 1),
(2795, 176, 'Ul''yanovsk', 'UL', 1),
(2796, 176, 'Ulan-Ude', 'UU', 1),
(2797, 176, 'Ust''-Ordynskiy', 'US', 1),
(2798, 176, 'Vladikavkaz', 'VL', 1),
(2799, 176, 'Vladimir', 'VA', 1),
(2800, 176, 'Vladivostok', 'VV', 1),
(2801, 176, 'Volgograd', 'VG', 1),
(2802, 176, 'Vologda', 'VD', 1),
(2803, 176, 'Voronezh', 'VO', 1),
(2804, 176, 'Vyatka', 'VY', 1),
(2805, 176, 'Yakutsk', 'YA', 1),
(2806, 176, 'Yaroslavl', 'YR', 1),
(2807, 176, 'Yekaterinburg', 'YE', 1),
(2808, 176, 'Yoshkar-Ola', 'YO', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A''ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa''asaleleaga', 'FA', 1),
(2856, 181, 'Gaga''emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa''itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va''a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '''Asir', 'AS', 1),
(2882, 184, 'Ha''il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand'' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand'' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3031, 197, 'Ascension', 'A', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3033, 197, 'Tristan da Cunha', 'T', 1),
(3034, 199, 'A''ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa''iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa''iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1);
INSERT INTO `zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'G&auml;vleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'J&auml;mtland', 'Z', 1),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, '&Ouml;rebro', 'T', 1),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'S&ouml;dermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'V&auml;rmland', 'S', 1),
(3091, 203, 'V&auml;sterbotten', 'AC', 1),
(3092, 203, 'V&auml;sternorrland', 'Y', 1),
(3093, 203, 'V&auml;stmanland', 'U', 1),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graub&uuml;nden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Z&uuml;rich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1),
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t''ou', 'NT', 1),
(3145, 206, 'P''eng-hu', 'PH', 1),
(3146, 206, 'P''ing-tung', 'PT', 1),
(3147, 206, 'T''ai-chung', 'TG', 1),
(3148, 206, 'T''ai-nan', 'TA', 1),
(3149, 206, 'T''ai-pei county', 'TP', 1),
(3150, 206, 'T''ai-tung', 'TT', 1),
(3151, 206, 'T''ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T''ai-chung', 'TH', 1),
(3157, 206, 'T''ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T''ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha''apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava''u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakir', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Cherkasy', 'CK', 1),
(3481, 220, 'Chernihiv', 'CH', 1),
(3482, 220, 'Chernivtsi', 'CV', 1),
(3483, 220, 'Crimea', 'CR', 1),
(3484, 220, 'Dnipropetrovs''k', 'DN', 1),
(3485, 220, 'Donets''k', 'DO', 1),
(3486, 220, 'Ivano-Frankivs''k', 'IV', 1),
(3487, 220, 'Kharkiv Kherson', 'KL', 1),
(3488, 220, 'Khmel''nyts''kyy', 'KM', 1),
(3489, 220, 'Kirovohrad', 'KR', 1),
(3490, 220, 'Kiev', 'KV', 1),
(3491, 220, 'Kyyiv', 'KY', 1),
(3492, 220, 'Luhans''k', 'LU', 1),
(3493, 220, 'L''viv', 'LV', 1),
(3494, 220, 'Mykolayiv', 'MY', 1),
(3495, 220, 'Odesa', 'OD', 1),
(3496, 220, 'Poltava', 'PO', 1),
(3497, 220, 'Rivne', 'RI', 1),
(3498, 220, 'Sevastopol', 'SE', 1),
(3499, 220, 'Sumy', 'SU', 1),
(3500, 220, 'Ternopil''', 'TE', 1),
(3501, 220, 'Vinnytsya', 'VI', 1),
(3502, 220, 'Volyn''', 'VO', 1),
(3503, 220, 'Zakarpattya', 'ZK', 1),
(3504, 220, 'Zaporizhzhya', 'ZA', 1),
(3505, 220, 'Zhytomyr', 'ZH', 1),
(3506, 221, 'Abu Zaby', 'AZ', 1),
(3507, 221, '''Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubayy', 'DU', 1),
(3511, 221, 'R''as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg''ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog''iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma''rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa''dah', 'SD', 1),
(3805, 235, 'San''a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta''izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3895, 105, 'Medio Campidano', 'VS', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3902, 105, 'Ogliastra', 'OG', 1),
(3903, 105, 'Olbia-Tempio', 'OT', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3972, 222, 'Isle of Man', 'IOM', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4019, 243, 'Kosovo', 'KM', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1);

-- --------------------------------------------------------

--
-- Structure de la table `zone_to_geo_zone`
--

CREATE TABLE IF NOT EXISTS `zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`zone_to_geo_zone_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=315 ;

--
-- Contenu de la table `zone_to_geo_zone`
--

INSERT INTO `zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(57, 222, 0, 3, '2010-02-26 22:33:24', '0000-00-00 00:00:00'),
(65, 222, 0, 4, '2010-12-15 15:18:13', '0000-00-00 00:00:00'),
(66, 81, 0, 5, '2015-04-13 20:40:43', '0000-00-00 00:00:00'),
(67, 14, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(68, 21, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(69, 33, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(70, 53, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(71, 55, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(72, 56, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(73, 57, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(74, 67, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(75, 72, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(76, 74, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(77, 84, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(78, 97, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(79, 103, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(80, 105, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(81, 117, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(82, 123, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(83, 124, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(84, 132, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(85, 150, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(86, 170, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(87, 171, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(88, 175, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(89, 189, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(90, 190, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(91, 195, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(92, 203, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(93, 222, 0, 6, '2015-04-13 20:40:44', '0000-00-00 00:00:00'),
(94, 244, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(95, 1, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(96, 2, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(97, 3, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(98, 4, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(99, 5, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(100, 6, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(101, 7, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(102, 8, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(103, 9, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(104, 10, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(105, 11, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(106, 12, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(107, 13, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(108, 15, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(109, 16, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(110, 17, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(111, 18, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(112, 19, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(113, 20, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(114, 22, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(115, 23, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(116, 24, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(117, 25, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(118, 26, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(119, 245, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(120, 27, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(121, 28, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(122, 29, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(123, 30, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(124, 31, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(125, 32, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(126, 34, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(127, 35, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(128, 36, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(129, 37, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(130, 38, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(131, 251, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(132, 39, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(133, 40, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(134, 41, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(135, 42, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(136, 43, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(137, 44, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(138, 45, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(139, 46, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(140, 47, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(141, 48, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(142, 49, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(143, 50, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(144, 51, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(145, 52, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(146, 54, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(147, 246, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(148, 237, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(149, 58, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(150, 59, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(151, 60, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(152, 61, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(153, 62, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(154, 63, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(155, 64, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(156, 65, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(157, 66, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(158, 68, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(159, 69, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(160, 70, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(161, 71, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(162, 75, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(163, 76, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(164, 77, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(165, 126, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(166, 78, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(167, 79, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(168, 80, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(169, 82, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(170, 83, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(171, 85, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(172, 86, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(173, 87, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(174, 88, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(175, 89, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(176, 241, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(177, 90, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(178, 91, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(179, 92, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(180, 93, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(181, 94, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(182, 95, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(183, 96, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(184, 98, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(185, 99, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(186, 100, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(187, 101, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(188, 102, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(189, 104, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(190, 106, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(191, 107, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(192, 240, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(193, 108, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(194, 109, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(195, 110, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(196, 111, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(197, 113, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(198, 114, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(199, 115, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(200, 116, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(201, 118, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(202, 119, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(203, 120, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(204, 121, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(205, 122, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(206, 125, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(207, 127, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(208, 128, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(209, 129, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(210, 130, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(211, 131, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(212, 133, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(213, 134, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(214, 135, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(215, 136, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(216, 137, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(217, 138, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(218, 139, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(219, 140, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(220, 141, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(221, 142, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(222, 242, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(223, 143, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(224, 144, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(225, 145, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(226, 146, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(227, 147, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(228, 148, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(229, 149, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(230, 151, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(231, 152, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(232, 153, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(233, 154, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(234, 155, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(235, 156, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(236, 157, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(237, 158, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(238, 112, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(239, 159, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(240, 160, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(241, 161, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(242, 162, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(243, 163, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(244, 247, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(245, 164, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(246, 165, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(247, 166, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(248, 167, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(249, 168, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(250, 169, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(251, 172, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(252, 173, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(253, 174, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(254, 176, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(255, 177, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(256, 178, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(257, 179, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(258, 180, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(259, 181, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(260, 182, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(261, 183, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(262, 184, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(263, 185, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(264, 243, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(265, 186, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(266, 187, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(267, 188, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(268, 191, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(269, 192, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(270, 193, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(271, 194, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(272, 248, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(273, 196, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(274, 249, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(275, 197, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(276, 250, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(277, 198, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(278, 199, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(279, 200, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(280, 201, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(281, 202, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(282, 204, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(283, 205, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(284, 206, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(285, 207, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(286, 208, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(287, 209, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(288, 210, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(289, 211, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(290, 212, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(291, 213, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(292, 214, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(293, 215, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(294, 216, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(295, 217, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(296, 218, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(297, 219, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(298, 220, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(299, 221, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(300, 223, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(301, 224, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(302, 225, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(303, 226, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(304, 227, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(305, 228, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(306, 229, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(307, 230, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(308, 231, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(309, 232, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(310, 233, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(311, 234, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(312, 235, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(313, 238, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00'),
(314, 239, 0, 7, '2015-04-13 20:40:45', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
