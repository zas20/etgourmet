<?php
class ModelTotalSubTotal extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$this->language->load('total/sub_total');
		
		$sub_total = $this->cart->getSubTotal();
		
		if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$sub_total += $voucher['amount'];
			}
		}
		

                
		/** OSWorX LEGAL price incl. tax */
		$tax_amount = 0;
		foreach( $taxes as $k => $v ) {
			$tax_amount += $v;
		}

		$amount = $sub_total + $tax_amount;
		/** OSWorX end */
                
            
		$total_data[] = array( 
			'code'       => 'sub_total',
			'title'      => $this->language->get('text_sub_total'),
			'text'       => $this->currency->format( $amount, '', '', true, false ), /** OSWorX LEGAL price incl. tax */
			'value'      => $amount, /** OSWorX LEGAL price incl. tax */
			'sort_order' => $this->config->get('sub_total_sort_order')
		);
		
		$total += $sub_total;
	}
}
?>