<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/halalexoticmeats/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/halalexoticmeats/');

// DIR
define('DIR_APPLICATION', 'E:\wamp\www\halalexoticmeats/catalog/');
define('DIR_SYSTEM', 'E:\wamp\www\halalexoticmeats/system/');
define('DIR_DATABASE', 'E:\wamp\www\halalexoticmeats/system/database/');
define('DIR_LANGUAGE', 'E:\wamp\www\halalexoticmeats/catalog/language/');
define('DIR_TEMPLATE', 'E:\wamp\www\halalexoticmeats/catalog/view/theme/');
define('DIR_CONFIG', 'E:\wamp\www\halalexoticmeats/system/config/');
define('DIR_IMAGE', 'E:\wamp\www\halalexoticmeats/image/');
define('DIR_CACHE', 'E:\wamp\www\halalexoticmeats/system/cache/');
define('DIR_DOWNLOAD', 'E:\wamp\www\halalexoticmeats/download/');
define('DIR_LOGS', 'E:\wamp\www\halalexoticmeats/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'halalexoticmeats');
define('DB_PREFIX', '');
?>