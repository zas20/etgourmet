<?php
/**
 * @version		$Id: legal.php 3580 2014-04-04 08:41:00Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2013 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

class ModelModuleLegal extends Model
{
    public $_version = '2.0.7';

    /**
     * proxy function to generate curreetn date for db.queries
     * note: generating date with php is faster than with mysql
     * @return string
     */
    private function getNow() {
        return date( 'Y-m-d H:i:s' );
    }

    /**
     * proxy function to generate a null date for db.queries
     * @return string
     */
    private function getNullDate() {
        return '0000-00-00 00:00:00';
    }

    /**
     * update all products with new taxclass id
     * @param int   $id
     */
	public function updateProducts( $id ) {
		$sql = '
        UPDATE
            `' . DB_PREFIX . 'product`
        SET
            tax_class_id = ' . $id . ',
            date_modified = \'' . $this->getNow() . '\'';

        $this->db->query( $sql );
	}

    /**
     * get taxrate data - depending in OC version
     * @param array $data
     * @return array
     */
    public function getTaxRates( $data = array() ) {
		$sql = '
        SELECT
            tr.tax_rate_id, tr.' . $data['name'] . ' AS name, tr.rate,'
            . ( $data['type'] ? ' tr.type,' : '' )
            . ' tr.date_added, tr.date_modified,
            gz.name AS geo_zone
        FROM
            `' .DB_PREFIX. 'tax_rate` AS tr
        LEFT JOIN `' .DB_PREFIX. 'geo_zone` AS gz
            ON ( tr.geo_zone_id = gz.geo_zone_id )
        ORDER BY
            tr.' . $data['name'] . ' ASC';

		if(
            isset( $data['start'] )
            || isset( $data['limit'] )
        )
        {
			if( $data['start'] < 0 ) {
				$data['start'] = 0;
			}

			if( $data['limit'] < 1 ) {
				$data['limit'] = 20;
			}

			$sql .= '
            LIMIT ' . (int) $data['start'] . ',' . (int)$data['limit'];
		}

		$query = $this->db->query( $sql );

		return $query->rows;
	}

    /**
     * add taxrate - depending on OC version
     * @param array $data
     */
    public function addTaxRate( $data ) {
        $sql = '
        INSERT
        INTO
            `' . DB_PREFIX . 'tax_rate`
        SET '
            . $data['fieldName'] . ' = \'' . $this->db->escape( $data['name'] ) . '\',
            rate = \'' . (float) $data['rate'] . '\','
            . ( $data['type'] ? '`type` = \'' . $this->db->escape( $data['type'] ) . '\',' : '' )
            . ' geo_zone_id = \'' . (int) $data['geo_zone_id'] . '\',
            date_added = \'' . $this->getNow() . '\',
            date_modified = \'' . $this->getNow() . '\'';

		$this->db->query( $sql );

		if( isset( $data['tax_rate_customer_group'] ) ) {
            $tax_rate_id = $this->db->getLastId();

			foreach( $data['tax_rate_customer_group'] as $customer_group_id ) {
                $sql = '
                INSERT
                INTO
                    `' .DB_PREFIX. 'tax_rate_to_customer_group`
                SET
                    tax_rate_id = \'' . (int) $tax_rate_id . '\',
                    customer_group_id = \'' . (int) $customer_group_id . '\'';

				$this->db->query( $sql );
			}
		}
	}

    /**
     * additional function for oc <= 151, because there the taxrate is already stored
     * @param array $args
     */
    public function updateTaxRate( $args ) {
        $sql = '
        UPDATE
            `' . DB_PREFIX . 'tax_rate`
        SET
            tax_class_id = ' . $args['tax_class_id'] . ',
            date_modified = \'' . $this->getNow() . '\'
        WHERE
            tax_rate_id = ' . (int) $args['tax_rate_id'];

        $this->db->query( $sql );
    }

    /**
     * depending on the opencart version,
     * 1. add tax class
     * 2. either add or update the taxrate
     * @param array $data
     */
    public function addTaxClass( $data ) {
        $sql = '
        INSERT
        INTO `' .DB_PREFIX. 'tax_class`
        SET
            title = \'' . $this->db->escape( $data['title'] ) . '\',
            description = \'' . $this->db->escape( $data['description'] ) . '\',
            date_added = \'' . $this->getNow() . '\'';

		$this->db->query( $sql );

		$tax_class_id = $this->db->getLastId();

		if( isset( $data['tax_rule'] ) ) {
            // opencart > 151
			foreach( $data['tax_rule'] as $tax_rule ) {
                $sql = '
                INSERT
                INTO `' .DB_PREFIX. 'tax_rule`
                SET
                    tax_class_id = \'' . (int) $tax_class_id . '\',
                    tax_rate_id = \'' . (int) $tax_rule['tax_rate_id'] . '\',
                    based = \'' . $this->db->escape( $tax_rule['based'] ) . '\',
                    priority = \'' . (int) $tax_rule['priority'] . '\'';

				$this->db->query( $sql );
			}
		}else{
            // opencart <= 151
            $args = array(
                'tax_class_id'  => $tax_class_id,
                'tax_rate_id'   => $data['tax_rate_id']
            );
            $this->updateTaxRate( $args );
		}

		$this->cache->delete('tax_class');
	}

    /**
     * because in OC < 1.5.2.x serialized vars are nut unserlialized
     * get a specific setting
     * @param string    $group  name if settings group to fetch
     * @param int       $store id
     * @return array
     */
    public function getSetting( $group, $store_id = 0 ) {
		$ret = array();
		$sql = '
        SELECT
            *
        FROM
            `' .DB_PREFIX. 'setting`
        WHERE
            store_id = \'' . (int) $store_id . '\'
        AND
            `group` = \'' . $this->db->escape( $group ) . '\'';

		$query = $this->db->query( $sql );

		foreach( $query->rows as $result ) {
			if( !$result['serialized'] ) {
				$ret[$result['key']] = $result['value'];
			}else{
				$ret[$result['key']] = unserialize( $result['value'] );
			}
		}

		return $ret;
	}

    /**
     * get a geozone id by name
     * @param string    name of geozone
     * @return int
     */
    public function getGeoZoneIdByName( $str ) {
        $sql = '
        SELECT
            geo_zone_id
        FROM
            `' .DB_PREFIX. 'geo_zone`
        WHERE
            name = \'' .  $str . '\'';

        $query = $this->db->query( $sql );

		return $query->row['geo_zone_id'];
    }

    /**
     * get all products without attributes
     * @return array
     */
    public function checkAttributes() {
        // get first all products
        $sql = '
        SELECT
            p.product_id, pd.name, pd.meta_description AS meta
        FROM
            `' .DB_PREFIX. 'product` AS p
        LEFT JOIN
            `' .DB_PREFIX. 'product_description` AS pd
            ON( pd.product_id = p.product_id )
        WHERE
            p.status = \'1\'
        AND
            pd.language_id = \'' . (int) $this->config->get('config_language_id') . '\'
        ORDER BY
            LCASE( pd.name ) ASC';

        $query = $this->db->query( $sql );

        // now check for attributes
        if( $query->num_rows ) {
            $resultProducts = $query->rows;

            foreach( $resultProducts as $k => $v ) {
                $sql = '
                SELECT
                    *
                FROM
                    `' .DB_PREFIX. 'product_attribute`
                WHERE
                    product_id = \'' . $v['product_id'] . '\'';

                $query = $this->db->query( $sql );

                if( $query->num_rows ) {
                    $resultProducts[$k]['attributes'] = true;
                }else{
                    $resultProducts[$k]['attributes'] = false;
                }
            }
        }

        return $resultProducts;
    }

    /**
     * get text for adding to emails
     * @param int   $information_id
     * @return array
     */
    public function getInformationForEmail( $information_id ) {
        $sql = '
        SELECT
            DISTINCT *
        FROM
            `' . DB_PREFIX . 'information` AS i
        LEFT JOIN
            `' . DB_PREFIX . 'information_description` AS id
            ON (i.information_id = id.information_id)
        LEFT JOIN
            `' . DB_PREFIX . 'information_to_store` AS i2s
            ON (i.information_id = i2s.information_id)
        WHERE
            i.information_id = \'' . (int) $information_id . '\'
        AND
            id.language_id = \'' . (int) $this->config->get( 'config_language_id' ) . '\'
        AND
            i2s.store_id = \'' . (int) $this->config->get( 'config_store_id' ) . '\'';

		$query = $this->db->query( $sql );

        if( $query->num_rows ) {
            return $this->replaceTextVars( $query->row );
        }else{
            return false;
        }
	}

    /**
     * replace possible placeholders in given text
     * @param string
     * @return string
     */
    public function replaceTextVars( $text ) {
        $find = array(
			'{owner}',
			'{address}',
			'{email}',
			'{telephone}',
			'{fax}',
            '{url}'
		);

		$replace = array(
			$this->config->get( 'config_owner' ),
			$this->config->get( 'config_address' ),
			$this->config->get( 'config_email' ),
			$this->config->get( 'config_telephone' ),
			$this->config->get( 'config_fax' ),
            HTTPS_SERVER
		);


		$text['description'] = str_replace( $find, $replace, $text['description'] );

        return $text;
    }

    /**
     * pre defined html body for email messages
     * @return string
     */
    public function getHtmlBody() {
        $ret = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #767676;">%s</body></html>';

        return $ret;
    }

    /**
     * create a new table if not exists
     * table: additional product attributes: product_additional
     * @since 3.0.0
     */
    public function checkForProductAdditional() {
        $sql = '
        CREATE TABLE IF NOT EXISTS `' .DB_PREFIX. 'product_additional` (
          `product_id` int(11) NOT NULL,
          `end_of_life` tinyint(1) NOT NULL DEFAULT \'0\',
          `condition` varchar(1) COLLATE utf8_general_ci NOT NULL,
          `guarantee` varchar(2) COLLATE utf8_general_ci NOT NULL,
          `package_content` varchar(5) COLLATE utf8_general_ci NOT NULL,
          `package_content_unit` varchar(4) COLLATE utf8_general_ci NOT NULL,
          `base_price_quantity` varchar(4) COLLATE utf8_general_ci NOT NULL,
          `base_price_unit` varchar(4) COLLATE utf8_general_ci NOT NULL,
          `base_price_factor` varchar(4) COLLATE utf8_general_ci NOT NULL,
          `group_access` int(11) DEFAULT NULL,
          `publish_down` datetime NOT NULL,
          PRIMARY KEY (`product_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT=\'OSWorX Legal Mod Additional Product Attributes\';';

        $this->db->query( $sql );
    }

    /**
     * get additonal product infos
     * note: bool value to check if record exists for adding or updating
     * @since 3.0.0
     * @param int   $id     product id
     * @return array/bool (false)
     */
    public function getProductAdditionalData( $id ) {
        $sql = '
        SELECT
            *
        FROM
            `' .DB_PREFIX . 'product_additional`
        WHERE
            product_id = \'' . (int) $id . '\'';

        $result = $this->db->query( $sql );

        if( $result->num_rows ) {
		  return $result->row;
        }

        return false;
    }

    /**
     * add data into tabel product_additonal
     * @param int   $product_id
     * @param array $data
     */
    public function addProductAdditionalData( $product_id, $data ) {
        if( $product_id == null ) {
            $product_id = $this->getIdByModel( $data );
        }

        $sql = '
        INSERT
            INTO `' .DB_PREFIX . 'product_additional`
        SET
            `product_id` = \'' . (int) $product_id . '\','
            . $this->getDataFields( $data );

        $this->db->query( $sql );
    }

    /**
     * update an existing record
     * @param int   $product_id
     * @param array $data
     */
    public function updateProductAdditionalData( $product_id, $data ) {
    	if( $product_id == null ) {
            $product_id = $this->getIdByModel( $data );
        }

    	if( $this->getProductAdditionalData( $product_id ) ) {
	        $sql = '
	        UPDATE
	            `' .DB_PREFIX. 'product_additional`
	        SET '
	            . $this->getDataFields( $data )
	        . '
	        WHERE
	            product_id = \'' . (int) $product_id . '\'';

			$this->db->query( $sql );
		}else{
			$this->addProductAdditionalData( $product_id, $data );
		}

		$this->cache->delete( 'product' );
	}

    /**
     * update an existing record
     * @param int   $product_id
     */
    public function deleteProductAdditionalData( $product_id ) {
        $sql = '
        DELETE
            FROM `' .DB_PREFIX. 'product_additional`
        WHERE
            product_id = \'' . (int) $product_id . '\'';

		$this->db->query( $sql );

		$this->cache->delete( 'product' );
	}

    /**
     * proxy function for common used fields
     * @param array     $data
     * @return string
     */
    private function getDataFields( $data ) {
        $ret = '
        `end_of_life` = \'' . ( !empty( $data['end_of_life'] ) ? $this->db->escape( $data['end_of_life'] ) : '0' ) . '\',
        `condition` = \'' . ( !empty( $data['condition'] ) ? $this->db->escape( $data['condition'] ) : '' ) . '\',
        `guarantee` = \'' . ( !empty( $data['guarantee'] ) ? $this->db->escape( $data['guarantee'] ) : '' ) . '\',
        `package_content` = \'' . ( !empty( $data['package_content'] ) ? $this->db->escape( $this->sanitizeData( $data['package_content'] ) ) : '' ) . '\',
        `package_content_unit` = \'' . ( !empty( $data['package_content_unit'] ) ? $this->db->escape( $data['package_content_unit'] ) : '' ) . '\',
        `base_price_quantity` = \'' . ( !empty( $data['base_price_quantity'] ) ? $this->db->escape( $data['base_price_quantity'] ) : '' ) . '\',
        `base_price_unit` = \'' . ( !empty( $data['base_price_unit'] ) ? $this->db->escape( $data['base_price_unit'] ) : '' ) . '\',
        `base_price_factor` = \'' . ( !empty( $data['base_price_factor'] ) ? $this->db->escape( $this->sanitizeData( $data['base_price_factor'] ) ) : '' ) . '\',
        `group_access` = \'' . ( !empty( $data['group_access'] ) ? $this->db->escape( $data['group_access'] ) : 'NULL' ) . '\',
        `publish_down` = \'' . ( !empty( $data['publish_down'] ) ? $this->db->escape( $data['publish_down'] ) : $this->getNullDate() ) . '\'';

        return $ret;
    }

    /**
     * return last ID set by last database action
     * @return int
     */
    private function getLastId() {
        return $this->db->getLastId();
    }

    private function getIdByModel( $data ) {
        $sql = '
        SELECT
            product_id
        FROM
            `' . DB_PREFIX . 'product`
        WHERE
            model = \'' . $data['model'] . '\'
        LIMIT 1';

        $result = $this->db->query( $sql );

        if( $result->num_rows ) {
            return $result->row['product_id'];
        }

        return false;
    }

    /**
     * sanitize data value
     * excepts only digits (0-9) and dor (.)
     * @param string    $val
     * @return string
     * @since 3.0.3
     */
    private function sanitizeData( $val, $type = 'numbers' ) {
        $val = trim( $val );

        switch( $type )
        {
            case 'letters':
                $val = preg_replace( '/[^a-zA-Z_-\.]/', '', $val );
                break;

            case 'numbers':
            default:
                $val = preg_replace( '/[^0-9\.]/', '.', $val );
                break;
        }

        return $val;
    }
}