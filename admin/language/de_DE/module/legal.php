<?php
/**
 * @version		$Id: legal.php 3810 2014-12-04 21:44:29Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2012 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */
// Heading
$_['heading_title']         = 'Legal<span style="float:right; color:#777777;">Webshop nach EU- &amp; nationalen Gesetzen  <a href="http://osworx.net" target="_blank">OSWorX</a></span>';
$_['bc_title']              = 'Legal';

$_['tab_common']            = 'Allgemein';
$_['tab_text']              = 'Texte';
$_['tab_locales']           = 'Lokale Einst.';
$_['tab_taxes']             = 'Steuern';
$_['tab_plugins']           = 'Plugins';
$_['tab_specials']          = 'Angebotspreis';
$_['tab_css']               = 'CSS';
$_['tab_attributes']        = 'Eigenschaften';
$_['tab_log']               = 'Logdatei';
$_['tab_baseprice']			= 'Grundpreis';
$_['tab_emailtexts']		= 'Text zu Email';

// Entry
$_['entry_hint']            = 'Hinweiszeichen';
$_['entry_text_id']         = 'Text für Emails';
$_['entry_text_wtax']       = 'Text für Preise inkl. Steuer';
$_['entry_text_wotax']      = 'Text für Preise exkl. Steuer';
$_['entry_countries']       = 'Land lt. aktueller Einstellung';
$_['entry_tax_class']       = 'Steuerklasse';
$_['entry_text_button']     = 'Text Bezahlbutton';
$_['entry_text_summary']    = 'Text Zusammenfassung';
$_['entry_upload']          = 'Plugin-Datei auswählen &amp; hochladen';
$_['entry_popup']           = 'Popup-Fenster (BxH)';
$_['entry_position_hint']   = 'Position Preisinformation';
$_['entry_xml_line']        = 'Ersetzung';
$_['entry_text_slider']     = 'Slider Bezeichnung';
$_['entry_image']           = 'Produktbild (BxH)';
$_['entry_baseprice_content']	= 'Text für Inhalt';
$_['entry_baseprice_price']		= 'Preisdarstellung';
$_['entry_text']				= 'Text';
$_['entry_status']				= 'Status';
$_['entry_order_status']		= 'Auftragsstatus';
$_['entry_description_len']		= 'Länge Artikeltext';

// Text
$_['text_success']          = 'Modul erfolgreich gespeichert';
$_['text_success_install']	= 'Modul erfolgreich installiert, bitte jetzt die Einstellungen vornehmen';
$_['text_module']           = 'Module';
$_['text_prices_w_tax']     = 'Alle Preisangaben inkl. USt. zzgl. <a target="_blank" href="index.php?route=information/information/info&information_id=6">Versand</a>';
$_['text_prices_wo_tax']    = 'Alle Preisangaben zzgl. USt. und <a target="_blank" href="index.php?route=information/information/info&information_id=6">Versand</a>';
$_['text_geo_zones']        = '1. Geografische Zonen (Geozonen)';
$_['text_country_ies']      = 'Land / Länder';
$_['text_taxes']            = '2. Steuersätze';
$_['text_title']            = 'Name';
$_['text_description']      = 'Beschreibung';
$_['text_rate']             = 'Höhe';
$_['text_type']				= 'Art';
$_['text_geo_zone']			= 'Zone';
$_['text_legal_texts_note']	= 'Für die Buttonlösung <b>muss</b> Zusammenfassung installiert werden.<br /><span style="color:red;">Texte müssen nach dem Speichern an die Shopanforderungen angepasst werden!</span> (Menü Katalog > Texte)';
$_['text_state']            = 'Status';
$_['text_tax_classes']      = '3. Steuerklassen';
$_['text_legal_texts']      = '4. Textvorlagen';
$_['text_installed']        = 'Installiert';
$_['text_show_hide_help']   = 'Zeige/Verberge Hilfe';
$_['text_supported_countries']      = 'Unterstützte Länder';
$_['text_not_supported_countries']  = 'Nicht unterstützte Länder';
$_['text_left']                     = 'Links';
$_['text_rightBefore']              = 'Rechts vor Powered';
$_['text_rightBottom']              = 'Rechts unterhalb Powered';
$_['text_rightTop']                 = 'Rechts oberhalb Powered';
$_['text_replace']                  = 'Ersetzt Powered';
$_['text_show_hide_content']        = 'Zeige/Verberge Inhalt';
$_['text_used_template']            = 'Verwendete Vorlage';
$_['text_used_css']                 = 'Verwendete CSS-Vorlage';
$_['text_id']                       = 'Art.Nr.';
$_['text_attributes']               = 'Eigenschaften';
$_['text_metatag']                  = 'Metatag';
$_['text_title_state']              = 'Name / Status';
$_['text_version']                  = 'Version';
$_['text_permission']               = 'Rechte';
$_['text_edit_file']                = 'Zum Bearbeiten anklicken';
$_['text_edit_mod']                 = 'WARNUNG: das Bearbeiten erfolgt auf eigenes Risiko!<br />Das Speichern einer fehlerhaften Datei kann zur Folge haben, dass der gesamte Shop nicht mehr funktioniert!!';
$_['text_no_data']                  = 'Noch keine Einträge vorhanden.';
$_['text_log_file']                 = 'VQMod Logdatei';
$_['text_log_size']                 = 'Aktuelle Dateigröße ist [<b>%s kB</b>] - maximal möglich sind %s kB';
$_['text_slider-top']               = 'Slider (Kopfbereich)';
$_['text_slider-bottom']            = 'Slider (Fussbereich)';
$_['text_bar-top']                  = 'Bar (Kopfbereich)';
$_['text_bar-bottom']               = 'Bar (Fussbereich)';
$_['text_checking_attributes']      = 'Überprüfe Eigenschaften .. bitte warten ..';
$_['text_no_log_file_existing']     = 'Bisher noch keine Logdatei vorhanden';
$_['text_title_header']             = 'Name';
$_['text_finish_install']           = 'Wurde das Modul neu installiert, müssen als Erstes die lokalen Einstellungen übertragen werden.<br />Dazu bitte auf den rot markierten Reiter "<b>Lokale Einstell.</b>" klicken, gewünschte Daten installieren und anschließend rechts oben den Button "<b>Übernehmen</b>" anklicken.<br />Um die Installation abzuschließen danach auf den Reiter "<b>Steuern</b>" klicken und die Steuersätze auf die Produkte bzw. Versandarten übertragen [optional].';
$_['text_bp_content']               = 'Bezeichung für Inhalt';
$_['text_bp_price']                 = 'Bezeichnung für Preis';
$_['text_expand']					= 'Vergrößern';
$_['text_reduce']					= 'Verkleinern';
$_['text_search']					= 'Suchen';
$_['text_disabled']					= 'Nicht aktiviert';

// help
    // inline
$_['help_hint']             = '<span class="help">Zeichen bei Preisen welches auf weitere Erklärungen bez. Steuer &amp; Versand hinweist.<br />Beispiel:  * (Leerzeichen Stern)<br /><b>Erklärender Text dazu muss unter `Texte > Text für Preise ...` definiert werden</b>.<br />HTML-Code kann verwendet werden</span>';
$_['help_text_id']          = '<span class="help">Text welcher allen Emails automatisch angehängt wird.<br />(siehe Muster in Lokale Einst. > Texte > Emailanhang)</span>';
$_['help_popup']            = '<span class="help">Hilfreich wenn anzuzeigender Text nur ein paar Zeilen sind, hier kann die Breite und Höhe (in Pixel) des Popup-Fensters angepasst werden (wenn verwendet wird - siehe Texte)<br />Angabe ohne Einheit, z.B. 650<br /><b>Hinweis</b>: wenn anstatt eines Wertes der Begriff <b>false</b> angegeben wird, richtet sich die Größe nach der tatsächlichen Textgröße</span>';
$_['help_position_hint']    = '<span class="help">Position für den Hinweis bezüglich Preisgestaltung (siehe auch Plugins sowie Texte).<br />Standardmässig wird der Hinweis im Fussbereich mit oder anstatt des `Powered ...` angezeigt, bei manchen Vorlagen kann es sein dass keine Anzeige erfolgt da die Variable `$powered` entfernt wurde, dann Position `Slider` oder `Bar` wählen - ansonsten muss die Datei footer.tpl bearbeitet und die php-Variable $powered hinzugefügt werden.<br /><b>Hinweis</b>: wird MijoShop verwendet, sollte entweder "Slider" oder "Bar" ausgewählt werden, zudem muss das betreffende Plugin aktiviert sein! Wird dann immer noch kein Hinweis angezeigt, muss die Vorlage dementsprechend bearbeitet werden!</span>';
$_['help_specials']         = '<span class="help">Werden sowohl Normalpreis als auch reduzierter Preis angezeigt, sollte um einer Abmahnung zu entgehen dies besonders hervor gehoben werden. Es genügt nicht den `STATT` Preis einfach durchzustreichen und den Angebotspreis daneben anzuführen. Diese Gegenüberstellung muss deutlich hervorheben auf was sich der verglichene Preis bezieht.<br /><br />Es wird daher empfohlen eine solche Gegenüberstellung mit einem aussagekräftigen Text zu versehen, z.B.<br /><b>Bisheriger Preis xx,yy € jetzt nur xx,yy €</b><br /><br />Der Text kann individuell sein, es muss nur deutlich sein was gemeint ist.<br /><em>Der bisherige Preis muss zudem nachweisbar schon einige Zeit bestehen, da ansonsten das "Gesetz für unlauteren Wettbewerb" zum tragen kommt (und das sieht tw. empfindliche Strafen vor!</em><br /><br />Nachfolgendes Beispiel orientiert sich an der Originalvorlage, Eigene können davon abweichen<br /><br />Bisheriger Preis &lt;span style="color:#5B5B5B;"&gt;%s&lt;/span&gt; &lt;span style="color:#0D6C2A; font-weight:bold;"&gt;jetzt nur %s&lt;/span&gt;<br />ergibt:<br />Bisheriger Preis <span style="color:#5B5B5B;">98,- € *</span> <span style="color:#0D6C2A; font-weight:bold;">jetzt nur 79,- € *</span><br /><br />* Als Platzhalter (2x) für die Preise ist <b>%s</b> zu verwenden welche dann durch die Preise ersetzt werden<br />* Erlaubt sind alle HTML-Formatierungsvariablen innerhalb der span.Tags (Farbe, Schriftgröße, usw.)<br /><br />In der darunter angezeigten Vorlage kann der Code aus der Zeile <b>Ersetzung</b> angepasst bzw. gesucht werden.<br /><b>Beide Zeilen müssen überein stimmen damit die Anpassung wirksam wird!</b></span>';
$_['help_image']            = '<span class="help">Bildmasse der Produktbilder in px für die abschliessende Zusammenfassung.<br />Empfohlen werden keine zu großen Bilder, 80 x 80 sollte im Allgemeinen passen</span>';
$_['help_text_slider']      = '<span class="help">Bezeichnung für Slider wenn Position für Hinweis mit `Slider` bestimmt ist</span>';
$_['help_baseprice_content']= '<span class="help">Bezeichnung für den Inhalt, z.B. Inhalt</span>';
$_['help_baseprice_price']	= '<span class="help">Art der Preisdarstellung.<br />Beispiel: <b>Preis für %s %s</b> ergibt je nach Definition bei Produkt: <b>Preis für 1 kg xx,- €</b><br />Die Platzhalter %s werden automatisch mit den richtigen Werten befüllt</span>';
$_['help_description_len']	= '<span class="help">Anzahl Zeichen für Anzeige Artikeltext in Zusammenfassung (keine Angabe oder 0 = keine Anzeige)<br />Hinweis: die Anzeige erfolgt Wortweise, es werden keine Wörter verstümmelt</span>';

    // outline help
$_['help_text']             = 'Hier Texte für Button und Hinweistext für Preise inkl. und exkl. Steuer angeben. Dieser Hinweis wird dann im Shop an der gewählten Stelle (meist Footer - siehe Reiter Allgemein > Position Hinweis) angezeigt.<br />Die Anzeige der Preise mit bzw. ohne Steuern wird in der Shopverwaltung definiert.<br /><br />Beispiele:<ul><li>Mit Link: Alle Preise inkl. USt. zzgl. &lt;a class="popTerms" href="index.php?route=information/information/info&information_id=6" target="_blank" title="Versandkosten"&gt;Versand&lt;/a&gt;<br />>>> Die Anzeige wäre dann so: Alle Preise inkl. USt. zzgl. <a class="popTerms" href="index.php?route=information/information/info&information_id=6" target="_blank" title="Versandkosten">Versand</a></li><li>Ohne Link: Alle Preise inkl. USt., ohne Versand</li></ul><b>Hinweise</b><ul><li>class="popTerms" ist optional, der Text wird dann in einer PopUp-Box angezeigt.</li><li>Falls Text als Popup angezeigt werden soll, muss das Plugin dafür (siehe Plugins) aktiviert sein</li><li>Als Hilfe für die Texte zur Einbindung, nachstehend die vorhandenen Texte mit ihrer Datenbanknummer in Klammer [] welche dann im Text unten (information_id=xx) anzuführen ist</li></ul><b>Buttonlösung</b><br />Seit 1. August 2012 muss jedem Käufer im Shop (speziell DE - siehe § 312g BGB) eine <b>Zusammenfassung aller im Warenkorb befindlichen Produkteigenschaften</b> angezeigt werden (siehe Produkte > Attribute/Eigenschaften).<br />Desweiteren muss der letzte Button vor dem Bezahlvorgang einen aussagekräftigen Text anzeigen!<br />Nicht einwandfrei ist: Bestellen, Hier bestellen, Weiter, usw.<br />Korrekt ist (z.B.): Kaufen, Kostenpflichtig Kaufen, usw.';
$_['help_locales']          = 'Nachfolgend werden diverse Einstellungen bezüglich Geozonen, Steuersätze usw. vorgenommen.<br />Dazu bitte zunächst einmal überprüfen ob das Land in welchem sich der Shop befindet, richtig definiert ist. Wenn nicht, dann ändern (<b>System > Einstellungen > Lokales</b>.<br /><br />Anschliessend in der Reihenfolge <ol><li>Geozonen</li><li>Steuersätze</li><li>Steuerklassen</li><li>Rechtliche Texte</li></ol>installieren.<br />Abschliessend kann noch optional (nachdem Steuerklassen installiert wurden) eine Aktualisierung der Produkte und Versandarten mit der richtigen Steuerklasse durchgeführt werden (Reiter <b>Steuern</b>).<br /><b>Hinweise</b>:<br />*alle Einstellungen können nach der Installation jederzeit im betreffenden Menüpunkt (<b>System > Lokale Einst. &amp; Katalog > Informationen</b>) weiter angepasst werden<br />* werden die Bezeichnungen nachträglich geändert (z.B. statt EX EU Ust. xx in USt. xx) ist dieser Punkt wieder als zu installieren gekennzeichnet. Er muss jedoch nicht nochmals installiert werden!<br /><br /><b>Steuersätze</b><br />Je nachdem in welche Länder verkauft werden soll (bw. Kunden kommen können), die passenden Steuersätze installieren.<br /><b>Wichtig für Unternehmer mit UID</b>: soll in die EU verkauft werden, muss auch der Satz <b>Export EU (mit UID)</b> installiert werden.<br />Wird nur in die EU exportiert, genügt es die Sätze für <b>Europa</b> zu installieren, andernfalls die für <b>Welt</b>.';
$_['help_taxes']            = 'Nachfolgend kann Produkten sowie Versandarten automatisch eine Steuerklasse zugewiesen werden.<br />Siehe Reiter <b>Lokale Einst.</b> - setzt die erfolgte Installation der Steuerklasse(n) voraus.<br /><br /><b>Hinweis</b>: es werden alle Produkte mit der ausgewählten Steuerklasse aktualisiert!<br />Ist das nicht gewünscht, dann KEINE Aktualisierung durchführen!';
$_['help_use_plugin']       = 'Nachstehend alle aktuell zur Verfügung stehenden Plugins (weitere Neue sowie Updates zu bestehenden erhältlich unter <a href="http://osworx.net" target="_blank">OSWorX</a>).<br />Zweck der Plugins siehe Hilfetexte daneben<br /><b>Hinweise</b>:<ul><li>Mit diesem Modul wird kein weiteres Verwaltungstool wie VQMod-Manager benötigt (soferne kein weiteren VQMod-Plugins eingesetzt werden)</li><li>Siehe auch weitere Einstellungen zu einigen Plugins unter <b>Allgemein</b> und <b>Texte</b></li><li><b>Wichtig</b>: die Anpassungen erfolgen auf Basis Standardinstallation (speziell Templates).<br />Wird ein individuelles Template verwendet und haben sich deren Entwickler nicht an die Standardvorgaben gehalten, kann es sein dass in den Templates Änderungen vorgenommen werden müssen!<br /><a href="http://osworx.net/de/component/content/article/77-opencart/122-opencart-and-custom-templates" target="_blank">Mehr dazu hier</a></li></ul>';
$_['help_text_summary']     = '<span class="help">Seit 1. August 2012 müssen alle Kunden (speziell DE - siehe § 312g BGB) vor dem Bestellabschluß mit einer <b>Übersicht / Zusammenfassung aller markanten Eigenschaften eines jeden Artikel</b> vor dem letzten Button (= vor Bezahlung) informiert werden.<br />Hier den vorhandenen Text (siehe Texte) auswählen welcher die benötigten Bestandteile (z.B. Erklärung, Hinweise auf AGBs und Widerrufsrecht, etc.) beinhaltet.<br />Zusätzlich muss der Buttontext (siehe Texte) auch dementsprechend formuliert sein, z.B. <b>Kaufen</b>, <b>Kostenpflichtig kaufen</b> oder <b>Kaufe bestellte Waren</b>.<br />Bezüglich vorhandener oder fehlender Artikeleigenschaften siehe Reiter oben <b>Eigenschaften</b></span>';
$_['help_tpl_content']      = 'Standardmässig wird der Preis und Angebotspreis mit diesem Code angezeigt (Originalinstallation - Standardvorlage):<br /><b>&lt;span class=&quot;price-old&quot;&gt;&lt;?php echo $price; ?&gt;&lt;/span&gt; &lt;span class=&quot;price-new&quot;&gt;&lt;?php echo $special; ?&gt;&lt;/span&gt;</b><br />Sollte dieser Code in der unten angezeigten Vorlage (lt. gewählter Vorlage in der Systemsteuerung) <b>nicht</b> vorhanden sein, muss in dem Feld oben `Ersetzung` der erforderliche Code angegeben werden!<br /><br /><b>Hinweis</b>: der zu ersetzende Code muss aus 1 Zeile bestehen, sind in der Vorlage mehrere ist zusätzlich die Vorlage unten anzupassen und anschliessend zu speichern (siehe eigenen Button `Speichern` unten)!';
$_['help_text_button']      = 'Teil der sogenannten <b>Buttonlösung</b>';
$_['help_css']              = 'Diese CSS-Vorlage wird für die Produktzusammenfassung sowie für Hinweise in den Positionen `Slider` und `Bar` verwendet.<br />Alle Definitionen dürfen geändert aber nicht gelöscht werden - wichtig: .attribs ist für Zusammenfassung!<br />Zusätzlich kann diese Vorlage mit eigenen Definitionen ergänzt werden um zum Bespiel die Preise (Normal- / Angebotspreis) (siehe Reiter `Angebotspreis`) individuell zu formatieren.';
$_['help_attributes']       = 'Für die abschließende Produktzusammenfassung müssen Produkte Attribute (Produkteigenschaften) besitzen um diese anzeigen zu können.<br />Für die Zusammenfassung werden folgende Felder in der Reihenfolge verwendet:<ol><li>Eigenschaften</li><li>Metatag Beschreibung</li><li>`Lazy Mode`</li></ol>In nachfolgender Tabelle werden Eigenschaften und Metatag angezeigt - jedes Produkt sollte mindestens 1 davon besitzen.<br />Mit anklicken des Produktnamens kann der Artikel direkt in einem neuen Fenster bearbeitet werden - danach nochmals überprüfen.<br /><br /><span style="text-decoration: underline;"><b>Hinweis</b>: um dem Gesetz zu entsprechen, sollten auf alle Fälle die Eigenschaften verwendet werden!</span><br />Siehe auch Erklärungen dazu im Handbuch';
$_['help_base_price']       = 'Produkte müssen wenn sie ein Teil von einem Kilo/Liter/Meter sind, zum besseren Vergleich für den Kunden, mit dem <b>Grundpreis</b> angezeigt werden.<br /><br />Siehe dazu:<br />AT: <a href="http://www.jusline.at/Preisauszeichnungsgesetz_%28PrAG%29.html" target="_blank">Österreich</a><br />DE: <a href="http://www.frankfurt-main.ihk.de/recht/themen/gewerberecht/grundpreis/" target="_blank">Deutschland</a><br /><a href="http://de.wikipedia.org/wiki/Grundpreisverordnung" target="_blank">Grundpreis bei Wikipedia</a><br /><br />In den Feldern unten können individuelle Texte angegeben werden, bleiben Felder leer wird Text lt. Sprachendatei verwendet.<br />Werden keine Grundpreise benötigt oder ist das betreffende Plugin deaktiviert, sind die Einstellungen wirkungslos';
$_['help_emailtexts']		= 'Allgemeiner Text als Anhang für alle ausgehenden Emails sowie spezielle Texte zusätzlich zum allgemeinen Emailanhang.<br />Neben dem allgemeinen Text kann hier für jeden Auftragsstatus eine weiterer Text definiert werden.<br />Texte müssen vor der Auswahl angelegt sein, siehe dazu Menü <b>Katalog > Texte</b>.<br />Im ausgewählten Text können neben reinem Text auch Links zu Seiten sowie - wenn aktiviert - Links zu PDF mitgesendet werden.<br /><b>Hinweis</b>: soll der Text nicht als Link im Shop angezeigt werden, dann Textstatus auf Deaktiviert setzen.';

$_['button_apply']          = 'Übernehmen';
$_['button_install']        = 'Installieren';
$_['button_help']           = 'Hilfe';
$_['btn_update_products']   = 'Produkte aktualisieren';
$_['btn_update_shippings']  = 'Versandarten aktualisieren';
$_['btn_upload']            = 'Hochladen';
$_['btn_save']              = 'Speichern';
$_['btn_check']             = 'Überprüfen';
$_['btn_clear']             = 'Logdatei leeren';

// messages
$_['msg_tax_installed']     = 'Steuersatz erfolgreich installiert';
$_['msg_class_installed']   = 'Steuerklasse erfolgreich installiert';
$_['msg_zone_installed']    = 'Geozone erfolgreich installiert';
$_['msg_text_installed']    = 'Text erfolgreich installiert';
$_['msg_install_taxclasses']= 'Bitte zuerst Steuerklassen installieren!';
$_['msg_plugin_enabled']    = 'Plugin aktiviert';
$_['msg_plugin_disabled']   = 'Plugin deaktiviert';
$_['msg_products_updated']  = 'Produkte wurden erfolgreich aktualisiert';
$_['msg_shipping_updated']  = 'Versandarten wurden erfolgreich aktualisiert';
$_['msg_file_saved']        = 'Datei erfolgreich gespeichert';
$_['msg_found_products']    = 'Insgesamt wurden %s Produkte gefunden, davon haben %s keine Eigenschaften und %s keinen Metatag.';

// base price
$_['entry_base_price_calculation']	= 'Grundpreisberechnung';
$_['entry_base_price_factor']       = 'Berechnungsfaktor';
$_['entry_package_content']         = 'Verpackungsinhalt (Menge - Einheit)';
$_['text_base_price']               = 'Grundpreis Info:';
$_['help_base_price_calculation']	= '<span style="margin-left: 30px; color: #666666; font-size: 11px; font-weight: normal; font-family: Verdana, Geneva, sans-serif;">Basis ist normalerweise 1 Kilo/Liter (Faktor = 1).<br />Ist der Packungsinhalt unter 250 Gramm/Milliliter kann der Wert für 100 Gramm/Milliliter angegeben werden (Faktor = 100).<br />Ist bei Faktor kein Wert angegeben, wird nichts angezeigt.</span>';
$_['help_base_price_extern']        = '<a href="http://www.juraforum.de/gesetze/pangv/2-grundpreis" title="Grundpreis im Juraforum" onclick="window.open(this,\'help\',\'width=850,height=600,top=20,left=20,scrollbars=yes,resizeable=yes\'); return false;"><img src="view/image/osworx/16/link_extern.png" /></a>';
$_['help_package_content']          = '<span style="margin-left: 30px; color: #666666; font-size: 11px; font-weight: normal; font-family: Verdana, Geneva, sans-serif;">Effektiver Verpackungsinhalt z.B. 500gr. oder 0.5 Kilo, 250ml oder 0.25 Liter</span>';

// Error
$_['error_permission']			= 'Warnung: keine Berechtigung Einstellungen in diesem Modul vorzunehmen!';
$_['error_hint']				= 'Kein Text bei Hinweiszeichen angegeben!';
$_['error_popup_height']		= 'Höhe Popup-Fenster muss angegeben werden!';
$_['error_popup_width']			= 'Breite Popup-Fenster muss angegeben werden!';
$_['error_not_supported']		= 'Steuersätze werden für dieses Land nicht unterstützt!<br />Bitte diese in "System > Lok. Einstellungen > Steuer" definieren.';
$_['error_xmlLine']				= 'Feld Ersetzung (Angebotspreis) muss ausgefüllt werden!';
$_['error_save_file']			= 'Plugin konnte nicht gespeichert werden!';
$_['err_tax_installed']			= 'Fehler: Steuersatz konnte nicht installiert werden!<br />Das kann daran liegen dass die Geozonen oben bereits umbenannt wurden.<br />Wenn nicht, dann bitte manuell erledigen.';
$_['err_class_installed']		= 'Fehler: Steuerklasse konnte nicht installiert werden!<br />Bitte manuell erledigen.';
$_['err_zone_installed']		= 'Fehler: Geozone konnte nicht installiert werden!<br />Bitte manuell erledigen.';
$_['err_text_installed']		= 'Fehler: Text konnte nicht installiert werden!<br />Bitte manuell erledigen.';
$_['err_class']					= 'Fehler: es müssen zuerst die entsprechenden Steuersätze installiert werden!';
$_['err_no_vqmod']				= 'Fehler! VQMod ist Voraussetzung zum Betrieb dieses Moduls - bitte installieren <a href="https://github.com/vqmod/vqmod" target="_blank">VQMod holen</a>.';
$_['err_vqmod_plg_missing']		= 'Fehler: VQMod Plugin [%s] fehlt!';
$_['err_vqmod_plg_disabled']	= 'Achtung: VQMod Plugin [%s] nicht aktiv!';
$_['err_plg_update']			= 'Fehler: Pluginstatus konnte nicht geändert werden!';
$_['err_no_locale_sets']		= 'ACHTUNG FEHLER: keine lokalen Einstellungen gefunden!';
$_['err_no_plugins']			= 'FEHLER: benötigte Plugins nicht gefunden!!';
$_['err_vqmod_not_installed']	= 'FEHLER!! VQMod ist zwar vorhanden, aber nicht installiert!<br />Bitte <a href="%s" target="_blank">hier klicken um zu installieren</a>.';
$_['err_products_updated']		= 'Fehler: Produkte konnten nicht aktualisiert werden!';
$_['err_shipping_updated']		= 'Fehler: Versandarten konnten nicht aktualisiert werden!';
$_['err_file_saved']			= 'Fehler: Datei [%s] konnte nicht gespeichert werden!';
$_['err_file_does_not_exist']	= 'Fehler: die angeforderte Datei [%s] konnte nicht gefunden werden!';
$_['err_no_content']			= 'Fehler: kein Inhalt (Ersetzung) zum Speichern angegeben!';
$_['err_wrong_oc_version']		= 'FEHLER: falsche OpenCart Version!<br />Benötigt wird mindestens 1.5.1.0';
$_['err_found_products']		= 'Alle Produkte haben Eigenschaften - sehr gut.';

// $_FILE Upload
    // Errors
$_['error_form_max_file_size']	= 'Fehler: die Datei übersteigt die erlaubte Maximalgröße!';
$_['error_ini_max_file_size']	= 'Fehler: die Datei übersteigt de Maximalgröße lt. php.ini!';
$_['error_no_temp_dir']			= 'Fehler: Zwischenverzeichnis kann nicht gefunden werden!';
$_['error_no_upload']			= 'Hinweis: keine Datei zum Upload gewählt!';
$_['error_partial_upload']		= 'Fehler: Upload nicht komplett!';
$_['error_php_conflict']		= 'Fehler: unbekannter PHP-Konflikt!';
$_['error_unknown']				= 'Fehler: unbekannter Fehler!';
$_['error_write_fail']			= 'Fehler: Datei konnte nicht geschrieben werden!';
$_['error_move']				= 'Achtung: Datei kann nicht auf Server geschrieben werden, bitte Verzeichnisrechte prüfen!';
$_['error_filetype']			= 'Achtung: ungültiger Dateityp! Nur Plugin-Dateien für Modul `Legal` sind zulässig';
    // success
$_['success_upload']			= 'Plugin erfolgreich hochgeladen';
$_['success_save_file']			= 'Plugin erfolgreich gespeichert';
$_['success_clear_log']			= 'Logdatei erfolgreich geleert';