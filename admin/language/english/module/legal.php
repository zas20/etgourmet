<?php
/**
 * @version		$Id: legal.php 3810 2014-12-04 21:44:29Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2012 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */
// Heading
$_['heading_title']         = 'Legal<span style="float:right; color:#777777;">Make shop ready for EU- &amp; National Regulations <a href="http://osworx.net" target="_blank">OSWorX</a></span>';
$_['bc_title']              = 'Legal';

$_['tab_common']            = 'Common';
$_['tab_text']              = 'Texts';
$_['tab_locales']           = 'Local Sett.';
$_['tab_taxes']             = 'Taxes';
$_['tab_plugins']           = 'Plugins';
$_['tab_specials']          = 'Specials Price';
$_['tab_css']               = 'CSS';
$_['tab_attributes']        = 'Attributes';
$_['tab_log']               = 'Log';
$_['tab_baseprice']			= 'Baseprice';
$_['tab_emailtexts']		= 'Text to Email';

// Entry
$_['entry_hint']            = 'Hint';
$_['entry_text_id']         = 'Text for emails';
$_['entry_text_wtax']       = 'Text for prices with tax';
$_['entry_text_wotax']      = 'Text for prices without tax';
$_['entry_countries']       = 'Country as currently defined';
$_['entry_tax_class']       = 'Taxclass';
$_['entry_text_button']     = 'Text Payment Button';
$_['entry_text_summary']    = 'Text Summary';
$_['entry_upload']          = 'Select plugin file &amp; upload';
$_['entry_popup']           = 'Popup-Window (WxH)';
$_['entry_position_hint']   = 'Position Hint';
$_['entry_xml_line']        = 'Replacement';
$_['entry_text_slider']     = 'Slider Title';
$_['entry_image']           = 'Product Image (WxH)';
$_['entry_baseprice_content']	= 'Text for Content';
$_['entry_baseprice_price']		= 'Display of Price';
$_['entry_text']				= 'Text';
$_['entry_status']				= 'Status';
$_['entry_order_status']		= 'Orderstatus';
$_['entry_description_len']		= 'Length product description';

// Text
$_['text_success']          = 'Module succesfully saved';
$_['text_success_install']	= 'Module successfully installed, now define your settings';
$_['text_module']           = 'Module';
$_['text_prices_w_tax']     = 'All prices incl. VAT plus <a target="_blank" href="index.php?route=information/information/info&information_id=6">shipping</a>';
$_['text_prices_wo_tax']    = 'All prices excl. VAT and <a target="_blank" href="index.php?route=information/information/info&information_id=6">shipping</a>';
$_['text_geo_zones']        = '1. Geo Zones';
$_['text_country_ies']      = 'Country / Countries';
$_['text_taxes']            = '2. Tax Rates';
$_['text_title']            = 'Title';
$_['text_description']      = 'Description';
$_['text_rate']             = 'Rate';
$_['text_type']				= 'Type';
$_['text_geo_zone']			= 'Zone';
$_['text_legal_texts_note']	= 'To use the "button-solution" you <b>have to install</b> the summary.<br /><span style="color:red;">Texts have to be edited after install!</span> (Menu Catalog > Texte (Information))';
$_['text_state']            = 'State';
$_['text_tax_classes']      = '3. Tax Classes';
$_['text_legal_texts']      = '4. Legal Texts (optional)';
$_['text_installed']        = 'Installed';
$_['text_show_hide_help']   = 'Show/Hide Help';
$_['text_supported_countries']      = 'Supported countries';
$_['text_not_supported_countries']  = 'Not supported countries';
$_['text_left']                     = 'Left';
$_['text_rightBefore']              = 'Right before Powered';
$_['text_rightBottom']              = 'Right below Powered';
$_['text_rightTop']                 = 'Right above Powered';
$_['text_replace']                  = 'Replace Powered';
$_['text_show_hide_content']        = 'Showe/Hide Content';
$_['text_used_template']            = 'Used template';
$_['text_used_css']                 = 'Used CSS stylesheet';
$_['text_id']                       = 'ID';
$_['text_attributes']               = 'Attributes';
$_['text_metatag']                  = 'Metatag';
$_['text_title_state']              = 'Title / State';
$_['text_version']                  = 'Version';
$_['text_permission']               = 'Perm.';
$_['text_edit_file']                = 'Click to edit';
$_['text_edit_mod']                 = 'WARNING: edit the file on your own risk! Saving an invalid file can lead to a not working shop!!!';
$_['text_no_data']                  = 'Still no entries recorded.';
$_['text_log_file']                 = 'VQMod Logfile';
$_['text_log_size']                 = 'Current filesize is [<strong>%s kB</strong>] - maximal allowed are %s kB';
$_['text_slider-top']               = 'Slider (header)';
$_['text_slider-bottom']            = 'Slider (footer)';
$_['text_bar-top']                  = 'Bar (header)';
$_['text_bar-bottom']               = 'Bar (footer)';
$_['text_checking_attributes']      = 'Checking attributes .. please wait ..';
$_['text_no_log_file_existing']     = 'No logfile existing until yet';
$_['text_title_header']             = 'Name <span style="color:red;">Note: after installing the texts you have to edit them!</span>';
$_['text_finish_install']           = 'To finish the installation, you have to save the locale settings.<br />To do click on the tab "<strong>Locale Sett.</strong>", choose required data. Then click on the button to the upper right "<strong>Apply</strong>".<br />To finish this installation click afterward on the tab "<strong>Taxes</strong>" to transfer correct taxes to your products and shipping methods [optional].';
$_['text_bp_content']               = 'Label for content';
$_['text_bp_price']                 = 'Label for price';
$_['text_expand']					= 'Expand';
$_['text_reduce']					= 'Reduce';
$_['text_search']					= 'Search';
$_['text_disabled']					= 'No enabled';

// help
    // inline
$_['help_hint']             = '<span class="help">Sign at prices which points to an additional explanation reg. tax and shipping<br />Sample:  * (space asterix)<br /><strong>Text has to be defined under the tab `Texts > Text for prices ...`</strong></span>';
$_['help_text_id']          = '<span class="help">Text which will be added to all outgoing emails automatically<br />Tip: set the state of the text to `disabled` for not showing it in the shop!</span>';
$_['help_popup']            = '<span class="help">Useful if displayed text is only a few lines, her you can adjust the width and height (in px) of the popup-window if used (see Texts)<br />Set only digits, e.g. 650<br /><strong>Tip</strong>: use <strong>false</strong> for auto adjusting</span>';
$_['help_position_hint']    = '<span class="help">Define here the position to display the text (the hint) about your prices (see also Plugins and Texts).<br />Standard is in the footer area with or instead the `Powered ..`, if you see nothing there choose `Slider` or `Bar` or edit the footer.tpl and add the php-variable `$powered`.<br /><strong>Note</strong>: if MijoShop is in use, select either "Slider" or "Bar", additional the corresponding plugin has to be enabled! If then no text is visible, you have to adjust your template!</span>';
$_['help_specials']         = '<span class="help">If you are showing regular and special price, you should change this to something with surrounding text to avoid punishments. It is not allowed to only strike through the regular price and show beside the special price. This comparison should clearly state what these prices are.<br /><br />Therefore we recommend some text like<br /><strong>Regular price xx,yy â‚¬ currently only xx,yy â‚¬</strong><br /><br />Text can be individual, but easy to understand.<br /><em>The regular price must be something you have used a longer time before, otherwise this could be punished!</em><br /><br />Sample below is based on the original template, if you use another you have to adopt the replacement above.<br /><br />Regular price &lt;span style="color:#5B5B5B;"&gt;%s&lt;/span&gt; &lt;span style="color:#0D6C2A; font-weight:bold;"&gt;now only %s&lt;/span&gt;<br />is displayed as:<br />Regular price <span style="color:#5B5B5B;">%s</span> <span style="color:#0D6C2A; font-weight:bold;">now only %s</span><br /><br />* Use as placeholder (2 times) the <strong>%s</strong><br />* Allowed are all HTML format tags inside the span.tags (color, fontsize, etc.)</span>';
$_['help_image']            = '<span class="help">Width &amp; height of product images. Recommended is 80 x 80 (px)</span>';
$_['help_text_slider']      = '<span class="help">Title when position for hint is `Slider`</span>';
$_['help_baseprice_content']= '<span class="help">Label for the content, e.g. Content</span>';
$_['help_baseprice_price']	= '<span class="help">How the price will be displayed.<br />E.g.: <b>Price for %s %s</b> results in (depend in definition) at product: <b>Price for 1 kg xx,- €</b><br />Placeholders will be replaced automatically with correct values</span>';
$_['help_description_len']	= '<span class="help">Amount of characters the product description will be displayed at final summary (field is empty or 0 = no display)<br />Note: only full words will be displayed</span>';

    // outline
$_['help_text']             = 'Define here the text for all prices with VAT and excl. shipping. It will be displayed in the shop (commonly at the footer position).<br />Display of prices w. or wo. VAT see System Settings.<br /><br />Samples:<ul><li>With link: All prices incl. VAT excl. &lt;a class="popTerms" href="index.php?route=information/information/info&information_id=6" target="_blank" title="Shipping"&gt;Shipping&lt;/a&gt;<br />>>> Display is like: All prices incl. VAT excl. <a class="popTerms" href="index.php?route=information/information/info&information_id=6" target="_blank" title="Shipping">Shipping</a></li><li>Without link: All prices incl. VAT, excl. shipping</li></ul><strong>Notes</strong><ul><li>class="popTerms" is optional, text will be displayed as popup</li><li>If text shall be shown as popup, you have to enable the plugin (see Plugins)</li><li>To help in finding the correct text, below are all stored texts with their ID [] - replace this number below in the text (information_id=xx)</li></ul>';
$_['help_locales']          = 'Below severall settings regarding taxes, geozones, etc.<br />Check first if the country - this shop belongs - is correct, otherwise change.<br /><br />After that step, install following in this order<ol><li>Geo zones</li><li>Tax rates</li><li>Tax classes</li><li>Legal texts (optional)</li></ol><br />Finally (after tax classes have been installed) you can update products and shipping methods with the new taxclass.<br /><strong>Note</strong>: all settings can be adjust afterwards (see <strong>System > Locale Settings &amp; Catalog > Informationen</strong>).';
$_['help_taxes']            = 'Here you can update all stored products and shipping methods with the new taxclass.<br />See tab "Locale Sett." - requires the installation of taxclass(es) prior.<br /><br /><strong>Note</strong>: all products will be updated with the selected taxclass!<br />If you do not want this, do NOT make the update!!';
$_['help_use_plugin']       = 'Below all currently installed plugins (additional plugins are available at <a href="http://osworx.net" target"_blank">OSWorX</a>).<br />Purpose of plugin see text beside each.<br /><strong>Notes</strong><ul><li>with the usage of this module another management tool like the VQMod-manager is not required anymore</li><li>see also additonal settings for some plugins at the other tabs `Common` and `Texts`</li><li><strong>Important</strong>: all changes are based on the standard installation (especially templates).<br />If a custom template is used and the developers of it did not follow the standard rules, it may result to a manual editing of templates!<br />Read more about this <a href="http://osworx.net/de/component/content/article/77-opencart/122-opencart-and-custom-templates" target="_blank">here</a></li></ul>';
$_['help_text_summary']     = 'With the 1. of August 2012 customers shall be informed what the are ordering. Before the last button (before payment) every selected article must be dispalyed with its most important features.<br />Aditionally the button text (see Texts) has to be renamed to something like "Buy", "Order liable for payment" or "Buy ordered items"';
$_['help_tpl_content']      = 'Regular price and special price will be displayed usually with this code:<br /><strong>&lt;span class=&quot;price-old&quot;&gt;&lt;?php echo $price; ?&gt;&lt;/span&gt; &lt;span class=&quot;price-new&quot;&gt;&lt;?php echo $special; ?&gt;&lt;/span&gt;</strong><br />If you do <strong>not</strong> see this code below, then you have to define the replacement above (see field `Replacement`!<br /><br /><strong>Note</strong>: the replacement has to be 1 line, if the code in the template are several lines, you have to edit also the template below and save afterwards!';
$_['help_text_button']      = 'Part of the so called `ButtonlÃ¶sung`: with the 1st of August 2012 every shop has to show a summary of all product features.<br />Additonally the final button (before payment) has to display a valid text.<br />Not valid are (e.g.): Order, Order here, Continue, etc.<br />Valid are (e.g.): Buy, I want to buy, etc.';
$_['help_css']              = 'This CSS stylesheet is used for displaying the product summary at the end of the order process and the slider or bar display.<br />All required definitions for the summary are starting with .attribs - you can change them but not delete!<br />This stylesheet can ba used also to set own definitions - for example for the comparison Normal -  Special Price (see tab `Specials Price`).';
$_['help_attributes']       = 'For the final product summary, each product has to have attributes.<br />The product summary use this order:<ol><li>attributes</li><li>metatag description</li><li>`lazy mode`</li></ol><br />Following table displays all products and if they have attributes and/or a metatag description.<br />You can directly edit the product in a new window by clicking on the name - after that check again.<br /><span style="text-decoration: underline;"><strong>Note</strong>: to meet the requirements you should use the attributes!</span><br /><br />See also help in the manual';
$_['help_base_price']       = 'If a product is a part of a whole (e.g. Kilo, Liter, Meter) the shops has to display for better comparison the price per kg/l/m - this is called the <b>baseprice</b>.<br /><br />See also:<br />AT: <a href="http://www.jusline.at/Preisauszeichnungsgesetz_%28PrAG%29.html" target="_blank">Austria</a><br />DE: <a href="http://www.frankfurt-main.ihk.de/recht/themen/gewerberecht/grundpreis/" target="_blank">Germany</a><br /><a href="http://de.wikipedia.org/wiki/Grundpreisverordnung" target="_blank">Baseprice at Wikipedia (German)</a><br /><br />You can define individual texts below, if not set vars from the language file will be used.<br />If you do not need the baseprice or the plugin is not enabled, settings are useless.';
$_['help_emailtexts']		= 'Common text as "addon" for all outgoing emails and below seperate settings for each orderstate.<br />Beside the common text you can define for each orderstate a own text which will be added.<br />Texts have to created first to see them in the dropdown, see menu <b>Catalog > Texts (Information</b>.<br />Inside each text you can use also links and - if enabled - links to PDF documents.<br /><b>Note</b>: if the text shall not be displayed in the shop, set the state of the text to disabled.';

$_['button_apply']          = 'Apply';
$_['button_install']        = 'Install';
$_['button_help']           = 'Help';
$_['btn_update_products']   = 'Update products';
$_['btn_update_shippings']  = 'Update shipping methods';
$_['btn_upload']            = 'Upload';
$_['btn_save']              = 'Save';
$_['btn_check']             = 'Check';
$_['btn_clear']             = 'Clear logfile';

// messages
$_['msg_tax_installed']     = 'Taxrate successfully installed';
$_['msg_class_installed']   = 'Taxclass successfully installed';
$_['msg_zone_installed']    = 'Geozone successfully installed';
$_['msg_text_installed']    = 'Text successfully installed';
$_['msg_install_taxclasses']= 'You have to install first taxclasses!';
$_['msg_plugin_enabled']    = 'Plugin enabled';
$_['msg_plugin_disabled']   = 'Plugin disabled';
$_['msg_products_updated']  = 'Products successfully updated.';
$_['msg_shipping_updated']  = 'Shipping methods successfully updated.';
$_['msg_file_saved']        = 'File sucessfully saved';
$_['msg_found_products']    = '%s products found, %s have no attributes, %s no metatag.';

// base price
$_['entry_base_price_calculation']	= 'Baseprice calculation';
$_['entry_base_price_factor']       = 'Calculation factor';
$_['entry_package_content']         = 'Packaging unit (quantity - unit)';
$_['text_base_price']               = 'Baseprice info';
$_['help_base_price_calculation']	= '<span style="margin-left: 30px; color: #666666; font-size: 11px; font-weight: normal; font-family: Verdana, Geneva, sans-serif;">Base is usually 1 Kilo/Liter (Factor = 1).<br />Is the package unit under 250 Gramm/Milliliter you can display the value for 100 Gramm/Milliliter (Factor = 100).<br />If no value for factor is given, nothing will be displayd.</span>';
$_['help_base_price_extern']        = '<a href="http://www.juraforum.de/gesetze/pangv/2-grundpreis" title="Baseprice at Juraforum" onclick="window.open(this,\'help\',\'width=850,height=600,top=20,left=20,scrollbars=yes,resizeable=yes\'); return false;"><img src="view/image/osworx/16/link_extern.png" /></a>';
$_['help_package_content']          = '<span style="margin-left: 30px; color: #666666; font-size: 11px; font-weight: normal; font-family: Verdana, Geneva, sans-serif;">Effective packaging unit e.g. 500gr. or 0.5 Kilo, 250ml or 0.25 Liter</span>';

// Error
$_['error_permission']      = 'Warning: no permissions for this action!';
$_['error_hint']            = 'Text for hint is missing!';
$_['error_not_supported']   = 'Taxes for this country are not supported!<br />Please go to "Settings > Locales > Taxes" and define there your values.';
$_['error_popup_height']    = 'Height of popup window has to be defined!';
$_['error_popup_width']     = 'Width of popup window has to be defined!';
$_['error_xmlLine']         = 'You have to define the code in the field `Replacement` (tab Special Price)!';
$_['error_save_file']       = 'Could not save plugin!';
$_['err_tax_installed']     = 'Error: taxrate could not be installed!<br />You have to do this manually.';
$_['err_class_installed']   = 'Error: taxclass could not be installed!<br />You have to do this manually.';
$_['err_zone_installed']    = 'Error: geozone could not be installed!<br />You have to do this manually.';
$_['err_text_installed']    = 'Error: text could not be installed!<br />You have to do this manually.';
$_['err_class']             = 'Error: you have to install first a taxclass!';
$_['err_no_vqmod']          = 'Error! VQMod is required to operate this module!! Please install - <a href="https://github.com/vqmod/vqmod" target="_blank">get VQMod</a>';
$_['err_vqmod_plg_missing'] = 'Error: VQMod plugin [%s] missing!';
$_['err_vqmod_plg_disabled']= 'Note: VQMod Plugin [%s] disabled!';
$_['err_plg_update']        = 'Error: could not update plugin state!';
$_['err_no_locale_sets']    = 'ATTENTION ERROR: could not load locale settings!';
$_['err_no_plugins']        = 'ERROR: required plugins not found!!';
$_['err_vqmod_not_installed']   = 'ERROR!! VQMod does exist, but is NOT installed!<br />Please <a href="%s" target="_blank">click here to install first</a>.';
$_['err_products_updated']		= 'Error: could not update products!';
$_['err_shipping_updated']		= 'Error: could not update shipping methods!';
$_['err_file_saved']			= 'Error: could not save file [%s]!';
$_['err_file_does_not_exist']	= 'Error: requested file [%s] could not be found!';
$_['err_no_content']			= 'Error: no content (replacement) to save!';
$_['err_wrong_oc_version']		= 'ERROR: wrong OpenCart Version!<br />Legal needs at least 1.5.1.0';
$_['err_found_products']		= 'No products without attributes are found - good.';

// $_FILE Upload
    // Errors
$_['error_form_max_file_size']  = 'Error: file is greater than allowed maximal size!';
$_['error_ini_max_file_size']   = 'Error: file is greater as allowed in php.ini!';
$_['error_no_temp_dir']         = 'Error: could not find temporary folder!';
$_['error_no_upload']           = 'Note: no file selected to upload!';
$_['error_partial_upload']      = 'Error: upload not complete!';
$_['error_php_conflict']        = 'Error: unknwon php error!';
$_['error_unknown']             = 'Error: unknown error!';
$_['error_write_fail']          = 'Error: could not write file!';
$_['error_move']                = 'Attention: file could written to server, please check folder permissions!';
$_['error_filetype']            = 'Attention: file not valid! Only plufin files for module `Legal` are allowed.';
    // success
$_['success_upload']            = 'Plugin successful uploaded.';
$_['success_save_file']         = 'Plugin successful saved.';
$_['success_clear_log']         = 'Logfile sucessfully cleared.';