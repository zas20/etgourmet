<?php
// HTTP
define('HTTP_SERVER', 'http://etgourmet.de/admin/');
define('HTTP_CATALOG', 'http://etgourmet.de/');

// HTTPS
define('HTTPS_SERVER', 'http://etgourmet.de//admin/')
define('HTTPS_CATALOG', 'http://etgourmet.de/');

// DIR
define('DIR_APPLICATION', '/var/www/vhosts/etgourmet.de/httpdocs/admin/');
define('DIR_SYSTEM', '/var/www/vhosts/etgourmet.de/httpdocs/system/');
define('DIR_DATABASE', '/var/www/vhosts/etgourmet.de/httpdocs/system/database/');
define('DIR_LANGUAGE', '/var/www/vhosts/etgourmet.de/httpdocs/admin/language/');
define('DIR_TEMPLATE', '/var/www/vhosts/etgourmet.de/httpdocs/admin/view/template/');
define('DIR_CONFIG', '/var/www/vhosts/etgourmet.de/httpdocs/system/config/');
define('DIR_IMAGE', '/var/www/vhosts/etgourmet.de/httpdocs/image/');
define('DIR_CACHE', '/var/www/vhosts/etgourmet.de/httpdocs/system/cache/');
define('DIR_DOWNLOAD', '/var/www/vhosts/etgourmet.de/httpdocs/download/');
define('DIR_LOGS', '/var/www/vhosts/etgourmet.de/httpdocs/system/logs/');
define('DIR_CATALOG', '/var/www/vhosts/etgourmet.de/httpdocs/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'etUser');
define('DB_PASSWORD', 'MkH14H');
define('DB_DATABASE', 'etgourmet');
define('DB_PREFIX', '');
?>