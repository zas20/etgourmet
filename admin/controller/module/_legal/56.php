<?php
/**
 * @version		$Id: 56.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[56] = array(
    'country_id'        => 56,
    'title'             => 'Tschechien',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 15%',
            'rate'      => 15,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'europe'
        ),
        3    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 15%',
            'rate'      => 15,
            'geo_zone'  => 'europe'
        ),
        4    => array(
            'display'   => 'Export EU (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'world'
        ),
        6    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 15%',
            'rate'      => 15,
            'geo_zone'  => 'europe'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'CZ21',
            'description'   => 'Tschechien 21%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 21%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 21%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 21%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        1 => array(
            'title'         => 'CZ15',
            'description'   => 'Tschechien 15%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 15%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 15%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 15%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Tschechien',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);