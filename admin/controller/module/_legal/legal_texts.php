<?php
/**
 * @version		$Id: NAME.xxx 2014-7-10 22:50Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial - http://osworx.net
 */

/**
 * note: each erray must have a unique number, needed later in module!
 * if a country setting has texts, they have to use the same numbering
 * meta* is for OC-forks which use these values
 */

$legalTexts = array(
	0 => array(
		'display'	=> 'Widerruf',
		'information_description' => array(
			'en' => array(
				'title'				=> 'Withdrawal',
				'meta_description'	=> '',
				'meta_keyword'		=> 'withdrawal',
				'description'		=> 'Annex 1 to Article 246 &sect; 2 Section 3 Clause 1 of the Introductory Act to the German Civil Code (EGBGB)<br/><br/><H1 class="revoc_title">Notice of revocation rights</H1><br/><br/><H2 class="revoc_title">Revocation rights</H2><br/>You can revoke your contractual statement within [14 days] (1) without indicating reasons in text form (e.g. letter, fax, e-mail) [or - if the item is delivered to you before expiry of the time limit - also by returning the item ] (2). The deadline takes effect on receipt of this notice in text form (3). Timely dispatch of revocation [or the item] is sufficient to observe the revocation deadline. Revocations are to be submitted to: (4)<br/><br/><H2 class="revoc_title">Revocation consequences (5)</H2><br/>In the event of a valid revocation, the performance delivered by either party is to be returned and the proceeds of any utilization (e.g. interest) submitted.  (6) If you cannot submit / return the received performance and benefits (e.g. utilization benefits) to us, or submit / return them only partially or in a deteriorated condition, you need to compensate us to the required extent.  (7) [You must pay compensation for degraded items only insofar as the deterioration is due to handling of the items beyond testing of properties and functionality.  (8) &quot;Testing of properties and functionality&quot; is to be understood as testing and evaluation of the respective goods as is possible and common in retail shops, for instance. (9) Items which can be dispatched in packages are to be returned at our [expense and]  (10) risk. Items which cannot be dispatched in packages will be fetched from your premises.] (2) Payment obligations must be fulfilled within 30 days. The time limit becomes effective on dispatch of your revocation notice [or the item] (2) for you, and on its receipt for us.<br/><br/><H2 class="revoc_title">Special notes</H2><br/> (11)<br/>(12) <br/>(13) <br/><br/>(Location), (date) (consumer\'s signature) (14)<br/><br/><H2 class="revoc_title">Notes on composition</H2><br/> (1) If the revocation notice is provided not by contract conclusion at the latest, but only afterward, the supplement in brackets must read &quot;one month&quot;.  In this case, composition note 9 also applies if the reference provided there is not issued in text form by contract conclusion at the latest. In the case of contracts on distance sales, a revocation notice issued in text form immediately after contract conclusion is equivalent to one issued on contract conclusion if the entrepreneur has informed the consumer as per Article 246 &sect; 1 Section 1 Number 10 EGBGB.<br/><br/> (2) The supplement in brackets does not apply to performances not involving provision of items.<br/><br/> (3) In any of the special cases mentioned next, the following must be inserted:<br/><br/> a) In the case of contracts to be concluded in writing: &quot;, but not before a contractual document, or your written request, or a duplicate of either of these has also been made available to you&quot;;<br/><br/> (b) In the case of distance sales contracts (&sect; 312b Section 1 Clause 1 of the German Civil Code (BGB)) concerning:<br/><br/> aa) Delivery of goods: &quot;, but not before receipt of the goods by the recipient (not before receipt of the first partial delivery in the case of recurrent supply of similar goods), nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with &sect; 1 Sections 1 and 2 EGBGB&quot;;<br/><br/>bb) Provision of services other than payment services: &quot;, but not before contract conclusion, nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with &sect; 1 Sections 1 and 2 EGBGB&quot;;<br/><br/>cc) Provision of payment services:<br/><br/>aaa) In the case of framework agreements on payment services: &quot;, but not before contract conclusion, nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with &sect; 1 Section 1 Numbers 8 to 12, Section 2 Numbers 2, 4 and 8, as well as Article 248 &sect; 4 Section 1 EGBGB&quot;;<br/><br/>bbb) In the case of instruments for small amounts in the sense of &sect; 675i Section 1 BGB: &quot;, but not before contract conclusion, nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with Section 1 Numbers 8 to 12, Section 2 Numbers 2, 4 and 8, as well as Article 248 &sect; 11 Section 1 EGBGB&quot;;<br/><br/>ccc) In the case of single payment contracts: &quot;, but not before contract conclusion, nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with &sect; 1 Section 1 Numbers 8 to 12, Section 2 Numbers 2, 4 and 8 as well as Article 248 &sect; 13, Sec. 1 EGBGB&quot;;<br/><br/> (c) In the case of contracts forming part of e-commerce (&sect; 312g Section 1 Clause 1 BGB): &quot;, but not before fulfilment of our obligations as per &sect; 312g Section 1 Clause 1 BGB in conjunction with Article 246 &sect; 3 EGBGB&quot;;<br/><br/> (d) In the case of trial purchases (&sect; 454 BGB): &quot;; but not before the sales contract has become binding through your endorsement of the purchased item&quot;;<br/><br/>If notice is served for a contract falling under several of the special cases mentioned above (for example, a distance sales contract for the supply of goods in e-commerce), the respective supplements must be combined (in this example, as follows: &quot;; but not before receipt of the goods by the recipient  [not before receipt of the first partial delivery in the case of recurrent supply of similar goods], nor before fulfilment of our information obligations as per Article 246 &sect; 2 in conjunction with &sect; 1 Sections 1 and 2 EGBGB, as well as our obligations as per &sect; 312g Section 1 Clause 1 BGB in conjunction with Article 246 &sect; 3 EGBGB&quot;). If supplements to be combined are linguistically identical, their wording need not be repeated.<br/><br/> (4) Insert: Name/company and summonable address of the revocation\'s intended recipient.<br/><br/>Also specifiable: Fax number, e-mail address and/or Internet address if the consumer receives confirmation of their revocation statement to the entrepreneur.<br/><br/> (5) This section may be omitted if the mutual performances are rendered only after expiry of the revocation deadline. The same applies if a reversal does not come into consideration (e.g. collection of a guarantee).<br/><br/> (6) If a fee for toleration of overdraft within the meaning of section &sect; 505 BGB has been agreed, the following must be inserted here:<br/><br/>&quot;If you overdraw your account without having been granted related approval, or exceed the overdraft scope granted to you, we may not demand any compensation of costs or interest from you beyond repayment of the overdraft or excess amount, if we have not informed you duly about the conditions and consequences of the overdraft or transgression (e.g. applicable interest on debt, costs).&quot;<br/><br/> (7) In the case of distance sales contracts for services, the following sentence must be added:<br/><br/>&quot;This may oblige you to nonetheless fulfil the contractual payment obligations for the period up to revocation.&quot;<br/><br/> (8) In the case of distance sales contracts for the supply of goods, the previously added sentence must be replaced by the following sentence: &quot;You must pay compensation for  degraded items and derived benefits only insofar as the benefits or deteriorations are due to handling of the items beyond testing of properties and functionality.&quot;<br/><br/> (9) If a reference concerning the obligation to compensate lost value as per &sect; 357 Section 3 Clause 1 BGB is not issued in text form by contract conclusion at the latest, the two preceding sentences must be replaced by the following supplement: &quot;You need not pay any compensation for deterioration caused by usage of the item for the intended purposes.&quot; In the case of distance sales contracts, a note provided in text form immediately after contract conclusion is equivalent to a note provided on contract conclusion, if the entrepreneur has informed the consumer in time, prior to issue of their contractual declaration, about the obligation for compensation of lost value in a manner appropriate for the employed means of telecommunication .<br/><br/>In the case of distance sales contracts for the supply of goods, the following must be added:<br/><br/>&quot;You must pay compensation for derived benefits only insofar as you have used the goods in a manner that goes beyond testing of properties and functionality. &quot;Testing of properties and functionality&quot; is to be understood as testing and evaluation of the respective goods as is possible and common in retail shops, for instance.<br/><br/>(10) If the consumer has agreed to bear shipping costs according to &sect; 357 Section 2 Clause 3 BGB, the addition in brackets can be omitted. Instead, the following must be inserted after &quot;to be returned at our [expense and] (10) risk.&quot;<br/><br/>&quot;You have to bear the regular costs of return if the delivered goods correspond to the order, and if the price of the goods to be returned does not exceed 40 EUR or, in case of a higher item price, if you had provided no consideration or contractually agreed partial payment at the time of revocation. Otherwise the return is free of charge for you.&quot;<br/><br/> (11) In the case of revocation rights as per &sect; 312d, Section 1 BGB, applicable to distance sales contracts for provision of services, the following reference must be included:<br/><br/>&quot;Your revocation right expires prematurely if the contract has been completely fulfilled by both parties at your explicit request before you exercise your revocation right.&quot;<br/><br/> (12) The following note for financed transactions can be omitted if there is no associated deal:<br/><br/>&quot;If you finance this contract through a loan and revoke it later, you are no longer bound to the loan contract either, if both contracts form a single economic unit. This can be assumed, in particular, if we are your lender or if your lender is enabling the financing with the help of our cooperation. If the loan has already been received by us once the revocation takes effect or the goods are returned, your lender, as concerns the legal consequences of the revocation or return in relation to you, assumes our rights and obligations arising from the financed contract. The latter does not apply if the present contract\'s object is the purchase of financial instruments (e.g. securities, foreign exchange or derivatives).<br/><br/>If you want to avoid contractual binding as far as possible, make use of your right of revocation, and also revoke the loan contract if you have this additional right to withdrawal.&quot;<br/><br/>In the case of financed acquisition of a plot of land or an equivalent right, Sentence 2 of the afore-mentioned note must be changed as follows:<br/><br/>&quot;This is to be assumed only if the parties to both contracts are identical or, if in addition to providing loans, the lender promotes your real-estate transaction through interaction with the seller by wholly or partly taking over their sales interests, or their functions in planning, advertising or project implementation, or by unilaterally favouring the seller.&quot;<br/><br/> (13) The following note for distance sales contracts for financial services can be omitted in an absence of any additional distance sales contract for services:<br/><br/>&quot;On revocation of this distance sales contract for financial services, you are also no longer bound to any additional distance sales contract, if this contract\'s object is a further service provided by us or a third party on the basis of an agreement between us and the third party.&quot;<br/><br/> (14) The location, date and signature strip can be omitted. In this case, these details must be replaced either by the words &quot;End of revocation notice&quot; or by the words &quot;Your (insert: entrepreneur\'s company)&quot;.<br/><br/><br/><i>Source: </i><a href="http://www.twigg.de/widerrusbelehrungs-muster.htm" target="_blank">Withdrawal/Revocation Sample in English</a><br/><br/>'
			),
			'de' => array(
				'title'				=> 'Widerruf',
				'meta_description'	=> '',
				'meta_keyword'		=> 'widerruf',
				'description'		=> '<p><b style="color: red;">Nachstehende Punkte <b>[x]</b> sind mit dem jeweiligen Text zu ergänzen!</b></p>
	<p><b>Widerrufsrecht</b></p>
	<p>Sie haben das Recht diesen Vertrag binnen vierzehn Tagen ohne Angabe von Gründen zu widerrufen.</p>
	<p>Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag <b>[1]</b>.</p>
	<p>Um Ihr Widerrufsrecht auszuüben, müssen Sie uns (<b>[2]</b>) mittels einer eindeutigen Erklärung (z.B. ein mit der Post versandter Brief, Telefax oder Email) über Ihren Entschluss, diesen Vertrag zu widerrufen, informieren. Sie können dafür das beigefügte Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist. <b>[3]</b></p>
	<p>Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.</p>
	<p><b>Folgen des Widerrufs</b></p>
	<p>Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, günstigste Standardlieferung gewählt haben), unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist. Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. <b>[4]</b> <b>[5]</b> <b>[6]</b></p>
	<p><b>[1]</b><br />
	a) im Falle eines Dienstleistungsvertrags oder eines Vertrags über die Lieferung von Wasser, Gas oder Strom, wenn sie nicht in einem begrenzten Volumen oder in einer bestimmten Menge zum Verkauf angeboten werden, von Fernwärme oder von digitalen Inhalten, die nicht auf einem körperlichen Datenträger geliefert werden:<br />
	<b>des Vertragsabschlusses.</b>;
	<br />
	b) im Falle eines Kaufvertrags:<br />
	<b>, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die Waren in Besitz genommen haben bzw. hat.</b>;
	<br />
	c) im Falle eines Vertrags über mehrere Waren, die der Verbraucher im Rahmen einer einheitlichen Bestellung bestellt hat und die getrennt geliefert werden:<br />
	<b>, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die letzte Ware in Besitz genommen haben bzw. hat.</b>;
	<br />
	d) im Falle eines Vertrags über die Lieferung einer Ware in mehreren Teilsendungen oder Stücken:<br />
	<b>, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die letzte Teilsendung oder das letzte Stück in Besitz genommen haben bzw. hat.</b>;
	<br />
	e) im Falle eines Vertrags zur regelmäßigen Lieferung von Waren über einen festgelegten Zeitraum hinweg:<br />
	<b>, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die erste Ware in Besitz genommen haben bzw. hat.</b></p>
	<p><b>[2]</b><br />
	Hier eigenen Namen, Anschrift, Telefonnummer, Emailadresse und optional Faxnummer einfügen</p>
	<p><b>[3]</b><br />
	Wird dem Verbraucher die Wahl eingeräumt die Information über seinen Widerruf des Vertrags auf der eigenen Webseite elektronisch auszufüllen und zu übermitteln, dann Folgendes einfügen:<br />
	Sie können das Muster-Widerrufsformular oder eine andere eindeutige Erklärung auch auf unserer Webseite [<i>Hier WebaAdresse einfügen</i>] elektronisch ausfüllen und übermitteln.<br />
	Machen Sie von dieser Möglichkeit Gebrauch, so werden wir Ihnen unverzüglich (z. B. per E-Mail) eine Bestätigung über den Eingang eines solchen Widerrufs übermitteln.</p>
	<p><b>[4]</b><br />
	Im Falle von Kaufverträgen, in denen Sie nicht angeboten haben, im Fall des Widerrufs die Waren selbst abzuholen, folgendes einfügen:<br />
	<b>Wir können die Rückzahlung verweigern, bis wir die Waren wieder zurückerhalten haben oder bis Sie den Nachweis erbracht haben, dass Sie die Waren zurückgesandt haben, je nachdem, welches der frühere Zeitpunkt ist.</b>
	</p>
	<p><b>[5]</b><br />
	Wenn der Verbraucher Waren im Zusammenhang mit dem Vertrag erhalten hat:<br />
	a) entweder:
	<b>Wir holen die Waren ab.</b> oder<br />
	<b>Sie haben die Waren unverzüglich und in jedem Fall spätestens binnen vierzehn Tagen ab dem Tag, an dem Sie uns über den Widerruf dieses Vertrags unterrichten, an ... uns oder an [<i>hier sind gegebenenfalls der Name und die Anschrift der von
	Ihnen zur Entgegennahme der Waren ermächtigten Person einzufügen</i>] zurückzusenden oder zu übergeben. Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen absenden.</b><br />
	b) oder:<br />
	<b>Wir tragen die Kosten der Rücksendung der Waren.</b><br />
	<b>Sie tragen die unmittelbaren Kosten der Rücksendung der Waren.</b><br /></p>
	<p>Wenn Sie bei einem Fernabsatzvertrag nicht anbieten, die Kosten der Rücksendung der Waren zu tragen und die Waren aufgrund ihrer Beschaffenheit nicht normal mit der Post zurückgesandt werden können:<br />
	<b>Sie tragen die unmittelbaren Kosten der Rücksendung der Waren in Höhe von ... EUR [<i>Betrag einfügen</i>].</b><br />,
	oder wenn die Kosten vernünftigerweise nicht im Voraus berechnet werden können:<br />
	<b>Sie tragen die unmittelbaren Kosten der Rücksendung der Waren. Die Kosten werden auf höchstens etwa ... EUR [<i>Betrag einfügen</i>] geschätzt.</b><br />
	oder<br />
	— wenn die Waren bei einem außerhalb von Geschäftsräumen geschlossenen Vertrag aufgrund ihrer Beschaffenheit nicht normal mit der Post zurückgesandt werden können und zum Zeitpunkt des Vertragsschlusses zur Wohnung des Verbrauchers geliefert worden sind:<br />
	<b>Wir holen die Waren auf unsere Kosten ab.</b><br />und<br />
	<b>Sie müssen für einen etwaigen Wertverlust der Waren nur aufkommen, wenn dieser Wertverlust auf einen zur Prüfung der Beschaffenheit, Eigenschaften und Funktionsweise der Waren nicht notwendigen Umgang mit ihnen zurückzuführen ist.</b></p>
	<p><b>[6]</b><br />
	Im Falle eines Vertrags zur Erbringung von Dienstleistungen oder der Lieferung von Wasser, Gas oder Strom, wenn sie nicht in einem begrenzten Volumen oder in einer bestimmten Menge zum Verkauf angeboten werden, oder von Fernwärme fügen Sie Folgendes ein:<br />
	<b>Haben Sie verlangt, dass die Dienstleistungen oder Lieferung von Wasser/Gas/Strom/Fernwärme [<b>Unzutreffendes streichen</b>] während der Widerrufsfrist beginnen soll, so haben Sie uns einen angemessenen Betrag zu zahlen, der dem Anteil der bis zu dem
	Zeitpunkt, zu dem Sie uns von der Ausübung des Widerrufsrechts hinsichtlich dieses Vertrags unterrichten, bereits erbrachten Dienstleistungen im Vergleich zum Gesamtumfang der im Vertrag vorgesehenen Dienstleistungen entspricht.</b></p>'
	        )
		),
		'information_store' => array(
	        0 => 0
	    ),
		'keyword'    => 'widerruf',
		'status'     => 1,
		'sort_order' => 5,
		'bottom'		=> 1,
		'information_layout' => array(
	        0 => array(
	            'layout_id' => 0
	        )
	    )
	),
	1 => array(
	    'display'   => 'Datenschutz',
	    'information_description' => array(
			'en' => array(
	            'title'         	=> 'Privacy',
	            'meta_description'	=> '',
				'meta_keyword'		=> 'privacy',
	            'description'   	=> '<p><h1>Privacy Statement</h1><h2>General</h2>Your personal data (e.g. title, name, house address, e-mail address, phone number, bank details, credit card number) are processed by us only in accordance with the provisions of German data privacy laws. The following provisions describe the type, scope and purpose of collecting, processing and utilizing personal data. This data privacy policy applies only to our web pages. If links on our pages route you to other pages, please inquire there about how your data are handled in such cases.<br/><h2>Inventory data</h2>(1) Your personal data, insofar as these are necessary for this contractual relationship (inventory data) in terms of its establishment, organization of content and modifications, are used exclusively for fulfilling the contract. For goods to be delivered, for instance, your name and address must be relayed to the supplier of the goods. <br/>(2) Without your explicit consent or a legal basis, your personal data are not passed on to third parties outside the scope of fulfilling this contract. After completion of the contract, your data are blocked against further use. After expiry of deadlines as per tax-related and commercial regulations, these data are deleted unless you have expressly consented to their further use.<br/><h2>Web analysis with Google Analytics</h2>This website uses Google Analytics, a web analysis service of Google Inc. (Google). Google Analytics uses cookies, i.e. text files stored on your computer to enable analysis of website usage by you. Information generated by the cookie about your use of this website is usually transmitted to a Google server in the United States and stored there. In case of activated IP anonymization on this website, however, your IP address is previously truncated by Google within member states of the European Union or in other states which are party to the agreement on the European Economic Area. Only in exceptional cases is a full IP address transmitted to a Google server in the United States and truncated there. On behalf this website\'s owner, Google will use this information to evaluate your use of the website, compile reports about website activities, and provide the website\'s operator with further services related to website and Internet usage. The IP address sent from your browser as part of Google Analytics is not merged with other data by Google. You can prevent storage of cookies by appropriately setting your browser software; in this case, however, please note that you might not be able to fully use all functions offered by this website. In addition, you can prevent data generated by the cookie and relating to your use of the website (including your IP address) from being collected and processed by Google, by downloading and installing a browser plug-in from the following link: <a href="http://tools.google.com/dlpage/gaoptout?hl=en" target="_blank">http://tools.google.com/dlpage/gaoptout?hl=en</a><br/><br/>This website uses Google Analytics with the extension &quot;anonymizeIP()&quot;, IP addresses being truncated before further processing in order to rule out direct associations to persons.<br/><h2>Information about cookies</h2>(1) To optimize our web presence, we use cookies. These are small text files stored in your computer\'s main memory. These cookies are deleted after you close the browser. Other cookies remain on your computer (long-term cookies) and permit its recognition on your next visit. This allows us to improve your access to our site.<br/>(2) You can prevent storage of cookies by choosing a &quot;disable cookies&quot; option in your browser settings. But this can limit the functionality of our Internet offers as a result.<br/><h2>Social plug-ins from Facebook</h2>We use social plug-ins from facebook.com, operated by Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA. The plug-ins can be recognized by way of the Facebook logo or the supplement &quot;Facebook Social Plug-in&quot;. For example, if you click on the &quot;Like&quot; button or leave a comment, the relevant information is transmitted directly from your browser to Facebook and stored there. Furthermore, Facebook makes your likes public for your Facebook friends. If you are logged into Facebook, it can assign the invocation of our page directly to your Facebook account. Even if you are not logged in or don\'t have a Facebook account, your browser sends information (e.g. which web pages you have called up, your IP address) which is then stored by Facebook. For details about handling of your personal data by Facebook and your related rights, please refer to the data privacy policy of Facebook: <a href="http://www.facebook.com/policy.php" target="_blank">http://www.facebook.com/policy.php</a>. If you do not want Facebook to map data collected about you via our Web sites to your Facebook account, you must log out of Facebook before you visit our web pages.<br/><h2>Social plug-ins from Twitter</h2>With Twitter and its Retweet functions, we use social plug-ins from Twitter.com, operated by Twitter Inc. 795 Folsom St., Suite 600, San Francisco, CA 94107. If you use Retweet, the websites visited by you are announced to third parties and associated with your Twitter account. Details about handling of your data by Twitter as well as your rights and setting options for protecting your personal information can be found in Twitter\'s data privacy policy: <a href="http://twitter.com/privacy" target="_blank">http://twitter.com/privacy </a><br/><h2>Newsletter</h2>Following subscription to the newsletter, your e-mail address is used for our own advertising purposes until you cancel the newsletter again. Cancellation is possible at any time. The following consent has been expressly granted by you separately, or possibly in the course of an ordering process: <br/>( Ich möchte den Newsletter abonnieren und willige ein meine Adresse dafür zu verwenden. )<br/>You may revoke your consent at any time with future effect. If you no longer want to receive the newsletter, then unsubscribe as follows: <br/>( Email, Link auf Webseite )<br/><h2>Disclosure</h2>According to the Federal Data Protection Act, you have a right to free-of-charge information about your stored data, and possibly entitlement to correction, blocking or deletion of such data. Inquiries can be directed to the following e-mail addresses: ( <a href="mailto:MY.EMAIL@MYSHOP.com">MY.EMAIL@MYSHOP.com</a> )<br/><br/><i>Quelle: </i><a href="http://www.twigg.de/" target="_blank">twigg.de</a><br/><br/>'
			),
			'de' => array(
	            'title'         	=> 'Datenschutz',
	            'meta_description'	=> '',
				'meta_keyword'		=> 'datenschutz',
	            'description'   	=> '<p><strong>Datenschutz</strong></p>
	<p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder Emailadressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.
	</p>
	<p>Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per Email) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich. </p>
	<p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.</p><br />
	<p><strong>Datenschutzerklärung für die Nutzung von Facebook-Plugins (Like-Button)</strong></p>
	 <p>Auf unseren Seiten sind Plugins des sozialen Netzwerks Facebook (Facebook Inc., 1601 Willow Road, Menlo Park, California, 94025, USA) integriert. Die Facebook-Plugins erkennen Sie an dem Facebook-Logo oder dem "Like-Button" ("Gefällt mir") auf unserer Seite. Eine Übersicht über die Facebook-Plugins finden Sie hier: <a href="http://developers.facebook.com/docs/plugins/" target="_blank">http://developers.facebook.com/docs/plugins/</a>.<br />Wenn Sie unsere Seiten besuchen, wird über das Plugin eine direkte Verbindung zwischen Ihrem Browser und dem Facebook-Server hergestellt. Facebook erhält dadurch die Information, dass Sie mit Ihrer IP-Adresse unsere Seite besucht haben. Wenn Sie den Facebook "Like-Button" anklicken während Sie in Ihrem Facebook-Account eingeloggt sind, können Sie die Inhalte unserer Seiten auf Ihrem Facebook-Profil verlinken. Dadurch kann Facebook den Besuch unserer Seiten Ihrem Benutzerkonto zuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Facebook erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von facebook unter <a href="http://de-de.facebook.com/policy.php" target="_blank">http://de-de.facebook.com/policy.php</a></p>
	<p>Wenn Sie nicht wünschen, dass Facebook den Besuch unserer Seiten Ihrem Facebook-Nutzerkonto zuordnen kann, loggen Sie sich bitte aus Ihrem Facebook-Benutzerkonto aus.</p><br />
	<p><strong>Datenschutzerklärung für die Nutzung von Google Analytics</strong></p>
	 <p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt.</p>
	<p>Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.</p>
	<p>Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: <a href="http://tools.google.com/dlpage/gaoptout?hl=de">http://tools.google.com/dlpage/gaoptout?hl=de</a>.<br />
	<p><strong>Datenschutzerklärung für die Nutzung von Google Adsense</strong></p>
	 <p>Diese Website benutzt Google AdSense, einen Dienst zum Einbinden von Werbeanzeigen der Google Inc. ("Google"). Google AdSense verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website ermöglicht. Google AdSense verwendet auch so genannte Web Beacons (unsichtbare Grafiken). Durch diese Web Beacons können Informationen wie der Besucherverkehr auf diesen Seiten ausgewertet werden.</p>
	<p>Die durch Cookies und Web Beacons erzeugten Informationen über die Benutzung dieser Website (einschließlich Ihrer IP-Adresse) und Auslieferung von Werbeformaten werden an einen Server von Google in den USA übertragen und dort gespeichert. Diese Informationen können von Google an Vertragspartner von Google weiter gegeben werden. Google wird Ihre IP-Adresse jedoch nicht mit anderen von Ihnen gespeicherten Daten zusammenführen.</p>
	<p>Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.</p><br />
	<p><strong>Datenschutzerklärung für die Nutzung von Google +1</strong></p>
	 <p><i>Erfassung und Weitergabe von Informationen:</i><br />Mithilfe der Google +1-Schaltfläche können Sie Informationen weltweit veröffentlichen. Über die Google +1-Schaltfläche erhalten Sie und andere Nutzer personalisierte Inhalte von Google und unseren Partnern. Google speichert sowohl die Information, dass Sie für einen Inhalt +1 gegeben haben, als auch Informationen über die Seite, die Sie beim Klicken auf +1 angesehen haben. Ihre +1 können als Hinweise zusammen mit Ihrem Profilnamen und Ihrem Foto in Google-Diensten, wie etwa in Suchergebnissen oder in Ihrem Google-Profil, oder an anderen Stellen auf Websites und Anzeigen im Internet eingeblendet werden.<br />Google zeichnet Informationen über Ihre +1-Aktivitäten auf, um die Google-Dienste für Sie und andere zu verbessern. Um die Google +1-Schaltfläche verwenden zu können, benötigen Sie ein weltweit sichtbares, öffentliches Google-Profil, das zumindest den für das Profil gewählten Namen enthalten muss. Dieser Name wird in allen Google-Diensten verwendet. In manchen Fällen kann dieser Name auch einen anderen Namen ersetzen, den Sie beim Teilen von Inhalten über Ihr Google-Konto verwendet haben. Die Identität Ihres Google-Profils kann Nutzern angezeigt werden, die Ihre Emailadresse kennen oder über andere identifizierende Informationen von Ihnen verfügen.<br /><br />
	<i>Verwendung der erfassten Informationen:</i><br />
	Neben den oben erläuterten Verwendungszwecken werden die von Ihnen bereitgestellten Informationen gemäß den geltenden Google-Datenschutzbestimmungen genutzt. Google veröffentlicht möglicherweise zusammengefasste Statistiken über die +1-Aktivitäten der Nutzer bzw. gibt diese an Nutzer und Partner weiter, wie etwa Publisher, Inserenten oder verbundene Websites. </p><br />
	<p><strong>Datenschutzerklärung für die Nutzung von Twitter</strong></p>
	 <p>Auf unseren Seiten sind Funktionen des Dienstes Twitter eingebunden. Diese Funktionen werden angeboten durch die Twitter Inc., Twitter, Inc. 1355 Market St, Suite 900, San Francisco, CA 94103, USA. Durch das Benutzen von Twitter und der Funktion "Re-Tweet" werden die von Ihnen besuchten Webseiten mit Ihrem Twitter-Account verknüpft und anderen Nutzern bekannt gegeben. Dabei werden auch Daten an Twitter übertragen.</p>
	<p>Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Twitter erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Twitter unter <a href="http://twitter.com/privacy" target="_blank">http://twitter.com/privacy</a>.</p>
	<p>Ihre Datenschutzeinstellungen bei Twitter können Sie in den Konto-Einstellungen unter <a href="http://twitter.com/account/settings" target="_blank">http://twitter.com/account/settings</a> ändern.</p>
	<br />
	<p><i>Quellen: <a rel="nofollow" href="http://www.e-recht24.de/muster-datenschutzerklaerung.html" target="_blank">eRecht24</a>, <a rel="nofollow" href="http://www.e-recht24.de/artikel/datenschutz/6590-facebook-like-button-datenschutz-disclaimer.html" target="_blank">eRecht24 Datenschutzerklärung für Facebook</a>, <a rel="nofollow" href="http://www.google.com/intl/de/analytics/learn/privacy.html" target="_blank">Google Analytics Bedingungen</a>, <a rel="nofollow" href="http://www.e-recht24.de/artikel/datenschutz/6635-datenschutz-rechtliche-risiken-bei-der-nutzung-von-google-analytics-und-googleadsense.html" target="_blank">Google Adsense Haftungsausschluss</a>, <a rel="nofollow" href="http://www.google.com/intl/de/+/policy/+1button.html" target="_blank">Datenschutzerklärung für Google +1</a>, <a rel="nofollow" href="http://twitter.com/privacy" target="_blank">Datenschutzerklärung für Twitter</a></i></p>'
	        )
		),
		'information_store' => array(
	        0 => 0
	    ),
		'keyword'    => 'datenschutz',
		'status'     => 1,
		'sort_order' => 6,
		'bottom'		=> 1,
		'information_layout' => array(
	        0 => array(
	            'layout_id' => 0
	        )
	    )
	),
	2 => array(
	    'display'   => 'Zusammenfassung (Buttonlösung)',
	    'information_description' => array(
			'en' => array(
	            'title'				=> 'Product Summary',
	            'meta_description'	=> '',
				'meta_keyword'		=> 'Buttonlösung',
	            'description'		=> '<p>
	Below the summary of your order.<br />
	To change an item click the link "Change".</p>
	<p>
	I have read the <a class="popTerms" href="index.php?route=information/information/info&amp;information_id=13" title="Terms">Terms</a> and <a class="popTerms" href="index.php?route=information/information/info&amp;information_id=17" title="Withdrawal">Widthdrawal</a> and agree.</p>'
			),
			'de' => array(
	            'title'				=> 'Zusammenfassung (Buttonlösung)',
	            'meta_description'	=> '',
				'meta_keyword'		=> 'Buttonlösung',
	            'description'		=> '<p>
	Nachstehend eine Zusammenfassung der Bestellung.<br />
	Für allfällige Änderungen bitte den Link "Ändern" oder das jeweilige Produktbild anklicken.</p>
	<p>
	<a class="popTerms" href="index.php?route=information/information/info&amp;information_id=13" title="AGB">AGB</a> und <a class="popTerms" href="index.php?route=information/information/info&amp;information_id=17" title="Widerruf">Widerruf</a> habe ich gelesen und bin damit einverstanden.</p>'
	        )
		),
		'information_store' => array(
	        0 => 0
	    ),
		'keyword'    => 'zusammenfassung',
		'status'     => 1,
		'sort_order' => 6,
		'information_layout' => array(
	        0 => array(
	            'layout_id' => 0
	        )
	    )
	),
	3 => array(
	    'display'   => 'Autom. Emailanhang',
	    'information_description' => array(
			'en' => array(
                'title'				=> 'Email Addon',
                'meta_description'	=> '',
				'meta_keyword'		=> '',
                'description'		=> '<p>Firma NAME HERE / CEO: NAME<br />ADDRESS<br />Tel: +41 123 45678 / Fax: +41 123 45678 / email: EMAIL HERE<br />USt-IdNr. AT 1234567 / Steuer-Nr.: 123/4567/8901 / Registered at XXXXX<br />Either you display the links to the texts or paste here the texts itself:<br /><a href="http://SHOPURL/agb">TOS</a> - <a href="http://SHOPURL/datenschutz">Privacy</a> - <a href="http://SHOPURL/versand">Delivery</a> - <a href="http://SHOPURL/withdrawal">Withdrawal</a> - <a href="http://SHOPURL/withdrawalform">Withdrawal PDF</a>
                </p>'
			),
			'de' => array(
                'title'				=> 'Emailanhang',
                'meta_description'	=> '',
				'meta_keyword'		=> '',
                'description'		=> '<p>Firma NAME HIER / Geschäftsführer: NAME ADRESSE<br />Tel: +41 123 45678 / Fax: +41 123 45678 / email: EMAIL HIER<br />USt-IdNr. XX1234567 / Steuer-Nr.: 123/4567/8901 / Registriert bei XXXXX<br />Hier wahlweise die Links zu den einzelnen Texten anführen oder die Text selber einfügen:<br /><a href="http://SHOPURL/agb">AGB</a> - <a href="http://SHOPURL/datenschutz">Privatsphäre und Datenschutz</a> - <a href="http://SHOPURL/versand">Versand und Lieferung</a> - <a href="http://SHOPURL/widerruf">Widerruf</a> - <a href="http://SHOPURL/widerrufsformular">Widerrufsformular PDF</a></p>'
            )
		),
		'information_store' => array(
	        0 => 0
	    ),
		'keyword'    => 'email',
		'status'     => 1,
		'sort_order' => 7,
		'information_layout' => array(
	        0 => array(
	            'layout_id' => 0
	        )
	    )
	),
	4 => array(
	    'display'   => 'Muster Widerrufformular',
		'information_store' => array(
	        0 => 0
	    ),
		'keyword'    => 'widerrufsformular',
		'status'     => 1,
		'sort_order' => 8,
		'information_layout' => array(
	        0 => array(
	            'layout_id' => 0
	        )
	    ),
	    'information_description' => array(
			'en' => array(
	            'title'				=> 'Sample Withdrawal Formular',
	            'meta_description'	=> '',
				'meta_keyword'		=> 'Revocation- Withdrawal',
	            'description'		=> '<p>If you want to cancel the contract, please fill in your details and send this formular back (email or yellow post).</p>
	<p>
	To<br /><br />
	Name of the shop<br />
	Adress of Shop<br />
	Emailadress or faxnumber of shop</p>
	<p>I the undersigned cancel the contract of purchasing following goods:<br /><br />
	..............................................................................<br />
	..............................................................................<br />
	(articles, model and price)</p>
	<p>Products ordered on:<br /><br />
	.............................<br />
	Date</p>
	<p>Products obtained:<br /><br />
	.............................<br />
	Date/p>
	<p>Name and address of buyer<br /><br />
	.............................<br />
	.............................<br />
	.............................<br />
	.............................<br />
	Date</p>
	<p>....................................................<br />
	Signature customer<br />
	(only at written withdrawal)</p>'
			),
			'de' => array(
	            'title'				=> 'Muster Widerrufsformular',
	            'meta_description'	=> '',
				'meta_keyword'		=> 'Widerruf',
	            'description'		=> '<p>Wenn Sie den Vertrag widerrufen wollen, dann füllen Sie bitte dieses Formular aus und senden Sie es zurück (Email oder Post).</p>
	<p>
	An<br /><br />
	Name des Shops / des Unternehmers<br />
	Anschrift Shopbetreiber<br />
	Emailadress oder ggf. Faxnummer Shopbetreiber</p>
	<p>Hiermit widerrufe(n) ich/wir den von mir/uns abgeschlossenen Vertrag über den Kauf der folgenden Waren / die Erbringung der folgenden Dienstleistung:<br /><br />
	..............................................................................<br />
	..............................................................................<br />
	(Name der Ware, ggf. Bestellnummer und Preis)</p>
	<p>Ware bestellt am:<br /><br />
	.............................<br />
	Datum</p>
	<p>Ware erhalten am:<br /><br />
	.............................<br />
	Datum</p>
	<p>Name und Anschrift des Verbrauchers<br /><br />
	.............................<br />
	.............................<br />
	.............................<br />
	.............................<br />
	Datum</p>
	<p>....................................................<br />
	Unterschrift Kunde<br />
	(nur bei schriftlichem Widerruf)</p>'
	        )
		)
	),
	5 => array(
	    'display'   => 'Impressum',
		'information_store' => array(
	        0 => 0
	    ),
		'keyword'    => 'impressum',
		'status'     => 1,
		'sort_order' => 8,
		'information_layout' => array(
	        0 => array(
	            'layout_id' => 0
	        )
	    ),
	    'information_description' => array(
			'en' => array(
	            'title'				=> 'Imprint',
	            'meta_description'	=> '',
				'meta_keyword'		=> 'Imprint',
	            'description'		=> '<h1>Legal Disclosure</h1>Information in accordance with section 5 TMG<br/><br/>NAME SHOPOWNER<br/>NAME COMPANY<br/>STREET<br/>ADD. ADRESS<br/>ZIP CITY<br/><h2>Contact</h2>Telephone: 12345678<br/>E-Mail: <a href="mailto:MY.EMAIL@SHOP.COM">MY.EMAIL@SHOP.COM</a><br/>Webshop: <a href="http://MYSHOP.com" target="_blank">http://MYSHOP.com</a><br/><h2>VAT number</h2>VAT indentification number in accorance with section 27 a of the German VAT act<br/>UID_HIER_ANGEBEN<br/><h2>Regulating authority</h2>AUFSICHTSBEHOERDE (optional)<br/><h2>Regulated professions / Independent Professions</h2>Job title (Independent professions): BERUFSBEZEICHNUNG (Freier Beruf)<br/>Professional chamber: ZUST._KAMMER<br/>Professional regulations and how to access: WEBSITE TO REGULATION<br/><h2>Person responsible for content in accordance with 55 Abs. 2 RStV</h2>RESPONSIBLE FOR TEXT CONTENT<br/><h2>Indication of source for images and graphics</h2>IMAGE SOURCES IF EXTERNAL ARE USED<br/><h2>Disclaimer</h2>Accountability for content<br/>The contents of our pages have been created with the utmost care. However, we cannot guarantee the contents\' accuracy, completeness or topicality. According to statutory provisions, we are furthermore responsible for our own content on these web pages. In this context, please note that we are accordingly not obliged to monitor merely the transmitted or saved information of third parties, or investigate circumstances pointing to illegal activity. Our obligations to remove or block the use of information under generally applicable laws remain unaffected by this as per &sect;&sect; 8 to 10 of the Telemedia Act (TMG).<br/><br/>Accountability for links<br/>Responsibility for the content of external links (to web pages of third parties) lies solely with the operators of the linked pages. No violations were evident to us at the time of linking. Should any legal infringement become known to us, we will remove the respective link immediately.<br/><br/>Copyright<br/> Our web pages and their contents are subject to German copyright law. Unless expressly permitted by law (&sect; 44a et seq. of the copyright law), every form of utilizing, reproducing or processing works subject to copyright protection on our web pages requires the prior consent of the respective owner of the rights. Individual reproductions of a work are allowed only for private use, so must not serve either directly or indirectly for earnings. Unauthorized utilization of copyrighted works is punishable (&sect; 106 of the copyright law).<br/><br/><i>Supported through:</i><a href="http://www.berlin-klinik.de/" target="_blank">berlin-klinik.de</a><br/><br/>'
			),
			'de' => array(
	            'title'				=> 'Impressum',
	            'meta_description'	=> '',
				'meta_keyword'		=> 'Impressum',
	            'description'		=> '<h1>Impressum</h1>Angaben gem&auml;&szlig; &sect; 5 TMG<br/><br/>NAME SHOPBETREIBER<br/>NAME FIRMA<br/>STRASSE<br/>ADRESSZUSATZ<br/>PLZ ORT<br/><h2>Kontakt</h2>Telefon: 12345678<br/>E-Mail: <a href="mailto:MEIN.EMAIL@SHOP.COM">MEIN.EMAIL@SHOP.COM</a><br/>Internetadresse: <a href="http://MEINSHOP.com" target="_blank">http://MEINSHOP.com</a><br/><h2>Umsatzsteuer-ID</h2>Umsatzsteuer-Identifikationsnummer gem&auml;&szlig; &sect;27 a Umsatzsteuergesetz<br/>UID_HIER_ANGEBEN<br/><h2>Aufsichtsbeh&ouml;rde</h2>AUFSICHTSBEHOERDE (optional)<br/><h2>Reglementierte Berufe / Freie Berufe</h2>Berufsbezeichnung<br/>(freie Berufe): BERUFSBEZEICHNUNG (Freier Beruf)<br/>Berufsst&auml;ndische Kammer: ZUST._KAMMER<br/>Bezeichnung Berufsregeln<br/>und deren Zugang: BERUFSREGELN_UND_WEBSEITE<br/><h2>Verantwortlich f&uuml;r den Inhalt nach &sect; 55 Abs. 2 RStV</h2>VERANTWORTLICH_FUER_REDAKTIONELLE_TEXTE<br/><h2>Quellenangaben f&uuml;r die verwendeten Bilder und Grafiken</h2>BILDQUELLEN_DRITTER_HIER_NENNEN<br/><h2>Haftungsausschluss</h2>Haftung f&uuml;r Inhalte<br/>Die Inhalte unserer Seiten wurden mit gr&ouml;&szlig;ter Sorgfalt erstellt. F&uuml;r die Richtigkeit, Vollst&auml;ndigkeit und Aktualit&auml;t der Inhalte k&ouml;nnen wir jedoch keine Gew&auml;hr &uuml;bernehmen. Als Diensteanbieter sind wir gem&auml;&szlig; &sect; 7 Abs.1 TMG f&uuml;r eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach &sect;&sect; 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, &uuml;bermittelte oder gespeicherte fremde Informationen zu &uuml;berwachen oder nach Umst&auml;nden zu forschen, die auf eine rechtswidrige T&auml;tigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unber&uuml;hrt. Eine diesbez&uuml;gliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung m&ouml;glich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.<br/><br/>Haftung f&uuml;r Links<br/>Unser Angebot enth&auml;lt Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb k&ouml;nnen wir f&uuml;r diese fremden Inhalte auch keine Gew&auml;hr &uuml;bernehmen. F&uuml;r die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf m&ouml;gliche Rechtsverst&ouml;&szlig;e &uuml;berpr&uuml;ft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.<br/><br/>Urheberrecht<br/>Die durch die Seitenbetreiber erstellten bzw. verwendeten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielf&auml;ltigung, Bearbeitung, Verbreitung und jede Art der Verwertung au&szlig;erhalb der Grenzen des Urheberrechtes bed&uuml;rfen der Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur f&uuml;r den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.<br/><br/><i>Unterstützt durch: </i><a href="http://www.berlin-klinik.de/" target="_blank">berlin-klinik.de</a><br/><br/>'
			)
		)
	)
);