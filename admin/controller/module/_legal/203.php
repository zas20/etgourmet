<?php
/**
 * @version		$Id: 203.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[203] = array(
    'country_id'        => 203,
    'title'             => 'Schweden',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 25%',
            'rate'      => 25,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Steuerfrei',
            'type'      => 'P',
            'title'     => 'USt. 0%',
            'rate'      => 0,
            'geo_zone'  => 'home'
        ),
        4    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 25%',
            'rate'      => 25,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'europe'
        ),
        7    => array(
            'display'   => 'Steuerfrei Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        8    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU UID 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        9    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 25%',
            'rate'      => 25,
            'geo_zone'  => 'world'
        ),
        10    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'world'
        ),
        11    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'world'
        ),
        12    => array(
            'display'   => 'Steuerfrei Export',
            'type'      => 'P',
            'title'     => 'EX 0%',
            'rate'      => 0,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'SE25',
            'description'   => 'Schweden 25%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 25%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 25%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 25%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU UID 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        1 => array(
            'title'         => 'SE12',
            'description'   => 'Schweden 12%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU UID 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        2 => array(
            'title'         => 'SE6',
            'description'   => 'Schweden 6%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 6%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 6%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX 6%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU UID 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        3 => array(
            'title'         => 'SE0',
            'description'   => 'Schweden 0%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU UID 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
    ),
    'geo_zones' => array(
        'home'      => 'Schweden',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);