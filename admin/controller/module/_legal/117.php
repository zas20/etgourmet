<?php
/**
 * @version		$Id: 117.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[117] = array(
    'country_id'        => 117,
    'title'             => 'Lettland',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Steuerfrei',
            'type'      => 'P',
            'title'     => 'USt. 0%',
            'rate'      => 0,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'europe'
        ),
        4    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'Export 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'world'
        ),
        7    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'world'
        ),
        8    => array(
            'display'   => 'Steuerfrei Export',
            'type'      => 'P',
            'title'     => 'EX 0%',
            'rate'      => 0,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'LV21',
            'description'   => 'Lettland 21%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 21%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 21%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        1 => array(
            'title'         => 'LV12',
            'description'   => 'Lettland 12%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        ),
        2 => array(
            'title'         => 'LV0',
            'description'   => 'Lettland 0%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Lettland',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);