<?php
/**
 * @version		$Id: 14.php 3684 2014-07-14 13:58:21Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2012 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[14] = array(
	'country_id'        => 14,
    'title'             => 'Österreich',
    'taxes'             => array(
		0	=> array(
			'display'   => 'Standard Inland',
			'type'      => 'P',
			'title'     => 'USt. 20%',
			'rate'      => 20,
			'geo_zone'  => 'home'
		),
		1	=> array(
			'display'   => 'Ermässigt Inland',
			'type'      => 'P',
			'title'     => 'USt. 10%',
			'rate'      => 10,
			'geo_zone'  => 'home'
		),
		2	=> array(
			'display'   => 'Ermässigt Inland (Weine aus eigener Erzeugung)',
			'type'      => 'P',
			'title'     => 'USt. 12%',
			'rate'      => 12,
			'geo_zone'  => 'home'
		),
		3	=> array(
			'display'   => 'Kleinunternehmerregelung (§ 6 Abs. 1 Z. 27 UStG)',
			'type'      => 'P',
			'title'     => 'USt. 0%',
			'rate'      => 0,
			'geo_zone'  => 'home'
		),
		4	=> array(
			'display'   => 'Standard Export EU',
			'type'      => 'P',
			'title'     => 'EX EU USt. 20%',
			'rate'      => 20,
			'geo_zone'  => 'europe'
		),
		5	=> array(
			'display'   => 'Ermässigt Export EU',
			'type'      => 'P',
			'title'     => 'EX EU USt. 10%',
			'rate'      => 10,
			'geo_zone'  => 'europe'
		),
		6	=> array(
			'display'   => 'Ermässigt Export EU (Weine aus eigener Erzeugung)',
			'type'      => 'P',
			'title'     => 'EX EU USt. 12%',
			'rate'      => 12,
			'geo_zone'  => 'europe'
		),
		7	=> array(
			'display'   => 'Export EU',
			'type'      => 'P',
			'title'     => 'EX EU 0%',
			'rate'      => 0,
			'geo_zone'  => 'europe'
		),
		8	=> array(
			'display'   => 'Export EU (mit UID)',
			'type'      => 'P',
			'title'     => 'Export EU UID 0%',
			'rate'      => 0,
			'geo_zone'  => 'europe'
		),
		9	=> array(
			'display'   => 'Standard Export',
			'type'      => 'P',
			'title'     => 'EX USt. 20%',
			'rate'      => 20,
			'geo_zone'  => 'world'
		),
		10	=> array(
			'display'   => 'Ermässigt Export',
			'type'      => 'P',
			'title'     => 'EX USt. 10%',
			'rate'      => 10,
			'geo_zone'  => 'world'
		),
		11	=> array(
			'display'   => 'Ermässigt Export (Weine aus eigener Erzeugung)',
			'type'      => 'P',
			'title'     => 'EX USt. 12%',
			'rate'      => 12,
			'geo_zone'  => 'world'
        ),
        12	=> array(
			'display'   => 'Export',
			'type'      => 'P',
			'title'     => 'Export 0%',
			'rate'      => 0,
			'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'USt. 20%',
            'description'   => 'Österreich 10%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 20%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 20%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 20%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'Export EU UID 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        1 => array(
            'title'         => 'USt. 12%',
            'description'   => 'Österreich 12%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'Export EU UID 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        2 => array(
            'title'         => 'USt. 10%',
            'description'   => 'Österreich 10%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'Export EU UID 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        3 => array(
            'title'         => 'USt. 0%',
            'description'   => 'Österreich 0%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'Export EU UID 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Österreich',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);