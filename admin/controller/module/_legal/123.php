<?php
/**
 * @version		$Id: 123.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[123] = array(
    'country_id'        => 123,
    'title'             => 'Litauen',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 9%',
            'rate'      => 12,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'europe'
        ),
        4    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 9%',
            'rate'      => 9,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        7    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'world'
        ),
        8    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 9%',
            'rate'      => 9,
            'geo_zone'  => 'world'
        ),
        9    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'LT21',
            'description'   => 'Litauen 21%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 21%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 21%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX Ust. 21%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        ),
        1 => array(
            'title'         => 'LT9',
            'description'   => 'Litauen 9%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 9%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 9%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX Ust. 9%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        ),
        2 => array(
            'title'         => 'LT5',
            'description'   => 'Litauen 5%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Litauen',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);