<?php
/**
 * @version		$Id: 124.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[124] = array(
    'country_id'        => 124,
    'title'             => 'Luxemburg',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 15%',
            'rate'      => 15,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland (Best. Weine, Waschpulver, etc.)',
            'type'      => 'P',
            'title'     => 'USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'home'
        ),
		3    => array(
            'display'   => 'Ermässigt Inland (z.B. Bücher, Rohwolle, usw.)',
            'type'      => 'P',
            'title'     => 'USt. 3%',
            'rate'      => 3,
            'geo_zone'  => 'home'
        ),
        4    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 15%',
            'rate'      => 15,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'europe'
        ),
        7    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 3%',
            'rate'      => 3,
            'geo_zone'  => 'europe'
        ),
        8    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        9    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 15%',
            'rate'      => 15,
            'geo_zone'  => 'world'
        ),
        10    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'world'
        ),
        11    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'world'
        ),
        12    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EU USt. 3%',
            'rate'      => 3,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'LX15',
            'description'   => 'Luxemburg 15%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 15%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 15%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 15%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        1 => array(
            'title'         => 'LX12',
            'description'   => 'Luxemburg 12%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        ),
        2 => array(
            'title'         => 'LX6',
            'description'   => 'Luxemburg 6%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 6%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 6%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 6%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        ),
        3 => array(
            'title'         => 'LX3',
            'description'   => 'Luxemburg 3%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 3%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 3%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 3%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Luxemburg',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);