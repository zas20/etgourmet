<?php
/**
 * @version		$Id: 74.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[74] = array(
    'country_id'        => 74,
    'title'             => 'Frankreich',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 20%',
            'rate'      => 20,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland 10 (Gastronomie, Transport, usw.)',
            'type'      => 'P',
            'title'     => 'USt. 10%',
            'rate'      => 10,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Ermässigt Inland 5.5 (Grundnahrungsmittel und Energie)',
            'type'      => 'P',
            'title'     => 'USt. 5.5%',
            'rate'      => 5.5,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Ermässigt Inland 2.1 (Zeitungen, Medikamente)',
            'type'      => 'P',
            'title'     => 'USt. 2.1%',
            'rate'      => 2.1,
            'geo_zone'  => 'home'
        ),
		4    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EU EX USt. 20%',
            'rate'      => 20,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Ermässigt Export Europa 10',
            'type'      => 'P',
            'title'     => 'EX EU USt. 10%',
            'rate'      => 10,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Ermässigt Export Europa 5.5',
            'type'      => 'P',
            'title'     => 'EX EU USt. 5.5%',
            'rate'      => 5.5,
            'geo_zone'  => 'europe'
        ),
        7    => array(
            'display'   => 'Ermässigt Export Europa 2.1',
            'type'      => 'P',
            'title'     => 'EX EU USt. 2.1%',
            'rate'      => 2.1,
            'geo_zone'  => 'europe'
        ),
        8    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        9	=> array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 20%',
            'rate'      => 20,
            'geo_zone'  => 'world'
        ),
        10	=> array(
            'display'   => 'Ermässigt Export ',
            'type'      => 'P',
            'title'     => 'EX USt. 10%',
            'rate'      => 10,
            'geo_zone'  => 'world'
        ),
        11	=> array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 5.5%',
            'rate'      => 5.5,
            'geo_zone'  => 'world'
        ),
        12	=> array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 2.1%',
            'rate'      => 2.1,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'FR20',
            'description'   => 'Frankreich 20%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 20%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 20%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 20%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        1 => array(
            'title'         => 'FR10',
            'description'   => 'Frankreich 10%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        2 => array(
            'title'         => 'FR5.5',
            'description'   => 'Frankreich 5.5%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 5.5%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 5.5%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 5.5%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'Export 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        3 => array(
            'title'         => 'FR2.1',
            'description'   => 'Frankreich 2.1%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 2.1%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 2.1%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 2.1%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'Export 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Frankreich',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);