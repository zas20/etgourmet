<?php
/**
 * @version		$Id: 57.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[57] = array(
    'country_id'        => 57,
    'title'             => 'Dänemark',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 25%',
            'rate'      => 25,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Steuerfrei (Zeitungen)',
            'type'      => 'P',
            'title'     => 'USt. 0%',
            'rate'      => 0,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 25%',
            'rate'      => 25,
            'geo_zone'  => 'europe'
        ),
        3    => array(
            'display'   => 'Steuerfrei Export Europa (Zeitungen)',
            'type'      => 'P',
            'title'     => 'EX EU FREE 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        4    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 25%',
            'rate'      => 25,
            'geo_zone'  => 'world'
        ),
        6    => array(
            'display'   => 'Steuerfrei (Zeitungen)',
            'type'      => 'P',
            'title'     => 'EX USt. 0%',
            'rate'      => 0,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'DK25',
            'description'   => 'Dänemark 25%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 25%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 25%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 25%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Dänemark',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);