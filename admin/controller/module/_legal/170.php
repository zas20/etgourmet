<?php
/**
 * @version		$Id: 170.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[170] = array(
    'country_id'        => 170,
    'title'             => 'Polen',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 23%',
            'rate'      => 23,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 8%',
            'rate'      => 8,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Ermässigt Inland (Dienstleistung)',
            'type'      => 'P',
            'title'     => 'USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Steuerfrei',
            'type'      => 'P',
            'title'     => 'USt. 0%',
            'rate'      => 0,
            'geo_zone'  => 'home'
        ),
        4    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 23%',
            'rate'      => 23,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 8%',
            'rate'      => 8,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'europe'
        ),
        7    => array(
            'display'   => 'Export Europe (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        8    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 23%',
            'rate'      => 23,
            'geo_zone'  => 'world'
        ),
        9    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 8%',
            'rate'      => 8,
            'geo_zone'  => 'world'
        ),
        10    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'world'
        ),
        11    => array(
            'display'   => 'Export',
            'type'      => 'P',
            'title'     => 'EX 0%',
            'rate'      => 0,
            'geo_zone'  => 'world'
        )

    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'PL23',
            'description'   => 'Polen 23%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 23%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 23%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 23%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        1 => array(
            'title'         => 'PL8',
            'description'   => 'Polen 8%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 8%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 8%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 8%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        2 => array(
            'title'         => 'PL5',
            'description'   => 'Polen 5%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        2 => array(
            'title'         => 'PL0',
            'description'   => 'Polen 0%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Polen',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);