<?php
/**
 * @version		$Id: 190.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[190] = array(
    'country_id'        => 190,
    'title'             => 'Slowenien',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 22%',
            'rate'      => 22,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 9.5%',
            'rate'      => 9.5,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 22%',
            'rate'      => 22,
            'geo_zone'  => 'europe'
        ),
        3    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 9.5%',
            'rate'      => 9.5,
            'geo_zone'  => 'europe'
        ),
        4    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 22%',
            'rate'      => 22,
            'geo_zone'  => 'world'
        ),
        6    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 9.5%',
            'rate'      => 9.5,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'SI22',
            'description'   => 'Slowenien 22%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 22%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 22%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 22%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        1 => array(
            'title'         => 'SI9.5',
            'description'   => 'Slowenien 9.5%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 9.5%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 9.5%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 9.5%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Slowenien',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);