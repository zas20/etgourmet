<?php
/**
 * @version		$Id: 55.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[55] = array(
    'country_id'        => 55,
    'title'             => 'Zypern',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 19%',
            'rate'      => 19,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Ermässigt Inland (Dienstleistung)',
            'type'      => 'P',
            'title'     => 'USt. 8%',
            'rate'      => 8,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 19%',
            'rate'      => 19,
            'geo_zone'  => 'europe'
        ),
        4    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 8%',
            'rate'      => 8,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Export EU (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        7    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 19%',
            'rate'      => 19,
            'geo_zone'  => 'world'
        ),
        8    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'world'
        ),
        9    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 8%',
            'rate'      => 8,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'CY19',
            'description'   => 'Zypern 19%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 19%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 19%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 19%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        1 => array(
            'title'         => 'CY5',
            'description'   => 'Zypern 5%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        2 => array(
            'title'         => 'CY8',
            'description'   => 'Zypern 8%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 8%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 8%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 8%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Zypern',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);