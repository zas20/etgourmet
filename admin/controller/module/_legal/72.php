<?php
/**
 * @version		$Id: 72.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[72] = array(
    'country_id'        => 72,
    'title'             => 'Finnland',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 24%',
            'rate'      => 24,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 14%',
            'rate'      => 14,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Dienstleistung',
            'type'      => 'P',
            'title'     => 'USt. 10%',
            'rate'      => 10,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Steuerfrei (z.B. Zeitungsabo mind. 3 Monate)',
            'type'      => 'P',
            'title'     => 'USt. 0%',
            'rate'      => 0,
            'geo_zone'  => 'home'
        ),
        4    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 24%',
            'rate'      => 24,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 14%',
            'rate'      => 14,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Dienstleistung',
            'type'      => 'P',
            'title'     => 'EX EU USt. 10%',
            'rate'      => 10,
            'geo_zone'  => 'home'
        ),
        7    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        8    => array(
            'display'   => 'Export Europa Steuerfrei',
            'type'      => 'P',
            'title'     => 'EX EU FREE 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        9    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 24%',
            'rate'      => 24,
            'geo_zone'  => 'world'
        ),
        10    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 14%',
            'rate'      => 14,
            'geo_zone'  => 'world'
        ),
        11    => array(
            'display'   => 'Export Steuerfrei',
            'type'      => 'P',
            'title'     => 'EX 0%',
            'rate'      => 0,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'FI24',
            'description'   => 'Finnland 24%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 24%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 24%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 24%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                )
            )
        ),
        1 => array(
            'title'         => 'FI14',
            'description'   => 'Finnland 14%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 14%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 14%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 14%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                )
            )
        ),
        2 => array(
            'title'         => 'FI10',
            'description'   => 'Finnland 10%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                )
            )
        ),
        3 => array(
            'title'         => 'FI0',
            'description'   => 'Finnland 0%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Finnland',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);