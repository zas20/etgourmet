<?php
/**
 * @version		$Id: 21.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[21] = array(
    'country_id'        => 21,
    'title'             => 'Belgien',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Ermässigt (Brennstoffe, Reifen)',
            'type'      => 'P',
            'title'     => 'USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Standard Export EU',
            'type'      => 'P',
            'title'     => 'EX EU USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'europe'
        ),
        4    => array(
            'display'   => 'Ermässigt Export EU',
            'type'      => 'P',
            'title'     => 'EX EU USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Ermässigt Export (Brennstoffe, usw.)',
            'type'      => 'P',
            'title'     => 'EX EU USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Export EU (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'Export 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        7    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 21%',
            'rate'      => 21,
            'geo_zone'  => 'world'
        ),
        8    => array(
            'display'   => 'Ermässigt Export (Brennstoffe, usw.)',
            'type'      => 'P',
            'title'     => 'EX USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'world'
        ),
        9    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'BE21',
            'description'   => 'Belgien 21%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 21%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 21%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 21%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'Export 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        1 => array(
            'title'         => 'BE6',
            'description'   => 'Belgien 6%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 6%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 6%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 6%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'Export 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        2 => array(
            'title'         => 'BE12',
            'description'   => 'Belgien 12%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'Export 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        3 => array(
            'title'         => 'BE0',
            'description'   => 'Belgien 0%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'Export 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Belgien',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);