<?php
/**
 * @version		$Id: 81.php 3684 2014-07-14 13:58:21Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2012 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[81] = array(
    'country_id'        => 81,
    'title'             => 'Deutschland',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 19%',
            'rate'      => 19,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 7%',
            'rate'      => 7,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Kleinunternehmerregelung (§ 19 UStG)',
            'type'      => 'P',
            'title'     => 'USt. 0%',
            'rate'      => 0,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 19%',
            'rate'      => 19,
            'geo_zone'  => 'europe'
        ),
        4    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 7%',
            'rate'      => 7,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 19%',
            'rate'      => 19,
            'geo_zone'  => 'world'
        ),
        7    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 7%',
            'rate'      => 7,
            'geo_zone'  => 'world'
        ),
        8    => array(
            'display'   => 'Export ',
            'type'      => 'P',
            'title'     => 'EX 0%',
            'rate'      => 0,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
		0 => array(
            'title'         => 'DE19',
            'description'   => 'Deutschland 19%',
            'tax_rule'      => array(
            	// note: value must be same as TITLE above, will be replaced later if match
                array(
                    'tax_rate_id'   => 'USt. 19%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 19%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 19%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        1 => array(
            'title'         => 'DE7',
            'description'   => 'Deutschland 7%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 7%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 7%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        2 => array(
            'title'         => 'DE0',
            'description'   => 'Deutschland 0%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Deutschland',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);