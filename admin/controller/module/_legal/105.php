<?php
/**
 * @version		$Id: 105.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[105] = array(
    'country_id'        => 105,
    'title'             => 'Italien',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 22%',
            'rate'      => 22,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland (z.B. Brennholz)',
            'type'      => 'P',
            'title'     => 'USt. 10%',
            'rate'      => 10,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Ermässigt Inland (z.B. Bücher)',
            'type'      => 'P',
            'title'     => 'USt. 4%',
            'rate'      => 4,
            'geo_zone'  => 'home'
        ),
        4    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 22%',
            'rate'      => 22,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 10%',
            'rate'      => 10,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'europe'
        ),
        7    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 4%',
            'rate'      => 4,
            'geo_zone'  => 'europe'
        ),
        8    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        9    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 22%',
            'rate'      => 22,
            'geo_zone'  => 'world'
        ),
        10    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 10%',
            'rate'      => 10,
            'geo_zone'  => 'world'
        ),
        11    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 6%',
            'rate'      => 6,
            'geo_zone'  => 'world'
        ),
        12    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 4%',
            'rate'      => 4,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'IT22',
            'description'   => 'Italien 22%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 22%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 22%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        ),
        1 => array(
            'title'         => 'IT10',
            'description'   => 'Italien 10%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 10%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        ),
        2 => array(
            'title'         => 'IT6',
            'description'   => 'Italien 6%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 6%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 6%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        ),
        3 => array(
            'title'         => 'IT4',
            'description'   => 'Italien 4%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 4%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 4%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Italien',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);