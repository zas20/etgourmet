<?php
/**
 * @version		$Id: 103.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[103] = array(
    'country_id'        => 103,
    'title'             => 'Irland',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 23%',
            'rate'      => 23,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland (z.B. Fahrräder)',
            'type'      => 'P',
            'title'     => 'USt. 13.5%',
            'rate'      => 13.5,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Ermässigt (Dienstleistung)',
            'type'      => 'P',
            'title'     => 'USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Ermässigt (z.B. Nahrungsmittel)',
            'type'      => 'P',
            'title'     => 'USt. 4.8%',
            'rate'      => 4.8,
            'geo_zone'  => 'home'
        ),
        4    => array(
            'display'   => 'Steuerfrei (z.B. Bücher)',
            'type'      => 'P',
            'title'     => 'USt. 0%',
            'rate'      => 0,
            'geo_zone'  => 'home'
        ),
        5    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 23%',
            'rate'      => 23,
            'geo_zone'  => 'europe'
        ),
        6    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 13.5%',
            'rate'      => 13.5,
            'geo_zone'  => 'europe'
        ),
        7    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'europe'
        ),
        8    => array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 4.8%',
            'rate'      => 4.8,
            'geo_zone'  => 'europe'
        ),
        9    => array(
            'display'   => 'Steuerfrei Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        10    => array(
            'display'   => 'Export Europa (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        11    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 23%',
            'rate'      => 23,
            'geo_zone'  => 'world'
        ),
        12    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 13.5%',
            'rate'      => 13.5,
            'geo_zone'  => 'world'
        ),
        13    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 12%',
            'rate'      => 12,
            'geo_zone'  => 'world'
        ),
        14    => array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 4.8%',
            'rate'      => 4.8,
            'geo_zone'  => 'world'
        ),
        15    => array(
            'display'   => 'Steuerfrei Export',
            'type'      => 'P',
            'title'     => 'EX USt. 0%',
            'rate'      => 0,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'IE23',
            'description'   => 'Irland 23%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 23%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 23%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 23%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        1 => array(
            'title'         => 'IE13.5',
            'description'   => 'Irland 13.5%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 13.5%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 13.5%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 13.5%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        2 => array(
            'title'         => 'IE12',
            'description'   => 'Irland 12%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 12%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        3 => array(
            'title'         => 'IE4.8',
            'description'   => 'Irland 4.8%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 4.8%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 4.8%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 4.8%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '5'
                )
            )
        ),
        4 => array(
            'title'         => 'IE0',
            'description'   => 'Irland 0%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Irand',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);