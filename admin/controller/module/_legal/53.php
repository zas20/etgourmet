<?php
/**
 * @version		$Id: 53.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[53] = array(
    'country_id'        => 53,
    'title'             => 'Kroatien',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 25%',
            'rate'      => 25,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 13%',
            'rate'      => 13,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'home'
        ),
        3    => array(
            'display'   => 'Standard Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 25%',
            'rate'      => 25,
            'geo_zone'  => 'europe'
        ),
        4	=> array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 13%',
            'rate'      => 13,
            'geo_zone'  => 'europe'
        ),
        5	=> array(
            'display'   => 'Ermässigt Export Europa',
            'type'      => 'P',
            'title'     => 'EX EU USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'europe'
        ),
        6	=> array(
            'display'   => 'Export EU (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        7    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 25%',
            'rate'      => 25,
            'geo_zone'  => 'world'
        ),
        8	=> array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 13%',
            'rate'      => 13,
            'geo_zone'  => 'world'
        ),
        9	=> array(
            'display'   => 'Ermässigt Export',
            'type'      => 'P',
            'title'     => 'EX USt. 5%',
            'rate'      => 5,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'HR25',
            'description'   => 'Kroatien 25%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 25%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 25%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 25%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        ),
        1 => array(
            'title'         => 'HR13',
            'description'   => 'Kroatien 13%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 13%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 13%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 13%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        ),
        2 => array(
            'title'         => 'HR5',
            'description'   => 'Kroatien 5%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 5%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Kroatien',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);