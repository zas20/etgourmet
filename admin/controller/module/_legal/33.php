<?php
/**
 * @version		$Id: 33.php 3236 2013-05-03 10:02:51Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

$localSetting[33] = array(
    'country_id'        => 33,
    'title'             => 'Bulgarien',
    'taxes'             => array(
        0    => array(
            'display'   => 'Standard Inland',
            'type'      => 'P',
            'title'     => 'USt. 20%',
            'rate'      => 20,
            'geo_zone'  => 'home'
        ),
        1    => array(
            'display'   => 'Ermässigt Inland',
            'type'      => 'P',
            'title'     => 'USt. 9%',
            'rate'      => 9,
            'geo_zone'  => 'home'
        ),
        2    => array(
            'display'   => 'Standard Export EU',
            'type'      => 'P',
            'title'     => 'EX EU USt. 20%',
            'rate'      => 20,
            'geo_zone'  => 'europe'
        ),
        3    => array(
            'display'   => 'Ermässigt Export EU',
            'type'      => 'P',
            'title'     => 'EX EU USt. 9%',
            'rate'      => 9,
            'geo_zone'  => 'europe'
        ),
        4    => array(
            'display'   => 'Export EU (mit UID-Nr.)',
            'type'      => 'P',
            'title'     => 'EX EU 0%',
            'rate'      => 0,
            'geo_zone'  => 'europe'
        ),
        5    => array(
            'display'   => 'Standard Export',
            'type'      => 'P',
            'title'     => 'EX USt. 20%',
            'rate'      => 20,
            'geo_zone'  => 'world'
        ),
        6    => array(
            'display'   => 'Standard Export EU',
            'type'      => 'P',
            'title'     => 'EX USt. 20%',
            'rate'      => 20,
            'geo_zone'  => 'world'
        )
    ),
    'tax_classes' => array(
        0 => array(
            'title'         => 'BG20',
            'description'   => 'Bulgarien 20%',
            'tax_rule'      => array(
                array(
                    // note: value must be same as TITLE above, will be replaced later if match
                    'tax_rate_id'   => 'USt. 20%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 20%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 20%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        1 => array(
            'title'         => 'BG9',
            'description'   => 'Bulgarien 9%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 9%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 9%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX USt. 9%',
                    'based'         => 'payment',
                    'priority'      => '3'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '4'
                )
            )
        ),
        2 => array(
            'title'         => 'BG0',
            'description'   => 'Bulgarien 0%',
            'tax_rule'      => array(
                array(
                    'tax_rate_id'   => 'USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '1'
                ),
                array(
                    'tax_rate_id'   => 'EX EU USt. 0%',
                    'based'         => 'payment',
                    'priority'      => '2'
                ),
                array(
                    'tax_rate_id'   => 'EX EU 0%',
                    'based'         => 'payment',
                    'priority'      => '3'
                )
            )
        )
    ),
    'geo_zones' => array(
        'home'      => 'Bulgarien',
        'europe'    => 'Europa',
        'world'     => 'Welt'
    )
);