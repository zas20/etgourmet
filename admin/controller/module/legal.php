<?php
/**
 * @version		$Id: legal.php 3812 2014-12-05 07:08:06Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2013 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */

class ControllerModuleLegal extends Controller
{
    private $error      = array();
    public $_name       = 'legal';
    public $_type       = 'module';
    public $_version    = '4.0.4';
    private $_prefix    = 'ox_legal_';
    private $_error     = null;
    private $_critical  = null;
    private $_ocVersion = false;

    private $_classes           = array();
    private $_countries         = array();
    private $_taxClasses        = array();
    private $_names             = array();

    // e.g.: AT = 14, GE = 81
    private $_countriesEurope   = array();

    private $_baseCountry       = null;
    private $_localeSettings    = null;
    private $_root              = null;
    private $_plugins           = array();

    /**
     * load common values
     */
	private function getBasics( $install = false ) {
		if( $install ) {
            $this->load->language( $this->_type . '/' . $this->_name );
        }else{
    		$this->data	= $this->load->language( $this->_type . '/' . $this->_name );

    		// only for that shit mijoshop
        	if( $this->isMijoShop() ) {
        		foreach( $this->data as $k => $v ) {
        			$this->data[$k] = $this->language->get( $k );
        		}
       		}
		}

		$this->data['token'] = $this->session->data['token'];
    }

    /**
     * install this module
     * @return void
     */
    public function install(){
        $this->getBasics( true );
        $this->getOCVersion();

        if( !$this->_ocVersion ) {
            $this->session->data['error'] = $this->language->get( 'err_wrong_oc_version' );
            $this->redirect(
                $this->url->link(
                    'extension/' . $this->_type,
                    'token=' . $this->session->data['token'],
                    'SSL'
                )
            );
        }else{
        	$this->session->data['success'] = $this->language->get( 'text_success_installed' );
        	$this->redirect(
                $this->url->link(
                    $this->_type . '/' . $this->_name,
                    'token=' . $this->session->data['token'],
                    'SSL'
                )
            );
        }
    }

    /**
     * uninstall this module
     * @return void
     */
    public function uninstall(){}

    /**
     * main function
     * @return void
     */
    public function index() {
        $this->getBasics();
        $this->getOCVersion();

        if( !$this->_ocVersion ) {
            $this->session->data['error'] = $this->language->get( 'err_wrong_oc_version' );
            $this->redirect(
                $this->url->link(
                    'extension/' . $this->_type,
                    'token=' . $this->session->data['token'],
                    'SSL'
                )
            );
        }

        $this->document->setTitle( $this->language->get( 'bc_title' ) );

        $this->load->model( 'setting/setting' );

        if( isset( $this->request->post['baseCountry'] ) ) {
            $this->_baseCountry = $this->request->post['baseCountry'];
        }else{
            $this->_baseCountry = $this->config->get( 'config_country_id' );
        }

        if(
            ( $this->request->server['REQUEST_METHOD'] == 'POST' )
            && $this->validate()
        )
        {
            if( isset( $this->request->post['upload'] ) ) {
				$this->uploadPlugin();
			}else{
                $this->updateSetting();
                $this->clear_cache();

                $this->session->data['success'] = '<br />' . $this->language->get( 'text_success' );

                if( !$this->replaceSaveFile() ) {
                    $this->_error[] = $this->language->get( 'err_file_saved' );
                    $this->session->data['errors']  = $this->_error;
                }else{
                    $this->session->data['success'] .= '<br />' . $this->language->get( 'msg_file_saved' );
                }

                if( isset( $this->request->post['mode'] ) ) {
                    $this->redirect(
                        $this->url->link(
                            $this->_type . '/' . $this->_name,
                            'token=' . $this->session->data['token'],
                            'SSL'
                        )
                    );
                }

                $this->redirect(
                    $this->url->link(
                        'extension/' . $this->_type,
                        'token=' . $this->session->data['token'],
                        'SSL'
                    )
                );
            }
        }

        if( isset( $this->error['warning'] ) ) {
            $this->data['error_permission'] = $this->error['warning'];
        }else{
            $this->data['error_permission'] = '';
        }

        if( isset( $this->session->data['success'] ) ) {
			$this->data['success'] = trim( $this->session->data['success'], '<br />' );

			unset( $this->session->data['success'] );
		}else{
			$this->data['success'] = '';
		}

        if( isset( $this->session->data['errors'] ) ) {
			$this->_error = $this->session->data['errors'];

			unset( $this->session->data['errors'] );
		}

        $this->data['breadcrumbs'] = array(
            array(
                'text'      => $this->language->get( 'text_home' ),
                'href'      => $this->url->link(
                    'common/home',
                    'token=' . $this->session->data['token'],
                    'SSL'
                ),
                'separator' => false
            ),
            array(
                'text'      => $this->language->get( 'text_module' ),
                'href'      => $this->url->link(
                    'extension/module',
                    'token=' . $this->session->data['token'],
                    'SSL'
                ),
                'separator' => ' :: '
            ),
            array(
                'text'      => $this->language->get( 'bc_title' ),
                'href'      => $this->url->link(
                    $this->_type . '/' . $this->_name,
                    'token=' . $this->session->data['token'],
                    'SSL'
                ),
                'separator' => ' :: '
            )
        );

        $this->data['links'] = array(
			'action'	=> $this->url->link(
                $this->_type . '/' . $this->_name,
                'token=' . $this->session->data['token'],
                'SSL'
            ),
			'cancel'	=> $this->url->link(
                'extension/' . $this->_type,
                'token=' . $this->session->data['token'],
                'SSL'
            ),
			'help'		=> 'http://osworx.net?option=com_search&amp;searchword='
                . 'help_'. $this->_type .'_'. $this->_name
		);

        // setting default values
        $vars = array(
            'hint'				=> ' *',
            'text_to_email'		=> '',
            'text_summary'		=> '',
            'popup_height'		=> '650',
            'popup_width'		=> '750',
            'position_hint'		=> 'rightBefore',
            'image_width'		=> 80,
            'image_height'		=> 80,
            'xmlLine'			=> '',
            'legal_supportKey'	=> '',
            'description_len'	=> ''
        );

        foreach( $vars as $k => $v ) {
            if( isset( $this->request->post[$k])) {
                $this->data[$k] = $this->request->post[$k];
            }elseif( $this->config->get( $k ) ) {
                $this->data[$k] = $this->config->get( $k );
            }else{
                $this->data[$k] = $v;
            }
        }

        // special, because arrays - for compatibility we use own model
        $this->load->model( 'module/legal' );
        $legalSets = $this->model_module_legal->getSetting( 'legal' );

        // text definitions
        $vars = array(
            'price_text', 'button_text', 'specials_text', 'slider_text',
			'base_price_content_text', 'base_price_price_text', 'textToEmails'
        );

        foreach( $vars as $var ) {
            if( isset( $this->request->post[$var] ) ) {
    			$this->data[$var] = $this->request->post[$var];
    		}elseif(
                $legalSets
                && !empty( $legalSets[$var] )
            )
            {
    			$this->data[$var] = $legalSets[$var];
    		}else{
    			$this->data[$var] = array();
    		}
        }

        $this->load->model( 'localisation/language' );

        $this->data['languages']    = $this->model_localisation_language->getLanguages();
        $this->data['texts']        = array();

        $this->_setClasses();

        if( !$this->loadLocalSettings() ) {
            $this->_error[] = $this->language->get( 'err_no_locale_sets' );
        }

        $this->_getBaseDefs();

        // mic: var needed also in template!
        if( $this->data['countryIsSupported'] = $this->isSupported( $this->_baseCountry ) ) {
            $this->_setDefinitions();
            $this->getLocaleSetting();
        }

		$this->getOrderStatus();
        $this->getLists();
        $this->getFooter();
        // check if VQMod and plugins are existing/installed
        $this->checkVqmod();
        $this->checkVqmodPlg();
        $this->getLogFile();

        $tpl = 'catalog/view/theme/' . $this->config->get( 'config_template' ) . '/template/product/product.tpl';

        if( !file_exists( $this->getRootPath() . $tpl ) ) {
            $tpl = 'catalog/view/theme/default/template/product/product.tpl';
        }

        $css = 'catalog/view/theme/' . $this->config->get( 'config_template' ) . '/stylesheet/legal.css';

        if( !file_exists( $this->getRootPath() . $css ) ) {
            $css = 'catalog/view/theme/default/stylesheet/legal.css';
        }

        $this->data['errors']		= $this->_error;
        $this->data['plugins']		= $this->_plugins;
        $this->data['critical']		= $this->_critical;
        $this->data['tplProduct']	= $this->getContent( $tpl );
        $this->data['tpl']			= $tpl;
        $this->data['cssContent']	= $this->getContent( $css );
        $this->data['css']			= $css;
        $this->data['_name']		= $this->_name;
        $this->data['langCode']		= 'en'; // default

		/**
		 * fine .. that shit MijoShop assigns wrong language code!
		 * loop through and get correct - otherwise we will display wrong texts
		 */
        foreach( $this->data['languages'] as $k => $v ) {
        	if( $v['language_id'] == $this->config->get( 'config_language_id' ) ) {
        		$this->data['langCode'] = $v['code'];
        		break;
        	}
        }

        if( count( $this->_plugins ) ) {
            foreach( $this->_plugins as $k => $v ) {
                $this->data['plg_' . $k] = $this->checkStateVqmodPlugin( $k );
            }
        }else{
            $this->data['errors'][] = $this->language->get( 'err_no_plugins' );
        }

        $this->data['edit']			= 'index.php?route='
									. $this->_type .'/'. $this->_name
									. '/getEditContent&token=' . $this->session->data['token'];
        $this->data['save']			= 'index.php?route='
									. $this->_type .'/'. $this->_name
									. '/saveEditContent&token=' . $this->session->data['token'];
        $this->data['redirect']		= 'index.php?route='
									. $this->_type .'/'. $this->_name
									. '&token=' . $this->session->data['token'];
        $this->data['clear_log']	= $this->url->link(
            $this->_type .'/'. $this->_name . '/clear_log',
            'token=' . $this->session->data['token'],
            'SSL'
        );

        $this->getSupportData();
        $this->addDocumentData( true );
        $this->getButtonCode();

        $this->template = $this->_type . '/' . $this->_name . '.tpl';
        $this->children = array(
            'common/header',
            'common/footer',
        );

        $this->response->setOutput( $this->render() );
    }

    private function getOrderStatus() {
    	$this->load->model( 'localisation/order_status' );
    	$this->data['order_status'] = $this->model_localisation_order_status->getOrderStatuses();
    }

    /**
     * get several lists
     */
    private function getLists() {
    	$this->load->model( 'localisation/country' );
    	$this->load->model( 'catalog/information' );
        $countries	= $this->model_localisation_country->getCountries();
        $texts      = $this->model_catalog_information->getInformations();

        $this->data['emailTexts'] = $texts;

        $list = '<select name="shopCountry">' . "\n"
        . '<option disabled="disabled" style="color: #07611F; background-color: #EBEBEB;">'
        . $this->language->get( 'text_supported_countries')
        . '</option>' . "\n"
        . '<option disabled="disabled" style="color: #7E7E7E;"> - - - - - - - -</option>' . "\n";
        $list1 = '';

        foreach( $countries as $country ) {
            if( $this->isSupported( $country['country_id'] ) ) {
                $list .= '<option value="' . $country['country_id'] . '"'
                . ( $country['country_id'] == $this->config->get( 'config_country_id' ) ? ' selected="selected"' : '' )
                . '>' . $country['name'] . '</option>' . "\n";
            }else{
                $list1 .= '<option value="' . $country['country_id'] . '" style="color: #7C7C7C;"'
                . ( $country['country_id'] == $this->config->get( 'config_country_id' ) ? ' selected="selected"' : '' )
                . '>' . $country['name'] . '</option>' . "\n";
            }
        }

        $list .= '<option disabled="disabled" style="color: #7E7E7E;"> - - - - - - - -</option>' . "\n"
        . '<option disabled="disabled" style="color: #FF0000; background-color: #EBEBEB;">'
        . $this->language->get( 'text_not_supported_countries')
        . '</option>' . "\n"
        . '<option disabled="disabled" style="color: #7E7E7E;"> - - - - - - - -</option>' . "\n"
        . $list1;

        $list .= '</select>' . "\n";

        $this->data['lists']['countries'] = $list;

        $textList = '<option disabled="disabled" style="color: #6D6D6D;">' . "\n"
            . $this->language->get( 'text_select' ) . '</option>' . "\n"
        . '<option disabled="disabled" style="color: #6D6D6D;">- - - - - - - -</option>' . "\n";

        foreach( $texts as $text ) {
            $textList .= '<option value="' . $text['information_id'] . '"'
            . ( $text['information_id'] == $this->data['text_to_email'] ? ' selected="selected"' : '' )
            . '>'
            . (
                ( strlen( $text['title'] ) > 25 )
                ? mb_substr( $text['title'], 0, 22 ) . ' ..'
                : $text['title']
            )
            . '</option>' . "\n";
        }

        $list = '<select name="text_to_email">' . "\n"
    	. '<option disabled="disabled" style="color: #6D6D6D;">' . "\n"
        	. $this->language->get( 'text_select' ) . '</option>' . "\n"
    	. '<option disabled="disabled" style="color: #6D6D6D;">- - - - - - - -</option>' . "\n";

    	foreach( $texts as $text ) {
            $list .= '<option value="' . $text['information_id'] . '"'
            . ( $text['information_id'] == $this->data['text_to_email'] ? ' selected="selected"' : '' )
            . '>'
            . (
                ( strlen( $text['title'] ) > 25 )
                ? mb_substr( $text['title'], 0, 22 ) . ' ..'
                : $text['title']
            )
            . '</option>' . "\n";
        }

        $list .= '</select>' . "\n";

        $this->data['lists']['text_to_email'] = $list;

        $list = '<select name="text_summary">' . "\n"
        . '<option disabled="disabled" style="color: #6D6D6D;">' . "\n"
            . $this->language->get( 'text_select' ) . '</option>' . "\n"
        . '<option disabled="disabled" style="color: #6D6D6D;">- - - - - - - -</option>' . "\n";

        foreach( $texts as $text ) {
            $list .= '<option value="' . $text['information_id'] . '"'
            . ( $text['information_id'] == $this->data['text_summary'] ? ' selected="selected"' : '' )
            . '>'
            . (
                ( strlen( $text['title'] ) > 25 )
                ? mb_substr( $text['title'], 0, 22 ) . ' ..'
                : $text['title']
            )
            . '</option>' . "\n";
        }

        $list .= '</select>' . "\n";

        $this->data['lists']['text_summary'] = $list;

        // helper select for info id
        $list = '<select>' . "\n"
        . '<option disabled="disabled" style="color: #6D6D6D;">' . "\n"
            . $this->language->get( 'text_select' ) . '</option>' . "\n"
        . '<option disabled="disabled" style="color: #6D6D6D;">- - - - - - - -</option>' . "\n";

        foreach( $texts as $text ) {
            $list .= '<option value="' . $text['information_id'] . '"'
            . ( $text['information_id'] == $this->data['text_to_email'] ? ' selected="selected"' : '' )
            . '>'
            . '[' . str_pad( $text['information_id'], 4, ' ', STR_PAD_LEFT ) . '] ' . $text['title']
            . '</option>' . "\n";
        }

        $list .= '</select>' . "\n";

        $this->data['lists']['text_ids'] = $list;

        // price info position
        $vars = array(
            'left', 'rightBefore', 'rightBottom', 'rightTop', 'replace',
            'slider-top', 'slider-bottom', 'bar-top', 'bar-bottom'
        );

        $list = '<select name="position_hint">' . "\n"
        . '<option disabled="disabled" style="color: #6D6D6D;">' . "\n"
            . $this->language->get( 'text_select' ) . '</option>' . "\n"
        . '<option disabled="disabled" style="color: #6D6D6D;">- - - - - - - -</option>' . "\n";

        foreach( $vars as $var ) {
            $list .= '<option value="' . $var . '"'
            . ( $var == $this->data['position_hint'] ? ' selected="selected"' : '' )
            . '>'
            . $this->language->get( 'text_' . $var )
            . '</option>' . "\n";
        }

        $list .= '</select>' . "\n";

        $this->data['lists']['position_hint'] = $list;

        unset( $vars );
    }

    /** ******* helper functions and definitions ********* */

    /**
     * get OpenCart Version
     * if < 1.5.1 let var = false
     */
    private function getOCVersion() {
        if( defined( 'VERSION' ) ) {
            if( version_compare( VERSION, '1.5.1', '>=' ) ) {
                $this->_ocVersion = VERSION;
            }
        }
    }

    /**
     * get required models and define classes for later using
     */
    private function _setClasses() {
        $this->load->model( 'catalog/information' );
        $this->load->model( 'localisation/country' );
        $this->load->model( 'localisation/geo_zone' );
        $this->load->model( 'localisation/tax_class' );
        //$this->load->model( 'localisation/tax_rate' );

        $this->load->model( 'module/legal' );

        $this->load->model( 'setting/extension' );
        $this->load->model( 'setting/setting' );
        $this->load->model( 'sale/customer_group' );

        $this->_classes['informations'] = new ModelCatalogInformation( $this->registry );
        $this->_classes['countries']    = new ModelLocalisationCountry( $this->registry );
        $this->_classes['geoZones']     = new ModelLocalisationGeoZone( $this->registry );
        $this->_classes['taxClasses']   = new ModelLocalisationTaxClass( $this->registry );
        // $this->_classes['taxRates']     = new ModelLocalisationTaxRate( $this->registry );

        $this->_classes['legal']        = new ModelModuleLegal( $this->registry );

        $this->_classes['extensions']   = new ModelSettingExtension( $this->registry );
        $this->_classes['settings']     = new ModelSettingSetting( $this->registry );
        $this->_classes['customerGroup']= new ModelSaleCustomerGroup( $this->registry );
    }

    /**
     * set several definitions based on selected country (id)
     * @param int   $id     country_id
     */
    private function _setDefinitions( $id = null ) {
        if( is_null( $id ) ) {
            $id = $this->_baseCountry;
        }

        $this->_getBaseDefs();

        // define countries
        foreach( $this->_countriesEurope as $ceu ) {
            $zone_to_geo_zone_eu[] = array(
                'country_id'    => $ceu,
                'zone_id'       => 0
            );
        }

        $this->_countries['home'] = array(
            'name'              => $this->_localeSettings[$id]['geo_zones']['home'],
            'description'       => $this->_localeSettings[$id]['geo_zones']['home'],
            'zone_to_geo_zone'  => array(
                array(
                    'country_id'    => $id,
                    'zone_id'       => 0
                )
            )
        );

        $this->_countries['europe'] = array(
            'name'              => $this->_localeSettings[$id]['geo_zones']['europe'],
            'description'       => $this->_localeSettings[$id]['geo_zones']['europe'],
            'zone_to_geo_zone'  => $this->getEuropeExceptBase( $id )
        );

        // whole world (all countries) except EU
        $countries = $this->_classes['countries']->getCountries();

        foreach( $countries as $country ) {
            if( !in_array( $country['country_id'], $this->_countriesEurope ) ) {
                $zone_to_geo_zone_world[] = array(
                    'country_id'    => $country['country_id'],
                    'zone_id'       => 0
                );
            }
        }

        $this->_countries['world'] = array(
            'name'              => $this->_localeSettings[$id]['geo_zones']['world'],
            'description'       => $this->_localeSettings[$id]['geo_zones']['world'],
            'zone_to_geo_zone'  => $zone_to_geo_zone_world
        );

        // get current geo zones
        $geo_zones = $this->_classes['geoZones']->getGeoZones();
        foreach( $geo_zones as $zone ) {
            $this->_names['zones'][] = $zone['name'];
        }

        // get taxrate names - data set for compatibility
        $data = array(
            'name'  => 'name',
            'type'  => true
        );

        if( !$this->_ocVersion ) {
            $this->getOCVersion();
        }

        if( version_compare( $this->_ocVersion, '1.5.1.1', '<=' ) ) {
            $data = array(
                'name'  => 'description',
                'type'  => false
            );
        }

		// get taxrates
        $tax_rates = $this->_classes['legal']->getTaxRates( $data );
        foreach( $tax_rates as $taxRate ) {
            $this->_names['tax'][] = $taxRate['name'];
        }

        // get information texts
        $informations = $this->_classes['informations']->getInformations();
        foreach( $informations as $information ) {
            $this->_names['text'][] = $information['title'];
        }

        // get taxclasses
        $tax_classes = $this->_classes['taxClasses']->getTaxClasses();
        foreach( $tax_classes as $taxClass ) {
            $this->_names['taxClass'][] = $taxClass['title'];
        }
    }

    /**
     * get several base definitions
     * e.g. eu-countries base 2014.07 (28 countries)
     */
    private function _getBaseDefs() {
        $this->_countriesEurope = array(
            14, 21, 33, 53, 55, 56, 57, 67, 72, 74, 81, 84, 97, 103, 105, 117,
            123, 124, 132, 150, 170, 171, 175, 189, 190, 195, 203, 222
        );

        $this->getPlugins();

        // initialize basic vars
        $this->data['taxes']        = array( 'display' => '', 'title' => '', 'rate' => '' );
        $this->data['taxClasses']   = array( 'title' => '', 'description' => '' );
        $this->data['texts']        = array( 'display' => '' );

        $this->data['geoZones'] = array(
            'home'      => array( 'name' => '' ),
            'europe'    => array( 'name' => '' ),
            'world'     => array( 'name' => '' )
        );

        $this->data['isInstalledTax']       = array();
        $this->data['isInstalledGeoZone']   = array();
        $this->data['isInstalledTaxClass']  = array();
        $this->data['isInstalledText']      = array();
        $this->data['tabTaxes']             = false;
        $this->data['lists']['updateTaxId'] = '';
    }

    /**
     * remove the defined base country from global array
     * @return array
     */
    private function getEuropeExceptBase( $id ) {
        $ret = array();

        foreach( $this->_countriesEurope as $ceu ) {
            if( $ceu != $id )
            {
                $ret[] = array(
                    'country_id'    => $ceu,
                    'zone_id'       => 0
                );
            }
        }

        $this->_europeCountries = $ret;

        return $ret;
    }

    /**
     * load and include locale settings from external files
     * @param int	$id		optional country id, if set loads only specified settings
     * @return bool;
     */
    private function loadLocalSettings( $id = '' ) {
    	if( $id ) {
    		if( file_exists( dirname( __FILE__ ) . '/_legal/' . $id . '.php' ) ) {
    			include( dirname( __FILE__ ) . '/_legal/' . $id . '.php' );
   			}
    	}else{
	        $files = glob( dirname( __FILE__ ) . '/_legal/' . '*.php' );

	        if( $files ) {
	            foreach( $files as $file ) {
          			include( $file );
	            }
	        }
		}

        if( !$id ) {
        	$id = $this->_baseCountry;
        }

        if( !empty( $localSetting ) ) {
            $this->_localeSettings = $localSetting;

            if( empty( $legalTexts) ) {
        		if( file_exists( dirname( __FILE__ ) . '/_legal/legal_texts.php' ) ) {
            		include( dirname( __FILE__ ) . '/_legal/legal_texts.php' );
        		}
        	}

            // use common text if no local defined
            if( empty( $localSetting[$id]['texts'] ) ) {
            	$this->_localeSettings[$id]['texts'] = array();

            	if( !empty( $legalTexts) ) {
            		$this->_localeSettings[$id]['texts'] = $legalTexts;
           		}
           	}else{
				// merge only not defined
           		foreach( $legalTexts as $k => $v ) {
           			if( !empty( $localSetting[$id]['texts'][$k] ) ) {
           				$this->_localeSettings[$id]['texts'][$k] = $localSetting[$id]['texts'][$k];
           			}else{
           				$this->_localeSettings[$id]['texts'][$k] = $legalTexts[$k];
           			}
           		}
           	}

           	unset( $localSetting );
        }else{
            // something went completely wrong, set default (dummy) values
            $this->_localeSettings[$id] = array(
                'taxes'         => array(),
                'tax_classes'   => array(),
                'texts'         => array()
            );

            return false;
        }

        return true;
    }

    /**
     * assign vars from external file into local vars
     * and check if already installed
     * @param int   $id
     * @return void/false
     */
    private function getLocaleSetting( $id = null ) {
        if( is_null( $id ) ) {
            $id = $this->_baseCountry;
        }

        // initialize vars
        $this->data['isInstalledTax']       = array();
        $this->data['isInstalledGeoZone']   = array();
        $this->data['isInstalledTaxClass']  = array();
        $this->data['isInstalledText']      = array();
        $this->data['tabTaxes']             = false;
        $this->data['lists']['updateTaxId'] = '';

        if( $this->isSupported( $id ) ) {
            // tax rates
            $count = count( $this->_localeSettings[$id]['taxes'] );

            for( $i = 0; $i < $count; ++$i ) {
                $this->data['isInstalledTax'][] = $this->isInstalled(
                    $this->_localeSettings[$id]['taxes'][$i]['title'],
                    'tax'
                );
            }

            $this->data['taxes'] = $this->_localeSettings[$id]['taxes'];

            // geozones
            $this->data['geoZones'] = $this->_countries;

            // check if installed
            foreach( $this->_countries as $k => $v ) {
                $this->data['isInstalledGeoZone'][$k] = $this->isInstalled(
                    //$v['name'],
                    $this->_localeSettings[$id]['geo_zones'][$k],
                    'zones'
                );
            }

            // tax classes
            $this->_taxClasses          = $this->_localeSettings[$id]['tax_classes'];
            $this->data['taxClasses']   = $this->_taxClasses;

            // check if installed
            $count = count( $this->_taxClasses );

            for( $i = 0; $i < $count; ++$i ) {
                $this->data['isInstalledTaxClass'][] = $this->isInstalled(
                    $this->_taxClasses[$i]['title'],
                    'taxClass'
                );
            }

            // texts
            $this->data['texts'] = $this->_localeSettings[$id]['texts'];

            // check if installed
            $count = count( $this->data['texts'] );

            if(
                $this->data['texts']
                && $count > 0
            )
            {
                for( $i = 0; $i < $count; ++$i ) {
                    if( !empty( $this->_localeSettings[$id]['texts'][$i]['information_description'][$this->config->get('config_language_id')]['title'] ) ) {
                        $text = $this->_localeSettings[$id]['texts'][$i]['information_description'][$this->config->get('config_language_id')]['title'];
                        $this->data['isInstalledText'][] = $this->isInstalled( $text, 'text' );
                    }else{
                        $this->data['isInstalledText'][] = false;
                    }
                }
            }

            // get values for tab updates
            if( count( $this->data['isInstalledTaxClass'] ) > 0 ) {
                $list = '<select name="updateTaxId">' . "\n";

                foreach( $this->data['isInstalledTaxClass'] as $k => $v ) {
                    if( $v ) {
                        $this->data['tabTaxes'] = true;

                        $list .= '<option value="' . $k . '"'. '>'
						. $this->_localeSettings[$id]['tax_classes'][$k]['title']
						. '</option>' . "\n";
                    }
                }

                $list .= '</select>' . "\n";

                $this->data['lists']['updateTaxId'] = $list;
            }

            return true;
        }else{
            return false;
        }
    }

    /**
     * check if selected/defined country is supported (locale setting file exists)
     * @param int   $id
     * @return bool
     */
    private function isSupported( $id ) {
        if( array_key_exists( $id, $this->_localeSettings ) !== false ) {
            return true;
        }

        return false;
    }

    /**
     * check if item is installed (in database)
     * @param int       $title
     * @param string    $type (e.g. tax, text, etc.)
     * @return bool
     */
    private function isInstalled( $title, $type ) {
        if( array_search( $title, $this->_names[$type] ) === false ) {
            return false;
        }

        return true;
    }

    /**
     * load definitions
     * called external by template via ajax
     * @return array
     */
    public function getLocaleSettings() {
        $this->getBasics();
        $json = array();
        $this->_localeSettings = null;

		if( !empty( $this->request->get['country_id'] ) ) {
            $id = $this->request->get['country_id'];
            $this->_baseCountry = $id;

            $this->_setClasses();
            $this->loadLocalSettings( $id );

            if( $this->isSupported( $id ) ) {
                $this->_setDefinitions( $id );

                if( $this->getLocaleSetting( $id ) ) {
                    $json = array(
                        'taxes'                 => $this->data['taxes'],
                        'taxClasses'            => $this->data['taxClasses'],
                        'geoZones'              => $this->data['geoZones'],
                        'texts'                 => $this->data['texts'],
                        'isInstalledTax'        => $this->data['isInstalledTax'],
                        'isInstalledTaxClass'   => $this->data['isInstalledTaxClass'],
                        'isInstalledGeoZone'    => $this->data['isInstalledGeoZone'],
                        'isInstalledText'       => $this->data['isInstalledText'],
                        'tabTaxes'              => $this->data['tabTaxes'],
                        'updateTaxId'           => $this->data['lists']['updateTaxId']
                    );
                }else{
                    $json['error'] = $this->language->get( 'error_not_supported' );
                }
            }else{
                $json['error'] = $this->language->get( 'error_not_supported' );
            }
		}

		$this->response->setOutput( json_encode( $json ) );
    }

    /**
     * basic function to install items (tax, class, text, etc.)
     * called external from template function
     * @return array
     * @see functions createxxxx
     */
    public function installItem() {
        $this->getBasics();
        $this->getOCVersion();
        $json = array();

		if( !empty( $this->request->get['country_id'] ) ) {
            $id         = (int) $this->request->get['country_id'];
            $fieldId    = $this->request->get['fieldId'];
            $type       = $this->request->get['type'];

            $this->_setClasses();
            $this->_setDefinitions( $id );
            $this->loadLocalSettings( $id );

            if( $this->{'create' . ucfirst( $type )}( $id, $fieldId ) ) {
                $json['success']    = $this->language->get( 'msg_' . $type . '_installed' );
            }else{
                if( $this->_error[$type] ) {
                    $json['error'] = $this->_error[$type];
                    unset( $this->_error );
                }else{
                    $json['error'] = $this->language->get( 'err_' . $type . '_installed' );
                }
            }
		}

		$this->response->setOutput( json_encode( $json ) );
    }

    /**
     * basic function for updates (products, shipping methods)
     * @return array
     * @see functions updatexxxx
     */
    public function update() {
        $this->getBasics();
        $this->getOCVersion();
        $json = array();

		if( !empty( $this->request->get['country_id'] ) ) {
            $id		= $this->request->get['country_id'];
            $taxId	= $this->request->get['tax_id'];
			$type	= $this->request->get['type'];

            $this->_setClasses();
            $this->_setDefinitions( $id );
            $this->loadLocalSettings();

            if( $this->{'update' . ucfirst( $type )}( $id, $taxId ) ) {
                $json['success']    = $this->language->get( 'msg_' . $type . '_updated' );
            }else{
                if( $this->_error[$type] ) {
                    $json['error'] = $this->_error[$type];
                    unset( $this->_error );
                }else{
                    $json['error'] = $this->language->get( 'err_' . $type . '_updated' );
                }
            }
		}

		$this->response->setOutput( json_encode( $json ) );
    }

    /**
     * create database entries for tax rates
     * @param int   $id
     * @param int   $taxId
     * @return true
     */
    private function createTax( $id, $taxId ) {
        $zones  = $this->_classes['geoZones']->getGeoZones();
        $text   = $this->_localeSettings[$id]['geo_zones'][$this->_localeSettings[$id]['taxes'][$taxId]['geo_zone']];

        foreach( $zones as $zone ) {
            if( $zone['name'] == $text ) {
                $zoneId = $zone['geo_zone_id'];
                break;
            }
        }

        if( empty( $zoneId ) ) {
            return false;
        }

        // create record
        // compatibility checl - some fields are available only if OC > 1.5.1.1
        if( version_compare( $this->_ocVersion, '1.5.1', '>' ) ) {
        	$groups	= $this->_classes['customerGroup']->getCustomerGroups();
            $data	= array(
                'fieldName'     => 'name',
                'name'          => $this->_localeSettings[$id]['taxes'][$taxId]['title'],
                'rate'          => $this->_localeSettings[$id]['taxes'][$taxId]['rate'],
                'type'          => $this->_localeSettings[$id]['taxes'][$taxId]['type'],
                'geo_zone_id'   => $zoneId
            );

            foreach( $groups as $group ) {
                $data['tax_rate_customer_group'][] = $group['customer_group_id'];
            }
        }else{
            $data = array(
                'fieldName'     => 'description',
                'name'          => $this->_localeSettings[$id]['taxes'][$taxId]['title'],
                'rate'          => $this->_localeSettings[$id]['taxes'][$taxId]['rate'],
                'type'          => false,
                'geo_zone_id'   => $zoneId
            );
        }

        $this->_classes['legal']->addTaxRate( $data );

        return true;
    }

    /**
     * create database entries for geo zones
     * @param int       $id
     * @param string    $zoneId title of zone: home, europe or world
     * @return true
     */
    private function createZone( $id, $zoneId  ) {
        $data = array(
            'name'              => $this->_localeSettings[$id]['geo_zones'][$zoneId],
            'description'       => $this->_localeSettings[$id]['geo_zones'][$zoneId],
            'zone_to_geo_zone'  => $this->_countries[$zoneId]['zone_to_geo_zone']
        );

        $this->_classes['geoZones']->addGeoZone( $data );

        return true;
    }

    /**
     * create database entries for tax classes
     * @param int   $id
     * @param int   $classId
     * @return bool
     */
    private function createClass( $id, $classId  ) {
        // get values from external definition
        $tmpTaxRules    = $this->_localeSettings[$id]['tax_classes'][$classId]['tax_rule'];
        $fieldName      = 'tax_rule';
        $tmpRules       = array();

        // get taxrate id - args is used for compatibility
        $args = array(
            'name'  => 'name',
            'type'  => true
        );

        if( version_compare( $this->_ocVersion, '1.5.1.1', '<=' ) ) {
            $fieldName = 'tax_rate';
            $args = array(
                'name'  => 'description',
                'type'  => false
            );
        }

        $tax_rates = $this->_classes['legal']->getTaxRates( $args );

        foreach( $tmpTaxRules as $ttr ) {
            foreach( $tax_rates as $tr ) {
                if( $ttr['tax_rate_id'] == $tr['name'] ) {
                    $taxRateId  = $tr['tax_rate_id'];
                    $tmpRules[] = array(
                        'tax_rate_id'   => $tr['tax_rate_id'],
                        'based'         => $ttr['based'],
                        'priority'      => $ttr['priority']
                    );

                    break;
                }
            }
        }

        if( count( $tmpRules ) > 0 ) {
            $data = array(
                'title'         => $this->_localeSettings[$id]['tax_classes'][$classId]['title'],
                'description'   => $this->_localeSettings[$id]['tax_classes'][$classId]['description'],
                $fieldName      => $tmpRules,
                // needed for update (oc <= 151 )
                'tax_rate_id'   => $taxRateId
            );

            $this->_classes['legal']->addTaxClass( $data );

            return true;
        }else{
            $this->load->language( $this->_type . '/' . $this->_name );
            $this->_error['class'] = $this->language->get( 'err_class' );
        }

        return false;
    }

    /**
     * create database entry (text / information)
     * @param int   $id
     * @param int   $textId
     * @return void
     */
    private function createText( $id, $textId ) {
        $this->load->model( 'localisation/language' );
        $data           = $this->_localeSettings[$id]['texts'][$textId];
        $languages      = $this->model_localisation_language->getLanguages();
        $langs          = array();
        $newData        = array();

        // assign only language_id to new array for check below
        foreach( $languages as $lang ) {
            $langs[$lang['language_id']] = $lang['code'];
        }

        // check language: in case user has other db.ids than expected, use fallback
        foreach( $data['information_description'] as $language_id => $value ) {
            if( $key = array_search( $language_id, $langs ) ) {
                $language = $key;
            }else{
                $language = $language_id;
            }

            $newData[$language] = $data['information_description'][$language_id];
        }

        $data['information_description'] = $newData;

        // add manually - needed by OC < 1.5.2
        $this->request->post['sort_order'] = $this->_localeSettings[$id]['texts'][$textId]['sort_order'];

        $this->_classes['informations']->addInformation( $data );

        unset( $data, $newData );

        return true;
    }

    /**
     * update all products with selected tax class
     * @param int       $countryId
     * @param string    $taxId
     * @return bool
     */
    private function updateProducts( $countryId, $taxId ) {
        if( $id = $this->_getTaxClassId( $countryId, $taxId ) ) {
            $this->load->model( 'module/legal' );

            $this->model_module_legal->updateProducts( $id );

            return true;
        }

        return false;
    }

    /**
     * update all enabled shipping methods
     * @param int       $countryId
     * @param string    $taxId
     * @return bool
     */
    private function updateShipping( $countryId, $taxId ) {
        if( $id = $this->_getTaxClassId( $countryId, $taxId ) ) {
            $shippings = $this->_classes['extensions']->getInstalled( 'shipping' );

            foreach( $shippings as $shipping ) {
                $configValues = $this->_classes['settings']->getSetting( $shipping, 0 );

                if( isset( $configValues[$shipping . '_tax_class_id'] ) ) {
                    $configValues[$shipping . '_tax_class_id'] = $id;

                    $this->_classes['settings']->editSetting( $shipping, $configValues, 0 );
                }
            }

            return true;
        }

        return false;
    }

    /**
     * get tax class id
     * @param int       $countrId
     * @param string    $taxId
     * @return mixed false/int
     */
    private function _getTaxClassId( $countryId, $taxId ) {
        $ret = false;

        // get tax class id
        $taxTitle = $this->_localeSettings[$countryId]['tax_classes'][$taxId]['title'];

        // get tax_rate_id
        $tax_classes = $this->_classes['taxClasses']->getTaxClasses();

        // loop through and get id
        foreach( $tax_classes as $tc ) {
            if( $tc['title'] == $taxTitle ) {
                $ret = $tc['tax_class_id'];

                break;
            }
        }

        return $ret;
    }

    public function checkAttributes() {
        $this->getBasics();
        $this->load->model( 'module/legal' );

        $json		= array();
        $products   = $this->model_module_legal->checkAttributes();
        $count      = count( $products );
        $attribs    = 0;
        $metas      = 0;
        $imgPath    = $this->isMijoShop() ? '../components/com_mijoshop/opencart/admin/' : '';

        if( $count ) {
            // loop through and assign image
            $imgTrue    = '<img src="' . $imgPath . 'view/image/success.png" height="16" width="16" />';
            $imgFalse   = '<img src="' . $imgPath . 'view/image/warning.png" height="16" width="16" />';

            foreach( $products as $k => $v ) {
                if( $v['attributes'] ) {
                    $products[$k]['attributes'] = $imgTrue;
                }else{
                    $products[$k]['attributes'] = $imgFalse;
                    ++$attribs;
                }

                if( $v['meta'] ) {
                    $products[$k]['meta'] = $imgTrue;
                }else{
                    $products[$k]['meta'] = $imgFalse;
                    ++$metas;
                }

            }

            $json = array(
                'products'  => $products,
                'success'   => sprintf(
                    $this->language->get( 'msg_found_products' ),
                    $count,
                    $attribs,
                    $metas
                )
            );
        }else{
            $json['error']  = $this->language->get( 'err_found_products');
        }

		$this->response->setOutput( json_encode( $json ) );
    }

    /**
     * load required plugins
     */
    private function getPlugins() {
        $ret        = array();
        $path       = $this->getRootPath() . 'vqmod/xml/';
        $plugins    = glob( $path . '*.xml*' );

        if( $plugins ) {
            foreach( $plugins as $plugin ) {
                if( strpos( $plugin, $this->_prefix ) !== false ) {
                    $xml    = simplexml_load_file( $plugin );
                    $id     = str_replace(
                        array( $path, '.xml_', '.xml', $this->_prefix ),
                        '',
                        $plugin
                    );

                    // a bit tricky, dont know what other devs are doing or set
                    if( isset( $xml->description_ml->{$this->language->get( 'code')} ) ) {
                        $text = $xml->description_ml->{$this->language->get( 'code')}->title;

                        $ret[$id] = array(
                            'title'     => (
                                isset( $xml->description_ml->{$this->language->get( 'code')}->title )
                                ? $xml->description_ml->{$this->language->get( 'code')}->title
                                : ( isset( $xml->id ) ? $xml->id : '' )
                            ),
                            'text'      => (
                                isset( $xml->description_ml->{$this->language->get( 'code')}->text )
                                ? $xml->description_ml->{$this->language->get( 'code')}->text
                                : ( isset( $xml->id ) ? $xml->id : '' )
                            ),
                            'perm'      => $this->getFilePerms( $plugin ),
                            'file_name' => basename( $plugin )
                        );

                    }else{
                        $ret[$id] = array(
                            'title'     => ( isset( $xml->id ) ? $xml->id : '' ),
                            'text'      => ( isset( $xml->description ) ? $xml->description : '' ),
                            'perm'      => $this->getFilePerms( $plugin ),
                            'file_name' => basename( $plugin )
                        );
                    }

                    $ret[$id]['version']    = $xml->version;
                    $ret[$id]['sortOrder']  = ( !empty( $xml->sortOrder ) ? $xml->sortOrder : 99 );
                }
            }

            // sort them
            $sort_order = array();

			foreach( $ret as $k => $v ) {
				$sort_order[$k] = $v['sortOrder'];
			}

			array_multisort( $sort_order, SORT_NUMERIC, $ret );
        }

        $this->_plugins = $ret;
    }

    /**
     * check if VQMod
     * 1. is existing (we assume it is installed then)
     * 2. is installed
     * 3. runs under Joomla (and derivates of OC like AceShop, MijoShop, etc.)
     * @return bool
     */
    private function checkVqmod() {
        // 1. step: look if vqmod file exist
        if( file_exists( $this->getRootPath() . 'vqmod' . DIRECTORY_SEPARATOR . 'vqmod.php' ) ) {
            // 2. step: check if installed
            $content = file_get_contents( $this->getRootPath() . 'admin' . DIRECTORY_SEPARATOR . 'index.php' );

            if(
				( strpos( $content, '$vqmod->modCheck' ) !== false )
				|| ( strpos( $content, 'VQMod::modCheck' ) !== false )
			)
			{
                unset( $content );

                return true;
            }

            $this->_critical = $this->language->get( 'err_vqmod_not_installed' ) . '<br />';
        }

        // 3. step: it seems VQMod exists, check for derivates inside Joomla
        // they have VQMod always enabled
        // (some users think everything goes by itself! - what a poor world)
        if(
            $this->isAceShop()
            || $this->isMijoShop()
        )
        {
        	$this->_critical = null;
            return true;
        }

        $this->_critical = $this->language->get( 'err_no_vqmod' );

        return false;
    }

    /**
     * generate class for buttons depending on OC-native or derivate
     */
    private function getButtonCode() {
    	$this->data['button'] = 'button';
        if( $this->isMijoShop() || $this->isAceShop() ) {
        	$this->data['button'] .= '_oc';
        }
    }

    /** check if OC is running inside Joomla:
     * @return bool
     */
    private function isJoomla() {
        $ret        = true;
        $content    = file_get_contents( $this->getRootPath() . 'admin' . DIRECTORY_SEPARATOR . 'index.php' );

        if( !defined( '_JEXEC' ) ) {
            $ret = false;
        }

		unset( $content );

        return $ret;
    }

    /** check if AceShop is running
     * @return bool
     */
    private function isAceShop() {
        $ret        = true;
        $content    = file_get_contents( $this->getRootPath() . 'admin' . DIRECTORY_SEPARATOR . 'index.php' );

        if(
            !defined( 'HTTP_SERVER_TEMP' )
            || ( strpos( $content, 'AceShop::getClass' ) === false )
        )
        {
            $ret = false;
        }

        unset( $content );

        return $ret;
    }

    /** check if MijoShop is running
     * @return bool
     */
    private function isMijoShop() {
        $ret        = true;
        $content    = file_get_contents( $this->getRootPath() . 'admin' . DIRECTORY_SEPARATOR . 'index.php' );

        if( strpos( $content, 'MijoShop::get' ) === false ) {
            $ret = false;
        }

        unset( $content );

        return $ret;
    }

    /**
     * check if required plugins is/are existing
     * @param string/array    $var [optional]
     * @return array/null
     */
    private function checkVqmodPlg( $var = null ) {
        $result = array();

        if( $var ) {
            if( !is_array( $var ) ) {
                $vars = array( $var );
            }else{
                $vars = $var;
            }
        }else{
            $vars = $this->_plugins;
        }

        $path = $this->getRootPath() . 'vqmod/xml/' . $this->_prefix;

        foreach( $vars as $k => $v ) {
            // basically: not existing, not enabled
            $result[$k] = array(
                    'exist' => false,
                    'active'=> false
                );

            // state is: enabled
            $file = $path . $k . '.xml';
            if( file_exists( $file ) ) {
                $result[$k] = array(
                    'exist' => true,
                    'active'=> true
                );
            }

            // state is: disabled
            $file = $path . $k . '.xml_';
            if( file_exists( $file ) ) {
                $result[$k] = array(
                    'exist' => true,
                    'active'=> true
                );
            }
        }

        foreach( $result as $k => $v ) {
            if( !$result[$k]['exist'] ) {
                $this->_error[] = sprintf(
                    $this->language->get( 'err_vqmod_plg_missing' ),
                    $k
                );
            }else{
                if( !$result[$k]['active'] ) {
                    $this->_error[] = sprintf(
                        $this->language->get( 'err_vqmod_plg_disabled' ),
                        $k
                    );
                }
            }
        }
    }

    /**
     * check if vqmod plugin is enabled (file = .xml, otherwise .xml_)
     * @return bool
     */
    private function checkStateVqmodPlugin( $var ) {
        $ret = false;

        if( file_exists( $this->getRootPath() . 'vqmod/xml/' . $this->_prefix . $var . '.xml' ) ) {
            $ret = true;
        }

        return $ret;
    }

    /**
     * change the state of a vqmod plugin
     * called by ajax inside template
     * @return bool
     */
    public function changeStateVqmodPlugin() {
        $this->getBasics();

        $json       = array();
        $success    = false;
        $active     = false;
        $enabled    = false;

		if( !empty( $this->request->get['plugin'] ) ) {
            $plugin = $this->request->get['plugin'];
            $state  = $this->request->get['state'];
            $path   = $this->getRootPath() . 'vqmod/xml/' . $this->_prefix;

            if( $this->checkStateVqmodPlugin( $plugin ) ) {
                if( rename(
                        $path . $plugin . '.xml',
                        $path . $plugin . '.xml_'
                    )
                )
                {
                    $success = true;
                }
            }else{
                if(
                    rename(
                        $path . $plugin . '.xml_',
                        $path . $plugin . '.xml'
                    )
                )
                {
                    $success    = true;
                    $active     = true;
                    $enabled    = true;
                }
            }

            $this->clear_cache();

            if( $success ) {
                $json = array(
                    'success'   => $this->language->get(
                        'msg_plugin_'
                        . ( $enabled ? 'enabled' : 'disabled' )
                    ),
                    'state'     => $active
                );
            }else{
                $json['error']    = $this->language->get( 'err_plg_update' );
            }
		}

		$this->response->setOutput( json_encode( $json ) );
    }

    /**
     * clear vqmod cache (delete all files)
     */
    private function clear_cache() {
		$files = glob( $this->getRootPath() . 'vqmod/vqcache/' . 'vq*' );

		if( $files ) {
			foreach( $files as $file ) {
				if( file_exists( $file ) ) {
					@unlink( $file );
					clearstatcache();
				}
			}
		}
	}

    /**
     * clear logfile
     */
    public function clear_log() {
		$this->getBasics();

		if( $this->checkPermission() ) {
			$file    = $this->getRootPath() . 'vqmod/vqmod.log';
			$handle  = fopen( $file, 'wb' );

			fclose( $handle );

			$this->session->data['success'] = $this->language->get( 'success_clear_log' );
		}

		$this->redirect(
            $this->url->link(
                $this->_type .'/'. $this->_name,
                'token=' . $this->session->data['token'],
                'SSL'
            )
        );
	}

    /**
     * get root path
     * @return string
     */
    private function getRootPath() {
        if( !is_null( $this->_root ) ) {
            return $this->_root;
        }

        $ret   = str_replace( 'system/', '/', DIR_SYSTEM );
        // avoid invalide path
        $ret   = str_replace( array( '//', '\\\\' ), '', $ret ) . '/';

        $this->_root = $ret;

        return $ret;
    }

    /**
     * upload a legal specific plugin
     */
    private function uploadPlugin() {
		$this->getBasics();

		if( !$this->user->hasPermission( 'modify', $this->_type . '/' . $this->_name ) ) {
			$this->error['warning'] = $this->language->get( 'error_permission' );
		}else{
			$file        = $this->request->files['legal_file']['tmp_name'];
			$file_name   = $this->request->files['legal_file']['name'];

			if( $this->request->files['legal_file']['error'] > 0 ) {
				switch( $this->request->files['legal_file']['error'] )
                {
					case 1:
						$this->_error[] = $this->language->get( 'error_ini_max_file_size' );
						break;
					case 2:
						$this->_error[] = $this->language->get( 'error_form_max_file_size' );
						break;
					case 3:
						$this->_error[] = $this->language->get( 'error_partial_upload' );
						break;
					case 4:
						$this->_error[] = $this->language->get( 'error_no_upload' );
						break;
					case 6:
						$this->_error[] = $this->language->get( 'error_no_temp_dir' );
						break;
					case 7:
						$this->_error[] = $this->language->get( 'error_write_fail' );
						break;
					case 8:
						$this->_error[] = $this->language->get( 'error_php_conflict' );
						break;
					default:
						$this->_error[] = $this->language->get( 'error_unknown' );
				}
			}else{
                if( strpos( $file_name, $this->_prefix ) === false ) {
                    $this->_error[] = $this->language->get( 'error_filetype' );
                }else{
					if(
                        move_uploaded_file(
                            $file,
                            $this->getRootPath() . 'vqmod/xml/' . $file_name
                        ) == false
                    )
                    {
						$this->_error[] = $this->language->get( 'error_move' );

					}else{
						$this->clear_cache();

						$this->session->data['success'] = $this->language->get( 'success_upload' );
					}
				}
			}
		}
	}

    /**
     * defines the editor to use (checking if tinymce is installed - see path)
     */
    private function defEditor() {
    	if( file_exists( DIR_APPLICATION . 'view/javascript/tinymce/jscripts/tiny_mce/tiny_mce.js' ) ) {
            // OSWorX
    		$this->editor = 'tinymce';
        }elseif( file_exists( '../media/editors/tinymce/jscripts/tiny_mce/tiny_mce.js' ) ) {
            // Joomla (e.g. aceshop) - use embedded tinyMCE
            $this->editor = '';
        }elseif( file_exists( DIR_APPLICATION . 'view/javascript/ckeditor/ckeditor.js' ) ) {
            // OC native
            $this->editor = 'ckeditor';
    	}else{
    		$this->editor = '';
    	}

    	$this->data['editor'] = $this->editor;
    }

    /**
    * add css and js to document
    * @param bool	$ace	aceeditor
    * @param bool	$cm		codemirror
    * @param bool	$tp		tooltip
    * @param bool	$ck		ckeditor
    */
    private function addDocumentData( $ace = false, $cm = false, $tp = false, $ck = false ) {
        $this->document->addStyle( 'view/stylesheet/legal.css' );

		if( $ace ) {
        	$this->document->addScript( 'view/javascript/ace-min/ace.js');
		}

        if( $cm ) {
        	$this->document->addStyle( 'view/javascript/codemirror/codemirror.css');
			$this->document->addScript( 'view/javascript/codemirror/codemirror.js');

			$this->document->addScript( 'view/javascript/codemirror/modes/xml.js');
			$this->document->addScript( 'view/javascript/codemirror/modes/javascript.js');
			$this->document->addScript( 'view/javascript/codemirror/modes/css.js');
			$this->document->addScript( 'view/javascript/codemirror/modes/htmlmixed.js');
        }

        if( $tp ) {
	        $this->document->addStyle( 'view/javascript/jquery/poshytip/css/tip-yellowsimple/tip-yellowsimple.css' );
	        $this->document->addScript( 'view/javascript/jquery/poshytip/jquery.poshytip.min.js' );
 		}

 		if( $ck ) {
 			$this->document->addScript( 'view/javascript/ckeditor/ckeditor.js' );
 		}
    }

    /**
     * read the content of a file into a string
     * @return array
     */
    public function getContent( $fileName = '' ) {
        $file = $this->getRootPath() . $fileName;

        if(
            file_exists( $file )
            && filesize( $file ) > 0
        )
        {
            // mic: works, but see alternative below to get lines for defining height
            //$ret = file_get_contents( $file, FILE_USE_INCLUDE_PATH, null );

            $lines = file( $file );

            $ret = '';
            $i = 0;
            foreach( $lines as $line ) {
                $ret .= $line;
                ++$i;
            }

            $this->_contentLines = $i;
        }else{
            $ret = sprintf( $this->language->get( 'err_file_does_not_exist' ), $file );
        }

        return $ret;
    }

    /**
     * save string into a file
     * called directly by template
     * @return void
     */
    public function saveContent() {
        $this->getBasics();

        $content    = html_entity_decode( $this->request->post['content'] );
        $tplFile    = $this->request->post['tplFile'];
        $file       = $this->getRootPath() . $tplFile;

        if(
            $content
            && $tplFile
        )
        {
            if( file_put_contents( $file, $content, LOCK_EX ) ) {
                $json['success'] = $this->language->get( 'msg_file_saved' );
            }else{
                $json['error'] = $this->language->get( 'err_file_saved' );
            }
        }else{
            $json['error'] = $this->language->get( 'err_no_content' );
        }

        $this->response->setOutput( json_encode( $json ) );
    }

    /**
     * save string into a file
     * called directly by template
     * @return bool
     */
    public function replaceSaveFile() {
        $ret    = true;
        $new    = trim( $this->request->post['xmlLine'] );
        $org    = '<span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>';

        if( $new == '' ) {
            $this->_error[] = $this->language->get( 'err_no_content' );
            return false;
        }

        // if( $res = $this->config->get( 'xmlLine' ) ) {
            // $org = $res;
        // }

        // process only if not equal
        if( strcmp( $org, $new ) != 0 ) {
            $file       = $this->getRootPath() . 'vqmod/xml/ox_legal_specials_price.xml';

            if( file_exists( $file ) ) {
                $content    = file_get_contents( $file );

                if( $content ) {
                    $content = preg_replace(
                        '#' . preg_quote(  $org ) . '#',
                        htmlspecialchars_decode( $new ),
                        $content
                    );

                    // do only if we not deleted accidentially the content above
                    if( strlen( $content ) > 0 ) {
                        if( !file_put_contents( $file, $content ) ) { // , LOCK_EX
                            $this->_error[] = sprintf( $this->language->get( 'err_file_saved' ), $file );
                            $ret = false;
                        }
                    }else{
                        $this->_error[] = sprintf( $this->language->get( 'err_file_saved' ), $file );
                        $ret = false;
                    }
                }else{
                    $this->_error[] = sprintf( $this->language->get( 'err_file_saved' ), $file );
                    $ret = false;
                }
            }else{
                $this->_error[] = sprintf( $this->language->get( 'err_file_does_not_exist' ), $file );
            }
        }

        return $ret;
    }

    /**
     * return file permission
     * @param string    $file
     * @return string
     */
    private function getFilePerms( $file ) {
        return substr( sprintf( '%o', fileperms( $file ) ), -4 );
    }

    /**
     * read the content of a file into a string and display
     * @return array
     */
    public function getEditContent( $fileName = '' ) {
        $this->getBasics();

        if( !$fileName ) {
            if( isset( $this->request->post['file'] ) ) {
                $fileName = $this->request->post['file'];
            }else{
                $fileName = $this->request->get['file'];
            }
        }

        $file = $this->getRootPath() . 'vqmod/xml/' . $fileName;

        if(
            file_exists( $file )
            && filesize( $file ) > 0
        )
        {
            $content = file_get_contents( $file, FILE_USE_INCLUDE_PATH, null );
        }else{
            $json['error'] = 'FILE DOES NOT EXIST!';
        }

        // mic: used if colorbox may be used
        // mic: works, but disabled for direct html output below
        // $json['content'] = $content;
        // $this->response->setOutput( json_encode( $json ) );
        ?>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
        <html>
        <head>
        <style type="text/css">
            .warning { color: #FF0000; font-weight: bold; font-size: 12px; font-family: Arial, Helvetica, sans-serif; margin: 7px; padding: 10px; border: 1px solid #FF0000; }
        </style>
        </head>
        <body>
        <div class="warning"><?php echo sprintf( $this->language->get( 'text_edit_mod' ), $fileName ); ?></div>
        <div id="message"></div>
        <div id="text">
            <textarea style="height: 560px; min-width: 820px;" rows="60" cols="80" name="content" id="content"><?php echo $content; ?></textarea>
        </div>
        </body>
        </html>
        <?php
    }

    /**
     * save string into a file
     * @return void
     */
    public function saveEditContent() {
        $this->getBasics();

        $content    = html_entity_decode( $this->request->post['content'] );
        $file       = $this->getRootPath() . 'vqmod/xml/' . $this->request->post['file'];

        if( $content ) {
            if( file_put_contents( $file, $content, LOCK_EX ) ) {
                $this->session->data['success'] = $this->language->get( 'success_save_file' );
                $json['success'] = $this->language->get( 'success_save_file' );
            }else{
                $this->session->data['error'] = $this->language->get( 'error_save_file' );
                $json['error'] = $this->language->get( 'error_save_file' );
            }
        }else{
            $this->session->data['error'] = $this->language->get( 'error_save_file' );
            $json['error'] = $this->language->get( 'error_save_file' );
        }

        $this->clear_cache();

        $this->response->setOutput( json_encode( $json ) );
    }

    /**
     * read content of entire VQMod logfile
     * checks server limits against logfile size
     * note: for comparison 75% of ini values is used
     */
    public function getLogFile() {
        // VQMod Error Log
        $log_file                   = $this->getRootPath() . 'vqmod/' . 'vqmod.log';
        $this->data['log']          = '';
        $this->data['log_filesize'] = '<strong style="color: #797979;">'
                                    . $this->language->get( 'text_no_log_file_existing' )
                                    . '</strong>';
        //$postMaxSize        = round( ( ( $this->getIniValue( ini_get( 'post_max_size' ), true ) / 1048 ) / 100 ) * 75, 2 );
        $memoryLimit                = round( ( ( $this->getIniValue( ini_get( 'memory_limit' ), true ) / 1048 ) / 100 ) * 75, 2 );

		if( file_exists( $log_file ) ) {
            $fileSize = filesize( $log_file );

            if( $fileSize > 0 ) {
                // get filesize of log in readable form
                $fileSize = (
                    round(
                        ( $fileSize / 1048 ), // 1048576
                        2
                    )
                );

    			// error if log file is larger than 6MB ( 6291 = 6291456 )- display message
                // mic: use memory_limit
    			if( $fileSize > $memoryLimit ) {
    				$this->data['error_warning']    = sprintf(
                        $this->language->get('error_log_size'),
                        $fileSize
                    );
    				$this->data['log']              = sprintf(
                        $this->language->get( 'error_log_size' ),
                        $fileSize
                    );
    			}else{
    			     // dosplay normal content
    				$this->data['log'] = file_get_contents(
                        $log_file,
                        FILE_USE_INCLUDE_PATH,
                        null
                    );
    			}
            }

            $this->data['log_filesize'] = sprintf(
                $this->language->get( 'text_log_size' ),
                '<span style="color:'
                .
                    (
                        ( $fileSize < ( $memoryLimit / 100 ) * 75 ) // 5400
                        ? 'green'
                        : 'red'
                    )
                . ';">' . $fileSize . '</span>',
                $memoryLimit
            );
		}
    }

    /**
     * clean a ini value
     * @param mixed $val
     * @param bool  convert [false] calculate value by 1024
     * @return int
     */
    private function getIniValue( $val, $convert = false ) {
        if( preg_match( '/^(\d+)(.)$/', $val, $matches ) ) {
            if( $convert ) {
                if( $matches[2] == 'M' ) {
                    $val = $matches[1] * 1024 * 1024; // nnnM -> nnn MB
                }elseif( $matches[2] == 'K' ) {
                    $val = $matches[1] * 1024; // nnnK -> nnn KB
                }
            }else{
                $val = $matches[1];
            }
        }

        return $val;
    }

    /**
	 * constructs the footer
	 *
	 * Note: displaying this footer is mandatory, removing violates the license!
	 * If you do not want to display the footer, contact the author.
	 */
	private function getFooter() {
		$this->data['oxfooter']	= '<div style="text-align:center; color:#666666; margin-top:5px">'
		. ucfirst( $this->_name ) . ' v.' .$this->_version. ' &copy; ' . date( 'Y' )
		. ' by <a href="http://osworx.net" onclick="window.open(this);return false;" title="OSWorX">OSWorX</a>'
		. '</div>'
		;
	}

    /**
	 * update only defined setting values
	 * @param array		$vals	key/value pairs ($_POST)
	 * @param string	$group	optional definition for config group (name)
	 */
	private function updateSetting( $group = '' ) {
        $this->load->model( 'setting/setting' );

        $data       = $this->request->post;
        $ignore     = array( 'task', 'mode', 'act', 'tplContent' );
        $newData    = array();

		if( !$group ) {
			$group = $this->_name;
		}

		// check for ignored vals and remove,
        // check also for arrays and serialize (older versions of OC)
        foreach( $data as $key => $value ) {
            if( !in_array( $key, $ignore ) ) {
                $newData[$key] = $value;
            }
		}

        $this->model_setting_setting->editSetting(
			$group,
			$newData
		);
	}

    /**
     * check users permission
     * @return bool
     */
    private function checkPermission() {
        if( !$this->user->hasPermission( 'modify', $this->_type . '/' . $this->_name ) ) {
            $this->error['warning'] = $this->language->get( 'error_permission' );
        }

        if(
            !$this->error
            && is_null( $this->_error )
        )
        {
            return true;
        }else{
            return false;
        }
    }

    /**
     * check access to this module
     * @return bool
     */
    private function validate() {
        $this->checkPermission();

        $vars = array(
            'hint', 'popup_height', 'popup_width', 'xmlLine'
        );

        foreach( $vars as $var ) {
            if( !$this->request->post[$var] ) {
                $this->_error[] = $this->language->get( 'error_' . $var );
            }
        }

        if(
            !$this->error
            && is_null( $this->_error )
        )
        {
            return true;
        }else{
            return false;
        }
    }

    /** ######### support functions ######### */

    /**
     * support: add all relevant support data to object
     * @param bool  $tpl    define also various template data
     */
    private function getSupportData( $tpl = true ) {
        $supported = false;

        if( $supporter = $this->getSupportInstance() ) {
            $supporter->_getSupportLinks();
            $supporter->_getSupportLangVars();
            $supporter->_addSupportDocumentData();

            $supported = true;

            if( $tpl ) {
                $this->setSupportTemplateData( $supported  );
            }

            $this->data['currentVersion'] = $supporter->translateResult( $this->getVersionOnly() );
        }else{
            $this->data['currentVersion'] = array(
                'changelog' => ''
            );

            $this->data['support'] = array(
                'tab'   => '',
                'js'    => ''
            );
        }

        return $supported;
    }

    /**
	 * support: get current version ( and changelog if newer )
	 */
    public function getVersionOnly() {
        if( $supporter = $this->getSupportInstance() ) {
            return $supporter->_getVersionOnly();
        }else{
            $ret = array(
                'changelog' => ''
            );
        }

        return $ret;
    }

    /**
	 * support: checks installed version vs current published
	 */
    public function checkVersion() {
        if( $supporter = $this->getSupportInstance() ) {
            $supporter->_checkVersion();
        }
    }

    /**
     * support: get date for valid support
     * @return array
     */
    public function isValidUntil() {
        if( $supporter = $this->getSupportInstance() ) {
            $supporter->_isValidUntil();
        }
    }

    /**
     * support: update current extension
     * @return array
     */
    public function updateNow() {
        if( $supporter = $this->getSupportInstance() ) {
            $supporter->_updateNow();
        }
    }

    /**
     * get instance of supporter class
     * @return mixed object/false
     */
    private function getSupportInstance() {
        $lib = DIR_SYSTEM . 'osworx/libraries/tools/oxsupporter.php';

        if( file_exists( $lib ) ) {
            require_once( $lib );
            return ControllerModuleOXSupporter::getInstance( $this );
        }

        return false;
    }

    /**
     * build additional data and add to global scope
     * @param bool  $supported
     */
    private function setSupportTemplateData( $supported ) {
        if( $supporter = $this->getSupportInstance() ) {
            $supporter->_setSupportTemplateData( $supported );
        }
    }
}