<?php
/**
 * @version		$Id: legal.tpl 3814 2014-12-05 08:21:56Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2013 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License
 */
echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php
        foreach( $breadcrumbs as $bc ) {
            echo $bc['separator']; ?><a href="<?php echo $bc['href']; ?>"><?php echo $bc['text']; ?></a>
            <?php
        } ?>
    </div>
    <?php
    if( $error_permission ) { ?>
        <div class="warning"><?php echo $error_permission; ?></div>
        <?php
    }
    if( $errors ) { ?>
        <div class="warning">
            <?php
            foreach( $errors as $error ) {
                echo $error . '<br />';
            } ?>
        </div>
        <?php
    }
    if( $success ) { ?>
        <div class="success"><?php echo $success; ?></div>
        <?php
    } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/osworx/legal.png" alt="" /> <?php echo $bc_title; ?></h1>
            <?php
		    if( $critical ) { ?>
		            </div>
		            <div class="content"><div class="warning"><?php echo $critical; ?></div></div>
		        </div>
		        <?php echo $oxfooter; ?>
		    </div>
		        <?php
		    }else{ ?>
                <div class="buttons">
                    <a onclick="jQuery('#form').submit();" class="<?php echo $button; ?>"><span><?php echo $button_save; ?></span></a>
                    <a onclick="apply();" class="<?php echo $button; ?> button_apply"><span><?php echo $button_apply; ?></span></a>
                    <a onclick="location='<?php echo $links['cancel']; ?>';" class="<?php echo $button; ?> button_cancel"><span><?php echo $button_cancel; ?></span></a>
                    <a onclick="window.open('<?php echo $links['help']; ?>','help','width=1024,height=720,left=20,scrollbars=yes,location=no');" class="<?php echo $button; ?> button_setting"><span><?php echo $button_help; ?></span></a>
                </div>
            </div>
            <div class="content">
                <div id="installNote"><div class="attention"><?php echo $text_finish_install; ?></div></div>
                <div id="tabs" class="htabs">
                    <a href="#tab-plugins" id="nav-plugins"><?php echo $tab_plugins; ?></a>
                    <a href="#tab-common"><?php echo $tab_common; ?></a>
                    <a href="#tab-text"><?php echo $tab_text; ?></a>
                    <a href="#tab-emailtexts"><?php echo $tab_emailtexts; ?></a>
                    <a href="#tab-specials"><?php echo $tab_specials; ?></a>
					<a href="#tab-baseprice"><?php echo $tab_baseprice; ?></a>
                    <a href="#tab-css"><?php echo $tab_css; ?></a>
                    <a href="#tab-locales" class="tablocales" id="nav-locales"><?php echo $tab_locales; ?></a>
                    <a href="#tab-attributes"><?php echo $tab_attributes; ?></a>
                    <a href="#tab-taxes" class="tabcss"><?php echo $tab_taxes; ?></a>
                    <a href="#tab-log" class="tablog"><?php echo $tab_log; ?></a>
                    <?php echo $support['tab']; ?>
                </div>
                <div style="clear: both;"></div>
                <form action="<?php echo $links['action']; ?>" method="post" enctype="multipart/form-data" id="form">
                    <div id="tab-plugins">
                        <div style="margin: 15px;">
                            <a class="showHide" data-field="help-plugins"><?php echo $text_show_hide_help; ?></a>
                        </div>
                        <div id="help-plugins" class="help-outline">
                            <div>
                                <?php echo $help_use_plugin; ?>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div style="float: left; margin: 5px 0 0 20px; padding: 5px 0 5px 25px; width: 95%; border: 1px solid #EBEBEB; color: #3C3C3C; font-weight: bold;">
                            <div style="float: left; width: 30%;"><?php echo $text_title_state; ?></div>
                            <div style="float: left; width: 50px;"><?php echo $text_version; ?></div>
                            <div style="float: left; width: 50px;"><?php echo $text_permission; ?></div>
                            <div style="float: left;"><?php echo $text_description; ?></div>
                            <div style="clear: both;"></div>
                        </div>
                        <table class="form">
                            <tr>
                                <td colspan="3">
                                    <?php
                                    $row0 = '#F8F8F8';
                                    $row1 = '#F2F2F2';
                                    $i = 0;
                                    $j = 0;
                                    foreach( $plugins as $k => $v ) { ?>
                                        <div id="hover_<?php echo $j; ?>" class="legal-plugin" style="background-color: <?php echo ${'row' . $i}; ?>;" onmouseover="changeBG('<?php echo $j; ?>');" onmouseout="changeBG('<?php echo $j; ?>','<?php echo ${'row' . $i}; ?>');">
                                            <div style="float: left; margin-left: 20px; width: 30%;" id="plugin_<?php echo $k; ?>">
                                                <strong id="plgTitle_<?php echo $k; ?>"><a onclick="getEditContent('<?php echo $v['file_name']; ?>');" title="<?php echo $text_edit_file; ?>" style="text-decoration: none;"><?php echo $v['title']; ?></a></strong>
                                                <br />
                                                <div style="float: left;">
                                                	<div style="float:left;">
		                                                <label<?php echo ${'plg_' . $k} ? ' style="color: #12772C;"' : ''; ?>>
		                                                    <input type="radio" value="1" onchange="changePlgState('<?php echo $k; ?>',this.value);"<?php echo ${'plg_' . $k} ? ' checked="checked"' : ''; ?> />
		                                                    <span><?php echo $text_yes; ?></span>
		                                                </label>
	                                                </div>
	                                                <div style="float:left; margin-left: 15px;">
		                                                <label<?php echo !${'plg_' . $k} ? ' style="color: #FF0000; font-weight: bold;"' : ''; ?>>
		                                                    <input type="radio" value="0" onchange="changePlgState('<?php echo $k; ?>',this.value);"<?php echo !${'plg_' . $k}  ? ' checked="checked"' : ''; ?> />
		                                                    <span><?php echo $text_no; ?></span>
		                                                </label>
	                                                </div>
	                                                <div style="clear: both;"></div>
                                                </div>
                                            </div>
                                            <div class="legal-plugin_col px50">
                                                (v.<?php echo $v['version']; ?>)
                                            </div>
                                            <div class="legal-plugin_col px50">
                                                <?php echo $v['perm']; ?>
                                            </div>
                                            <div class="legal-plugin_col pc50">
                                                <?php echo $v['text']; ?>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                        <?php
                                        $i = 1 - $i;
                                        ++$j;
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $entry_upload; ?>
                                </td>
                                <td colspan="2">
                                    <input type="file" name="legal_file" />
                                    <input type="submit" name="upload" value="<?php echo $btn_upload; ?>" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="tab-common">
                        <table class="form">
                            <tr>
                                <td class="vtop">
                                    <span class="required">*</span> <?php echo $entry_hint; ?>
                                </td>
                                <td class="vtop">
                                    <input type="text" name="hint" value="<?php echo $hint; ?>" size="40" />
                                </td>
                                <td>
                                    <?php echo $help_hint; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="vtop">
                                    <?php echo $entry_text_summary; ?>
                                </td>
                                <td class="vtop">
                                    <?php echo $lists['text_summary']; ?>
                                </td>
                                <td style="border: 1px solid #FF0000; color: #FF0000; background-color: #FFECEC; padding: 10px;">
                                    <?php echo $help_text_summary; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="vtop">
                                    <?php echo $entry_popup; ?>
                                </td>
                                <td class="vtop">
                                    <input type="text" name="popup_width" value="<?php echo $popup_width; ?>" size="4" />
                                    &nbsp;x&nbsp;
                                    <input type="text" name="popup_height" value="<?php echo $popup_height; ?>" size="4" />
                                </td>
                                <td>
                                    <?php echo $help_popup; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="vtop">
                                    <?php echo $entry_image; ?>
                                </td>
                                <td class="vtop">
                                    <input type="text" name="image_width" value="<?php echo $image_width; ?>" size="4" />
                                    &nbsp;x&nbsp;
                                    <input type="text" name="image_height" value="<?php echo $image_height; ?>" size="4" />
                                </td>
                                <td>
                                    <?php echo $help_image; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="vtop">
                                    <?php echo $entry_position_hint; ?>
                                </td>
                                <td class="vtop">
                                    <?php echo $lists['position_hint']; ?>
                                </td>
                                <td>
                                    <?php echo $help_position_hint; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="vtop">
                                    <?php echo $entry_description_len; ?>
                                </td>
                                <td class="vtop">
                                    <input type="text" name="description_len" value="<?php echo $description_len; ?>" size="5" />
                                </td>
                                <td>
                                    <?php echo $help_description_len; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="tab-text">
                        <div style="margin: 15px;">
                            <a class="showHide" data-field="help-text"><?php echo $text_show_hide_help; ?></a>
                        </div>
                        <div id="help-text" class="help-outline">
                            <div>
                                <?php echo $help_text; ?>
                            </div>
                            <div style="margin: 15px 0 0 220px;">
                                <?php echo $lists['text_ids']; ?>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div id="languages" class="htabs">
                            <?php
                            foreach( $languages as $language ) {
                                if( $language['status'] ) { ?>
                                    <a href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
                                    <?php
                                }
                            } ?>
                        </div>
                        <?php
                        foreach( $languages as $language ) {
                            if( $language['status'] ) { ?>
                                <div id="language<?php echo $language['language_id']; ?>">
                                    <table class="form">
                                        <tr style="background-color: #FFEBEB;">
                                            <td class="vtop">
                                                <?php echo $entry_text_button; ?>
                                            </td>
                                            <td class="vtop">
                                                <input type="text" name="button_text[<?php echo $language['language_id']; ?>]" size="60" value="<?php echo isset( $button_text[$language['language_id']] ) ? $button_text[$language['language_id']] : ''; ?>" />
                                            </td>
                                            <td style="color: red;">
                                                <?php echo $help_text_button; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="vtop">
                                                <?php echo $entry_text_wtax; ?>
                                            </td>
                                            <td colspan="2">
                                                <textarea name="price_text[<?php echo $language['language_id']; ?>][wtax]" id="price_text_<?php echo $language['language_id']; ?>_wtax" cols="80" rows="6"><?php echo isset( $price_text[$language['language_id']] ) ? $price_text[$language['language_id']]['wtax'] : ''; ?></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="vtop">
                                                <?php echo $entry_text_wotax; ?>
                                            </td>
                                            <td colspan="2">
                                                <textarea name="price_text[<?php echo $language['language_id']; ?>][wotax]" id="price_text_<?php echo $language['language_id']; ?>_wotax" cols="80" rows="6"><?php echo isset( $price_text[$language['language_id']] ) ? $price_text[$language['language_id']]['wotax'] : ''; ?></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="vtop">
                                                <?php echo $entry_text_slider; ?>
                                            </td>
                                            <td class="vtop">
                                                <input type="text" name="slider_text[<?php echo $language['language_id']; ?>]" size="50" value="<?php echo isset( $slider_text[$language['language_id']] ) ? $slider_text[$language['language_id']] : ''; ?>" />
                                            </td>
                                            <td>
                                                <?php echo $help_text_slider; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <?php
                            }
                        } ?>
                    </div>

                    <div id="tab-emailtexts">
						<div style="margin: 15px;">
                            <a class="showHide" data-field="help-emailtexts"><?php echo $text_show_hide_help; ?></a>
                        </div>
                        <div id="help-emailtexts" class="help-outline">
                            <div>
                                <?php echo $help_emailtexts; ?>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div>
							<table class="form">
								<tr>
	                                <td class="vtop">
	                                    <?php echo $entry_text_id; ?>
	                                </td>
	                                <td class="vtop">
	                                    <?php echo $lists['text_to_email']; ?>
	                                </td>
	                                <td>
	                                    <?php echo $help_text_id; ?>
	                                </td>
	                            </tr>
							</table>
						</div>
                        <div class="vtabs">
                        	<?php
                        	foreach( $order_status as $status ) {
                        		$bgColor = '#F7F7F7';
                        		if( ( !empty( $textToEmails[$status['order_status_id']]['text_id'] ) ) && ( $textToEmails[$status['order_status_id']]['text_id'] != '' ) ) {
                        			$bgColor = '#E3FFEA';
                        		} ?>
                        		<a class="tab-bg" id="bg_<?php echo $status['order_status_id']; ?>" href="#tab-text-<?php echo $status['order_status_id']; ?>" style="background-color: <?php echo $bgColor; ?>"><?php echo $status['name']; ?></a>
                        		<?php
                        	} ?>
						</div>
						<?php
						foreach( $order_status as $status ) { ?>
							<div id="tab-text-<?php echo $status['order_status_id']; ?>" class="vtabs-content">
								<table class="form">
									<tr>
										<td><?php echo $entry_text; ?></td>
										<td>
											<select name="textToEmails[<?php echo $status['order_status_id']; ?>][text_id]">
												<option value="" style="color: #F00;"><?php echo $text_disabled; ?></option>
												<option disabled="disabled" style="color: #6D6D6D;">- - - - - - - -</option>
												<?php
												foreach( $emailTexts as $text ) { ?>
													<option value="<?php echo $text['information_id']; ?>"<?php echo ( !empty( $textToEmails[$status['order_status_id']]['text_id'] ) && ( $textToEmails[$status['order_status_id']]['text_id'] == $text['information_id'] ) ? ' selected="selected"' : '' ); ?>><?php echo $text['title']; ?></option>
													<?php
												} ?>
											</select>
										</td>
									</tr>
								</table>
							</div>
							<?php
						} ?>
					</div>

                    <div id="tab-specials">
                        <div style="margin: 15px;">
                            <a class="showHide" data-field="help-specials"><?php echo $text_show_hide_help; ?></a>
                        </div>
                        <div id="help-specials" class="help-outline">
                            <div>
                                <?php echo $help_specials; ?>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div id="languages2">
                            <?php
                            foreach( $languages as $language ) {
                                if( $language['status'] ) { ?>
                                    <div style="margin: 10px 0 0 25px;">
                                        <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" alt="" />
                                        <input type="text" name="specials_text[<?php echo $language['language_id']; ?>]" size="120" class="textLine" value="<?php echo isset( $specials_text[$language['language_id']] ) ? $specials_text[$language['language_id']] : ''; ?>" />
                                    </div>
                                    <?php
                                }
                            } ?>
                        </div>
                        <div id="displayContent" style="margin: 15px;">
                            <span class="required">*</span> <?php echo $entry_xml_line; ?>
                            <br />
                            <input id="searchItem" type="text" name="xmlLine" size="120" class="textLine" value="<?php echo $xmlLine; ?>" />
                            <br />
                            <a id="findItem" class="<?php echo $button; ?> button_setting"><span><?php echo $text_search; ?></span></a>
                        </div>
                        <div id="template" style="margin: 15px;">
                            <div class="help-outline">
                                <?php echo $help_tpl_content; ?>
                            </div>
                            <div style="clear: both;"></div>
                            <div style="margin: 15px; padding: 3px; border: 1px solid #9F9F9F;">
                                <?php echo $text_used_template; ?>:
                                &nbsp;
                                <strong>../<?php echo $tpl; ?></strong>
                            </div>
                            <div class="help_link">
                            	<div class="buttons">
                            		<div style="float: left;">
		                				<a class="expand" data-field="Product"><?php echo $text_expand; ?></a>&nbsp;|&nbsp;<a class="reduce" data-field="Product"><?php echo $text_reduce; ?></a>
									</div>
									<div style="float: right; margin: 10px;">
	                                	<a onclick="saveContent();" class="<?php echo $button; ?> button_apply"><span><?php echo $button_save; ?></span></a>
	                            	</div>
	                            	<div style="clear: both;"></div>
	       						</div>
		               		</div>
                            <div id="tplProduct" class="ace_editor"><?php echo htmlspecialchars( $tplProduct ); ?></div>
                        </div>
                    </div>
					<div id="tab-baseprice">
						<div style="margin: 15px;">
                            <a class="showHide" data-field="help-baseprice"><?php echo $text_show_hide_help; ?></a>
                        </div>
                        <div id="help-baseprice" class="help-outline">
                            <div>
                                <?php echo $help_base_price; ?>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div id="languages3" class="htabs">
                            <?php
                            foreach( $languages as $language ) {
                                if( $language['status'] ) { ?>
                                    <a href="#language3<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
                                    <?php
                                }
                            } ?>
                        </div>
                        <?php
                        foreach( $languages as $language ) {
                            if( $language['status'] ) { ?>
                                <div id="language3<?php echo $language['language_id']; ?>">
                                    <table class="form">
										<tr>
                                            <td class="vtop">
                                                <?php echo $entry_baseprice_content; ?>
                                            </td>
                                            <td>
                                                <input type="text" name="base_price_content_text[<?php echo $language['language_id']; ?>]" size="50" value="<?php echo isset( $base_price_content_text[$language['language_id']] ) ? $base_price_content_text[$language['language_id']] : ''; ?>" title="<?php echo $text_bp_content; ?>" />
                                            </td>
                                            <td class="vtop"><?php echo $help_baseprice_content; ?></td>
										</tr>
										<tr>
											<td class="vtop">
                                                <?php echo $entry_baseprice_price; ?>
                                            </td>
											<td>
                                                <input type="text" name="base_price_price_text[<?php echo $language['language_id']; ?>]" size="50" value="<?php echo isset( $base_price_price_text[$language['language_id']] ) ? $base_price_price_text[$language['language_id']] : ''; ?>" title="<?php echo $text_bp_price; ?>" style="margin-top: 5px;" />
                                            </td>
                                            <td class="vtop"><?php echo $help_baseprice_price; ?></td>
                                        </tr>
									</table>
								</div>
								<?php
							}
						} ?>
					</div>
                    <div id="tab-css">
                        <div style="margin: 15px;">
                            <a class="showHide" data-field="help-css"><?php echo $text_show_hide_help; ?></a>
                        </div>
                        <div id="help-css" class="help-outline">
                            <div>
                                <?php echo $help_css; ?>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div id="css" style="margin: 15px;">
		               		<div style="margin: 15px; padding: 3px; border: 1px solid #9F9F9F;">
                                <?php echo $text_used_css; ?>:
                                &nbsp;
                                <strong>../<?php echo $css; ?></strong>
                            </div>
                            <div class="help_link">
                            	<div class="buttons">
                            		<div style="float: left;">
		                				<a class="expand" data-field="Css"><?php echo $text_expand; ?></a>&nbsp;|&nbsp;<a class="reduce" data-field="Css"><?php echo $text_reduce; ?></a>
									</div>
									<div style="float: right; margin: 10px;">
	                                	<a onclick="saveContent('css');" class="<?php echo $button; ?> button_apply"><span><?php echo $button_save; ?></span></a>
	                            	</div>
	                            	<div style="clear: both;"></div>
	       						</div>
		               		</div>
                            <div id="tplCss" class="ace_editor"><?php echo htmlspecialchars( $cssContent ); ?></div>
                        </div>
                    </div>
                    <div id="tab-locales">
                        <div style="margin: 15px;">
                            <a class="showHide" data-field="help-locales"><?php echo $text_show_hide_help; ?></a>
                        </div>
                        <div id="help-locales" class="help-outline">
                            <?php echo $help_locales; ?>
                        </div>
                        <table class="form">
                            <tr>
                                <td><?php echo $entry_countries; ?></td>
                                <td><?php echo $lists['countries']; ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                	<div id="accordion">
										<h3><a href="#"><?php echo $text_geo_zones; ?></a></h3>
										<div id="zone">
                                            <table class="list">
                                                <thead>
                                                    <tr>
                                                        <td class="left" style="width: 90%;"><?php echo $text_country_ies; ?></td>
                                                        <td class="right"><?php echo $text_state; ?></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="geoZones">
                                                    <?php
                                                    $j = 1;
                                                    foreach( $geoZones as $k => $v ) {
                                                    	if( $v['name'] ) { ?>
	                                                        <tr class="row<?php echo ( ++$j % 2 ); ?>">
	                                                            <td class="left" style="font-weight: bold;">
	                                                                <span id="zone_<?php echo $k; ?>_display">
	                                                                    <?php echo $v['name']; ?>
	                                                                </span>
	                                                            </td>
	                                                            <td class="right">
	                                                                <div id="zone_<?php echo $k; ?>_state"></div>
	                                                            </td>
	                                                        </tr>
	                                                        <?php
														}
                                                    } ?>
                                                </tbody>
                                            </table>
	                                    </div>

										<h3><a href="#"><?php echo $text_taxes; ?></a></h3>
										<div id="tax">
                                            <table class="list">
                                                <thead>
                                                    <tr>
                                                        <td class="left"><?php echo $text_title; ?></td>
                                                        <td class="left"><?php echo $text_description; ?></td>
                                                        <td class="center"><?php echo $text_rate; ?></td>
                                                        <td class="center"><?php echo $text_type; ?></td>
                                                        <td class="center"><?php echo $text_geo_zone; ?></td>
                                                        <td class="right"><?php echo $text_state; ?></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="displayTaxes">
                                                    <?php
                                                    $i = 0;
                                                    $j = 1;
                                                    foreach( $taxes as $tax ) { ?>
                                                        <tr class="row<?php echo ( ++$j % 2 ); ?>">
                                                            <td class="left vtop" style="width: 40%;">
                                                                <span id="tax_<?php echo $i; ?>_display">
                                                                    <?php echo $tax['display']; ?>
                                                                </span>
                                                            </td>
                                                            <td class="left vtop" style="width: 40%;">
                                                                <span id="tax_<?php echo $i; ?>_title">
                                                                    <?php echo $tax['title']; ?>
                                                                </span>
                                                            </td>
                                                            <td class="center vtop">
                                                                <span id="tax_<?php echo $i; ?>_rate">
                                                                    <?php echo $tax['rate']; ?>
                                                                </span>
                                                            </td>
                                                            <td class="center vtop">
                                                                <span id="tax_<?php echo $i; ?>_type">
                                                                    <?php echo $tax['type']; ?>
                                                                </span>
                                                            </td>
                                                            <td class="center vtop">
                                                                <span id="tax_<?php echo $i; ?>_zone">
                                                                    <?php echo $geoZones[$tax['geo_zone']]['name']; ?>
                                                                </span>
                                                            </td>
                                                            <td class="right vtop">
                                                                <span id="tax_<?php echo $i; ?>_state"></span>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        ++$i;
                                                    } ?>
                                                </tbody>
                                            </table>
	                                    </div>

	                                    <h3><a href="#"><?php echo $text_tax_classes; ?></a></h3>
                                    	<div id="class">
                                            <table class="list">
                                                <thead>
                                                    <tr>
                                                        <td class="left" style="width: 20%;"><?php echo $text_title; ?></td>
                                                        <td class="left" style="width: 70%;"><?php echo $text_description; ?></td>
                                                        <td class="right"><?php echo $text_state; ?></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="displayTaxClasses">
                                                    <?php
                                                    $i = 0;
                                                    $j = 1;
                                                    foreach( $taxClasses as $class ) { ?>
                                                        <tr class="row<?php echo ( ++$j % 2 ); ?>">
                                                            <td class="left" style="font-weight: bold;">
                                                                <span id="class_<?php echo $i; ?>_display">
                                                                    <?php echo $class['title']; ?>
                                                                </span>
                                                            </td>
                                                            <td class="left">
                                                                <span id="class_<?php echo $i; ?>_desc">
                                                                    <?php echo $class['description']; ?>
                                                                </span>
                                                            </td>
                                                            <td class="right">
                                                                <div id="class_<?php echo $i; ?>_state"></div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        ++$i;
                                                    } ?>
                                                </tbody>
                                            </table>
	                                    </div>

	                                    <h3><a href="#"><?php echo $text_legal_texts; ?></a></h3>
	                                    <div id="text">
	                                    	<div class="attention" style="margin: 3px;">
												<?php echo $text_legal_texts_note; ?>
											</div>
                                            <table class="list">
                                                <thead>
                                                    <tr>
                                                        <td class="left" style="width: 90%;"><?php echo $text_title_header; ?></td>
                                                        <td class="right"><?php echo $text_state; ?></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="texts">
                                                    <?php
                                                    $i = 0;
                                                    $j = 1;
                                                    foreach( $texts as $text ) { ?>
                                                        <tr class="row<?php echo ( ++$j % 2 ); ?>">
                                                            <td class="left">
                                                                <span id="text_<?php echo $i; ?>_display">
                                                                    <?php echo $text['information_description'][$langCode]['title']; /*$text['display'];*/ ?>
                                                                </span>
                                                                <div id="text_<?php echo $i; ?>_content" style="display: none;"><?php echo $text['information_description'][$langCode]['description']; ?></div>
																<img onclick="showText('<?php echo $i; ?>');" src="view/image/view.png" width="16" height="16" style="cursor: pointer;" alt="" />
                                                            </td>
                                                            <td class="right">
                                                                <div id="text_<?php echo $i; ?>_state"></div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        ++$i;
                                                    } ?>
                                                </tbody>
                                            </table>
	                                    </div>
									</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="tab-attributes">
                        <div style="margin: 15px;">
                            <div style="float: left; width: 80%;">
                                <a class="showHide" data-field="help-attributes"><?php echo $text_show_hide_help; ?></a>
                            </div>
                            <div class="buttons" style="float: right; margin: 10px;">
                                <a onclick="checkAttributes();" class="<?php echo $button; ?> button_apply"><span><?php echo $btn_check; ?></span></a>
                            </div>
                            <div style="clear: both;"></div>
                        </div>

                        <div id="help-attributes" class="help-outline">
                            <div>
                                <?php echo $help_attributes; ?>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div id="attributes" style="margin: 15px;">
                            <div id="products"></div>
                        </div>
                    </div>
                    <div id="tab-taxes">
                        <div style="margin: 15px;">
                            <a class="showHide" data-field="help-taxes"><?php echo $text_show_hide_help; ?></a>
                        </div>
                        <div id="help-taxes" class="help-outline">
                            <?php echo $help_taxes; ?>
                        </div>
                        <div id="tabTaxes">
                            <table class="form">
                                <tr>
                                    <td>
                                        <?php echo $entry_tax_class; ?>
                                    </td>
                                    <td>
                                        <span id="updateTaxId">
                                            <?php echo $lists['updateTaxId']; ?>
                                        </span>
                                    </td>
                                    <td>
                                        <a onclick="update('products');" title="<?php echo $btn_update_products; ?>"><?php echo $btn_update_products; ?></a>
                                        &nbsp;
                                        <a onclick="update('shipping');" title="<?php echo $btn_update_shippings; ?>"><?php echo $btn_update_shippings; ?></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div id="tab-log">
                        <h4 style="font-size:15px; background-color:#EEEEEE; padding:9px 0px 9px 40px; border:solid 1px #B6B8D3; background-image:url('view/image/log.png'); background-repeat:no-repeat; background-position:1% 50%;"><?php echo $text_log_file; ?></h4>
                        <div>
                            <div>
                                <?php echo $log_filesize; ?>
                            </div>
                            <table class="form">
                                <tr>
                                    <td style="border-bottom-color:#fff;">
                                        <textarea class="legal-textarea" cols="160" rows="40"><?php echo $log ? $log : $text_no_data; ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top-color:#fff;">
                                        <div style="text-align:right;">
                                            <a href="<?php echo $clear_log; ?>" class="<?php echo $button; ?> button_orange"><span><?php echo $btn_clear; ?></span></a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php eval( $support['tpl'] ); ?>
                </form>
            </div>
        </div>
        <?php echo $oxfooter; ?>
    </div>
    <script type="text/javascript">
        /* <![CDATA[ */
        jQuery(document).ready(function(){
            jQuery('#tabs a').tabs();
            jQuery('#languages a').tabs();
            jQuery('#languages2 a').tabs();
            jQuery('#languages3 a').tabs();
            jQuery('.vtabs a').tabs();
        // });

	        jQuery(function() {
				jQuery('#accordion').accordion({
					collapsible	: true,
					autoHeight	: false,
					heightStyle : 'content'
				});
			});
		});

		jQuery('.tab-bg').on('click', function() {
			checkBg();
			jQuery('#' + this.id).css('background-color','#FFFFFF');
		});

		function checkBg() {
			<?php foreach( $order_status as $status ) { ?>
			var value = jQuery('select[name=\'textToEmails[<?php echo $status['order_status_id']; ?>][text_id]\']').val();
			if( value ) {
				jQuery('#bg_<?php echo $status['order_status_id']; ?>').css('background-color','#E3FFEA')
			}else{
				jQuery('#bg_<?php echo $status['order_status_id']; ?>').css('background-color','#F7F7F7')
			}
			<?php
			} ?>
		};

		function showText(id) {
	        var text = jQuery('#text_' + id + '_content').html();
	        jQuery('#dialog').remove();
	    	jQuery('#content').prepend('<div id="dialog" title="" style="padding: 3px 0px 0px 0px;">' + text + '<\/div>');
	    	jQuery('#dialog').dialog({
	    		bgiframe	: false,
	    		width		: 800,
	    		height		: 600,
	    		resizable	: true,
	    		modal		: true
	   		});
		};

        var edProduct = ace.edit('tplProduct');
		edProduct.getSession().setMode('ace/mode/php');
		edProduct.getSession().setUseWrapMode(true);
		edProduct.setShowPrintMargin(false);

		var newProduct = ( edProduct.getSession().getLength() * 16 ) + 50;

		var edCss = ace.edit('tplCss');
		edCss.getSession().setMode('ace/mode/css');
		edCss.getSession().setUseWrapMode(true);
		edCss.setShowPrintMargin(false);

		var newCss = ( edCss.getSession().getLength() * 16 ) + 50;

		jQuery('.expand').on('click', function() {
			var field = jQuery(this).attr('data-field');
			switch( field ) {
				case 'Product':
					jQuery('#tplProduct').css('height',newProduct + 'px');
					edProduct.resize();
					edProduct.moveCursorTo(0,0);
					break;
				case 'Css':
					jQuery('#tplCss').css('height',newCss + 'px');
					edCss.resize();
					edCss.moveCursorTo(0,0);
					break;
			}
		});

		jQuery('.reduce').on('click', function() {
			var field = jQuery(this).attr('data-field');
			switch( field ) {
				case 'Product':
					jQuery('#tplProduct').css('height','450px');
					edProduct.resize();
					edProduct.moveCursorTo(0,0);
					break;
				case 'Css':
					jQuery('#tplCss').css('height','450px');
					edCss.resize();
					edCss.moveCursorTo(0,0);
					break;
			}
		});

		jQuery('#findItem').on('click', function() {
			var search = jQuery('#searchItem').val();

			edProduct.find(search);
		});

        var html = '';

        function changeBG(row,val) {
            var id = '#hover_' + row;

            if( val == undefined ) {
    			var val = '#CAE0F1';
    		}

            jQuery(id).css( 'background-color',val );
        };

        function apply() {
    		jQuery('#form').append('<input type="hidden" name="mode" value="apply" />');
    		jQuery('#form').submit();
    	};

        jQuery('.showHide').click(function() {
        	var field = jQuery(this).attr('data-field');
			$('#' + field ).toggle('slow', function() {});
		});

        function hideShow(val) {
            if( val ) {
                jQuery('#tax').fadeIn('slow');
            }else{
                jQuery('#tax').hide();
            }
        };

        function setState(val,field,type) {
        	var html = '<div id="' + type + '_' + field + '_state">';
            if( val ) {
                html += '<span style="color: #2E8A12; font-weight: bold;"><?php echo $text_installed; ?><\/span><\/div>';
            }else{
                html += '<a onclick="installItem(\'' + field + '\',\'' + type + '\');"><?php echo $button_install; ?><\/a><\/div>';
            }
            jQuery('#' + type + '_' + field + '_state').replaceWith(html);
        };

        function hideShowTab(val) {
            if( val ) {
                jQuery('#updateMsg').remove();
                jQuery('#tabTaxes').show();
                jQuery('.tabcss').css({'color':'#010101'});
            }else{
                jQuery('.tabcss').css({'color':'#ACACAC'});
                jQuery('#updateMsg').remove();
                jQuery('#tabTaxes').before('<div id="updateMsg" class="attention"><?php echo $msg_install_taxclasses; ?><\/div>');
                jQuery('#tabTaxes').hide();
            }
        };

        function showMessage(msg,type,field,hide) {
            if( undefined == type ) {
                var type = 'attention';
            }
            if( undefined == field ) {
                var field = 'tax';
            }
            if( undefined == hide ) {
                var hide = true;
            }

            jQuery('.success, .warning, .attention, .error').remove();
            jQuery('#' + field).before('<div style="margin: 10px;" class="' + type + '">' + msg + '<\/div>');
            if( hide == true ) {
                jQuery('.' + type).delay(4000).fadeOut(1000);
            }
        };

        // load locale country settings
        jQuery('select[name=\'shopCountry\']').bind('change', function() {
        	jQuery.ajax({
        		url: 'index.php?route=module/legal/getLocaleSettings',
				data: {
					token: '<?php echo $token; ?>',
					country_id: this.value,
				},
        		dataType: 'json',
        		success: function( json ) {
        			if( json != '' && !json['error'] ) {
                        hideShowTab(json['tabTaxes']);
                        hideShow(1);

                        var html = '';
						var j = 1;
                        for( i = 0; i < json['taxes'].length; ++i ) {
                            html += '<tr class="row' + ( ++j % 2 ) + '">' + "\n"
								+ '<td class="left vtop" style="width: 40%;">'
	                                + '<span id="tax_' + i + '_display">' + json['taxes'][i]['display'] + '</span>'
	                            + '</td>'
	                            + '<td class="left vtop" style="width: 40%;">'
	                                + '<span id="tax_' + i + '_title">' + json['taxes'][i]['title'] + '</span>'
	                            + '</td>'
	                            + '<td class="center vtop">'
	                                + '<span id="tax_' + i + '_rate">' + json['taxes'][i]['rate'] + '</span>'
	                            + '</td>'
	                            + '<td class="center vtop">'
	                                + '<span id="tax_' + i + '_type">' + json['taxes'][i]['type'] + '</span>'
	                            + '</td>'
	                            + '<td class="center vtop">'
	                                + '<span id="tax_' + i + '_zone">' + json['geoZones'][json['taxes'][i]['geo_zone']]['name'] + '</span>'
	                            + '</td>'
	                            + '<td class="right vtop">'
	                                + '<span id="tax_' + i + '_state"></span>'
	                            + '</td>'
							+ '</tr>' + "\n";
                		}

                        jQuery('#displayTaxes').html(html);

                        html = '';
						j = 1;
                        for( i = 0; i < json['taxClasses'].length; ++i ) {
                            html += '<tr class="row' + ( ++j % 2 ) + '">' + "\n"
                            	+ '<td class="left" style="font-weight: bold;">'
                            		+ '<span id="class_' + i + '_display">' + json['taxClasses'][i]['title'] + '<\/span>'
                            	+ '<\/td>' + "\n"
                            	+ '<td class="left">'
                            		+ '<span id="class_' + i + '_desc">' + json['taxClasses'][i]['description'] + '<\/span>'
                            	+ '<\/td>' + "\n"
                            	+ '<td class="right">'
                            		+ '<div id="class_' + i + '_state"></div>'
                            	+ '<\/td>' + "\n"
                            + '<\/tr>' + "\n";
                		}

                        jQuery('#displayTaxClasses').html(html);

                        html = '';
						j = 1;
                        for( var i in json['geoZones'] ) {
                            html += '<tr class="row' + ( ++j % 2 ) + '">' + "\n"
                                + '<td class="left" style="font-weight: bold;">'
                                    + '<span id="zone_' + i + '_display">'
                                        + json['geoZones'][i]['name']
                                    + '<\/span>'
                                + '<\/td>'
                                + '<td class="right">'
                                    + '<div id="zone_' + i + '_state"><\/div>'
                                + '<\/td>'
                            + '<\/tr>' + "\n";
                        }

                        jQuery('#geoZones').html(html);

						html = '';
						j = 1;
						ii = 0;
    					for( i = 0; i < json['texts'].length; ++i ) {
    						var text = json['texts'][i]['information_description']['<?php echo $langCode; ?>'];
                            html += '<tr class="row' + ( ++j % 2 ) + '">' + "\n"
                            	+ '<td class="left">'
                            		+ '<span id="text_' + ii + '_display">' + text['title'] + '</span>'
									+ '<div id="text_' + ii + '_content" style="display: none;">' + text['description'] + '</div>'
									+ '&nbsp;<img onclick="showText(\'' + ii + '\');" src="view/image/view.png" width="16" height="16" style="cursor: pointer;" alt="" />'
                                + '</td>'
                                + '<td class="right">'
                                    + '<div id="text_' + ii + '_state"></div>'
                                + '</td>'
                            + '</tr>' + "\n";

                            ++ii;
                		}

						jQuery('#texts').html(html);

                        for( i = 0; i < json['isInstalledTax'].length; i++ ) {
                            setState(json['isInstalledTax'][i],i,'tax');
                        }

                        for( i = 0; i < json['isInstalledTaxClass'].length; i++ ) {
                            setState(json['isInstalledTaxClass'][i],i,'class');
                        }

                        for( var i in json['isInstalledGeoZone'] ) {
                            setState(json['isInstalledGeoZone'][i],i,'zone');
                        }

                        for( i = 0; i < json['isInstalledText'].length; i++ ) {
                            setState(json['isInstalledText'][i],i,'text');
                        }

                        jQuery('#updateTaxId').replaceWith('<span id="updateTaxId">' + json['updateTaxId'] + '<\/span>');
        			}else{
                        jQuery('.success, .warning, .attention, .error').remove();
                        jQuery('#tax').before('<div class="attention">' + json['error'] + '<\/div>');
                        jQuery('.attention').delay(4000).fadeOut(1000);
                        hideShow(0);
                        hideShowTab(json['tabTaxes']);
        			}
        		}
        	});
        });

        function installItem(fieldId,type) {
            var countryId = jQuery('select[name=\'shopCountry\']').val();

            jQuery.ajax({
        		url: 'index.php?route=module/legal/installItem',
                data: { token: '<?php echo $token; ?>', country_id: countryId, fieldId: fieldId, type: type },
        		dataType: 'json',
        		beforeSend: function() {
					jQuery('#' + type + '_' + fieldId + '_state').html('<span class="wait">&nbsp;<img src="view/image/loading.gif" alt="" /></span>');
				},
				complete: function() {
					jQuery('.wait').remove();
				},
        		success: function( json ) {
        			if( json != '' && !json['error'] ) {
                        showMessage(json['success'],'success',type);
                        setState(true,fieldId,type);
        			}else{
                        showMessage(json['error'],'attention',type);
        			}
        		}
        	});
        };

        function update(type) {
            var taxId = jQuery('select[name=\'updateTaxId\']').val();
            var countryId = jQuery('select[name=\'shopCountry\']').val();

            jQuery.ajax({
        		url: 'index.php?route=module/legal/update',
                data: { token: '<?php echo $token; ?>', country_id: countryId, tax_id: taxId, type: type },
        		dataType: 'json',
        		success: function( json ) {
        			if( json != '' && !json['error'] ) {
                        showMessage(json['success'],'success','tabTaxes');

                        // setState(true,type);
        			}else{
                        showMessage(json['error'],'','tabTaxes');
        			}
        		}
        	});
        };

        function changePlgState(plg,val) {
            jQuery.ajax({
        		url: 'index.php?route=module/legal/changeStateVqmodPlugin',
                data: { token: '<?php echo $token; ?>', plugin: plg, state: val },
        		dataType: 'json',
        		success: function( json ) {
        			if( json != '' && !json['error'] ) {
                        var title = jQuery('#plgTitle_' + plg).html();

                        html = '<div style="float: left; margin-left: 20px; width: 30%;" id="plugin_' + plg + '">'
                        + '<strong id="plgTitle_' + plg + '">' + title + '<\/strong>'
                        + '<br \/>'
                        + '<div style="float: left;">'
                       		+ '<div style="float:left;">'
                         		+ '<label' + ( json['state'] ? ' style="color: #12772C;"' : '' ) + '>'
                           			+ '<input type="radio" value="1" onchange="changePlgState(\'' + plg + '\',this.value);" ' + ( json['state'] ? ' checked="checked"' : '' ) + ' />'
                              		+ '&nbsp;<span><?php echo $text_yes; ?></span>'
                                + '</label>'
							+ '</div>'
							+ '<div style="float:left; margin-left: 15px;">'
								+ '<label' + ( !json['state'] ? ' style="color: #FF0000; font-weight: bold;"' : '' ) + '>'
									+ '<input type="radio" value="0" onchange="changePlgState(\'' + plg + '\',this.value);" ' + ( !json['state'] ? ' checked="checked"' : '' ) + ' />'
									+ '&nbsp;<span><?php echo $text_no; ?></span>'
								+ '</label>'
							+ '</div>'
							+ '<div style="clear: both;"></div>'
						+ '</div>' + "\n";

                        showMessage(json['success'],'success','plugin_' + plg);
                        jQuery('#plugin_' + plg).replaceWith(html);
        			}else{
                        showMessage(json['error'],'','plugin_' + plg);
        			}
        		}
        	});
        }

        hideShow('<?php echo $countryIsSupported; ?>');
        hideShowTab('<?php echo $tabTaxes; ?>');

        <?php
        $i = 0;
        $installedTax = false;
        foreach( $isInstalledTax as $installed ) {
            if( $installed ) {
                $installedTax = true;
            } ?>
            setState('<?php echo $installed; ?>','<?php echo $i; ?>','tax');
            <?php
            ++$i;
        }

        $i = 0;
        foreach( $isInstalledTaxClass as $installed ) { ?>
            setState('<?php echo $installed; ?>','<?php echo $i; ?>','class');
            <?php
            ++$i;
        }

        foreach( $isInstalledGeoZone as $k => $v ) { ?>
            setState('<?php echo $v; ?>','<?php echo $k; ?>','zone');
            <?php
        }

        $i = 0;
        foreach( $isInstalledText as $installed ) { ?>
            setState('<?php echo $installed; ?>','<?php echo $i; ?>','text');
            <?php
            ++$i;
        } ?>

        function saveContent(field) {
            if( undefined == field ) {
                var content = edProduct.getValue();
                var tplFile = '<?php echo $tpl; ?>';
                var msg = 'tplProduct';
            }else{
                var content = edCss.getValue();
                var tplFile = '<?php echo $css; ?>';
                var msg = 'tplCss';
            }

            jQuery.ajax({
        		url: 'index.php?route=module/legal/saveContent&token=<?php echo $token; ?>',
                data: { token: '<?php echo $token; ?>', content: content, tplFile: tplFile },
        		dataType: 'json',
                type: 'POST',
        		success: function( json ) {
        			if( json != '' && !json['error'] ) {
                        showMessage(json['success'],'success',msg);
        			}else{
                        showMessage(json['error'],'error',msg);
        			}
        		}
        	});
        };

        function checkAttributes() {
            jQuery('.success, .warning, .attention, .error').remove();

            var html = '<div id="loading"><img src="view/image/loading.gif" alt="" /></div>';
            jQuery('#attributes').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_checking_attributes; ?></div>');
            jQuery('#products').hide();

            jQuery.ajax({
        		url: 'index.php?route=module/legal/checkAttributes',
                data: { token: '<?php echo $token; ?>' },
        		dataType: 'json',
        		success: function( json ) {
                    jQuery('.success, .warning, .attention, .error').remove();
        			if( json != '' && !json['error'] ) {
                        var j = 1;
                        var html = '<div id="products">' + "\n"
                        + '<div class="content">'
                              + '<table class="list">'
                                + '<thead>'
                                  + '<tr>'
                                    + '<td class="right" style="width: 30px;">#</td>'
                                    + '<td class="right" style="width: 50px;"><?php echo $text_id; ?></td>'
                                    + '<td class="left" style="width: 80%;"><?php echo $text_title; ?></td>'
                                    + '<td style="text-align: center;"><?php echo $text_attributes; ?></td>'
                                    + '<td style="text-align: center;"><?php echo $text_metatag; ?></td>'
                                  + '</tr>'
                                + '</thead>'
                                + '<tbody>' + "\n";

                        for( i = 0; i < json['products'].length; i++ ) {
                            html += '<tr class="row' + ( j % 2 ) + '">'
                            + '<td class="right">' + j + '<\/td>'
                            + '<td class="right">' + json['products'][i]['product_id'] + '<\/td>'
                            + '<td class="left"><a href="index.php?route=catalog/product/update&token=<?php echo $token; ?>&amp;product_id=' + json['products'][i]['product_id'] + '" target="_blank">' + json['products'][i]['name'] + '</a><\/td>'
                            + '<td style="text-align: center;">' + json['products'][i]['attributes'] + '<\/td>'
                            + '<td style="text-align: center;">' + json['products'][i]['meta'] + '<\/td>'
                            + '<\/tr>' + "\n";

                            ++j;
                		}

                        html += '<\/tbody><\/table><\/div>' + "\n";

                        showMessage(json['success'],'success','attributes',false);
                        jQuery('#products').replaceWith(html);
        			}else{
                        showMessage(json['error'],'','attributes');
        			}
        		}
        	});
        };

        function getEditContent(val) {
            jQuery('#dialog').remove();

        	jQuery('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="<?php echo $edit; ?>&file=' + val + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto" id="frame1"><\/iframe><\/div>');

        	jQuery('#dialog').dialog({
        		bgiframe  : false,
        		width     : 880,
        		height    : 720,
        		resizable : true,
        		modal     : true,
                buttons: {
                    '<?php echo $button_save; ?>': function() {
                        var content = jQuery('#frame1').contents().find('#content').val();

                        jQuery.ajax({
                    		url: '<?php echo $save; ?>',
                    		type: 'post',
                    		data: 'file=' + val + '&content=' + content,
                    		dataType: 'json',
                            beforeSend: function() {
                    			jQuery('#wait_' + val).after('<span class="wait">&nbsp;<img src="view/image/loading.gif" alt="" \/>Loading content ...<\/span>');
                    		},
                            success: function(json) {
                                jQuery('.success, .warning, .attention, .error').remove();

                                if( json['error'] ) {
                                    alert(json['error']);
                                    jQuery(this).dialog('close');
                                }

                                if( json['success'] ) {
                                    alert(json['success']);

                                    jQuery(this).dialog('close');
                                    location = '<?php echo $redirect; ?>';
                                }
                            }
                        });
    				},
                    '<?php echo $button_cancel; ?>': function() {
    					jQuery(this).dialog('close');
    				}
    			}
        	});
        };
        <?php eval( $support['js'] ); ?>
        <?php
        if( !$log ) { ?>
            jQuery('.tablog').css({'color':'#ACACAC'});
            <?php
        }

        if( $installedTax ) { ?>
            jQuery('#installNote').hide();
            jQuery('.tablocales').css({'color':'#27B27F'});
            <?php
        }else{ ?>
            jQuery('#installNote').show();
            jQuery('.tablocales').css({'color':'#CA3A3A','background-color':'#FFECEC'});
            <?php
        }?>
        /* ]]> */
    </script>
    <?php
} ?>
<?php echo $footer; ?>