#!/bin/bash

function remove_dir () {
    rm -rf "$1_"

    if [ -d "$1" ]
    then
        mv "$1" "$1_"
    fi
}

echo "*** Removing saved user, repositories, and orgsâ€¦"

remove_dir "${HOME}/Library/Application Support/GitHub for Mac"
remove_dir "${HOME}/Library/Application Support/com.github.GitHub"

echo "*** Removing preferencesâ€¦"

if [ -e "${HOME}/Library/Preferences/com.github.GitHub.plist" ]
then
    cp -f "${HOME}/Library/Preferences/com.github.GitHub.plist" "${HOME}/Library/Preferences/com.github.GitHub.plist_"
fi

defaults delete com.github.GitHub
defaults delete com.github.GitHub.LSSharedFileList

echo "*** Removing cachesâ€¦"

rm -rf "${HOME}/Library/Caches/GitHub for Mac" "${HOME}/Library/Caches/com.github.Github"

echo "*** Stopping and removing Conduitâ€¦"

launchctl remove com.github.GitHub.Conduit
rm -rf "${HOME}/Library/Containers/com.github.GitHub.Conduit"

echo "*** Removing SSH keyâ€¦"

find ${HOME}/.ssh -name "*github*_rsa" | while read KEY
do
    ssh-add -dK "$KEY.pub"
    mv -f "$KEY" "$KEY.bak"
    mv -f "$KEY.pub" "$KEY.pub.bak"
done

echo "*** Removing keychain itemsâ€¦"
security -q delete-internet-password -s github.com/mac
security -q delete-generic-password -l 'GitHub for Mac â€” github.com'
security -q delete-generic-password -l 'GitHub for Mac SSH key passphrase â€”Â github.com'

echo "*** Removing command line utilityâ€¦"

if [ -e "/usr/local/bin/github" ]
then
    sudo rm -f /usr/local/bin/github
fi

if [ -e "/Library/LaunchDaemons/com.github.GitHub.GHInstallCLI.plist" ]
then
    sudo rm -f /Library/LaunchDaemons/com.github.GitHub.GHInstallCLI.plist
fi

echo "*** Removing git symlinksâ€¦"

find /usr/local -lname '*GitHub.app*' -exec sudo rm -f {} \;