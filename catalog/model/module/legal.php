<?php
/**
 * @version		$Id: legal.php 3510 2013-12-07 16:41:02Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2012 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial http://osworx.net
 */

class ModelModuleLegal extends Model
{
    private $_version   = '3.0.3';
    private $_isExisting = null;

    /**
     * check if defined tables are existing
     * can be called intern and extern
     * @since 3.0.3 prefix check for mijoshop [how can someone use that shit!]
     * @return mixed
     */
    public function checkTables() {
        if( is_null( $this->_isExisting ) ) {
        	$dbprefix = DB_PREFIX;

        	if( class_exists( 'MijoShop' ) ) {
				$dbprefix = str_replace( '#__', MijoShop::get( 'base' )->getJConfig()->dbprefix, DB_PREFIX );
			}

            $tbls	= $this->getTables();
            $ret	= false;

            if( array_key_exists( $dbprefix . 'product_additional', $tbls ) ) {
            	$ret = true;
           	}

            $this->_isExisting = $ret;
            unset( $tbls );
        }

        return $this->_isExisting;
    }

    /**
	 * get all tables from the database
	 * @param bool		$flds	include field names
	 * @access public
	 * @return array
	 */
	public function getTables( $flds = false ) {
		$arr		= array();
		// show these tables not
		$notAllowed	= array(
			'extension', 'geo_zone', 'language', 'length_class',
			'length_class_description', 'review', 'setting',
			'url_alias', 'user', 'user_group', 'zone', 'zone_to_geo_zone'
		);

		$query	= 'SHOW TABLES FROM `' . DB_DATABASE . '`';
		$result = $this->db->query( $query );

		foreach( $result->rows as $key => $value ) {
			foreach( $value as $k => $v ) {
				if( !in_array( $v, $notAllowed ) ) {
					if( $flds ) {
						if( $arr[$v]['fields']	= self::getFields( $v ) ) {
						}
					}else{
						$arr[$v]['table'] = $v;
					}
				}
			}
		}

		return $arr;
	}

	/**
	 * get fields from defined table
	 * returns only fields from tables which has a PRIMARY defined
	 *
	 * @param string	$tbl	tablename to get fields from (full with PREFIX)
	 * @access public
	 * @return array/bool
	 */
	public function getFields( $tbl ) {
		$ret	= array();
		$key	= '';
		$query	= 'SHOW FIELDS FROM `' . DB_PREFIX . $tbl. '`';
		$result = $this->db->query( $query );

		foreach( $result->rows as $row ) {
			// check and get key (only the first if more are defined)
			foreach( $row as $k => $v ) {
				if( $k == 'Key' ) {
					if( $v == 'PRI' || $v == 'MUL' ) {
						if( strpos( $key, $row['Field'] ) === false ) {
							$key .= $row['Field'] . '|';
						}
					}
				}
			}
		}

		if( $key ) {
			$key = rtrim( $key, '|' );
			foreach( $result->rows as $row ) {
				$ret[$key][] = $row['Field'];
			}
		}else{
			$ret = false;
		}

		return $ret;
	}

    /**
     * using date-function is faster than xxsql NOW()
     * @return string
     */
    private function _getNow() {
         return date( 'Y-m-d H:i:s' );
    }

    /**
     * get a text by id
     * @param int   $id
     * @return mixed array/bool (false)
     */
    public function getInformationById( $id ) {
        $sql = '
        SELECT
            DISTINCT *
        FROM
            `' . DB_PREFIX . 'information` AS i
        LEFT JOIN
            `' . DB_PREFIX . 'information_description` AS id
                ON ( i.information_id = id.information_id )
        LEFT JOIN
            `' . DB_PREFIX . 'information_to_store` AS i2s
                ON ( i.information_id = i2s.information_id )
        WHERE
            i.information_id = \'' . (int) $id . '\'
        AND
            id.language_id = \'' . (int) $this->config->get( 'config_language_id' ) . '\'
        AND
            i2s.store_id = \'' . (int) $this->config->get( 'config_store_id' ) . '\'';

        $query = $this->db->query( $sql );

        if( $query->num_rows ) {
            return $query->row;
        }

        return false;
    }

    /**
     * get text description by title
     * @param string
     * @return string/bool (false)
     */
    public function getInformationByTitle( $val ) {
        $sql = '
        SELECT
            DISTINCT *
        FROM
            `' . DB_PREFIX . 'information` AS i
        LEFT JOIN
            `' . DB_PREFIX . 'information_description` AS id
                ON ( i.information_id = id.information_id )
        LEFT JOIN
            `' . DB_PREFIX . 'information_to_store` AS i2s
                ON (i.information_id = i2s.information_id)
        WHERE
            LCASE( id.title ) = \'' . mb_strtolower( $val ) . '\'
        AND
            id.language_id = \'' . (int) $this->config->get( 'config_language_id' ) . '\'
        AND
            i2s.store_id = \'' . (int) $this->config->get( 'config_store_id' ) . '\'';

        $query = $this->db->query( $sql );

        if( $query->num_rows ) {
            return $query->row['description'];
        }

        return false;
    }

    /**
     * get text for adding to emails
     * @param int   $information_id
     * @return array
     */
    public function getInformationForEmail( $information_id ) {
        $sql = '
        SELECT
            DISTINCT *
        FROM
            `' . DB_PREFIX . 'information` AS i
        LEFT JOIN
            `' . DB_PREFIX . 'information_description` AS id
                ON (i.information_id = id.information_id)
        LEFT JOIN
            `' . DB_PREFIX . 'information_to_store` AS i2s
                ON (i.information_id = i2s.information_id)
        WHERE
            i.information_id = \'' . (int) $information_id . '\'
        AND
            id.language_id = \'' . (int) $this->config->get( 'config_language_id' ) . '\'
        AND
            i2s.store_id = \'' . (int) $this->config->get( 'config_store_id' ) . '\'';

		$query = $this->db->query( $sql );

        if( $query->num_rows ) {
            return $this->replaceTextVars( $query->row );
        }else{
            return false;
        }
	}

    /**
     * replace possible placeholders in given text
     * @param string
     * @return string
     */
    public function replaceTextVars( $text ) {
        $find = array(
			'{owner}',
			'{address}',
			'{email}',
			'{telephone}',
			'{fax}',
            '{url}'
		);

		$replace = array(
			$this->config->get( 'config_owner' ),
			$this->config->get( 'config_address' ),
			$this->config->get( 'config_email' ),
			$this->config->get( 'config_telephone' ),
			$this->config->get( 'config_fax' ),
            HTTPS_SERVER
		);


		$text['description'] = str_replace( $find, $replace, $text['description'] );

        return $text;
    }

    /**
     * load a setting
     * this function is here, because oc < 152 does not have it!
     * @param string    $group
     * @param int       $store_id
     * @return array
     */
    public function getSetting( $group, $store_id = 0 ) {
		$ret = array();
		$sql  = '
        SELECT
            *
        FROM
            `' .DB_PREFIX. 'setting`
        WHERE
            store_id = \'' . (int) $store_id . '\'
        AND
            `group` = \'' . $this->db->escape( $group ) . '\'';

        $query = $this->db->query( $sql );

		foreach( $query->rows as $result ) {
			if( !$result['serialized'] ) {
				$ret[$result['key']] = $result['value'];
			}else{
				$ret[$result['key']] = unserialize( $result['value'] );
			}
		}

		return $ret;
	}

    /**
     * pre defined html body for email messages
     * @return string
     */
    public function getHtmlBody() {
        $ret = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #767676;">%s</body></html>';

        return $ret;
    }

    /**
     * get the value of meta_description for a single product
     * @param int   $id
     * @return mixed string/false
     */
    public function getMetaDescription( $id ) {
        $sql = '
        SELECT
            meta_description
        FROM
            `' .DB_PREFIX. 'product_description`
        WHERE
            product_id = \'' . (int) $id . '\'
        AND
            language_id = \'' . (int) $this->config->get( 'config_language_id' ) . '\'';

        $query = $this->db->query( $sql );

        if( $query->num_rows ) {
            return $query->row['meta_description'];
        }

        return false;
    }

    /**
     * get db values
     * function is here, because prior to OC 1.5.3.x
     * wether the model nor the field 'approval' is existing
     * @param int   $customer_group_id
     * @return array
     */
    public function getCustomerGroup( $customer_group_id ) {
        $sql = '
        SELECT
            DISTINCT *
        FROM
            `' .DB_PREFIX. 'customer_group`
        WHERE
            customer_group_id = \'' . (int) $customer_group_id . '\'';

		$query = $this->db->query( $sql );

		return $query->row;
	}

    /**
     * get additonal product infos
     * notes:
     *  - bool value to check if record exists for adding or updating
     *  - checks first if table exists
     * @since 3.0.0
     * @return array/bool (false)
     */
    public function getProductAdditionalData( $id ) {
        if( $this->checkTables() == true  ) {
            $sql = '
            SELECT
                *
            FROM
                `' .DB_PREFIX . 'product_additional`
            WHERE
                product_id = \'' . (int) $id . '\'';

            $result = $this->db->query( $sql );

            if( $result->num_rows ) {
    		  return $result->row;
            }
        }

        return false;
    }

    /**
     * get discount price
     * @param int   $pid    product_id
     * @param int   $cgid   customer_group_id
     * @since 3.0.1
     * @return mixed    float/false
     */
    public function getDiscount( $pid, $cgid ) {
        $sql = '
        SELECT
            price
        FROM
            `' . DB_PREFIX . 'product_discount`
        WHERE
            product_id = \'' . (int) $pid . '\'
        AND
            customer_group_id = \'' . (int) $cgid . '\'
        AND
            quantity > 0
        AND
            (
                (
                    date_start = \'0000-00-00\'
                    OR date_start < \'' . $this->_getNow() . '\'
                )
                AND
                (
                    date_end = \'0000-00-00\'
                    OR date_end > \'' . $this->_getNow() . '\'
                )
            )
        ORDER BY
            quantity DESC, priority ASC, price ASC
        LIMIT
            1';

        $result = $this->db->query( $sql );

		if( $result->num_rows ) {
            return $result->row['price'];
		}

        return false;
    }
}