<?php
/**
 * @version		$Id: confirm.tpl 2872 2012-08-01 19:58:39Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2012 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License - http://osworx.net
 */
 
if( !isset( $redirect ) ) { ?>
    <div class="payment"><?php echo $payment; ?></div>
    <?php 
}else{ ?>
    <script type="text/javascript">
        /* <![CDATA[ */
        location = '<?php echo $redirect; ?>';
        /* ]]> */
    </script> 
    <?php 
}