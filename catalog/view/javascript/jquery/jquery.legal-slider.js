/**
 * @version		$Id: jquery.legal-slider.js 2912 2012-09-07 14:23:38Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2012 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial License - http://osworx.net
 */

jQuery(document).ready(function(){
	jQuery('.legal_btn-slide').click(function(){
		jQuery('#legal_panel').slideToggle('slow');
		jQuery(this).toggleClass('legal_active');
        return false;
	});
});