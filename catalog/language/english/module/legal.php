<?php
/**
 * @version		$Id: legal.php 3753 2014-09-30 11:05:56Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2012 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial http://osworx.net
 */

$_['text_weight']			= 'Weight';
$_['text_height']			= 'Height';
$_['text_width']			= 'Width';
$_['text_length']			= 'Length';
$_['text_shipping_address']	= 'Shipping Address';
$_['text_payment_address']	= 'Billing Address';
$_['text_payment_method']	= 'Payment via';
$_['text_change']			= 'Modify';
$_['text_number']			= 'Amount';
$_['text_price_single']		= 'Price';
$_['text_total']			= 'Total';

// tax only
$_['text_taxOnly']          = 'Tax:';

// base price
$_['text_package_content']	= 'Packaging Unit';
$_['text_base_price']		= 'Baseprice for %s %s';