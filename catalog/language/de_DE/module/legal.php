<?php
/**
 * @version		$Id: legal.php 3683 2014-07-13 12:01:48Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2012 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial http://osworx.net
 */

$_['text_weight']			= 'Gewicht';
$_['text_height']			= 'Höhe';
$_['text_width']			= 'Breite';
$_['text_length']			= 'Länge';
$_['text_shipping_address']	= 'Lieferadresse';
$_['text_payment_address']	= 'Rechnungsadresse';
$_['text_payment_method']	= 'Gewählte Zahlungsart';
$_['text_change']			= 'Ändern';
$_['text_number']			= 'Anzahl';
$_['text_price_single']		= 'Einzelpreis';
$_['text_total']			= 'Preis';

// tax only
$_['text_taxOnly']			= 'Steuer';

// base price
$_['text_package_content']	= 'Verpackungsinhalt';
$_['text_base_price']		= 'Grundpreis für %s %s';