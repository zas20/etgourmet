<?php
/**
 * @version		$Id: legal.php 3812 2014-12-05 07:08:06Z mic $
 * @package		Legal
 * @author		mic - http://osworx.net
 * @copyright	2013 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial http://osworx.net
 */

class ControllerLegal
{
    private $_this      = null;
    private $_retVal    = array();
    private $_version   = '4.0.2';
    private $_ocVersion = false;

    private $_registry;
    private $_models    = null;
    private $_langVars	= array();

    /**
     * constructor
     * @param object    $registry
     */
    public function __construct( $registry ) {
        $this->_registry = $registry;

        $this->config   = $registry->get( 'config' );
        $this->document = $registry->get( 'document' );
        $this->language = $registry->get( 'language' );
        $this->load     = $registry->get( 'load' );
        $this->db       = $registry->get( 'db' );
        $this->request  = $registry->get( 'request' );
        $this->session  = $registry->get( 'session' );
        $this->customer = $registry->get( 'customer' );

        $this->cart     = $registry->get( 'cart' );
        $this->url      = $registry->get( 'url' );
        $this->currency = $registry->get( 'currency' );
        $this->tax      = $registry->get( 'tax' );
        $this->weight   = $registry->get( 'weight' );
        $this->length   = $registry->get( 'length' );

        $this->getOCVersion();
    }

    /**
     * register the current object
     * used for common functions if called directly like getSummary()
     */
    public function registerThis( $obj ) {
        $this->_this = & $obj;
    }

    /**
     * register all required models
     */
    private function registerModels() {
        if( is_null( $this->_models ) ) {
            $this->load->model( 'module/legal' );
            $this->load->model( 'account/address' );
            $this->load->model( 'catalog/product' );
            $this->load->model( 'tool/image' );
            $this->load->model( 'setting/extension' );

            $this->_models['legal']     = new ModelModuleLegal( $this->_registry );
            $this->_models['address']   = new ModelAccountAddress( $this->_registry );
            $this->_models['product']   = new ModelCatalogProduct( $this->_registry );
            $this->_models['image']     = new ModelToolImage( $this->_registry );
            $this->_models['extension'] = new ModelSettingExtension( $this->_registry );
        }
    }

    /**
     * load lang vars and assign
     * this because if that shit MijoShop
     */
    private function getLangVars() {
    	$vars = $this->load->language( 'module/legal' );
    	foreach( $vars as $k => $v ) {
			$this->_langVars[$k] = $this->language->get( $k );
		}

		unset( $vars );
    }

    /**
     * get all details for summary (button solution) and send back
     * @return array
     * */
    public function getSummary() {
        $this->registerModels();
        $this->getLangVars();

        $this->_retVal['text_summary'] = '
        <div class="attribs">' . "\n"
            // section intro
            . '<div class="intro round">' . "\n"
                . $this->getIntro()
                . $this->getAddresses()
                . $this->getPaymentMethod()
            . '</div>' . "\n"
            // section products and totals
            . '<div class="content round">' . "\n"
                . $this->getPriceHeader()
                . $this->getProducts()
                . $this->getVouchers()
                . $this->getTotals()
            . '</div>' . "\n"
        . '</div>' . "\n"
        . '<div style="clear:both;"></div>' . "\n"
        . $this->getJavascript();

        // finally: the button text
    	$tmp = $this->config->get( 'button_text' );

        $this->_retVal['button_confirm'] = $tmp[(int)$this->config->get( 'config_language_id' )];

        return $this->_retVal;
    }

    /**
     * build address header
     * @param array $vals
     * @return string
     */
    private function buildCustomerAddress( $vals ) {
        if( $vals['address_format'] ) {
			$format = $vals['address_format'];
		}else{
            $format = '{firstname} {lastname}' . "\n"
            . '{company}' . "\n"
            . '{address_1}' . "\n"
            . '{address_2}' . "\n"
            . '{city} {postcode}' . "\n"
            . '{zone}' . "\n"
            . '{country}';
		}

        $find = array(
			'{firstname}',
			'{lastname}',
			'{company}',
			'{address_1}',
			'{address_2}',
			'{city}',
			'{postcode}',
			'{zone}',
			'{zone_code}',
			'{country}'
		);

		$replace = array(
			'firstname' => $vals['firstname'],
			'lastname'  => $vals['lastname'],
			'company'   => $vals['company'],
			'address_1' => $vals['address_1'],
			'address_2' => $vals['address_2'],
			'city'      => $vals['city'],
			'postcode'  => $vals['postcode'],
			'zone'      => '',
			'zone_code' => '',
			'country'   => ''
		);

		$ret = str_replace(
            array( "\r\n", "\r", "\n" ),
            '<br />',
            preg_replace(
                array( "/\s\s+/", "/\r\r+/", "/\n\n+/" ),
                '<br />',
                trim( str_replace( $find, $replace, $format ) )
            )
        );

        return $ret;
    }

    /**
     * construct the display of normal and special price
     * @param array
     * @return string
     */
    public function getSpecial( $data ) {
        $vars   = $this->config->get( 'specials_text' );
        $lng    = $this->config->get( 'config_language_id' );
        $ret    = '';

        if(
            $data['price']
            && $data['special']
        )
        {
            if(
                !empty( $vars )
                && ( $vars[$lng] != '' )
            )
            {
                $ret = sprintf(
                    htmlspecialchars_decode( $vars[$lng] ),
                    $data['price'],
                    $data['special']
                );
            }
        }

        return $ret;
    }

    /**
     * get meta description for a single product
     * @param int   $id
     * @return mixed string/false
     */
    private function getMetaDescription( $id )  {
        if( $meta = $this->_models['legal']->getMetaDescription( $id ) ) {
            $meta = str_replace( array( "\r\n", "\n" ), '#', $meta );
            $meta = str_replace( ',', '#', trim( $meta ) );
            $meta = str_replace( array( '# #', '##'), '#', $meta );
            $meta = str_replace( '#', '<br />', $meta );
            $meta = html_entity_decode( trim( $meta, '<br />' ), ENT_QUOTES, 'UTF-8' );

            return $meta;
        }else{
            return false;
        }
    }

    /**
     * summary: construct the info header
     * @return string
     */
    private function getIntro() {
        if( $this->config->get( 'text_summary' ) ) {
            $tmp = $this->_models['legal']->getInformationById(
                $this->config->get( 'text_summary' )
            );

            return '
            <div class="text_summary">'
                . html_entity_decode( $tmp['description'], ENT_QUOTES, 'utf-8' )
            . '</div>' . "\n";
        }
    }

    /**
     * summary: get addresses and construct text
     * @return string
     */
    private function getAddresses() {
        // payment
        $vals = '';
        if( !empty( $this->session->data['guest']['payment'] ) ) {
            $vals = $this->session->data['guest']['payment'];
        }else{
            if(
                $this->customer->isLogged()
                && isset( $this->session->data['payment_address_id'] )
            )
            {
                $vals = $this->_models['address']->getAddress( $this->session->data['payment_address_id'] );
            }
        }

        $payment = $this->buildCustomerAddress( $vals );

        // shipping
        $vals = '';
        if( !empty( $this->session->data['guest']['shipping'] ) ) {
            $vals = $this->session->data['guest']['shipping'];
        }else{
            if(
                $this->customer->isLogged()
                && isset( $this->session->data['shipping_address_id'] )
            )
            {
				$vals = $this->_models['address']->getAddress( $this->session->data['shipping_address_id'] );
            }
        }

        if( $vals ) {
            $shipping = $this->buildCustomerAddress( $vals );
        }else{
            $shipping = $payment;
        }

        return '
        <div class="addresses">' . "\n"
            . '<div class="address">
                <strong>' . $this->_langVars['text_shipping_address'] . '</strong>
                (<a id="changeShippingAddress">' . $this->_langVars['text_change'] . '</a>)
                <br />'
                . $shipping
            . '</div>' . "\n"
            . '<div class="address">
                <strong>' . $this->_langVars['text_payment_address'] . '</strong>
                (<a id="changePaymentAddress">' . $this->_langVars['text_change'] . '</a>)
                <br />'
                . $payment
            . '</div>' . "\n"
        . '</div>' . "\n";
    }

    /**
     * summary: get chosenpayment method
     * @return string
     */
    private function getPaymentMethod() {
        return '
        <div class="payment_method">
            <strong>' . $this->_langVars['text_payment_method'] . '</strong>
            (<a id="changePaymentMethod">' . $this->_langVars['text_change'] . '</a>)
            <br />'
            . $this->session->data['payment_method']['title']
        . '</div>' . "\n";
    }

    /**
     * summary: construct the header for prices
     * @return string
     */
    private function getPriceHeader() {
        return '
        <div class="priceHeader">
            <div class="priceItem">' . $this->_langVars['text_price_single'] . '</div>
            <div class="priceItem">' . $this->_langVars['text_number'] . '</div>
            <div class="priceItem">' . $this->_langVars['text_total'] . '</div>
        </div>' . "\n";
    }

    /**
     * summary: get all products and relevant infos to them
     * @return string
     */
    private function getProducts() {
        $products   = $this->cart->getProducts();
        $texts      = '';
        $j          = 0;
        $ret        = '';

        foreach( $products as $product ) {
            $summary = false;

            // get categories to fetch possible text
            $categories = $this->_models['product']->getCategories( $product['product_id'] );

            if( $categories ) {
                foreach( $categories as $cat ) {
                    if( $text = $this->_models['legal']->getInformationByTitle( 'legal_cat_' . $cat['category_id'] ) ) {
                        $texts .= $text . '<br />';
                    }
                }
            }

            $ret .= '
            <div class="summary' . ( ( $j % 2 ) == 0 ? ' odd' : '' ) . '" title="' . $product['name'] . '">' . "\n";

                if( $product['image'] ) {
                    $ret .= '
                    <div class="image">' . "\n"
                        . '<a href="'
                            . $this->url->link(
                                'product/product',
                                'product_id=' . $product['product_id']
                            )
                            . '">'
                            . '<img src="'
                            . $this->_models['image']->resize(
                                $product['image'],
                                $this->config->get( 'image_width' ),
                                $this->config->get( 'image_height' )
                            )
                            . '" width="'
                            . $this->config->get( 'image_width' )
                            . '" height="'
                            . $this->config->get( 'image_height' )
                            . '" alt="'
                            . $product['name']
                            . '" title="'
                            . $product['name']
                            . '">'
                        . '</a>'
                    . '</div>' . "\n";
                }

                $ret .= '
                <div class="text">' . "\n"
                    . '<div class="title">'
                        . $product['name']
                    . '</div>' . "\n"
                    . $this->getDescriptionText( $product['product_id'] )

                    // options
                    . '<div class="options">' . "\n"
                        .
                        (
                            $product['weight'] != '0.00'
                            ? '<div class="item">'
                                . '<span class="title">'
                                    . $this->_langVars['text_weight']
                                . '</span>'
                                . '<span class="value">'
                                    . $this->weight->format(
                                        $product['weight'],
                                        $product['weight_class_id']
                                    )
                                . '</span>'
                                . '</div>' . "\n"
                            : ''
                        )
                        . (
                            $product['length'] != '0.00'
                            ? '<div class="item">'
                                . '<span class="title">'
                                    . $this->_langVars['text_length']
                                . '</span>'
                                . '<span class="value">'
                                    . $this->length->format(
                                        $product['length'],
                                        $product['length_class_id']
                                    )
                                . '</span>'
                                . '</div>' . "\n"
                            : ''
                        )
                        . (
                            $product['width'] != '0.00'
                            ? '<div class="item">'
                                . '<span class="title">'
                                    . $this->_langVars['text_width']
                                . '</span>'
                                . '<span class="value">'
                                    . $this->length->format(
                                        $product['width'],
                                        $product['length_class_id']
                                    )
                                . '</span>'
                                . '</div>' . "\n"
                            : ''
                        )
                        . (
                            $product['height'] != '0.00'
                            ? '<div class="item">'
                                . '<span class="title">'
                                    . $this->_langVars['text_height']
                                . '</span>'
                                . '<span class="value">'
                                    . $this->length->format(
                                        $product['height'],
                                        $product['length_class_id']
                                    )
                                . '</span>'
                                . '</div>' . "\n"
                            : ''
                        );

                        if( $product['option'] ) {
                            foreach( $product['option'] as $option ) {
                                $ret .= '<div class="item">'
                                    . '<span class="title">'
                                        . $option['name']
                                    . '</span>'
                                    . '<span class="value">'
                                        . $option['option_value']
                                    . '</span>'
                                . '</div>' . "\n";
                            }
                        }

                    $ret .= '</div>' . "\n";

                    // attributes
                    $attribute_groups = $this->_models['product']->getProductAttributes(
                        $product['product_id']
                    );

                    if( $attribute_groups ) {
                        $summary = true;

                        $ret .= '
                        <div class="attrib">' . "\n";

                            foreach( $attribute_groups as $attribute_group ) {
                                $ret .= '<br /><span class="title">'
                                    . $attribute_group['name']
                                . '</span>' . "\n";

                                foreach( $attribute_group['attribute'] as $attrib ) {
                                    $ret .= '<br />'
                                    . $attrib['name'] .' '. $attrib['text'] . "\n";
                                }
                            }

                        $ret .= '</div>' . "\n";
                    }

                    if(
                        !$summary
                        && $texts
                    )
                    {
                        $summary = true;
                        $ret .= '
                        <div class="text lazy">'
                            . '<br />'
                            . html_entity_decode( trim( $texts, '<br />' ), ENT_QUOTES, 'UTF-8' )
                        . '</div>' . "\n";
                    }

                    // final change, try to get meta description
                    if(
                        !$summary
                        && $meta = $this->getMetaDescription( $product['product_id'] )
                    )
                    {
                        $ret .= '
                        <div class="attrib">'
                            . '<br />'
                            . $meta
                        . '</div>' . "\n";
                    }

                $ret .= '
                </div>' . "\n"
                // price infos
                . '<div class="price">'
                    . '<div class="priceItem">'
                        . $this->currency->format(
                            $this->tax->calculate(
                                $product['price'],
                                $product['tax_class_id'],
                                $this->config->get( 'config_tax' )
                            )
                        )
                    . '</div>'
                    . '<div class="priceItem">' . $product['quantity'] . '</div>'
                    . '<div class="priceItem">'
                        . $this->currency->format(
                            $this->tax->calculate(
                                $product['total'],
                                $product['tax_class_id'],
                                $this->config->get( 'config_tax' )
                            )
                        )
                    . '</div>'
                    . '<div style="clear: both;"></div>' . "\n"
                . '</div>' . "\n"
            . '</div>' . "\n";

            ++$j;
        }

        return $ret;
    }

    /**
     * get product description text
     * @param int	$id		product_id
     * @return string
     */
    private function getDescriptionText( $id ) {
    	$ret = '';
    	$del = ' ';

    	if( $this->config->get( 'description_len' ) ) {
    		$chars	= (int) $this->config->get( 'description_len' );
    		$result = $this->_models['product']->getProduct( $id );

    		if( $result['description'] ) {
    			$ret = html_entity_decode( $result['description'], ENT_NOQUOTES, 'UTF-8' );
	            $ret = strip_tags( $ret );
				$ret = trim( $ret );
				$ret = rtrim( $ret, ',' );

				if( mb_strlen( $ret ) > $chars ) {
					// limit the text
					$ret	= mb_substr( $ret, 0, $chars );
					// find last occurence of delimiter
					$pos	= strrpos( $ret, $del );
					// cut
					$ret	= mb_substr( $ret, 0, ( $pos ? $pos : 80 ) );
				}

				$ret = '<div class="description">' . $ret . '</div>';
			}
    	}

   		return $ret;
    }

    /**
     * summary: get possible voucher
     * @return string
     */
    private function getVouchers() {
        $voucher_data   = array();
        $ret            = '';
        $j				= 1;

        if( !empty( $this->session->data['vouchers'] ) ) {
			foreach( $this->session->data['vouchers'] as $voucher ) {
				$voucher_data[] = array(
					'description'  => $voucher['description'],
					'amount'       => $this->currency->format( $voucher['amount'], '', '', true, false )
				);
			}
		}

        foreach( $voucher_data as $voucher ) {
            $ret .= '
            <div class="summary' . ( ( $j % 2 ) == 0 ? ' odd' : '' ) . '" title="' . $voucher['description'] . '">' . "\n"
                . '<div class="text">' . "\n"
                    . '<div class="title">' . $voucher['description'] . '</div>'
                . '</div>' . "\n"
                // price infos
                . '<div class="price">' . "\n"
                    . '<div class="priceItem">' . $voucher['amount'] . '</div>'
                    . '<div class="priceItem">' . '1' . '</div>'
                    . '<div class="priceItem">' . $voucher['amount'] . '</div>'
                    . '<div style="clear: both;"></div>' . "\n"
                . '</div>' . "\n"
            . '</div>' . "\n";

            ++$j;
        }

        return $ret;
    }

    /**
     * summary: get all total figures
     * @return string
     */
    private function getTotals() {
        $total_data = array();
        $total      = 0;
        $taxes      = $this->cart->getTaxes();
        $sort_order = array();
        $results    = $this->_models['extension']->getExtensions( 'total' );

		foreach( $results as $key => $value ) {
			$sort_order[$key] = $this->config->get( $value['code'] . '_sort_order' );
		}

		array_multisort( $sort_order, SORT_ASC, $results );

		foreach( $results as $result ) {
			if( $this->config->get( $result['code'] . '_status' ) ) {
				$this->load->model( 'total/' . $result['code'] );

                $model = 'Total' . ucfirst( $result['code'] );
                $class = 'Model' . preg_replace('/[^a-zA-Z0-9]/', '', $model);

                $this->_models[$class] = new $class( $this->_registry );
                $this->_models[$class]->getTotal( $total_data, $total, $taxes );
			}
		}

		$sort_order = array();

		foreach( $total_data as $key => $value ) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort( $sort_order, SORT_ASC, $total_data );

		/** CustomShipping start */
		if( !empty( $this->session->data['customShipping'] ) ) {
			foreach( $total_data as $key => $val ) {
				if( $val['code'] == 'shipping' ) {
					foreach( $this->session->data['customShipping'] as $k => $v ) {
						$total_data[$key]['title'] = str_replace( $v, '', $total_data[$key]['title'] );
					}
				}
			}
		}
		/** CustomShipping end */

        $ret = '
        <div style="clear: both;"></div>' . "\n"
        . '<div class="spacer"></div>' . "\n"
        . '<div class="totals">' . "\n";

        foreach( $total_data as $total ) {
        	$design = '%s';
        	if( strpos( $total['code'], 'total' ) !== false ) {
        		$design = '<span style="font-weight: bold;">%s</span>';
        	}

            $ret .= '
            <div class="totalTitle">' . sprintf( $design, $total['title'] ) . '</div>'
            . '<div class="totalText">'
				. sprintf( $design, rtrim( $total['text'], htmlspecialchars_decode( $this->config->get( 'hint' ) ) ) )
			. '</div>';
        }

        $ret .= '<div style="clear: both;"></div>' . "\n"
        . '</div>' . "\n";

        return $ret;
    }

    /**
     * general base price function
     * @param array 	$product_info
     * @param bool		$isOption		indicates if price is option price
     * @since 3.0.0
     * @see calculateBasePrice()
     */
    public function getBasePrice( $product_info, $isOption = false ) {
        $this->registerModels();
        $this->getLangVars();

        $ret = array();

        if( !empty( $this->request->get['product_id'] ) ) {
            $pid = $this->request->get['product_id'];
        }else{
            $pid = $product_info['product_id'];
        }

        if( $productAddInfo = $this->_models['legal']->getProductAdditionalData( $pid ) ) {
            if( $productAddInfo['base_price_factor'] ) {
                // get correct price
                if( $product_info['special'] ) {
                    // if special is set, it should be okay
                    $price = $product_info['special'];
                }else{
                    if( $result = $this->_models['legal']->getDiscount( $pid, $this->getCustomerId() ) ) {
                        // get disount price if set
                        $price = $result;
                    }else{
                        // or at least take normal price
                        $price = $product_info['price'];
                    }
                }

                // add to array
                $productAddInfo['price']        = $price;
                $productAddInfo['tax_class_id'] = $product_info['tax_class_id'];

                $textContent = $this->_langVars['text_package_content'];

				if( $isOption ) {
					// override values
					$textContent							= $product_info['name'];
					$productAddInfo['package_content']		= $product_info['name'];
					$productAddInfo['package_content_unit'] = '';
				}

                $textPrice	= $this->_langVars['text_base_price'];
                $text		= $this->config->get( 'base_price_content_text' );

                if( $txt = $text[$this->config->get( 'config_language_id' )] ) {
                    $textContent = $txt;
                }

                $text = $this->config->get( 'base_price_price_text' );

                if( $txt = $text[$this->config->get( 'config_language_id' )] ) {
                    $textPrice = $txt;
                }

                $base_price			= $this->calculateBasePrice( $productAddInfo );
                $text_base_price	= sprintf(
                    $textPrice,
                    $productAddInfo['base_price_quantity'],
                    $productAddInfo['base_price_unit']
                );

                $option_price = ' <span style="color:#6F6F6F;">- '
					. $text_base_price
					. ' '
					. $base_price
				. '</span>';

                $ret = array(
					'text_package_content'	=> $textContent,
                	'text_base_price'		=> $text_base_price,
                	'base_price'			=> $base_price,
    				'package_content'		=> $productAddInfo['package_content'] . $productAddInfo['package_content_unit'],
    				'option_price'			=> $option_price
 				);
            }
        }

        return $ret;
    }

    /**
	 * calculates the base price
	 * @param array		product values
	 * @return string
     * @since 3.0.0
     * @see getBasePrice()
	 */
	private function calculateBasePrice( $arr ) {
		$ret = '';

		if(
            $arr['package_content']
            && $arr['base_price_factor']
        )
        {
			$tmp = ( $arr['price'] / $arr['package_content'] ) * $arr['base_price_factor'];
			$ret = $this->_registry->get( 'currency' )->format(
				$this->_registry->get( 'tax' )->calculate(
					$tmp,
					$arr['tax_class_id'],
					$this->config->get( 'config_tax' )
				)
			);
		}

		return $ret;
	}

    /**
     * construct the price info
     * @param string    $data
     * @return array
     */
    public function getPriceInfo( $powered ) {
        $ret = array();

        if( $powered == '' ) {
            return $ret;
        }

        $text = '';
        $ret['legal_bar'] = '';

        $this->registerModels();

        if( $vars = $this->_models['legal']->getSetting( 'legal' ) ) {
            $hint = $vars['hint'];

            if( !empty( $vars['price_text'][$this->config->get( 'config_language_id' )] ) ) {
                $text .= $hint . ' ';

                if( $this->config->get( 'config_tax' ) === '1' ) {
                    $text .= $vars['price_text'][$this->config->get( 'config_language_id' )]['wtax'];
                }else{
                    $text .= $vars['price_text'][$this->config->get( 'config_language_id' )]['wotax'];
                }
            }

            $text = htmlspecialchars_decode( $text );
        }

        $text = '<span class="legal_priceInfo">' . $text . '</span>';

        $value = $powered;

        if( $pos = $this->config->get( 'position_hint' ) ) {
            switch( $pos )
            {
                case 'left':
                    $value = '<div style="float: left; width: 50%; text-align: left;">'
                        . $text
                    . '</div>'
                    . '<div style="float: right; width: 50%;">'
                        . $powered
                    . '</div>' . "\n";
                    break;

                case 'rightBefore':
                    $value = $text .'&nbsp;' . $powered;
                    break;

                case 'rightBottom':
                    $value = $powered . '<br />' . $text;
                    break;

                case 'replace':
                    $value = $text . '<br />'
                    . '&copy; ' . $this->config->get( 'config_name' )
                    . '&nbsp;' . date( 'Y' );
                    break;

                case 'bar-bottom':
                case 'bar-top':
                    $this->document->addStyle( 'catalog/view/theme/default/stylesheet/legal.css' );

                    $ret['legal_bar'] = '<div class="legal_wrapper legal_wrapper_' . ( $pos == 'bar-bottom' ? 'bottom' : 'top' ) . '">'
    . '<div class="legal_bar legal_bar_' . ( $pos == 'bar-bottom' ? 'bottom' : 'top' ) . '">' . $text . '</div>'
. '</div>' . "\n"
. '<script type="text/javascript">'
    . '/* <![CDATA[ */'
    . 'jQuery(\'#container\').css({\'margin-' . ( $pos == 'bar-bottom' ? 'bottom' : 'top' ) . '\': \'' . ( $pos == 'bar-bottom' ? '40' : '20' ) . 'px\'});'
    . '/* ]]]]><![CDATA[> */'
. '</script>';
                    break;

                case 'slider-top':
                case 'slider-bottom':
                    $this->document->addStyle( 'catalog/view/theme/default/stylesheet/legal.css' );
                    $this->document->addScript( 'catalog/view/javascript/jquery/jquery.legal-slider.js' );

                    $tmp    = $this->config->get( 'slider_text' );
                    $title  = $tmp[(int)$this->config->get( 'config_language_id' )];

                    $ret['legal_bar'] = '<div id="legal_panel"' . ( $pos == 'slider-bottom' ? ' class="legal_wrapper_bottom"' : ' class="legal_wrapper_top"' ) . '>'
    	. $text
    . '</div>' . "\n"
    . '<p class="legal_slide' . ( $pos == 'slider-bottom' ? ' legal_wrapper_bottom' : ' legal_wrapper_top' ) . '"><a href="#" class="legal_btn-slide">' . $hint .' '. $title . '</a></p>';
                    break;

                case 'rightTop':
                    $value = $text . '<br />'
                    . $powered;
                    break;

                default:
                    $value = '';
            }
        }

        $ret['powered'] = $value;

        return $ret;
    }

    /**
     * construct the javascript code for poup window
     * used for dusplaying terms in window
     * @return string
     */
    public function getPopupBlock() {
        $ret = '';

        if(
            $this->config->get( 'popup_width' )
            && $this->config->get( 'popup_height' )
        )
        {
            $width    = $this->config->get( 'popup_width' );
            $height   = $this->config->get( 'popup_height' );
            $lib      = 'colorbox';

            if( version_compare( VERSION, '1.5.2', '<' ) ) {
                $lib = 'fancybox';
            }

            $ret = '<script type="text/javascript">
    /* <![CDATA[ */
    jQuery(\'.popTerms\').' . $lib . '({
    	width  : ' . $width . ',
    	height : ' . $height . '
    });
    /* ]]> */
</script>';
        }

        return $ret;
    }

    /**
     * get defined text addon for email
     * @param int	$order_status	optional - set if email is for order change
     * @return string
     * @since 4.0.0
     */
    public function getTextToEmail( $order_status = '' ) {
    	$this->load->model( 'module/legal' );
    	$this->_models['legal'] = new ModelModuleLegal( $this->_registry );
    	$ret = '';

		// 1. get generic text
		if( $textId = $this->config->get( 'text_to_email' ) ) {
			if( $text = $this->_models['legal']->getInformationForEmail( $textId ) ) {
				$ret .= '<br /><br />'
	            . '<b>' . html_entity_decode( $text['title'], ENT_QUOTES, 'UTF-8' ) . '</b>'
	            .'<br /><br />'
				. html_entity_decode( $text['description'], ENT_QUOTES, 'UTF-8' );
			}
        }

		// 2. get text if order state is set and add to generic
		if( $order_status ) {
			$text = $this->config->get( 'textToEmails' );
			if( !empty( $text[$order_status] ) ) {
				if( $textId = $text[$order_status]['text_id'] ) {
					if( $text = $this->_models['legal']->getInformationForEmail( $textId ) ) {
						$ret .= '<br /><br />'
			            . '<b>' . html_entity_decode( $text['title'], ENT_QUOTES, 'UTF-8' ) . '</b>'
			            .'<br /><br />'
						. html_entity_decode( $text['description'], ENT_QUOTES, 'UTF-8' );
					}
				}
			}
		}

		return $ret;
    }

    /**
     * get body for html messages
     * @return string
     */
    public function getHtmlBody() {
    	$ret = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #767676;">%s</body></html>';

    	return $ret;
    }

    /** #### helper functions #### **/

    /**
     * get customer id
     * either real id if logged in or predefined
     * @since 3.0.1
     * @return int
     */
    private function getCustomerId() {
        if( $this->customer->isLogged() ) {
			return $this->customer->getCustomerGroupId();
		}else{
			return $this->config->get( 'config_customer_group_id' );
		}
    }

    /**
     * construct required js.functions for smoothscroll actions
     * @return string
     */
    private function getJavascript() {
        $isThemeShoppica = false;

        // define js.library because of that stupid core changes!!
        $lib = 'colorbox';
        if( $this->_ocVersion < '1.5.2' ) {
            $lib = 'fancybox';
        }

        // check for shoppica theme if used
        if( $this->config->get( 'shoppica2' ) ) {
            $isThemeShoppica = true;
        }

        $ret = '<script type="text/javascript">
        /* <![CDATA[ */
        jQuery(\'#changeShippingAddress\').click(function() {
            legalSmoothScroll(\'#shipping-address\');
            return false;
        });

        jQuery(\'#changePaymentAddress\').click(function() {
            legalSmoothScroll(\'payment-address\');
            return false;
        });

        jQuery(\'#changePaymentMethod\').click(function() {
            legalSmoothScroll(\'payment-method\');
            return false;
        });

        function legalSmoothScroll(target){
            jQuery.smoothScroll({
                scrollTarget: \'#payment-address\',
                easing: \'swing\',
                speed: 1500
            });
        };' . "\n";

    if( $isThemeShoppica ) {
        $ret .= 'jQuery(document).ready(function(){
            jQuery("a[rel^=\'popTerms\']").prettyPhoto();
        });';
    }else{
        $ret .= 'jQuery(\'.popTerms\').' . $lib . '({
        	width  : 750,
        	height : 650
        });';
    }

    $ret .= "\n" . '/* ]]> */
</script>';

        return $ret;
    }

    /**
     * get OpenCart Version
     * if < 1.5.1 let var = false
     */
    private function getOCVersion() {
        if( defined( 'VERSION' ) ) {
            if( version_compare( VERSION, '1.5.1', '>=' ) ) {
                $this->_ocVersion = VERSION;
            }
        }
    }

    /**
     * add css stylesheet to global document object
     */
    public function addCss() {
        $css = $this->config->get( 'config_template' )
        . '/stylesheet/legal.css';

        if( file_exists( DIR_TEMPLATE . $css ) ) {
            $css = 'catalog/view/theme/'
            . $this->config->get( 'config_template' )
            . '/stylesheet/legal.css';
        }else{
            $css = 'catalog/view/theme/default/stylesheet/legal.css';
        }

        $this->document->addStyle( $css );
    }

    /**
     * add javascript to global document object
     */
    public function addJs() {
        $this->document->addScript( 'catalog/view/javascript/jquery/jquery.smooth-scroll.min.js' );
    }
}