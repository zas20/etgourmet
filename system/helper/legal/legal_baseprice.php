<?php
/**
 * @version		$Id: legal_baseprice.php 3683 2014-07-13 12:01:48Z mic $
 * @package		LEGAL
 * @author		mic - http://osworx.net
 * @copyright	2013 OSWorX - http://osworx.net
 * @license		OCL OSWorX Commercial - http://osworx.net
 */

/**
 * base price functions > backend only
 * @since 3.0.0
 */
class ControllerLegalBasePrice extends Controller
{
    private $_this      = null;
    public $_version    = '1.0.5 - 2014.07.08';

    /**
     * constructor
     * @param object    $obj
     */
    public function __construct( $obj = null ) {
        if( $obj ) {
            $this->_this = & $obj;
        }

        $this->getModels();
        $this->checkDatabase();
        $this->loadLanguage();
        $this->getLangVars();
    }

    /**
     * load all required models at one place
     */
    private function getModels() {
        $this->_this->load->model( 'module/legal' );
    }

    private function loadLanguage() {
        $this->_this->language->load( 'module/legal' );
    }

    /**
     * check if a specific table exists, if not create it
     * @since 3.0.0
     */
    private function checkDatabase() {
        $this->_this->model_module_legal->checkForProductAdditional();
    }

    /**
     * get all needed language vars and assign to global object
     */
    private function getLangVars() {
        $vars = array(
            'entry_base_price_calculation', 'entry_base_price_factor',
            'entry_package_content',
            'text_base_price',
            'help_base_price_calculation', 'help_base_price_extern',
            'help_package_content'
        );

        foreach( $vars as $var ) {
            $this->_this->data[$var] = $this->_this->language->get( $var );
        }
    }

    /**
     * get all relevant admin data
     * @return mixed
     */
    public function getAdminBasePriceValues() {
        $product_info = $this->getProductAdditionalData();

        $vars = array(
            'package_content'       => '',
			'package_content_unit'	=> '',
			'base_price_factor'		=> '',
			'base_price_quantity'	=> '',
			'base_price_unit'		=> ''
        );

        foreach( $vars as $k => $v ) {
            if( isset( $this->_this->request->post[$k])) {
          		$this->_this->data[$k] = $this->_this->request->post[$k];
        	}elseif(
                !empty( $product_info )
                && !empty( $product_info[$k] )
            )
            {
    			$this->_this->data[$k] = $product_info[$k];
    		}else{
          		$this->_this->data[$k] = $v;
        	}
        }

        // packaging units
		$this->_this->data['lists']['package_content_unit'] = $this->getUnits( 'package_content_unit' );

		// base price
		$this->_this->data['lists']['base_price_unit'] = $this->getUnits( 'base_price_unit' );

		$this->calculateBasePrice();
    }

    /**
     * calculate base price (only for displaying at the backend)
     * function can be called inline or via jQuery
     * styling has to be inline - we have no css loaded
     * @return string
     */
    public function calculateBasePrice() {
        $content    = false;
        $factor     = false;
        $price      = false;

        $this->_this->data['base_price'] = '';

        if( !empty( $this->_this->request->get['package_content'] ) ) {
            $price      = $this->_this->request->get['price'];
            $content    = $this->_this->request->get['package_content'];
            $factor     = $this->_this->request->get['base_price_factor'];
            $quantity   = $this->_this->request->get['base_price_quantity'];
            $unit       = $this->_this->request->get['base_price_unit'];
        }elseif( !empty( $this->_this->data['price'] ) ) {
            $price      = $this->_this->data['price'];
            $content    = $this->_this->data['package_content'];
            $factor     = $this->_this->data['base_price_factor'];
            $quantity   = $this->_this->data['base_price_quantity'];
            $unit       = $this->_this->data['base_price_unit'];
        }

        // replace unwanted chars
        $price      = $this->sanitizeData( $price );
        $factor     = $this->sanitizeData( $factor );
        $content    = $this->sanitizeData( $content );

		if(
            $content
            && $factor
            && $price
        )
        {
			$this->_this->data['base_price'] = '<span class="basePriceInfo" style="border: 1px solid #07A252; padding: 5px; background-color: #EDFFF7; color: #176B4F; font-weight: bold; border-radius: 5px;">'
            . $this->_this->data['text_base_price'] .'&nbsp;'
			. '(' . $quantity . '&nbsp;' . $unit . ')&nbsp;'
			. $this->_this->currency->format( ( $price / $content ) * $factor )
            . '</span>';
		}

        // needed if called via jQuery
        return $this->_this->data['base_price'];
    }

    /**
	 * get units (weight and length) and build a dropdown list
	 * NOTE: data values must be already existing!
	 *
	 * @param string	$val	placeholder vor the value/key
	 * @return string (html list)
	 */
	private function getUnits( $val ) {
		$ret = '<select name="' .$val. '" id="' .$val. '" onchange="calculateBasePrice();">' . "\n";

		foreach( $this->_this->data['weight_classes'] as $wc ) {
			$ret .= '<option value="' . trim( $wc['unit'] ) . '"'
			. ( !empty( $this->_this->data[$val] ) && ( $this->_this->data[$val] == trim( $wc['unit'] ) ) ? ' selected="selected"' : '' )
			. '>' . $wc['title'] . '</option>' . "\n";
		}

		$ret .= '<option value="" disabled="disabled"'
		. ' style="color:#999999">- - - - - - - - -</option>' . "\n";

		foreach( $this->_this->data['length_classes'] as $lc ) {
			$ret .= '<option value="' . trim( $lc['unit'] ) . '"'
			. ( !empty( $this->_this->data[$val] ) && ( $this->_this->data[$val] == trim( $lc['unit'] ) ) ? ' selected="selected"' : '' )
			. '>' . $lc['title'] . '</option>' . "\n";
		}

		$ret .= '</select>' . "\n";

		return $ret;
	}

    /**
     * add new product data to db
     */
    public function addProductAdditionalData() {
        $this->_this->model_module_legal->addProductAdditionalData( $this->_this->request->post );
    }

    /**
     * update product data to db
     */
    public function updateProductAdditionalData() {
        $this->_this->model_module_legal->updateProductAdditionalData( $this->_this->request->post );
    }

    /**
     * get all additional product data
     * @return array
     */
    public function getProductAdditionalData() {
        $ret = null;

        if( !empty( $this->_this->request->get['product_id'] ) ) {
            $ret = $this->_this->model_module_legal->getProductAdditionalData( $this->_this->request->get['product_id'] );
        }

        return $ret;
    }

    /**
     * sanitize data value
     * excepts only digits (0-9) and dor (.)
     * @param string    $val
     * @return string
     * @since 1.0.1
     */
    private function sanitizeData( $val ) {
        $val = trim( $val );
        $val = preg_replace( '/[^0-9\.]/', '.', $val );

        return $val;
    }
}